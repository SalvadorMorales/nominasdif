package cfdixmlv11;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


@XmlRootElement(name = "TimbreFiscalDigital")
@XmlType(propOrder = {"FechaTimbrado","UUID","noCertificadoSAT","SelloCFD","SelloSAT","version"})
public class TimbreFiscalDigital {

        @XmlAttribute(name = "version", required = true)
        @XmlSchemaType(name = "anySimpleType")
        protected String version;
        @XmlAttribute(name = "UUID", required = true)
        protected String uuid;
        @XmlAttribute(name = "FechaTimbrado", required = true)
        protected XMLGregorianCalendar fechaTimbrado;
        @XmlAttribute(name = "selloCFD", required = true)
        protected String selloCFD;
        @XmlAttribute(name = "noCertificadoSAT", required = true)
        protected String noCertificadoSAT;
        @XmlAttribute(name = "selloSAT", required = true)
        protected String selloSAT;


    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setFechaTimbrado(XMLGregorianCalendar fechaTimbrado) {
        this.fechaTimbrado = fechaTimbrado;
    }

    public XMLGregorianCalendar getFechaTimbrado() {
        return fechaTimbrado;
    }

    public void setSelloCFD(String selloCFD) {
        this.selloCFD = selloCFD;
    }

    public String getSelloCFD() {
        return selloCFD;
    }

    public void setNoCertificadoSAT(String noCertificadoSAT) {
        this.noCertificadoSAT = noCertificadoSAT;
    }

    public String getNoCertificadoSAT() {
        return noCertificadoSAT;
    }

    public void setSelloSAT(String selloSAT) {
        this.selloSAT = selloSAT;
    }

    public String getSelloSAT() {
        return selloSAT;
    }
}
