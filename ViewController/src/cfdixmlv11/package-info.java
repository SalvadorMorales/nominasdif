//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2015.10.15 at 06:02:04 PM CDT
//


@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.sat.gob.mx/cfd/3",
                                     xmlns = { @XmlNs(namespaceURI = "http://www.sat.gob.mx/cfd/3", prefix = "cfdi"),
                                               @XmlNs(namespaceURI = "http://www.w3.org/2001/XMLSchema-instance",
                                                      prefix = "xsi"),
                                               @XmlNs(namespaceURI = "http://www.sat.gob.mx/nomina",
                                                      prefix = "nomina") },
                                     elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package cfdixmlv11;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
