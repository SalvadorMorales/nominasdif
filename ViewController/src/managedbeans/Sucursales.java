package managedbeans;

import entities.Empresa;
import entities.Sucursal;
import entities.Trabajador;
import interfaces.SessionSucursalRemote;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import servicelocator.MyServiceLocator;

public class Sucursales
{
  private Sucursal sucursal;
  private List<Sucursal> listSucursal = new ArrayList();
  private SessionSucursalRemote ejbSucursal;
  private RichTable bindTabla = new RichTable();
  private String rfcSucursal;
  private String razonSocial;
  private String identificador;
  private String clave;
  private byte[] cer;
  private byte[] key;
  private RichInputText bindRfc = new RichInputText();
  private RichInputText bindRazonSocial = new RichInputText();
  private RichInputText bindClave = new RichInputText();
  private RichInputFile bindCer = new RichInputFile();
  private RichInputFile bindKey = new RichInputFile();
  private RichInputText bindIdentificador = new RichInputText();
  private RichCommandButton btnActualizar = new RichCommandButton();
  private Trabajador user;
  private Empresa admin;
  
  public Sucursales()
  {
    obtieneSesion();
    this.ejbSucursal = MyServiceLocator.getSucursalRemote();
    cargaListaSucursal();
    partial();
    limpiarCajas();
  }
  
  public void cargaListaSucursal()
  {
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindTabla());
    
    this.listSucursal.clear();
    this.listSucursal = new ArrayList();
    this.listSucursal = this.ejbSucursal.getSucursalEmpresa(this.admin.getRfc());
    for (Sucursal s : this.listSucursal) {
      System.out.println(s.getRazonSocial());
    }
  }
  
  public void obtieneSesion()
  {
    this.user = new Trabajador();
    this.admin = new Empresa();
    FacesContext ctx = FacesContext.getCurrentInstance();
    HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
    HttpSession session = req.getSession(true);
    if (session.getAttribute("usuario") != null)
    {
      this.user = ((Trabajador)session.getAttribute("usuario"));
      System.out.println("mm" + this.user.getCurp());
    }
    else
    {
      this.admin = ((Empresa)session.getAttribute("admin"));
      System.out.println(this.admin.getRazonSocial().toUpperCase());
    }
  }
  
  public void seleccionarFila(SelectionEvent selectionEvent)
  {
    Sucursal sucursalTabla = new Sucursal();
    
    sucursalTabla = (Sucursal)this.bindTabla.getSelectedRowData();
    if (sucursalTabla != null)
    {
      partial();
      limpiarCajas();
      activaIn();
      this.bindRfc.setValue(sucursalTabla.getRfcSucural());
      this.bindRazonSocial.setValue(sucursalTabla.getRazonSocial());
      this.bindClave.setValue(sucursalTabla.getClave());
      this.bindIdentificador.setValue(sucursalTabla.getIdentificador());
      this.bindKey.setValue(sucursalTabla.getLlavekey());
      this.bindCer.setValue(sucursalTabla.getCer());
    }
  }
  
  public void partial()
  {
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindRfc());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindRazonSocial());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindClave());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindIdentificador());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindCer());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindKey());
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.btnActualizar);
  }
  
  public void limpiarCajas()
  {
    this.bindRfc.resetValue();
    this.bindRazonSocial.resetValue();
    this.bindClave.resetValue();
    this.bindIdentificador.resetValue();
    this.bindKey.resetValue();
    this.bindCer.resetValue();
    this.bindRfc.setValue("");
    this.bindRazonSocial.setValue("");
    this.bindClave.setValue("");
    this.bindIdentificador.setValue("");
    this.bindKey.setValue("");
    this.bindCer.setValue("");
  }
  
  public void activaIn()
  {
    this.bindRfc.setDisabled(false);
    this.bindRazonSocial.setDisabled(false);
    this.bindClave.setDisabled(false);
    this.bindIdentificador.setDisabled(false);
    this.bindKey.setDisabled(false);
    this.bindCer.setDisabled(false);
    this.btnActualizar.setDisabled(false);
  }
  
  public void actualizaS(Sucursal sucursal)
  {
    System.out.println(getRfcSucursal());
    System.out.println(getRazonSocial());
    System.out.println(getClave());
    System.out.println(getIdentificador());
    
    sucursal.setRfcSucural(getRfcSucursal());
    sucursal.setRazonSocial(getRazonSocial());
    sucursal.setIdentificador(getIdentificador());
    sucursal.setClave(getClave());
    this.ejbSucursal.mergeSucursal(sucursal);
    this.listSucursal.clear();
  }
  
  public void actualizaSucursales(ActionEvent actionEvent)
  {
    Sucursal sucursalSelecc = (Sucursal)this.bindTabla.getSelectedRowData();
    actualizaS(sucursalSelecc);
    cargaListaSucursal();
  }
  
  public String cancelar()
  {
    partial();
    limpiarCajas();
    return "/paginaPrincipal.jspx";
  }
  
  public void setListSucursal(List<Sucursal> listSucursal)
  {
    this.listSucursal = listSucursal;
  }
  
  public List<Sucursal> getListSucursal()
  {
    return this.listSucursal;
  }
  
  public void setBindTabla(RichTable bindTabla)
  {
    this.bindTabla = bindTabla;
  }
  
  public RichTable getBindTabla()
  {
    return this.bindTabla;
  }
  
  public void setRfcSucursal(String rfcSucursal)
  {
    this.rfcSucursal = rfcSucursal;
  }
  
  public String getRfcSucursal()
  {
    return this.rfcSucursal;
  }
  
  public void setRazonSocial(String razonSocial)
  {
    this.razonSocial = razonSocial;
  }
  
  public String getRazonSocial()
  {
    return this.razonSocial;
  }
  
  public void setIdentificador(String identificador)
  {
    this.identificador = identificador;
  }
  
  public String getIdentificador()
  {
    return this.identificador;
  }
  
  public void setClave(String clave)
  {
    this.clave = clave;
  }
  
  public String getClave()
  {
    return this.clave;
  }
  
  public void setCer(byte[] cer)
  {
    this.cer = cer;
  }
  
  public byte[] getCer()
  {
    return this.cer;
  }
  
  public void setKey(byte[] key)
  {
    this.key = key;
  }
  
  public byte[] getKey()
  {
    return this.key;
  }
  
  public void setBindRfc(RichInputText bindRfc)
  {
    this.bindRfc = bindRfc;
  }
  
  public RichInputText getBindRfc()
  {
    return this.bindRfc;
  }
  
  public void setBindRazonSocial(RichInputText bindRazonSocial)
  {
    this.bindRazonSocial = bindRazonSocial;
  }
  
  public RichInputText getBindRazonSocial()
  {
    return this.bindRazonSocial;
  }
  
  public void setBindClave(RichInputText bindClave)
  {
    this.bindClave = bindClave;
  }
  
  public RichInputText getBindClave()
  {
    return this.bindClave;
  }
  
  public void setBindCer(RichInputFile bindCer)
  {
    this.bindCer = bindCer;
  }
  
  public RichInputFile getBindCer()
  {
    return this.bindCer;
  }
  
  public void setBindKey(RichInputFile bindKey)
  {
    this.bindKey = bindKey;
  }
  
  public RichInputFile getBindKey()
  {
    return this.bindKey;
  }
  
  public void setBindIdentificador(RichInputText bindIdentificador)
  {
    this.bindIdentificador = bindIdentificador;
  }
  
  public RichInputText getBindIdentificador()
  {
    return this.bindIdentificador;
  }
  
  public void setBtnActualizar(RichCommandButton btnActualizar)
  {
    this.btnActualizar = btnActualizar;
  }
  
  public RichCommandButton getBtnActualizar()
  {
    return this.btnActualizar;
  }
}
