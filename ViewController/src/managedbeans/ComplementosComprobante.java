package managedbeans;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import org.apache.myfaces.trinidad.event.DisclosureEvent;
import util.MensajesPop;

public class ComplementosComprobante
{
  BigDecimal sumPercepcionExento;
  BigDecimal sumPercepcionGravado;
  private Date fechaPago;
  private Date fechaInicialPago;
  private Date fechaFinalPago;
  private Date fechaInicioRelacionLaboral;
  private List<SelectItem> listaHorasExtras;
  private RichInputText nominaRegistroPatronal;
  private RichInputText nominaNumeroEmpleado;
  private RichInputText nominaCurp;
  private RichInputText nominaTipoRegimen;
  private RichInputText nominaNumeroSeguridadSocial;
  private RichInputDate nominaFechaPago;
  private RichInputDate nominaFechaInicialPago;
  private RichInputDate nominaFechaFinalPago;
  private RichInputText nominaNumeroDiasPagados;
  private RichInputText nominaDepartamento;
  private RichInputText nominaClabe;
  private RichInputText nominaBanco;
  private RichInputDate nominaFechaInicioRelacionLaboral;
  private RichInputText nominaAntiguedad;
  private RichInputText nominaPuesto;
  private RichInputText nominaTipoContrato;
  private RichInputText nominaTipoJornada;
  private RichInputText nominaPeriodicidadPago;
  private RichInputText nominaSalarioBaseCotApor;
  private RichInputText nominaRiesgoPuesto;
  private RichInputText nominaSalarioDiarioIntegrado;
  private RichInputText nomPercepcioTipoPercepcion;
  private RichInputText nomPercepcionClabe;
  private RichInputText nomPercepcionConcepto;
  private RichInputText nomPercepcionImporteGravado;
  private RichInputText nomPercepcionTotalGravado;
  private RichInputText nomPercepcionTotalExento;
  private RichInputText nomDeduccionTipo;
  private RichInputText nomDeduccionClave;
  private RichInputText nomDeduccionConcepto;
  private RichInputText nomDeduccionImporteExento;
  private RichInputText nomDeduccionImporteGravado;
  private RichInputText nomDeduccionTotalGravado;
  private RichInputText nomDeduccionTotalExento;
  private RichInputText nomIncapacidadDiasIncapacidad;
  private RichInputText nomIncapacidadTipoIncapacidad;
  private RichInputText nomIncapacidadDescuento;
  private RichInputText nomHorasExtrasDias;
  private RichInputText nomHorasExtrasTipoHorasExtra;
  private RichInputText nomHorasExtrasHorasExtras;
  private RichInputText nomHorasExtrasImportePagado;
  private String CURP;
  private RichInputText nominaPercepcionImporteExento;
  BigDecimal sumDeduccionExento;
  BigDecimal sumDeduccionGravado;
  private String mensajeErrorPestanaDatos;
  private String valNominaRegistroPatronal;
  private String valNominaNumeroEmpleado;
  private String valNominaCurp;
  private String valNominaTipoRegimen;
  private String valNominaNumeroSeguridadSocial;
  private String valNominaNumeroDiasPagados;
  private String valNominaDepartamento;
  private String valNominaClabe;
  private String valNominaBanco;
  private String valNominaFechaInicioRelacionLaboral;
  private String valNominaAntiguedad;
  private String valNominaPuesto;
  private String valNominaTipoContrato;
  private String valNominaTipoJornada;
  private String valNominaPeriodicidadPago;
  private String valNominaSalarioBaseCotApor;
  private String valNominaRiesgoPuesto;
  private String valNominaSalarioDiarioIntegrado;
  private String valorHorasExtras;
  private String valNominaFechaFinal;
  private String valNominaFechaInicial;
  private String valNominaFechaPago;
  private String valNominaFechaInicioRelLaboral;
  private RichShowDetailItem pestanaDatosTrabajador;
  private RichInputText nomPercepcionImporteExento;
  private RichSelectOneChoice horasExtrasTipoChoice;
  private RichPopup popErrorDatosPestanaTrabajador;
  private RichSelectOneChoice comboTipoRegimen;
  private RichSelectOneChoice comboBanco;
  private RichSelectOneChoice comboRiesgoPuesto;
  
  public ComplementosComprobante()
  {
    this.listaHorasExtras = new ArrayList();
    subirAMemoria();
    
    llenaHorasExtras();
  }
  
  public void nominaEnviaDatos(ActionEvent actionEvent)
    throws ParseException
  {}
  
  public void subirAMemoria() {}
  
  public void validaCURP(FacesContext facesContext, UIComponent uIComponent, Object object) {}
  
  public void agregaLista() {}
  
  public void setFechaPago(Date fechaPago)
  {
    this.fechaPago = fechaPago;
  }
  
  public Date getFechaPago()
  {
    return this.fechaPago;
  }
  
  public void setFechaInicialPago(Date fechaInicialPago)
  {
    this.fechaInicialPago = fechaInicialPago;
  }
  
  public Date getFechaInicialPago()
  {
    return this.fechaInicialPago;
  }
  
  public void setFechaFinalPago(Date fechaFinalPago)
  {
    this.fechaFinalPago = fechaFinalPago;
  }
  
  public Date getFechaFinalPago()
  {
    return this.fechaFinalPago;
  }
  
  public void setFechaInicioRelacionLaboral(Date fechaInicioRelacionLaboral)
  {
    this.fechaInicioRelacionLaboral = fechaInicioRelacionLaboral;
  }
  
  public Date getFechaInicioRelacionLaboral()
  {
    return this.fechaInicioRelacionLaboral;
  }
  
  public void setNomPercepcioTipoPercepcion(RichInputText nomPercepcioTipoPercepcion)
  {
    this.nomPercepcioTipoPercepcion = nomPercepcioTipoPercepcion;
  }
  
  public RichInputText getNomPercepcioTipoPercepcion()
  {
    return this.nomPercepcioTipoPercepcion;
  }
  
  public void setNomPercepcionClabe(RichInputText nomPercepcionClabe)
  {
    this.nomPercepcionClabe = nomPercepcionClabe;
  }
  
  public RichInputText getNomPercepcionClabe()
  {
    return this.nomPercepcionClabe;
  }
  
  public void setNomPercepcionConcepto(RichInputText nomPercepcionConcepto)
  {
    this.nomPercepcionConcepto = nomPercepcionConcepto;
  }
  
  public RichInputText getNomPercepcionConcepto()
  {
    return this.nomPercepcionConcepto;
  }
  
  public void setNomPercepcionImporteGravado(RichInputText nomPercepcionImporteGravado)
  {
    this.nomPercepcionImporteGravado = nomPercepcionImporteGravado;
  }
  
  public RichInputText getNomPercepcionImporteGravado()
  {
    return this.nomPercepcionImporteGravado;
  }
  
  public void setNomPercepcionTotalGravado(RichInputText nomPercepcionTotalGravado)
  {
    this.nomPercepcionTotalGravado = nomPercepcionTotalGravado;
  }
  
  public RichInputText getNomPercepcionTotalGravado()
  {
    return this.nomPercepcionTotalGravado;
  }
  
  public void setNomPercepcionTotalExento(RichInputText nomPercepcionTotalExento)
  {
    this.nomPercepcionTotalExento = nomPercepcionTotalExento;
  }
  
  public RichInputText getNomPercepcionTotalExento()
  {
    return this.nomPercepcionTotalExento;
  }
  
  public void setNomDeduccionClave(RichInputText nomDeduccionClave)
  {
    this.nomDeduccionClave = nomDeduccionClave;
  }
  
  public RichInputText getNomDeduccionClave()
  {
    return this.nomDeduccionClave;
  }
  
  public void setNomDeduccionConcepto(RichInputText nomDeduccionConcepto)
  {
    this.nomDeduccionConcepto = nomDeduccionConcepto;
  }
  
  public RichInputText getNomDeduccionConcepto()
  {
    return this.nomDeduccionConcepto;
  }
  
  public void setNomDeduccionImporteExento(RichInputText nomDeduccionImporteExento)
  {
    this.nomDeduccionImporteExento = nomDeduccionImporteExento;
  }
  
  public RichInputText getNomDeduccionImporteExento()
  {
    return this.nomDeduccionImporteExento;
  }
  
  public void setNomDeduccionImporteGravado(RichInputText nomDeduccionImporteGravado)
  {
    this.nomDeduccionImporteGravado = nomDeduccionImporteGravado;
  }
  
  public RichInputText getNomDeduccionImporteGravado()
  {
    return this.nomDeduccionImporteGravado;
  }
  
  public void setNomDeduccionTotalGravado(RichInputText nomDeduccionTotalGravado)
  {
    this.nomDeduccionTotalGravado = nomDeduccionTotalGravado;
  }
  
  public RichInputText getNomDeduccionTotalGravado()
  {
    return this.nomDeduccionTotalGravado;
  }
  
  public void setNomDeduccionTotalExento(RichInputText nomDeduccionTotalExento)
  {
    this.nomDeduccionTotalExento = nomDeduccionTotalExento;
  }
  
  public RichInputText getNomDeduccionTotalExento()
  {
    return this.nomDeduccionTotalExento;
  }
  
  public void setNomIncapacidadDiasIncapacidad(RichInputText nomIncapacidadDiasIncapacidad)
  {
    this.nomIncapacidadDiasIncapacidad = nomIncapacidadDiasIncapacidad;
  }
  
  public RichInputText getNomIncapacidadDiasIncapacidad()
  {
    return this.nomIncapacidadDiasIncapacidad;
  }
  
  public void setNomIncapacidadTipoIncapacidad(RichInputText nomIncapacidadTipoIncapacidad)
  {
    this.nomIncapacidadTipoIncapacidad = nomIncapacidadTipoIncapacidad;
  }
  
  public RichInputText getNomIncapacidadTipoIncapacidad()
  {
    return this.nomIncapacidadTipoIncapacidad;
  }
  
  public void setNomIncapacidadDescuento(RichInputText nomIncapacidadDescuento)
  {
    this.nomIncapacidadDescuento = nomIncapacidadDescuento;
  }
  
  public RichInputText getNomIncapacidadDescuento()
  {
    return this.nomIncapacidadDescuento;
  }
  
  public void setNomHorasExtrasDias(RichInputText nomHorasExtrasDias)
  {
    this.nomHorasExtrasDias = nomHorasExtrasDias;
  }
  
  public RichInputText getNomHorasExtrasDias()
  {
    return this.nomHorasExtrasDias;
  }
  
  public void setNomHorasExtrasTipoHorasExtra(RichInputText nomHorasExtrasTipoHorasExtra)
  {
    this.nomHorasExtrasTipoHorasExtra = nomHorasExtrasTipoHorasExtra;
  }
  
  public RichInputText getNomHorasExtrasTipoHorasExtra()
  {
    return this.nomHorasExtrasTipoHorasExtra;
  }
  
  public void setNomHorasExtrasHorasExtras(RichInputText nomHorasExtrasHorasExtras)
  {
    this.nomHorasExtrasHorasExtras = nomHorasExtrasHorasExtras;
  }
  
  public RichInputText getNomHorasExtrasHorasExtras()
  {
    return this.nomHorasExtrasHorasExtras;
  }
  
  public void setNomHorasExtrasImportePagado(RichInputText nomHorasExtrasImportePagado)
  {
    this.nomHorasExtrasImportePagado = nomHorasExtrasImportePagado;
  }
  
  public RichInputText getNomHorasExtrasImportePagado()
  {
    return this.nomHorasExtrasImportePagado;
  }
  
  public void setCURP(String CURP)
  {
    this.CURP = CURP;
  }
  
  public String getCURP()
  {
    return this.CURP;
  }
  
  public void confirmaNominaPercepcionesListerner(ActionEvent actionEvent) {}
  
  public void confirmaNominaDeduccionesListerner(ActionEvent actionEvent) {}
  
  public void setSumPercepcionExento(BigDecimal sumPercepcionExento)
  {
    this.sumPercepcionExento = sumPercepcionExento;
  }
  
  public BigDecimal getSumPercepcionExento()
  {
    return this.sumPercepcionExento;
  }
  
  public void setSumPercepcionGravado(BigDecimal sumPercepcionGravado)
  {
    this.sumPercepcionGravado = sumPercepcionGravado;
  }
  
  public BigDecimal getSumPercepcionGravado()
  {
    return this.sumPercepcionGravado;
  }
  
  public void setNominaPercepcionImporteExento(RichInputText nominaPercepcionImporteExento)
  {
    this.nominaPercepcionImporteExento = nominaPercepcionImporteExento;
  }
  
  public RichInputText getNominaPercepcionImporteExento()
  {
    return this.nominaPercepcionImporteExento;
  }
  
  public void setNomDeduccionTipo(RichInputText nomDeduccionTipo)
  {
    this.nomDeduccionTipo = nomDeduccionTipo;
  }
  
  public RichInputText getNomDeduccionTipo()
  {
    return this.nomDeduccionTipo;
  }
  
  public void setSumDeduccionExento(BigDecimal sumDeduccionExento)
  {
    this.sumDeduccionExento = sumDeduccionExento;
  }
  
  public BigDecimal getSumDeduccionExento()
  {
    return this.sumDeduccionExento;
  }
  
  public void setSumDeduccionGravado(BigDecimal sumDeduccionGravado)
  {
    this.sumDeduccionGravado = sumDeduccionGravado;
  }
  
  public BigDecimal getSumDeduccionGravado()
  {
    return this.sumDeduccionGravado;
  }
  
  public void confirmaNominaIncapacidadListener(ActionEvent actionEvent) {}
  
  public void confirmaNominaHorasExtrasListerner(ActionEvent actionEvent) {}
  
  public void GUARD(ActionEvent actionEvent)
    throws ParseException
  {}
  
  public void setValNominaRegistroPatronal(String valNominaRegistroPatronal)
  {
    this.valNominaRegistroPatronal = valNominaRegistroPatronal;
  }
  
  public String getValNominaRegistroPatronal()
  {
    return this.valNominaRegistroPatronal;
  }
  
  public void setValNominaNumeroEmpleado(String valNominaNumeroEmpleado)
  {
    this.valNominaNumeroEmpleado = valNominaNumeroEmpleado;
  }
  
  public String getValNominaNumeroEmpleado()
  {
    return this.valNominaNumeroEmpleado;
  }
  
  public void setValNominaCurp(String valNominaCurp)
  {
    this.valNominaCurp = valNominaCurp;
  }
  
  public String getValNominaCurp()
  {
    return this.valNominaCurp;
  }
  
  public void setValNominaTipoRegimen(String valNominaTipoRegimen)
  {
    this.valNominaTipoRegimen = valNominaTipoRegimen;
  }
  
  public String getValNominaTipoRegimen()
  {
    return this.valNominaTipoRegimen;
  }
  
  public void setValNominaNumeroSeguridadSocial(String valNominaNumeroSeguridadSocial)
  {
    this.valNominaNumeroSeguridadSocial = valNominaNumeroSeguridadSocial;
  }
  
  public String getValNominaNumeroSeguridadSocial()
  {
    return this.valNominaNumeroSeguridadSocial;
  }
  
  public void setValNominaNumeroDiasPagados(String valNominaNumeroDiasPagados)
  {
    this.valNominaNumeroDiasPagados = valNominaNumeroDiasPagados;
  }
  
  public String getValNominaNumeroDiasPagados()
  {
    return this.valNominaNumeroDiasPagados;
  }
  
  public void setValNominaDepartamento(String valNominaDepartamento)
  {
    this.valNominaDepartamento = valNominaDepartamento;
  }
  
  public String getValNominaDepartamento()
  {
    return this.valNominaDepartamento;
  }
  
  public void setValNominaClabe(String valNominaClabe)
  {
    this.valNominaClabe = valNominaClabe;
  }
  
  public String getValNominaClabe()
  {
    return this.valNominaClabe;
  }
  
  public void setValNominaBanco(String valNominaBanco)
  {
    this.valNominaBanco = valNominaBanco;
  }
  
  public String getValNominaBanco()
  {
    return this.valNominaBanco;
  }
  
  public void setValNominaFechaInicioRelacionLaboral(String valNominaFechaInicioRelacionLaboral)
  {
    this.valNominaFechaInicioRelacionLaboral = valNominaFechaInicioRelacionLaboral;
  }
  
  public String getValNominaFechaInicioRelacionLaboral()
  {
    return this.valNominaFechaInicioRelacionLaboral;
  }
  
  public void setValNominaAntiguedad(String valNominaAntiguedad)
  {
    this.valNominaAntiguedad = valNominaAntiguedad;
  }
  
  public String getValNominaAntiguedad()
  {
    return this.valNominaAntiguedad;
  }
  
  public void setValNominaPuesto(String valNominaPuesto)
  {
    this.valNominaPuesto = valNominaPuesto;
  }
  
  public String getValNominaPuesto()
  {
    return this.valNominaPuesto;
  }
  
  public void setValNominaTipoContrato(String valNominaTipoContrato)
  {
    this.valNominaTipoContrato = valNominaTipoContrato;
  }
  
  public String getValNominaTipoContrato()
  {
    return this.valNominaTipoContrato;
  }
  
  public void setValNominaTipoJornada(String valNominaTipoJornada)
  {
    this.valNominaTipoJornada = valNominaTipoJornada;
  }
  
  public String getValNominaTipoJornada()
  {
    return this.valNominaTipoJornada;
  }
  
  public void setValNominaPeriodicidadPago(String valNominaPeriodicidadPago)
  {
    this.valNominaPeriodicidadPago = valNominaPeriodicidadPago;
  }
  
  public String getValNominaPeriodicidadPago()
  {
    return this.valNominaPeriodicidadPago;
  }
  
  public void setValNominaSalarioBaseCotApor(String valNominaSalarioBaseCotApor)
  {
    this.valNominaSalarioBaseCotApor = valNominaSalarioBaseCotApor;
  }
  
  public String getValNominaSalarioBaseCotApor()
  {
    return this.valNominaSalarioBaseCotApor;
  }
  
  public void setValNominaRiesgoPuesto(String valNominaRiesgoPuesto)
  {
    this.valNominaRiesgoPuesto = valNominaRiesgoPuesto;
  }
  
  public String getValNominaRiesgoPuesto()
  {
    return this.valNominaRiesgoPuesto;
  }
  
  public void setValNominaSalarioDiarioIntegrado(String valNominaSalarioDiarioIntegrado)
  {
    this.valNominaSalarioDiarioIntegrado = valNominaSalarioDiarioIntegrado;
  }
  
  public String getValNominaSalarioDiarioIntegrado()
  {
    return this.valNominaSalarioDiarioIntegrado;
  }
  
  public void listenerComplementoNomina(DisclosureEvent disclosureEvent)
  {
    if (validaPestanaDatosTrabajador()) {}
  }
  
  public void setPestanaDatosTrabajador(RichShowDetailItem pestanaDatosTrabajador)
  {
    this.pestanaDatosTrabajador = pestanaDatosTrabajador;
  }
  
  public RichShowDetailItem getPestanaDatosTrabajador()
  {
    return this.pestanaDatosTrabajador;
  }
  
  public void setNominaRegistroPatronal(RichInputText nominaRegistroPatronal)
  {
    this.nominaRegistroPatronal = nominaRegistroPatronal;
  }
  
  public RichInputText getNominaRegistroPatronal()
  {
    return this.nominaRegistroPatronal;
  }
  
  public void setNominaNumeroEmpleado(RichInputText nominaNumeroEmpleado)
  {
    this.nominaNumeroEmpleado = nominaNumeroEmpleado;
  }
  
  public RichInputText getNominaNumeroEmpleado()
  {
    return this.nominaNumeroEmpleado;
  }
  
  public void setNominaCurp(RichInputText nominaCurp)
  {
    this.nominaCurp = nominaCurp;
  }
  
  public RichInputText getNominaCurp()
  {
    return this.nominaCurp;
  }
  
  public void setNominaTipoRegimen(RichInputText nominaTipoRegimen)
  {
    this.nominaTipoRegimen = nominaTipoRegimen;
  }
  
  public RichInputText getNominaTipoRegimen()
  {
    return this.nominaTipoRegimen;
  }
  
  public void setNominaNumeroSeguridadSocial(RichInputText nominaNumeroSeguridadSocial)
  {
    this.nominaNumeroSeguridadSocial = nominaNumeroSeguridadSocial;
  }
  
  public RichInputText getNominaNumeroSeguridadSocial()
  {
    return this.nominaNumeroSeguridadSocial;
  }
  
  public void setNominaFechaPago(RichInputDate nominaFechaPago)
  {
    this.nominaFechaPago = nominaFechaPago;
  }
  
  public RichInputDate getNominaFechaPago()
  {
    return this.nominaFechaPago;
  }
  
  public void setNominaFechaInicialPago(RichInputDate nominaFechaInicialPago)
  {
    this.nominaFechaInicialPago = nominaFechaInicialPago;
  }
  
  public RichInputDate getNominaFechaInicialPago()
  {
    return this.nominaFechaInicialPago;
  }
  
  public void setNominaFechaFinalPago(RichInputDate nominaFechaFinalPago)
  {
    this.nominaFechaFinalPago = nominaFechaFinalPago;
  }
  
  public RichInputDate getNominaFechaFinalPago()
  {
    return this.nominaFechaFinalPago;
  }
  
  public void setNominaNumeroDiasPagados(RichInputText nominaNumeroDiasPagados)
  {
    this.nominaNumeroDiasPagados = nominaNumeroDiasPagados;
  }
  
  public RichInputText getNominaNumeroDiasPagados()
  {
    return this.nominaNumeroDiasPagados;
  }
  
  public void setNominaDepartamento(RichInputText nominaDepartamento)
  {
    this.nominaDepartamento = nominaDepartamento;
  }
  
  public RichInputText getNominaDepartamento()
  {
    return this.nominaDepartamento;
  }
  
  public void setNominaClabe(RichInputText nominaClabe)
  {
    this.nominaClabe = nominaClabe;
  }
  
  public RichInputText getNominaClabe()
  {
    return this.nominaClabe;
  }
  
  public void setNominaBanco(RichInputText nominaBanco)
  {
    this.nominaBanco = nominaBanco;
  }
  
  public RichInputText getNominaBanco()
  {
    return this.nominaBanco;
  }
  
  public void setNominaFechaInicioRelacionLaboral(RichInputDate nominaFechaInicioRelacionLaboral)
  {
    this.nominaFechaInicioRelacionLaboral = nominaFechaInicioRelacionLaboral;
  }
  
  public RichInputDate getNominaFechaInicioRelacionLaboral()
  {
    return this.nominaFechaInicioRelacionLaboral;
  }
  
  public void setNominaAntiguedad(RichInputText nominaAntiguedad)
  {
    this.nominaAntiguedad = nominaAntiguedad;
  }
  
  public RichInputText getNominaAntiguedad()
  {
    return this.nominaAntiguedad;
  }
  
  public void setNominaPuesto(RichInputText nominaPuesto)
  {
    this.nominaPuesto = nominaPuesto;
  }
  
  public RichInputText getNominaPuesto()
  {
    return this.nominaPuesto;
  }
  
  public void setNominaTipoContrato(RichInputText nominaTipoContrato)
  {
    this.nominaTipoContrato = nominaTipoContrato;
  }
  
  public RichInputText getNominaTipoContrato()
  {
    return this.nominaTipoContrato;
  }
  
  public void setNominaTipoJornada(RichInputText nominaTipoJornada)
  {
    this.nominaTipoJornada = nominaTipoJornada;
  }
  
  public RichInputText getNominaTipoJornada()
  {
    return this.nominaTipoJornada;
  }
  
  public void setNominaPeriodicidadPago(RichInputText nominaPeriodicidadPago)
  {
    this.nominaPeriodicidadPago = nominaPeriodicidadPago;
  }
  
  public RichInputText getNominaPeriodicidadPago()
  {
    return this.nominaPeriodicidadPago;
  }
  
  public void setNominaSalarioBaseCotApor(RichInputText nominaSalarioBaseCotApor)
  {
    this.nominaSalarioBaseCotApor = nominaSalarioBaseCotApor;
  }
  
  public RichInputText getNominaSalarioBaseCotApor()
  {
    return this.nominaSalarioBaseCotApor;
  }
  
  public void setNominaRiesgoPuesto(RichInputText nominaRiesgoPuesto)
  {
    this.nominaRiesgoPuesto = nominaRiesgoPuesto;
  }
  
  public RichInputText getNominaRiesgoPuesto()
  {
    return this.nominaRiesgoPuesto;
  }
  
  public void setNominaSalarioDiarioIntegrado(RichInputText nominaSalarioDiarioIntegrado)
  {
    this.nominaSalarioDiarioIntegrado = nominaSalarioDiarioIntegrado;
  }
  
  public RichInputText getNominaSalarioDiarioIntegrado()
  {
    return this.nominaSalarioDiarioIntegrado;
  }
  
  public void setValNominaFechaFinal(String valNominaFechaFinal)
  {
    this.valNominaFechaFinal = valNominaFechaFinal;
  }
  
  public String getValNominaFechaFinal()
  {
    return this.valNominaFechaFinal;
  }
  
  public void setValNominaFechaInicial(String valNominaFechaInicial)
  {
    this.valNominaFechaInicial = valNominaFechaInicial;
  }
  
  public String getValNominaFechaInicial()
  {
    return this.valNominaFechaInicial;
  }
  
  public void setValNominaFechaPago(String valNominaFechaPago)
  {
    this.valNominaFechaPago = valNominaFechaPago;
  }
  
  public String getValNominaFechaPago()
  {
    return this.valNominaFechaPago;
  }
  
  public void setValNominaFechaInicioRelLaboral(String valNominaFechaInicioRelLaboral)
  {
    this.valNominaFechaInicioRelLaboral = valNominaFechaInicioRelLaboral;
  }
  
  public String getValNominaFechaInicioRelLaboral()
  {
    return this.valNominaFechaInicioRelLaboral;
  }
  
  public void setNomPercepcionImporteExento(RichInputText nomPercepcionImporteExento)
  {
    this.nomPercepcionImporteExento = nomPercepcionImporteExento;
  }
  
  public RichInputText getNomPercepcionImporteExento()
  {
    return this.nomPercepcionImporteExento;
  }
  
  public void validaDecimal(FacesContext facesContext, UIComponent uIComponent, Object object) {}
  
  public void validaBanco(FacesContext facesContext, UIComponent uIComponent, Object object) {}
  
  public void validaSemanaTrabajo(FacesContext facesContext, UIComponent uIComponent, Object object) {}
  
  public void validaEnteros(FacesContext facesContext, UIComponent uIComponent, Object object) {}
  
  public void validaTipoPercepcionPercepciones(FacesContext facesContext, UIComponent uIComponent, Object object) {}
  
  public void validaClavePercepciones(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String clave = object.toString();
    if ((clave.length() < 3) || (clave.length() > 15)) {
      throw new ValidatorException(new FacesMessage(null, "El dato no es un valor valido. Vertifique longitud del valor por favor."));
    }
  }
  
  public void validaTipoDeduccion(FacesContext facesContext, UIComponent uIComponent, Object object) {}
  
  public void validaClaves(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String clave = object.toString();
    if ((clave.length() < 3) || (clave.length() > 15)) {
      throw new ValidatorException(new FacesMessage(null, "El dato no es un valor valido. Vertifique longitud del valor por favor."));
    }
  }
  
  public void llenaHorasExtras()
  {
    try
    {
      this.listaHorasExtras.add(new SelectItem("Dobles", "Dobles"));
      this.listaHorasExtras.add(new SelectItem("Triples", "Triples"));
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public void setListaHorasExtras(List<SelectItem> listaHorasExtras)
  {
    this.listaHorasExtras = listaHorasExtras;
  }
  
  public List<SelectItem> getListaHorasExtras()
  {
    return this.listaHorasExtras;
  }
  
  public void setValorHorasExtras(String valorHorasExtras)
  {
    this.valorHorasExtras = valorHorasExtras;
  }
  
  public String getValorHorasExtras()
  {
    return this.valorHorasExtras;
  }
  
  public void listenerHorasExtras(ValueChangeEvent valueChangeEvent)
  {
    this.valorHorasExtras = valueChangeEvent.getNewValue().toString();
  }
  
  public boolean validaCamposHE()
  {
    if (this.nomHorasExtrasDias.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomHorasExtrasDias.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomHorasExtrasHorasExtras.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomHorasExtrasHorasExtras.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomHorasExtrasImportePagado.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomHorasExtrasImportePagado.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.horasExtrasTipoChoice.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.horasExtrasTipoChoice.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    return true;
  }
  
  public boolean validaCamposIncapacidad()
  {
    if (this.nomIncapacidadDiasIncapacidad.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomIncapacidadDiasIncapacidad.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomIncapacidadTipoIncapacidad.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomIncapacidadTipoIncapacidad.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomIncapacidadDescuento.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomIncapacidadDescuento.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    return true;
  }
  
  public boolean validaCamposDeducciones()
  {
    if (this.nomDeduccionTipo.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomDeduccionTipo.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomDeduccionClave.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomDeduccionClave.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomDeduccionConcepto.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomDeduccionConcepto.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomDeduccionImporteExento.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomDeduccionImporteExento.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomDeduccionImporteGravado.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomDeduccionImporteGravado.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    return true;
  }
  
  public boolean validaDeduccionesTotal()
  {
    if (this.nomDeduccionTotalGravado == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomDeduccionTotalGravado.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomDeduccionTotalExento == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomDeduccionTotalExento.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    return true;
  }
  
  public boolean validaCamposPercepciones()
  {
    if (this.nomPercepcioTipoPercepcion.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomPercepcioTipoPercepcion.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomPercepcionClabe.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomPercepcionClabe.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (getNomPercepcionConcepto().getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomPercepcionConcepto.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomPercepcionImporteExento.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomPercepcionImporteExento.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    if (this.nomPercepcionImporteGravado.getValue() == null)
    {
      FacesContext.getCurrentInstance().addMessage(this.nomPercepcionImporteGravado.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Campo vacio", "Debe ingresar un valor por favor"));
      
      return false;
    }
    return true;
  }
  
  public boolean validaPestanaDatosTrabajador()
  {
    MensajesPop objMensajes = new MensajesPop();
    ActionEvent action = new ActionEvent(this.pestanaDatosTrabajador);
    if ((this.nominaNumeroEmpleado.getValue() == null) || (this.nominaNumeroEmpleado.getValue().equals("")))
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese un Numero de Trabajador por favor. ";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    if (this.nominaCurp.getValue() == null)
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese el CURP del trabajador. Por favor ";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    if (this.comboRiesgoPuesto.getValue() == null)
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese el tipo de riesgo por favor.";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    if (this.comboRiesgoPuesto.getValue() == null)
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese el tipo de R����gimen por favor. ";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    if (this.nominaFechaPago.getValue() == null)
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese la fecha de pago por favor. ";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    if (this.nominaFechaInicialPago.getValue() == null)
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese la fecha Inicial de pago por favor. ";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    if (this.nominaFechaFinalPago.getValue() == null)
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese la fecha final de pago por favor. ";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    if (this.nominaNumeroDiasPagados.getValue() == null)
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese el n����mero de dias pagados. ";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    if (this.nominaFechaInicioRelacionLaboral.getValue() == null)
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese la fecha de Inicio de relacion Laboral por favor. ";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    if (this.nominaPeriodicidadPago.getValue() == null)
    {
      this.pestanaDatosTrabajador.setDisclosed(true);
      this.mensajeErrorPestanaDatos = "Ingrese la periocidad de pago por favor. ";
      objMensajes.showPopup(action, this.popErrorDatosPestanaTrabajador);
      return false;
    }
    return true;
  }
  
  public void setHorasExtrasTipoChoice(RichSelectOneChoice horasExtrasTipoChoice)
  {
    this.horasExtrasTipoChoice = horasExtrasTipoChoice;
  }
  
  public RichSelectOneChoice getHorasExtrasTipoChoice()
  {
    return this.horasExtrasTipoChoice;
  }
  
  public void setMensajeErrorPestanaDatos(String mensajeErrorPestanaDatos)
  {
    this.mensajeErrorPestanaDatos = mensajeErrorPestanaDatos;
  }
  
  public String getMensajeErrorPestanaDatos()
  {
    return this.mensajeErrorPestanaDatos;
  }
  
  public void setPopErrorDatosPestanaTrabajador(RichPopup popErrorDatosPestanaTrabajador)
  {
    this.popErrorDatosPestanaTrabajador = popErrorDatosPestanaTrabajador;
  }
  
  public RichPopup getPopErrorDatosPestanaTrabajador()
  {
    return this.popErrorDatosPestanaTrabajador;
  }
  
  public void listenerRegimenContratacion(ValueChangeEvent valueChangeEvent)
  {
    String tipoRegimen = valueChangeEvent.getNewValue().toString();
  }
  
  public void listenerBancos(ValueChangeEvent valueChangeEvent)
  {
    String banco = valueChangeEvent.getNewValue().toString();
  }
  
  public void listenerRiesgoPuesto(ValueChangeEvent valueChangeEvent)
  {
    String riesgoPuesto = valueChangeEvent.getNewValue().toString();
  }
  
  public void setComboTipoRegimen(RichSelectOneChoice comboTipoRegimen)
  {
    this.comboTipoRegimen = comboTipoRegimen;
  }
  
  public RichSelectOneChoice getComboTipoRegimen()
  {
    return this.comboTipoRegimen;
  }
  
  public void setComboBanco(RichSelectOneChoice comboBanco)
  {
    this.comboBanco = comboBanco;
  }
  
  public RichSelectOneChoice getComboBanco()
  {
    return this.comboBanco;
  }
  
  public void setComboRiesgoPuesto(RichSelectOneChoice comboRiesgoPuesto)
  {
    this.comboRiesgoPuesto = comboRiesgoPuesto;
  }
  
  public RichSelectOneChoice getComboRiesgoPuesto()
  {
    return this.comboRiesgoPuesto;
  }
}
