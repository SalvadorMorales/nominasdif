package managedbeans;

import classjava.TablaAdmin;
import entities.Empresa;
import entities.Permisos;
import entities.Sucursal;
import entities.Trabajador;
import interfaces.SessionEmpresaRemote;
import interfaces.SessionPermisosRemote;
import interfaces.SessionSucursalRemote;
import interfaces.SessionTrabajadorRemote;
import java.awt.event.ItemEvent;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.util.FacesMessageUtils;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import servicelocator.MyServiceLocator;
import util.CifradoCesar;

public class Roles
{
  private List<Trabajador> listAdmins = new ArrayList();
  private SessionTrabajadorRemote ejbTrabajador;
  private List<TablaAdmin> listTabla = new ArrayList();
  private CifradoCesar cifrado;
  private Permisos permiso;
  private Permisos permisoTabla;
  private Trabajador trabajador;
  private Empresa empresa;
  private Sucursal sucursal;
  private SessionEmpresaRemote ejbEmpresa;
  private SessionSucursalRemote ejbSucursal;
  private SessionPermisosRemote ejbPermiso;
  private String nombre;
  private String usuario;
  private String pass;
  private String pass2;
  private String apellidos;
  private String addNombre;
  private String addUsuario;
  private String addPass;
  private String addPass2;
  private String addApellidos;
  private String addSucursal;
  private boolean addPermiso1;
  private boolean addPermiso2;
  private boolean addPermiso3;
  private boolean addPermiso4;
  private boolean addPermiso5;
  private boolean addPermiso6;
  private List<ItemEvent> listItem = new ArrayList();
  private Trabajador user;
  private Empresa admin;
  private RichTable bindTabla;
  private RichInputText bindNombre;
  private RichInputText bindApellidos;
  private RichInputText bindUsuario;
  private RichInputText bindPass;
  private RichInputText bindPass2;
  private boolean permiso1;
  private boolean permiso2;
  private boolean permiso3;
  private boolean permiso4;
  private boolean permiso5;
  private boolean permiso6;
  private RichSelectBooleanCheckbox bindPermiso1;
  private RichSelectBooleanCheckbox bindPermiso2;
  private RichSelectBooleanCheckbox bindPermiso3;
  private RichSelectBooleanCheckbox bindPermiso4;
  private RichSelectBooleanCheckbox bindPermiso5;
  private RichSelectBooleanCheckbox bindPermiso6;
  private RichCommandButton bindBtnRol;
  private List<SelectItem> listItemEmpresa = new ArrayList();
  private String selceccionaSucursal = "[ SELECCIONA SUCURSAL ]";
  private RichInputText bindAddNombre = new RichInputText();
  private RichInputText bindAddApellido = new RichInputText();
  private RichInputText bindAddUsuario = new RichInputText();
  private RichInputText bindAddContrasenia = new RichInputText();
  private RichInputText bindAddContrasenia2 = new RichInputText();
  private RichSelectOneChoice bindAddSucursal = new RichSelectOneChoice();
  private RichSelectBooleanCheckbox bindAddPermiso1 = new RichSelectBooleanCheckbox();
  private RichSelectBooleanCheckbox bindAddPermiso2 = new RichSelectBooleanCheckbox();
  private RichSelectBooleanCheckbox bindAddPermiso3 = new RichSelectBooleanCheckbox();
  private RichSelectBooleanCheckbox bindAddPermiso4 = new RichSelectBooleanCheckbox();
  private RichSelectBooleanCheckbox bindAddPermiso5 = new RichSelectBooleanCheckbox();
  private RichSelectBooleanCheckbox bindAddPermiso6 = new RichSelectBooleanCheckbox();
  
  public Roles()
  {
    obtieneSesion();
    this.cifrado = new CifradoCesar();
    this.ejbTrabajador = MyServiceLocator.getTrabajadorRemote();
    this.ejbPermiso = MyServiceLocator.getPermisosRemote();
    this.ejbSucursal = MyServiceLocator.getSucursalRemote();
    cargaUsuarios();
    cargaSucursales();
  }
  
  public void cargaUsuarios()
  {
    TablaAdmin usuario = null;
    this.listTabla.clear();
    this.listAdmins.clear();
    this.listAdmins = this.ejbTrabajador.getFindAdmins(this.admin.getRfc());
    try
    {
      for (Trabajador t : this.listAdmins)
      {
        usuario = new TablaAdmin();
        usuario.setIdAdmin(t.getIdtrabajador());
        usuario.setApellidos(CifradoCesar.Desencriptar(t.getApellidos(), 10));
        usuario.setDepartamento(CifradoCesar.Desencriptar(t.getDepartamento(), 10));
        usuario.setEmpresaRazon(CifradoCesar.Desencriptar(t.getEmpresa1().getRazonSocial(), 10));
        usuario.setNombre(CifradoCesar.Desencriptar(t.getNombre(), 10));
        usuario.setPuesto(CifradoCesar.Desencriptar(t.getPuesto(), 10));
        usuario.setRfc(CifradoCesar.Desencriptar(t.getRfc(), 10));
        usuario.setSucursalRazon(t.getSucursal1().getRazonSocial());
        usuario.setUsuario(CifradoCesar.Desencriptar(t.getUsuario(), 10));
        usuario.setCorreo(t.getCorreo());
        usuario.setPass(CifradoCesar.Desencriptar(t.getPassword(), 10));
        this.listTabla.add(usuario);
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public void agregaAdmin()
  {
    this.permiso = new Permisos();
    this.trabajador = new Trabajador();
    this.trabajador.setNombre(CifradoCesar.Encriptar(getAddNombre(), 10));
    this.trabajador.setApellidos(CifradoCesar.Encriptar(getAddApellidos(), 10));
    this.trabajador.setUsuario(CifradoCesar.Encriptar(getAddUsuario(), 10));
    this.trabajador.setPassword(CifradoCesar.Encriptar(getAddPass(), 10));
    this.trabajador.setEmpresa1(this.admin);
    this.trabajador.setTipo("1");
    if (this.admin.getSucursalList().size() > 0) {
      this.trabajador.setSucursal1(this.ejbSucursal.getFindRazonSocial(this.selceccionaSucursal));
    }
    this.ejbTrabajador.persistTrabajador(this.trabajador);
    this.trabajador = new Trabajador();
    this.trabajador = this.ejbTrabajador.getFindUsuario(CifradoCesar.Encriptar(getAddUsuario(), 10), CifradoCesar.Encriptar(getAddPass(), 10));
    
    this.permiso.setRecibos(this.addPermiso1);
    this.permiso.setEmpleados(this.addPermiso2);
    this.permiso.setSucursales(this.addPermiso3);
    this.permiso.setCorreo(this.addPermiso4);
    this.permiso.setDisenio(this.addPermiso5);
    this.permiso.setSucursales(this.addPermiso6);
    this.permiso.setTrabajador(this.trabajador);
    this.ejbPermiso.persistPermisos(this.permiso);
  }
  
  public void cargaSucursales()
  {
    SelectItem item = new SelectItem();
    if (this.admin != null)
    {
      List<Sucursal> listTemporal = this.ejbSucursal.getSucursalEmpresa(this.admin.getRfc());
      item.setLabel(this.selceccionaSucursal);
      item.setValue(this.selceccionaSucursal);
      this.listItemEmpresa.add(item);
      for (Sucursal s : listTemporal)
      {
        item = new SelectItem();
        item.setLabel(s.getRazonSocial());
        item.setValue(s.getRazonSocial());
        this.listItemEmpresa.add(item);
      }
    }
  }
  
  public void obtieneSesion()
  {
    this.user = new Trabajador();
    this.admin = new Empresa();
    FacesContext ctx = FacesContext.getCurrentInstance();
    HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
    HttpSession session = req.getSession(true);
    if (session.getAttribute("usuario") != null) {
      this.user = ((Trabajador)session.getAttribute("usuario"));
    } else {
      this.admin = ((Empresa)session.getAttribute("admin"));
    }
  }
  
  public void seleccionFila(SelectionEvent selectionEvent)
  {
    TablaAdmin rolTabla = new TablaAdmin();
    Permisos p = new Permisos();
    Trabajador trabajadorPer = new Trabajador();
    rolTabla = (TablaAdmin)this.bindTabla.getSelectedRowData();
    if (rolTabla != null)
    {
      p = this.ejbPermiso.getFindPermisoTrabajador(this.ejbTrabajador.getAdmin(CifradoCesar.Encriptar(rolTabla.getUsuario(), 10), CifradoCesar.Encriptar(rolTabla.getPass(), 10)).getIdtrabajador());
      if (p != null)
      {
        partialAdf();
        activarComponente();
        this.bindNombre.setValue(rolTabla.getNombre());
        this.bindApellidos.setValue(rolTabla.getApellidos());
        this.bindUsuario.setValue(rolTabla.getUsuario());
        this.bindPass.setValue(rolTabla.getPass());
        this.bindPermiso1.setValue(Boolean.valueOf(p.getRecibos()));
        this.bindPermiso2.setValue(Boolean.valueOf(p.getEmpleados()));
        this.bindPermiso3.setValue(Boolean.valueOf(p.getSucursales()));
        this.bindPermiso4.setValue(Boolean.valueOf(p.getCorreo()));
        this.bindPermiso5.setValue(Boolean.valueOf(p.getDisenio()));
        this.bindPermiso6.setValue(Boolean.valueOf(p.getRoles()));
      }
      else
      {
        System.out.println("p no entra");
      }
    }
    else
    {
      System.out.println("No entra");
    }
  }
  
  public void partialAdf()
  {
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindApellidos());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindNombre());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindPass());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindPass2());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindTabla());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindUsuario());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindBtnRol());
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindPermiso1);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindPermiso2);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindPermiso3);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindPermiso4);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindPermiso5);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindPermiso6);
  }
  
  public void activarComponente()
  {
    getBindApellidos().setDisabled(false);
    getBindNombre().setDisabled(false);
    getBindPass().setDisabled(false);
    getBindPass2().setDisabled(false);
    getBindUsuario().setDisabled(false);
    getBindPermiso1().setDisabled(false);
    getBindPermiso2().setDisabled(false);
    getBindPermiso3().setDisabled(false);
    getBindPermiso4().setDisabled(false);
    getBindPermiso5().setDisabled(false);
    getBindPermiso6().setDisabled(false);
    getBindBtnRol().setDisabled(false);
  }
  
  public void actualizaRol(ActionEvent actionEvent)
  {
    TablaAdmin adminSelec = (TablaAdmin)this.bindTabla.getSelectedRowData();
    actualiza(adminSelec);
    cargaUsuarios();
  }
  
  public void actualiza(TablaAdmin admin)
  {
    Trabajador traba = this.ejbTrabajador.getIdAdmin(admin.getIdAdmin());
    if (traba != null)
    {
      Permisos p = this.ejbPermiso.getFindPermisoTrabajador(traba.getIdtrabajador());
      if (p != null)
      {
        p.setRecibos(isPermiso1());
        p.setEmpleados(isPermiso2());
        p.setSucursales(isPermiso5());
        p.setCorreo(isPermiso4());
        p.setDisenio(isPermiso5());
        p.setRoles(isPermiso6());
        this.ejbPermiso.mergePermisos(p);
        traba.setNombre(CifradoCesar.Encriptar(getNombre(), 10));
        traba.setApellidos(CifradoCesar.Encriptar(getApellidos(), 10));
        traba.setUsuario(CifradoCesar.Encriptar(getUsuario(), 10));
        traba.setPassword(CifradoCesar.Encriptar(getPass(), 10));
        this.ejbTrabajador.mergeTrabajador(traba);
      }
      else
      {
        System.out.println("No encuentra p");
      }
    }
    else
    {
      System.out.println("no encuentra " + traba);
    }
  }
  
  public void addAmin(ActionEvent actionEvent)
  {
    if (!this.selceccionaSucursal.equals("[ SELECCIONA SUCURSAL ]"))
    {
      System.out.println(getAddPass() + "               " + getAddPass2());
      if (getAddPass().equals(getAddPass2()))
      {
        agregaAdmin();
        
        cargaUsuarios();
        agregaAddPartial();
        limpiaVariablesAdd();
        FacesContext.getCurrentInstance().addMessage(null, FacesMessageUtils.getConfirmationMessage(null, "Usuario agregado."));
      }
      else
      {
        FacesContext.getCurrentInstance().addMessage(this.bindAddContrasenia2.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe repetir la misma contrase��a.".toUpperCase()));
      }
    }
    else
    {
      FacesContext.getCurrentInstance().addMessage(this.bindAddSucursal.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe ingresar una sucursal.".toUpperCase()));
    }
  }
  
  public void limpiaVariablesAdd()
  {
    this.bindAddApellido.resetValue();
    this.bindAddApellido.setValue("");
    this.bindAddContrasenia.resetValue();
    this.bindAddContrasenia.setValue("");
    this.bindAddContrasenia2.resetValue();
    this.bindAddContrasenia2.setValue("");
    this.bindAddNombre.resetValue();
    this.bindAddNombre.setValue("");
    this.bindAddSucursal.resetValue();
    this.bindAddSucursal.setValue(this.selceccionaSucursal);
    this.bindAddUsuario.resetValue();
    this.bindAddUsuario.setValue("");
    this.bindApellidos.resetValue();
    this.bindApellidos.setValue("");
    this.bindAddPermiso1.resetValue();
    this.bindAddPermiso1.setValue(Boolean.valueOf(false));
    this.bindAddPermiso2.resetValue();
    this.bindAddPermiso2.setValue(Boolean.valueOf(false));
    this.bindAddPermiso3.resetValue();
    this.bindAddPermiso3.setValue(Boolean.valueOf(false));
    this.bindAddPermiso4.resetValue();
    this.bindAddPermiso4.setValue(Boolean.valueOf(false));
    this.bindAddPermiso5.resetValue();
    this.bindAddPermiso5.setValue(Boolean.valueOf(false));
    this.bindAddPermiso6.resetValue();
    this.bindAddPermiso6.setValue(Boolean.valueOf(false));
  }
  
  public void agregaAddPartial()
  {
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindAddApellido());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindAddNombre());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindAddContrasenia());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindAddContrasenia2());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindAddUsuario());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindAddSucursal());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindTabla());
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindAddPermiso1);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindAddPermiso2);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindAddPermiso3);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindAddPermiso4);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindAddPermiso5);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.bindAddPermiso6);
  }
  
  public void setListAdmins(List<Trabajador> listAdmins)
  {
    this.listAdmins = listAdmins;
  }
  
  public List<Trabajador> getListAdmins()
  {
    return this.listAdmins;
  }
  
  public void setListTabla(List<TablaAdmin> listTabla)
  {
    this.listTabla = listTabla;
  }
  
  public List<TablaAdmin> getListTabla()
  {
    return this.listTabla;
  }
  
  public void setBindTabla(RichTable bindTabla)
  {
    this.bindTabla = bindTabla;
  }
  
  public RichTable getBindTabla()
  {
    return this.bindTabla;
  }
  
  public void setPermisoTabla(Permisos permisoTabla)
  {
    this.permisoTabla = permisoTabla;
  }
  
  public Permisos getPermisoTabla()
  {
    return this.permisoTabla;
  }
  
  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }
  
  public String getNombre()
  {
    return this.nombre;
  }
  
  public void setUsuario(String usuario)
  {
    this.usuario = usuario;
  }
  
  public String getUsuario()
  {
    return this.usuario;
  }
  
  public void setPass(String pass)
  {
    this.pass = pass;
  }
  
  public String getPass()
  {
    return this.pass;
  }
  
  public void setPass2(String pass2)
  {
    this.pass2 = pass2;
  }
  
  public String getPass2()
  {
    return this.pass2;
  }
  
  public void setApellidos(String apellidos)
  {
    this.apellidos = apellidos;
  }
  
  public String getApellidos()
  {
    return this.apellidos;
  }
  
  public void setBindNombre(RichInputText bindNombre)
  {
    this.bindNombre = bindNombre;
  }
  
  public RichInputText getBindNombre()
  {
    return this.bindNombre;
  }
  
  public void setBindApellidos(RichInputText bindApellidos)
  {
    this.bindApellidos = bindApellidos;
  }
  
  public RichInputText getBindApellidos()
  {
    return this.bindApellidos;
  }
  
  public void setBindUsuario(RichInputText bindUsuario)
  {
    this.bindUsuario = bindUsuario;
  }
  
  public RichInputText getBindUsuario()
  {
    return this.bindUsuario;
  }
  
  public void setBindPass(RichInputText bindPass)
  {
    this.bindPass = bindPass;
  }
  
  public RichInputText getBindPass()
  {
    return this.bindPass;
  }
  
  public void setBindPass2(RichInputText bindPass2)
  {
    this.bindPass2 = bindPass2;
  }
  
  public RichInputText getBindPass2()
  {
    return this.bindPass2;
  }
  
  public void setPermiso1(boolean permiso1)
  {
    this.permiso1 = permiso1;
  }
  
  public boolean isPermiso1()
  {
    return this.permiso1;
  }
  
  public void setPermiso2(boolean permiso2)
  {
    this.permiso2 = permiso2;
  }
  
  public boolean isPermiso2()
  {
    return this.permiso2;
  }
  
  public void setPermiso3(boolean permiso3)
  {
    this.permiso3 = permiso3;
  }
  
  public boolean isPermiso3()
  {
    return this.permiso3;
  }
  
  public void setPermiso4(boolean permiso4)
  {
    this.permiso4 = permiso4;
  }
  
  public boolean isPermiso4()
  {
    return this.permiso4;
  }
  
  public void setPermiso5(boolean permiso5)
  {
    this.permiso5 = permiso5;
  }
  
  public boolean isPermiso5()
  {
    return this.permiso5;
  }
  
  public void setBindPermiso1(RichSelectBooleanCheckbox bindPermiso1)
  {
    this.bindPermiso1 = bindPermiso1;
  }
  
  public RichSelectBooleanCheckbox getBindPermiso1()
  {
    return this.bindPermiso1;
  }
  
  public void setBindPermiso2(RichSelectBooleanCheckbox bindPermiso2)
  {
    this.bindPermiso2 = bindPermiso2;
  }
  
  public RichSelectBooleanCheckbox getBindPermiso2()
  {
    return this.bindPermiso2;
  }
  
  public void setBindPermiso3(RichSelectBooleanCheckbox bindPermiso3)
  {
    this.bindPermiso3 = bindPermiso3;
  }
  
  public RichSelectBooleanCheckbox getBindPermiso3()
  {
    return this.bindPermiso3;
  }
  
  public void setBindPermiso4(RichSelectBooleanCheckbox bindPermiso4)
  {
    this.bindPermiso4 = bindPermiso4;
  }
  
  public RichSelectBooleanCheckbox getBindPermiso4()
  {
    return this.bindPermiso4;
  }
  
  public void setBindPermiso5(RichSelectBooleanCheckbox bindPermiso5)
  {
    this.bindPermiso5 = bindPermiso5;
  }
  
  public RichSelectBooleanCheckbox getBindPermiso5()
  {
    return this.bindPermiso5;
  }
  
  public void setBindPermiso6(RichSelectBooleanCheckbox bindPermiso6)
  {
    this.bindPermiso6 = bindPermiso6;
  }
  
  public RichSelectBooleanCheckbox getBindPermiso6()
  {
    return this.bindPermiso6;
  }
  
  public void setPermiso6(boolean permiso6)
  {
    this.permiso6 = permiso6;
  }
  
  public boolean isPermiso6()
  {
    return this.permiso6;
  }
  
  public void setBindBtnRol(RichCommandButton bindBtnRol)
  {
    this.bindBtnRol = bindBtnRol;
  }
  
  public RichCommandButton getBindBtnRol()
  {
    return this.bindBtnRol;
  }
  
  public String retornaPag()
  {
    limpiaVariablesAdd();
    return "/paginaPrincipal.jspx";
  }
  
  public String retornaAgregaAdmin()
  {
    return "/Roles/AgregarAdmin.jspx";
  }
  
  public void agregarUsuario(ActionEvent actionEvent) {}
  
  public void setSelceccionaSucursal(String selceccionaSucursal)
  {
    this.selceccionaSucursal = selceccionaSucursal;
  }
  
  public String getSelceccionaSucursal()
  {
    return this.selceccionaSucursal;
  }
  
  public void setListItemEmpresa(List<SelectItem> listItemEmpresa)
  {
    this.listItemEmpresa = listItemEmpresa;
  }
  
  public List<SelectItem> getListItemEmpresa()
  {
    return this.listItemEmpresa;
  }
  
  public void setAddNombre(String addNombre)
  {
    this.addNombre = addNombre;
  }
  
  public String getAddNombre()
  {
    return this.addNombre;
  }
  
  public void setAddUsuario(String addUsuario)
  {
    this.addUsuario = addUsuario;
  }
  
  public String getAddUsuario()
  {
    return this.addUsuario;
  }
  
  public void setAddPass(String addPass)
  {
    this.addPass = addPass;
  }
  
  public String getAddPass()
  {
    return this.addPass;
  }
  
  public void setAddPass2(String addPass2)
  {
    this.addPass2 = addPass2;
  }
  
  public String getAddPass2()
  {
    return this.addPass2;
  }
  
  public void setAddApellidos(String addApellidos)
  {
    this.addApellidos = addApellidos;
  }
  
  public String getAddApellidos()
  {
    return this.addApellidos;
  }
  
  public void setAddSucursal(String addSucursal)
  {
    this.addSucursal = addSucursal;
  }
  
  public String getAddSucursal()
  {
    return this.addSucursal;
  }
  
  public void setAddPermiso1(boolean addPermiso1)
  {
    this.addPermiso1 = addPermiso1;
  }
  
  public boolean isAddPermiso1()
  {
    return this.addPermiso1;
  }
  
  public void setAddPermiso2(boolean addPermiso2)
  {
    this.addPermiso2 = addPermiso2;
  }
  
  public boolean isAddPermiso2()
  {
    return this.addPermiso2;
  }
  
  public void setAddPermiso3(boolean addPermiso3)
  {
    this.addPermiso3 = addPermiso3;
  }
  
  public boolean isAddPermiso3()
  {
    return this.addPermiso3;
  }
  
  public void setAddPermiso4(boolean addPermiso4)
  {
    this.addPermiso4 = addPermiso4;
  }
  
  public boolean isAddPermiso4()
  {
    return this.addPermiso4;
  }
  
  public void setAddPermiso5(boolean addPermiso5)
  {
    this.addPermiso5 = addPermiso5;
  }
  
  public boolean isAddPermiso5()
  {
    return this.addPermiso5;
  }
  
  public void setAddPermiso6(boolean addPermiso6)
  {
    this.addPermiso6 = addPermiso6;
  }
  
  public boolean isAddPermiso6()
  {
    return this.addPermiso6;
  }
  
  public void setBindAddNombre(RichInputText bindAddNombre)
  {
    this.bindAddNombre = bindAddNombre;
  }
  
  public RichInputText getBindAddNombre()
  {
    return this.bindAddNombre;
  }
  
  public void setBindAddApellido(RichInputText bindAddApellido)
  {
    this.bindAddApellido = bindAddApellido;
  }
  
  public RichInputText getBindAddApellido()
  {
    return this.bindAddApellido;
  }
  
  public void setBindAddUsuario(RichInputText bindAddUsuario)
  {
    this.bindAddUsuario = bindAddUsuario;
  }
  
  public RichInputText getBindAddUsuario()
  {
    return this.bindAddUsuario;
  }
  
  public void setBindAddContrasenia(RichInputText bindAddContrasenia)
  {
    this.bindAddContrasenia = bindAddContrasenia;
  }
  
  public RichInputText getBindAddContrasenia()
  {
    return this.bindAddContrasenia;
  }
  
  public void setBindAddContrasenia2(RichInputText bindAddContrasenia2)
  {
    this.bindAddContrasenia2 = bindAddContrasenia2;
  }
  
  public RichInputText getBindAddContrasenia2()
  {
    return this.bindAddContrasenia2;
  }
  
  public void setBindAddSucursal(RichSelectOneChoice bindAddSucursal)
  {
    this.bindAddSucursal = bindAddSucursal;
  }
  
  public RichSelectOneChoice getBindAddSucursal()
  {
    return this.bindAddSucursal;
  }
  
  public void setBindAddPermiso1(RichSelectBooleanCheckbox bindAddPermiso1)
  {
    this.bindAddPermiso1 = bindAddPermiso1;
  }
  
  public RichSelectBooleanCheckbox getBindAddPermiso1()
  {
    return this.bindAddPermiso1;
  }
  
  public void setBindAddPermiso2(RichSelectBooleanCheckbox bindAddPermiso2)
  {
    this.bindAddPermiso2 = bindAddPermiso2;
  }
  
  public RichSelectBooleanCheckbox getBindAddPermiso2()
  {
    return this.bindAddPermiso2;
  }
  
  public void setBindAddPermiso3(RichSelectBooleanCheckbox bindAddPermiso3)
  {
    this.bindAddPermiso3 = bindAddPermiso3;
  }
  
  public RichSelectBooleanCheckbox getBindAddPermiso3()
  {
    return this.bindAddPermiso3;
  }
  
  public void setBindAddPermiso4(RichSelectBooleanCheckbox bindAddPermiso4)
  {
    this.bindAddPermiso4 = bindAddPermiso4;
  }
  
  public RichSelectBooleanCheckbox getBindAddPermiso4()
  {
    return this.bindAddPermiso4;
  }
  
  public void setBindAddPermiso5(RichSelectBooleanCheckbox bindAddPermiso5)
  {
    this.bindAddPermiso5 = bindAddPermiso5;
  }
  
  public RichSelectBooleanCheckbox getBindAddPermiso5()
  {
    return this.bindAddPermiso5;
  }
  
  public void setBindAddPermiso6(RichSelectBooleanCheckbox bindAddPermiso6)
  {
    this.bindAddPermiso6 = bindAddPermiso6;
  }
  
  public RichSelectBooleanCheckbox getBindAddPermiso6()
  {
    return this.bindAddPermiso6;
  }
  
  public String regresaRoles()
  {
    return "/Roles/roles.jspx";
  }
}
