package managedbeans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import oracle.adf.view.rich.component.rich.nav.RichCommandImageLink;

public class descargarManualPDF
{
  private RichCommandImageLink manualPDF;
  private String ArhivoPdf = "";
  
  public void setManualPDF(RichCommandImageLink manualPDF)
  {
    this.manualPDF = manualPDF;
  }
  
  public RichCommandImageLink getManualPDF()
  {
    return this.manualPDF;
  }
  
  public void setArhivoPdf(String ArhivoPdf)
  {
    this.ArhivoPdf = ArhivoPdf;
  }
  
  public String getArhivoPdf()
  {
    return this.ArhivoPdf;
  }
  
  public void descargarPDFs(FacesContext facesContext, OutputStream out)
  {
    this.ArhivoPdf = "ManualConsultaEmpleado";
    File file = new File(System.getProperty("user.dir") + "/PDF/" + this.ArhivoPdf + ".pdf");
    if (file.length() > 0L)
    {
      HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
      
      String contentType = "application/octet-stream";
      try
      {
        FileInputStream fdwd = new FileInputStream(file);
        int checkline;
        while ((checkline = fdwd.available()) > 0)
        {
          byte[] bt = new byte[checkline];
          response.setContentType(contentType);
          response.setHeader("Content-Disposition", "attachment;filename=\"" + this.ArhivoPdf + ".pdf");
          
          int rst = fdwd.read(bt);
          out.write(bt, 0, bt.length);
          if (rst == -1) {
            break;
          }
        }
        out.flush();
        
        out.close();
        fdwd.close();
      }
      catch (IOException e) {}
    }
    else
    {
      FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Lo sentimos por el momento no se encuentra disponible el Manual.");
      FacesContext.getCurrentInstance().addMessage(null, msg);
    }
  }
}
