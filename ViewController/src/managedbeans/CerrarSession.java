package managedbeans;

import java.io.IOException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import oracle.adf.view.rich.component.rich.input.RichInputText;

public class CerrarSession
{
  private String login;
  private RichInputText bindCorreo;
  
  public void session()
  {
    FacesContext ctx = FacesContext.getCurrentInstance();
    HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
    HttpSession session = req.getSession(true);
    setLogin((String)session.getAttribute("login"));
  }
  
  public String cerrarSession()
  {
    FacesContext fctx = FacesContext.getCurrentInstance();
    HttpServletResponse res = (HttpServletResponse)fctx.getExternalContext().getResponse();
    ExternalContext ectx = fctx.getExternalContext();
    try
    {
      HttpSession session = (HttpSession)ectx.getSession(false);
      if (session != null)
      {
        session.invalidate();
        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
      }
      limpiaVariables();
      String logoutUrl = ectx.getRequestContextPath() + "/quadrum/inicioSesion.jspx";
      ectx.redirect(logoutUrl);
      fctx.responseComplete();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    return null;
  }
  
  public void forwardPage(ActionEvent e)
  {
    try
    {
      FacesContext fctx = FacesContext.getCurrentInstance();
      ExternalContext ectx = fctx.getExternalContext();
      HttpSession session = (HttpSession)ectx.getSession(true);
      if (session != null) {
        session.invalidate();
      }
      FacesContext.getCurrentInstance().getExternalContext().redirect("http://www.cfdiquadrum.com.mx/");
    }
    catch (IOException f)
    {
      f.printStackTrace();
    }
  }
  
  public void verificaSesion()
  {
    HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    if (session == null) {
      try
      {
        FacesContext.getCurrentInstance().getExternalContext().redirect("/premium/cfdi/IniciarSesion.jspx");
      }
      catch (IOException e) {}
    }
  }
  
  public void mataSesiones()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    Object session = externalContext.getSession(false);
    HttpSession httpSession = (HttpSession)session;
    httpSession.invalidate();
  }
  
  private void limpiaVariables() {}
  
  public void setLogin(String login)
  {
    this.login = login;
  }
  
  public String getLogin()
  {
    return this.login;
  }
  
  public void setBindCorreo(RichInputText bindCorreo)
  {
    this.bindCorreo = bindCorreo;
  }
  
  public RichInputText getBindCorreo()
  {
    return this.bindCorreo;
  }
}
