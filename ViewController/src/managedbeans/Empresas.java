package managedbeans;

import entities.Empresa;

import entities.Sucursal;
import entities.Trabajador;

import interfaces.SessionEmpresaRemote;

import interfaces.SessionSucursalRemote;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;

import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.myfaces.trinidad.event.SelectionEvent;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;

import org.apache.myfaces.trinidad.util.Service;

import servicelocator.MyServiceLocator;

public class Empresas {
    
    private Empresa empresaTabla;
    private List<Empresa> listEmpresa = new ArrayList<Empresa>();
    private SessionEmpresaRemote ejbEmpresa;
    private SessionSucursalRemote ejbSucursal;
    private RichTable bindTabla = new RichTable();
    
    private String rfcEmpresa;
    private String rfcSucursal;
    private String razonSocial;
    private String regimenPatronal;
    private String regimenFiscal;
    private String correo;
    private String pass;
    
    private RichInputText bindRfc = new RichInputText();
    private RichInputText bindRfcSucursal = new RichInputText();
    private RichInputText bindRazonSocial = new RichInputText();
    private RichInputText bindRegimenPatronal = new RichInputText();
    private RichInputText bindRegimenFiscal = new RichInputText();
    private RichSelectOneChoice bindTipoPersona = new RichSelectOneChoice();
    private RichInputText bindCorreo = new RichInputText();
    private RichInputText bindPass = new RichInputText();
    private RichInputText bindEmpresa = new RichInputText();
    
    private Trabajador user;
    private Empresa admin;
    
    private RichPopup popupSucursal = new RichPopup();
    
    private RichCommandButton btnAgregar = new RichCommandButton();
    private RichCommandButton btnAgregaSucursal= new RichCommandButton();
    
    public Empresas() {
        super();
        obtieneSesion();
        ejbEmpresa = MyServiceLocator.getEmpresaRemote();
        cargaListaEmpresa();
        partial();
        limpiarCajas();
    }

    public void cargaListaEmpresa() {
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBindTabla());
        //this.getBindTabla().setValue(listSucursal);
        listEmpresa.clear();
        listEmpresa = new ArrayList<Empresa>();
        listEmpresa = ejbEmpresa.getEmpresaFindAll();

        for (Empresa empresa : listEmpresa) {
            System.out.println(empresa.getRazonSocial());
        }
    }   
    
    public void agregaSucursalEmpresa(ActionEvent actionEvent){
        this.cerrarPopUp(popupSucursal);
    }
    
    public void seleccionarFila(SelectionEvent selectionEvent) {
        //Empresa empresaTabla = new Empresa();

        empresaTabla = (Empresa)bindTabla.getSelectedRowData();
        if (empresaTabla != null) {
            partial();
            limpiarCajas();
            activaIn();
            this.bindRfc.setValue(empresaTabla.getRfc());
            rfcSucursal = empresaTabla.getRfc();
            pass = "75452";
            this.bindRfcSucursal.setValue(empresaTabla.getRfc());
            this.bindRazonSocial.setValue(empresaTabla.getRazonSocial());
            this.bindRegimenPatronal.setValue(empresaTabla.getRegimenPatronal());
            this.bindRegimenFiscal.setValue(empresaTabla.getRegimenFiscal());
            this.bindTipoPersona.setValue(empresaTabla.getTipoPersona());
            this.bindCorreo.setValue(empresaTabla.getCorreo());
            this.bindPass.setValue(empresaTabla.getPass());
            
            //Llamamos PopUp
            ActionEvent evento = new ActionEvent(popupSucursal);
            this.showPopup(evento, popupSucursal);
        }
    }
    
    /**
         * Método que muestra componente PopUp
         * 
         * @param event     Evento que se detona por un componente de la Vista
         * @param popup1    PopUp a mostrar
         */
        
        public void showPopup(ActionEvent event,RichPopup popup1) {
            FacesContext context = FacesContext.getCurrentInstance();
            UIComponent source = (UIComponent)event.getSource();
            String alignId = source.getClientId(context);
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            popup1.show(hints);
        }
        
        /**
         * Método que oculta componente PopUp
         * 
         * @param popup     PopUp a ocultar
         */
        
        public void cerrarPopUp(RichPopup popup) {
            
            FacesContext fctx = FacesContext.getCurrentInstance();
            UIComponent component = fctx.getViewRoot().findComponent(popup.getClientId());
            ExtendedRenderKitService service = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
            StringBuffer strbuffer = new StringBuffer();
            strbuffer.append("popup=AdfPage.PAGE.findComponent('");
            strbuffer.append(component.getClientId(fctx));
            strbuffer.append("');popup.hide();");
            service.addScript(fctx, strbuffer.toString());
            partial();
            limpiarCajas();
            cargaListaEmpresa();
        }
    
    public void activaIn() {
        this.bindRfc.setDisabled(false);
        this.bindRazonSocial.setDisabled(false);
        this.bindRegimenPatronal.setDisabled(false);
        this.bindRegimenFiscal.setDisabled(false);
        this.bindTipoPersona.setDisabled(false);
        this.bindCorreo.setDisabled(false);
        this.bindPass.setDisabled(false);
        //this.btnInsertar.setDisabled(false);
    }
   
    public void obtieneSesion() {
        user = new Trabajador();
        admin = new Empresa();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
        HttpSession session = req.getSession(true);
        if (session.getAttribute("usuario") != null) {
            user = (Trabajador)session.getAttribute("usuario");
            System.out.println("mm" + user.getCurp());
        } else {
            admin = (Empresa)session.getAttribute("admin");
            System.out.println(admin.getRazonSocial().toUpperCase());
        }

    }
    
    public void partial() {
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBindRfc());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBindRfcSucursal());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBindRazonSocial());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBindRegimenPatronal());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBindRegimenFiscal());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBindTipoPersona());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBindCorreo());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBindPass());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.btnAgregar);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.btnAgregaSucursal);
    }
    
    public void limpiarCajas() {
        this.bindRfc.resetValue();
        this.bindRazonSocial.resetValue();
        this.bindRegimenPatronal.resetValue();
        this.bindRegimenFiscal.resetValue();
        this.bindTipoPersona.resetValue();
        this.bindCorreo.resetValue();
        this.bindPass.resetValue();
        this.bindRfc.setValue("");
        this.bindRazonSocial.setValue("");
        this.bindRegimenPatronal.setValue("");
        this.bindRegimenFiscal.setValue("");
        this.bindTipoPersona.setValue("");
        this.bindCorreo.setValue("");
        this.bindPass.setValue("");
    }

    public void agregaEmpresa(ActionEvent actionEvent) {
        Empresa existeEmpresa = ejbEmpresa.getEmpresaFindEmpresa(this.getRfcEmpresa());
        if(existeEmpresa == null){
        Empresa empresa = new Empresa();
        empresa.setRfc(this.getRfcEmpresa());
        empresa.setRazonSocial(this.getRazonSocial());
        empresa.setRegimenPatronal(this.getRegimenPatronal());
        empresa.setRegimenFiscal(this.getRegimenFiscal());
        empresa.setTipoPersona(this.getBindTipoPersona().getValue().toString());
        empresa.setCorreo(this.getCorreo());
        empresa.setPass(this.getPass());
        ejbEmpresa.persistEmpresa(empresa);
        cargaListaEmpresa();
        /* partial();
        limpiarCajas(); */
        }else{
            FacesMessage Message = new FacesMessage("Error, el RFC ya ha sido registrado!");   
            Message.setSeverity(FacesMessage.SEVERITY_INFO);   
            FacesContext fc = FacesContext.getCurrentInstance();   
            fc.addMessage(null, Message);
        }
    }
    
    public String cancelar() {
        // Add event code here...
        partial();
        limpiarCajas();
        return "/paginaPrincipal.jspx";
    }

    public void setRfcEmpresa(String rfcEmpresa) {
        this.rfcEmpresa = rfcEmpresa;
    }

    public String getRfcEmpresa() {
        return rfcEmpresa;
    }

    public void setBindRfc(RichInputText bindRfc) {
        this.bindRfc = bindRfc;
    }

    public RichInputText getBindRfc() {
        return bindRfc;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setBindRazonSocial(RichInputText bindRazonSocial) {
        this.bindRazonSocial = bindRazonSocial;
    }

    public RichInputText getBindRazonSocial() {
        return bindRazonSocial;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPass() {
        return pass;
    }

    public void setBindRegimenPatronal(RichInputText bindRegimenPatronal) {
        this.bindRegimenPatronal = bindRegimenPatronal;
    }

    public RichInputText getBindRegimenPatronal() {
        return bindRegimenPatronal;
    }

    public void setBindRegimenFiscal(RichInputText bindRegimenFiscal) {
        this.bindRegimenFiscal = bindRegimenFiscal;
    }

    public RichInputText getBindRegimenFiscal() {
        return bindRegimenFiscal;
    }

    public void setBindTipoPersona(RichSelectOneChoice bindTipoPersona) {
        this.bindTipoPersona = bindTipoPersona;
    }

    public RichSelectOneChoice getBindTipoPersona() {
        return bindTipoPersona;
    }

    public void setBindCorreo(RichInputText bindCorreo) {
        this.bindCorreo = bindCorreo;
    }

    public RichInputText getBindCorreo() {
        return bindCorreo;
    }

    public void setBindPass(RichInputText bindPass) {
        this.bindPass = bindPass;
    }

    public RichInputText getBindPass() {
        return bindPass;
    }

    public void setRegimenPatronal(String regimenPatronal) {
        this.regimenPatronal = regimenPatronal;
    }

    public String getRegimenPatronal() {
        return regimenPatronal;
    }

    public void setRegimenFiscal(String regimenFiscal) {
        this.regimenFiscal = regimenFiscal;
    }

    public String getRegimenFiscal() {
        return regimenFiscal;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setBtnAgregar(RichCommandButton btnAgregar) {
        this.btnAgregar = btnAgregar;
    }

    public RichCommandButton getBtnAgregar() {
        return btnAgregar;
    }

    public void setBindTabla(RichTable bindTabla) {
        this.bindTabla = bindTabla;
    }

    public RichTable getBindTabla() {
        return bindTabla;
    }

    public void setListEmpresa(List<Empresa> listEmpresa) {
        this.listEmpresa = listEmpresa;
    }

    public List<Empresa> getListEmpresa() {
        return listEmpresa;
    }

    public void setPopupSucursal(RichPopup popupSucursal) {
        this.popupSucursal = popupSucursal;
    }

    public RichPopup getPopupSucursal() {
        return popupSucursal;
    }

   

    public void setBindEmpresa(RichInputText bindEmpresa) {
        this.bindEmpresa = bindEmpresa;
    }

    public RichInputText getBindEmpresa() {
        return bindEmpresa;
    }

    public void setRfcSucursal(String rfcSucursal) {
        this.rfcSucursal = rfcSucursal;
    }

    public String getRfcSucursal() {
        return rfcSucursal;
    }

    public void setBtnAgregaSucursal(RichCommandButton btnAgregaSucursal) {
        this.btnAgregaSucursal = btnAgregaSucursal;
    }

    public RichCommandButton getBtnAgregaSucursal() {
        return btnAgregaSucursal;
    }

    public void setBindRfcSucursal(RichInputText bindRfcSucursal) {
        this.bindRfcSucursal = bindRfcSucursal;
    }

    public RichInputText getBindRfcSucursal() {
        return bindRfcSucursal;
    }

    public void setAdmin(Empresa admin) {
        this.admin = admin;
    }

    public Empresa getAdmin() {
        return admin;
    }

    public void setEmpresaTabla(Empresa empresaTabla) {
        this.empresaTabla = empresaTabla;
    }

    public Empresa getEmpresaTabla() {
        return empresaTabla;
    }
}
