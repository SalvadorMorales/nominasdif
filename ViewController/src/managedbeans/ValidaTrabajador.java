package managedbeans;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

public class ValidaTrabajador
{
  public void validaNombre(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String nombre = (String)object;
    if ((nombre == null) || (nombre.length() == 0)) {
      return;
    }
    String expression = "^[A-Z����\\s]{2,25}$";
    CharSequence inputStr = nombre;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El nombre no es v��lido.", null));
    }
  }
  
  public void validaApellidoPaterno(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String apellidoPaterno = (String)object;
    if ((apellidoPaterno == null) || (apellidoPaterno.length() == 0)) {
      return;
    }
    String expression = "^[A-Z����\\s]{2,25}$";
    CharSequence inputStr = apellidoPaterno;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El apellido paterno no es v��lido.", null));
    }
  }
  
  public void validaApellidoMaterno(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String apellidoMaterno = (String)object;
    if ((apellidoMaterno == null) || (apellidoMaterno.length() == 0)) {
      return;
    }
    String expression = "^[A-Z����\\s]{2,25}$";
    CharSequence inputStr = apellidoMaterno;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El apellido materno no es v��lido.", null));
    }
  }
  
  public void validarRFC(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String rfc = (String)object;
    if ((rfc == null) || (rfc.length() == 0)) {
      return;
    }
    String expression = "^[A-Z]{4}[0-9]{6}[A-Z0-9]{3}";
    CharSequence inputStr = rfc;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El RFC no es valido. \nLas primeras dos letras (AP) son el apellido paterno m��s la primera vocal interna del apellido paterno.\nEl tercer d��gito (M) es la inicial del apellido materno. Si no existe se utiliza una X.\nEl cuarto d��gito (I) es la inicial del primer nombre.\nLos primeros dos d��gitos son el a��o de nacimiento (88).\nLos segundos d��gitos son el mes de nacimiento (08 o Agosto).\nLos terceros d��gitos son el d��a de nacimiento (29).\nLos ��ltimos d��gitos (XXX) es la homoclave, designada por el SAT.", null));
    }
  }
  
  public void validarCURP(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String curp = (String)object;
    if ((curp == null) || (curp.length() == 0)) {
      return;
    }
    String expression = "[A-Z]{4}[0-9]{6}[H,M][A-Z]{5}[A-Z0-9]{2}";
    CharSequence inputStr = curp;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El CURP no es v��lido.", null));
    }
  }
  
  public void validaNumeroSeguroSocial(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String numsg = (String)object;
    if ((numsg == null) || (numsg.length() == 0)) {
      return;
    }
    String expression = "[0-9]{11}";
    CharSequence inputStr = numsg;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El n��mero del seguro social no es v��lido. Introducir a 11 d��gitos.", null));
    }
  }
  
  public void validaDepto(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String depto = (String)object;
    if ((depto == null) || (depto.length() == 0)) {
      return;
    }
    String expression = "^[A-Z����\\s]{2,25}$";
    CharSequence inputStr = depto;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El nombre de departamento no es v��lido.", null));
    }
  }
  
  public void validaPuesto(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String puesto = (String)object;
    if ((puesto == null) || (puesto.length() == 0)) {
      return;
    }
    String expression = "^[A-Z����\\s]{2,25}$";
    CharSequence inputStr = puesto;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El Nombre del puesto no es v��lido.", null));
    }
  }
  
  public void validaBanco(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String banco = (String)object;
    if ((banco == null) || (banco.length() == 0)) {
      return;
    }
    String expression = "^[A-Z����\\s]{2,25}$";
    CharSequence inputStr = banco;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El nombre del banco no es v��lido.", null));
    }
  }
  
  public void validaNumeroCuenta(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String numc = (String)object;
    if ((numc == null) || (numc.length() == 0)) {
      return;
    }
    String expression = "[0-9]{2,25}$";
    CharSequence inputStr = numc;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El nombre del banco no es v��lido.", null));
    }
  }
  
  public void validaClave(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String clave = (String)object;
    if ((clave == null) || (clave.length() == 0)) {
      return;
    }
    String expression = "[0-9]{2,6}$";
    CharSequence inputStr = clave;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El clave no es valida.", null));
    }
  }
  
  public void validarCorreo(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String correo = (String)object;
    if ((correo == null) || (correo.length() == 0)) {
      return;
    }
    String expression = "[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$";
    CharSequence inputStr = correo;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Escriba su correo electr��nico.", null));
    }
  }
  
  public void validaSalario(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    BigDecimal salario = BigDecimal.ZERO;
    try
    {
      salario = (BigDecimal)object;
    }
    catch (Exception e) {}
    String sueldo = String.valueOf(salario);
    if (salario == BigDecimal.ZERO) {
      return;
    }
    String expression = "[0-9]+(.[0-9][0-9]?)?";
    CharSequence inputStr = sueldo;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "El salario no es valido. Escriba usando decimales", null));
    }
  }
  
  public void validaPassword(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String password = (String)object;
    if ((password == null) || (password.length() == 0)) {
      return;
    }
    String expression = ".{6,18}";
    CharSequence inputStr = password;
    Pattern pattern = Pattern.compile(expression, 2);
    Matcher matcher = pattern.matcher(inputStr);
    if (!matcher.matches()) {
      throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La contrase��a debe tener como m��nimo  6 caracteres.", null));
    }
  }
}
