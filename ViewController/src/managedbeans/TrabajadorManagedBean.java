package managedbeans;

import entities.Empresa;
import entities.Sucursal;
import entities.Trabajador;
import interfaces.SessionTrabajadorRemote;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import servicelocator.MyServiceLocator;
import util.CifradoCesar;

@ManagedBean(name="formBean")
public class TrabajadorManagedBean
{
  static final Logger LOGGER = Logger.getLogger(TrabajadorManagedBean.class.getName());
  @EJB
  private SessionTrabajadorRemote trabajadorFacade;
  private List<Trabajador> listTabla = new ArrayList();
  List<Trabajador> lisDesnc;
  CifradoCesar convert;
  private static String eliminaNSC;
  private Trabajador user;
  private Empresa admin;
  boolean entroAdmin;
  boolean entroUser;
  
  public TrabajadorManagedBean()
  {
    this.trabajadorFacade = MyServiceLocator.getTrabajadorRemote();
    obtieneSesion();
    this.lisDesnc = new ArrayList();
    this.convert = null;
    this.entroAdmin = false;
    this.entroUser = false;
    cargaLista();
  }
  
  public String cargaLista()
  {
    this.lisDesnc.clear();
    try
    {
      if (this.admin.getIdempresa() != 0) {
        this.entroAdmin = true;
      }
      if (this.user.getIdtrabajador() != 0) {
        this.entroUser = true;
      }
    }
    catch (Exception e) {}
    if (this.entroAdmin) {
      this.lisDesnc = this.trabajadorFacade.getfindListXEpresaXT(this.admin.getIdempresa(), "1");
    }
    if (this.entroUser) {
      this.lisDesnc = this.trabajadorFacade.getfindListXSucursalXT(this.user.getSucursal1().getIdsucursales(), "1");
    }
    int key = 10;
    for (Trabajador t : this.lisDesnc)
    {
      if (t.getNombre() != null) {
        t.setNombre(CifradoCesar.Desencriptar(t.getNombre(), key));
      }
      if (t.getApellidos() != null) {
        t.setApellidos(CifradoCesar.Desencriptar(t.getApellidos(), key));
      }
      if (t.getRfc() != null) {
        t.setRfc(CifradoCesar.Desencriptar(t.getRfc(), key));
      }
      if (t.getCurp() != null) {
        t.setCurp(CifradoCesar.Desencriptar(t.getCurp(), key));
      }
      if (t.getNumSeguridadSocial() != null) {
        t.setNumSeguridadSocial(CifradoCesar.Desencriptar(t.getNumSeguridadSocial(), key));
      }
      if (t.getCorreo() != null) {
        t.setCorreo(CifradoCesar.Desencriptar(t.getCorreo(), key));
      }
      if (t.getUsuario() != null) {
        t.setUsuario(CifradoCesar.Desencriptar(t.getUsuario(), key));
      }
      this.listTabla.add(t);
    }
    return "/Trabajador/AdminTrabajador.jspx";
  }
  
  public void obtieneSesion()
  {
    this.user = new Trabajador();
    this.admin = new Empresa();
    FacesContext ctx = FacesContext.getCurrentInstance();
    HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
    HttpSession session = req.getSession(true);
    if (session.getAttribute("usuario") != null) {
      this.user = ((Trabajador)session.getAttribute("usuario"));
    } else {
      this.admin = ((Empresa)session.getAttribute("admin"));
    }
  }
  
  public void preElimina(ActionEvent actionEvent)
  {
    FacesContext ctx = FacesContext.getCurrentInstance();
    ExpressionFactory ef = ctx.getApplication().getExpressionFactory();
    ValueExpression ve = ef.createValueExpression(ctx.getELContext(), "#{row.numSeguridadSocial}", String.class);
    setEliminaNSC((String)ve.getValue(ctx.getELContext()));
    
    Trabajador delete = new Trabajador();
    if (!getEliminaNSC().isEmpty())
    {
      try
      {
        String nsg = CifradoCesar.Encriptar(getEliminaNSC(), 10);
        delete = this.trabajadorFacade.getFindNumSC(nsg);
        delete.setEstatus("0");
        this.trabajadorFacade.mergeTrabajador(delete);
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
      this.listTabla.clear();
      cargaLista();
      String empleado = "";
      if (delete.getNombre() != null) {
        empleado = CifradoCesar.desencriptar(delete.getNombre(), 10);
      }
      if (delete.getApellidos() != null) {
        empleado = empleado + " " + CifradoCesar.desencriptar(delete.getApellidos(), 10);
      }
      FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, null, "El trabajador " + empleado + "  con el n��mero de Seguro Social " + getEliminaNSC() + " ha sido eliminado con ��xito.");
      FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    else
    {
      FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "El trabajador seleccionado no se puede Eliminar, Es requerido un  valor en el campo n��mero de seguridad social, Contacte al administrador de la Base de Datos. ");
      FacesContext.getCurrentInstance().addMessage(null, msg);
    }
  }
  
  public void setEliminaNSC(String eliminaNSC)
  {
    eliminaNSC = eliminaNSC;
  }
  
  public String getEliminaNSC()
  {
    return eliminaNSC;
  }
  
  public void setListTabla(List<Trabajador> listTabla)
  {
    this.listTabla = listTabla;
  }
  
  public List<Trabajador> getListTabla()
  {
    return this.listTabla;
  }
}
