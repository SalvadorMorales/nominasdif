package managedbeans;

import cargaxmlreporte.CargaXml;

import classjava.Anexo20;
import classjava.EjecutaReporte;
import classjava.EnviarCorreo;
import classjava.PistasAuditoria;
import classjava.ReciboTabla;

import classjava.TablaAdmin;

import entities.Recibos;
import entities.Usuarios;

import interfaces.SessionRecibosRemote;

import java.io.OutputStream;

import java.util.List;

import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;

import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;

import oracle.adf.view.rich.event.DialogEvent;

import servicelocator.MyServiceLocator;

import util.CifradoCesar;

import classjava.TablaRecibo;

import classjava.UuidCancela;

import entities.Empresa;
import entities.Permisos;
import entities.Trabajador;

import interfaces.SessionPermisosRemote;

import interfaces.SessionTrabajadorRemote;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.util.GregorianCalendar;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;

import javax.faces.validator.ValidatorException;

import javax.servlet.http.HttpServletResponse;

import javax.xml.bind.JAXBException;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.myfaces.trinidad.component.UIXIterator;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

import utils.system;


public class PaginaTabla {
    private Trabajador user;
    private SessionRecibosRemote ejbRecibos;
    private List<Recibos> listRecibos = new ArrayList<Recibos>();
    private List<TablaRecibo> listaTabla = new ArrayList<TablaRecibo>();
    private CifradoCesar cifrado;
    private boolean selecc;
    private String tipo = "";
    private String estatus;
    
    private String version;
    private String fechaCertificacio;
    private String fechaEmision;
    private Date fechaFinalPago;
    private Date fechaInicialPago;
    private Date fechaPago;
    private int idrecibos;
    private long noDiasPagados;
    private String reciboXML;
    private BigDecimal totalExcentoDeduccion;
    private BigDecimal totalExcentoPercepcion;
    private BigDecimal totalGravadoDeduccion;
    private BigDecimal totalGravadoPercepcion;
    private String uuid;
    private RichSelectOneChoice choiceBusquedas;
    private RichPanelGroupLayout criterioEmpleado;
    private RichInputText uuidcomprobante;
    private RichCommandButton botonBuscarEmpleado;
    private RichPanelGroupLayout panelEmpleado;
    private RichInputText cajaTextoRFCEmpleado;
    private RichPopup popNoSeleccionados;


    private String nombreArchivoPdf;
    private String nombreArchivoXml;
    private RichTable tablaRecibos;
    private RichPopup popNoSeleccionop;
    private String campouuidcomprobante;

    private Anexo20 anexo;

    private String empleado;

    private EjecutaReporte reporte;

    private String sesionUser;
    private Empresa admin;


    private List<String> misUUIDS = new ArrayList<String>();
    private List<TablaRecibo> listaRecibosTabla = new ArrayList<TablaRecibo>();
    private RichPopup popMail;
    private RichInputText cajaEmail;
    private String emailCliente;
    private RichPopup popMailEnviadoBien;
    private RichSelectBooleanCheckbox bindSelectAll;
    private RichSelectBooleanCheckbox checkBoxCancelar;

    //private static String RUTA = "/data/dif/xml/DIFXML2/";
    private static String RUTA = "E:\\DIFXML2\\";
    private Permisos permiso;
    private SessionPermisosRemote ejbPermisos;

    private String rfcImagen;
    private String opcion = "[ seleccione criterio de búsqueda ]";

    private int firstRow = 0;
    private int rowsPerPage = 12;
    private int totalRows;
    private int totalPages;
    private int currentPage = 1;
    private RichCommandButton bindCancelar = new RichCommandButton();
    private int idUsuario;


    private String ArhivoPdf = "";
    private RichCommandButton bindPdfs;


    private List<TablaRecibo> listUuids = new ArrayList<TablaRecibo>();
    private RichPopup popAvisoCancela;
    private RichTable tablaACancelar;
    private RichCommandButton bindClick;

    private int rows;
    private RichPanelGroupLayout bindPanelFecha;
    private RichInputDate paramFecha1;
    private RichInputDate paramFecha2;

    private Date miFechaRango1;
    private Date miFechaRango2;
    private RichCommandButton botonCancelacion;

    private String mensajeCancelacion = "";
    private RichPopup popupAvisoCancela;
    private RichSelectBooleanCheckbox checkBoxCancela;
    private RichColumn columnaEstatus = new RichColumn();
    private RichColumn columnaCancela = new RichColumn();
    
    private PistasAuditoria pistasAuditoria = null;


    public PaginaTabla() {
        cifrado = new CifradoCesar();
        anexo = new Anexo20();
        pistasAuditoria = new PistasAuditoria();
        obtieneSesion();
        ejbRecibos = MyServiceLocator.getSessionRecibosRemote();
        ejbPermisos = MyServiceLocator.getPermisosRemote();
        //desencriptaTabla();
        limpiaCorreo();
        funciones(permiso);
        rows = 0;
        String i = "";
        if (admin != null) {
            rows = ejbRecibos.recibosTotalEmpresa(admin.getSucursalList().get(0).getIdsucursales());
            System.out.println("iii  " + i);
            System.out.println("rowsss " + rows);
        } else {
            rows = ejbRecibos.recibosTotalEmpleado(user.getIdtrabajador(), user.getTipo());
        }
        this.setTotalRows(rows);
        double val1 = ((double)this.getTotalRows() / this.getRowsPerPage());
        int totalPagesCal = (int)Math.ceil(val1);
        this.setTotalPages((totalPagesCal != 0) ? totalPagesCal : 1);
        cargaTabla();

    }


    public void funciones(Object obj) {
        adfPart();
        if (obj != null) {
            Permisos permiso = (Permisos)obj;
            this.getColumnaCancela().setVisible(permiso.getCancela());
            this.getColumnaEstatus().setVisible(permiso.getCancela());
        } else {
            this.getColumnaCancela().setVisible(false);
            this.getColumnaEstatus().setVisible(false);
        }

    }


    public void adfPart() {
    }

    /*
    public void obtieneSesion() {
        user = new Trabajador();
        admin = new Empresa();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
        HttpSession session = req.getSession(true);
        if (session.getAttribute("usuario") != null) {
            user = (Trabajador)session.getAttribute("usuario");
            admin = null;
        } else {
            admin = (Empresa)session.getAttribute("admin");
            user = null;
        }

    }
*/

    public void obtieneSesion() {
        user = new Trabajador();
        admin = new Empresa();
        permiso = new Permisos();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
        HttpSession session = req.getSession(true);
        if (session.getAttribute("usuario") != null) {
            System.out.println("entra usuario");
            user = (Trabajador)session.getAttribute("usuario");
            admin = null;
        } else {
            System.out.println("entra admin");
            admin = (Empresa)session.getAttribute("admin");
            permiso = (Permisos)session.getAttribute("permiso");
            user = null;
        }
    }

    public void cargaTabla() {
        user = new Trabajador();
        admin = new Empresa();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
        HttpSession session = req.getSession(true);
        if (session.getAttribute("usuario") != null) {
            user = (Trabajador)session.getAttribute("usuario");
            admin = null;
            System.out.println(cifrado.Desencriptar(user.getUsuario(), 10).toUpperCase());
            System.out.println(user.getSucursal1().getEmpresa().getRfc());
            sesionUser = cifrado.Desencriptar(user.getUsuario(), 10).toUpperCase();
            rfcImagen = user.getSucursal1().getEmpresa().getRfc();
            desencriptaTabla(user, firstRow, rowsPerPage);
            idUsuario = user.getIdtrabajador();

        } else {
            admin = (Empresa)session.getAttribute("admin");
            user = null;
            sesionUser = cifrado.Desencriptar(admin.getRazonSocial(), 10);
            rfcImagen = admin.getRfc();
            idUsuario = admin.getIdempresa();
            desencriptaTabla(admin, firstRow, rowsPerPage);
        }

    }


    public void metodoEnviarCorreo(ActionEvent actionEvent) {
        if (this.getMisUUIDS().size() > 0) {
            Anexo20 anexo = new Anexo20();
            anexo.showPopup(actionEvent, popMail);
        } else {
            ActionEvent evento = new ActionEvent(botonBuscarEmpleado);
            anexo.showPopup(evento, popNoSeleccionop);
        }
    }


    public void listenerCheck(ValueChangeEvent valueChangeEvent) {
        if ((Boolean)valueChangeEvent.getNewValue()) {
            TablaRecibo seleccionados = (TablaRecibo)tablaRecibos.getSelectedRowData();
            misUUIDS.add(seleccionados.getUuid());
            listaRecibosTabla.add(seleccionados);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindPdfs);
            this.getBindPdfs().setDisabled(false);
        } else {
            TablaRecibo seleccionados = (TablaRecibo)tablaRecibos.getSelectedRowData();
            if (misUUIDS.size() > 0) {
                misUUIDS.remove(seleccionados.getUuid());
            } else if (misUUIDS.size() == 0) {
                this.getBindPdfs().setDisabled(true);
            }
        }
    }


    public void imprimeRecibos(ActionEvent actionEvent) {
        firstRow = 0;
        rowsPerPage = 10;

        if (admin != null)
            desencriptaTabla(admin, firstRow, rowsPerPage);
        else
            desencriptaTabla(user, firstRow, rowsPerPage);


    }

    public String retornaFecha() {
        Calendar fecha = new GregorianCalendar();
        //Obtenemos el valor del año, mes, día,
        //hora, minuto y segundo del sistema
        //usando el método get y el parámetro correspondiente
        int anio = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        return dia + "-" + (mes + 1) + "-" + anio + "--" + hora + "-" + minuto + "-" + segundo;
    }


    public void descargarPDFs(FacesContext facesContext, OutputStream out) {
        if (misUUIDS.size() > 0) {
            tipo = "web";
            try {
                sacaArchivosPdfBD(misUUIDS, tipo);

                File file = new File(System.getProperty("user.dir") + "/PDF/" + "Prueba2222" + ".pdf");
                if (file.isFile()) {
                    HttpServletResponse response =
                        (HttpServletResponse)facesContext.getExternalContext().getResponse();
                    String contentType = "application/octet-stream";
                    FileInputStream fdwd;
                    byte[] bt;
                    try {
                        fdwd = new FileInputStream(file);
                        int checkline;
                        while ((checkline = fdwd.available()) > 0) {
                            bt = new byte[checkline];
                            response.setContentType(contentType);
                            response.setHeader("Content-Disposition", "attachment;filename=\"" + "Prueba222" + ".pdf");
                            int rst = fdwd.read(bt);
                            out.write(bt, 0, bt.length);
                            if (rst == -1)
                                break;
                        }
                        out.flush();
                        out.close();
                        fdwd.close();
                        misUUIDS.clear();
                        file.deleteOnExit();
                        file.delete();
                    } catch (IOException e) {

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AdfFacesContext.getCurrentInstance().addPartialTarget(checkBoxCancelar);
            FacesContext.getCurrentInstance().addMessage(checkBoxCancelar.getClientId(FacesContext.getCurrentInstance()),
                                                         new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                                                                          "Debe seleccionar al menos uno".toUpperCase()));

        }
    }


    public void refreshPage() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String refreshpage = facesContext.getViewRoot().getViewId();
        ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
        UIViewRoot viewroot = viewHandler.createView(facesContext, refreshpage);
        viewroot.setViewId(refreshpage);
        facesContext.setViewRoot(viewroot);
        // anexo.cerrarPopUp(popup2);
    }


    public void desencriptaTabla(Object obj, int inicio, int fin) {
        String tipo = obj.getClass().toString();
        listRecibos.clear();
        listaTabla.clear();
        if (tipo.equals("class entities.Empresa")) {
            Empresa empr = (Empresa)obj;
            listRecibos = ejbRecibos.getAllEmpresa(empr.getSucursalList().get(0).getIdsucursales(), inicio, fin);
            listaTabla = new ArrayList<TablaRecibo>();
            int tamRecibos = listRecibos.size();
            if (tamRecibos != 0) {
                for (Recibos r : listRecibos) {
                    fechaCertificacio = r.getFechaCertificacio().toString();
                    fechaEmision = r.getFechaEmision();
                    fechaFinalPago = r.getFechaFinalPago();
                    fechaInicialPago = r.getFechaInicialPago();
                    fechaPago = r.getFechaPago();
                    idrecibos = r.getIdrecibos();
                    noDiasPagados = r.getNoDiasPagados();
                    reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                    totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                    totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                    totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                    totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                    uuid = cifrado.Desencriptar(r.getUuid(), 10);
                    selecc = false;
                    estatus = r.getEstatus();
                    version = "1.2";
                    TablaRecibo tabla =
                        new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago, fechaPago,
                                        idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                        totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion, uuid,
                                        selecc, estatus,version);
                    listaTabla.add(tabla);
                    //refreshPage();
                }
            }

        } else {

            Trabajador traba = (Trabajador)obj;
            listRecibos = ejbRecibos.getAllTrabajador(traba.getIdtrabajador(), traba.getTipo(), inicio, fin);
            listaTabla = new ArrayList<TablaRecibo>();
            int tamRecibos = listRecibos.size();
            System.out.println("Tam " + tamRecibos);
            if (tamRecibos != 0) {
                for (Recibos r : listRecibos) {
                    fechaCertificacio = r.getFechaCertificacio();
                    fechaEmision = r.getFechaEmision();
                    fechaFinalPago = r.getFechaFinalPago();
                    fechaInicialPago = r.getFechaInicialPago();
                    fechaPago = r.getFechaPago();
                    idrecibos = r.getIdrecibos();
                    noDiasPagados = r.getNoDiasPagados();
                    reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                    totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                    totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                    totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                    totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                    uuid = cifrado.Desencriptar(r.getUuid(), 10);
                    selecc = false;
                    version = "1.2";                    //estatus = r.getEstatus();

                    TablaRecibo tabla =
                        new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago, fechaPago,
                                        idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                        totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion, uuid,
                                        selecc, estatus,version);
                    listaTabla.add(tabla);
                    //refreshPage();
                }
            }

        }
    }

    public void firstActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(1);
        this.setFirstRow(0);
        if (admin != null) {
            desencriptaTabla(admin, firstRow, rowsPerPage);
        } else {
            desencriptaTabla(user, firstRow, rowsPerPage);
        }
    }

    public void previousActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(this.getCurrentPage() - 1);
        this.setFirstRow(this.getFirstRow() - this.getRowsPerPage());
        if (admin != null) {
            desencriptaTabla(admin, firstRow, rowsPerPage);
        } else {
            desencriptaTabla(user, firstRow, rowsPerPage);
        }
    }

    public void nextActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(this.getCurrentPage() + 1);
        this.setFirstRow(this.getFirstRow() + this.getRowsPerPage());
        if (admin != null) {
            desencriptaTabla(admin, firstRow, rowsPerPage);
        } else {
            desencriptaTabla(user, firstRow, rowsPerPage);
        }
    }

    public void lastActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(this.getTotalPages());
        this.setFirstRow(this.getTotalRows() -
                         ((this.getTotalRows() % this.getRowsPerPage() != 0) ? this.getTotalRows() %
                          this.getRowsPerPage() : this.getRowsPerPage()));
        if (admin != null) {
            desencriptaTabla(admin, firstRow, rowsPerPage);
        } else {
            desencriptaTabla(user, firstRow, rowsPerPage);
        }
    }

    public boolean isBeforeDisabled() {
        return this.getFirstRow() == 0;
    }

    public boolean isAfterDisabled() {
        return this.getFirstRow() >= this.getTotalRows() - this.getRowsPerPage();
    }


    public void setListRecibos(List<Recibos> listRecibos) {
        this.listRecibos = listRecibos;
    }

    public List<Recibos> getListRecibos() {
        return listRecibos;
    }

    public void setFechaCertificacio(String fechaCertificacio) {
        this.fechaCertificacio = fechaCertificacio;
    }

    public String getFechaCertificacio() {
        return fechaCertificacio;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaFinalPago(Date fechaFinalPago) {
        this.fechaFinalPago = fechaFinalPago;
    }

    public Date getFechaFinalPago() {
        return fechaFinalPago;
    }

    public void setFechaInicialPago(Date fechaInicialPago) {
        this.fechaInicialPago = fechaInicialPago;
    }

    public Date getFechaInicialPago() {
        return fechaInicialPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setIdrecibos(int idrecibos) {
        this.idrecibos = idrecibos;
    }

    public int getIdrecibos() {
        return idrecibos;
    }

    public void setNoDiasPagados(long noDiasPagados) {
        this.noDiasPagados = noDiasPagados;
    }

    public long getNoDiasPagados() {
        return noDiasPagados;
    }

    public void setReciboXML(String reciboXML) {
        this.reciboXML = reciboXML;
    }

    public String getReciboXML() {
        return reciboXML;
    }

    public void setTotalExcentoDeduccion(BigDecimal totalExcentoDeduccion) {
        this.totalExcentoDeduccion = totalExcentoDeduccion;
    }

    public BigDecimal getTotalExcentoDeduccion() {
        return totalExcentoDeduccion;
    }

    public void setTotalExcentoPercepcion(BigDecimal totalExcentoPercepcion) {
        this.totalExcentoPercepcion = totalExcentoPercepcion;
    }

    public BigDecimal getTotalExcentoPercepcion() {
        return totalExcentoPercepcion;
    }

    public void setTotalGravadoDeduccion(BigDecimal totalGravadoDeduccion) {
        this.totalGravadoDeduccion = totalGravadoDeduccion;
    }

    public BigDecimal getTotalGravadoDeduccion() {
        return totalGravadoDeduccion;
    }

    public void setTotalGravadoPercepcion(BigDecimal totalGravadoPercepcion) {
        this.totalGravadoPercepcion = totalGravadoPercepcion;
    }

    public BigDecimal getTotalGravadoPercepcion() {
        return totalGravadoPercepcion;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setListaTabla(List<TablaRecibo> listaTabla) {
        this.listaTabla = listaTabla;
    }

    public List<TablaRecibo> getListaTabla() {
        return listaTabla;
    }

    public void fechaIniciRelacion(ValueChangeEvent value) {
        try {
            listRecibos.clear();
            listaTabla.clear();
            opcion = (String)value.getNewValue();
            AdfFacesContext.getCurrentInstance().addPartialTarget(criterioEmpleado);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindPanelFecha);
            AdfFacesContext.getCurrentInstance().addPartialTarget(uuidcomprobante);
            AdfFacesContext.getCurrentInstance().addPartialTarget(paramFecha1);
            AdfFacesContext.getCurrentInstance().addPartialTarget(paramFecha2);
            if (opcion.equals("UUID")) {
                this.criterioEmpleado.setVisible(true);
                this.bindPanelFecha.setVisible(false);
                this.getUuidcomprobante().resetValue();
                this.getUuidcomprobante().setValue("");
                this.getParamFecha1().resetValue();
                this.getParamFecha1().setValue("");
                this.getParamFecha2().resetValue();
                this.getParamFecha2().setValue("");

            } else if (opcion.equals("TODOS")) {
                this.criterioEmpleado.setVisible(false);
                this.bindPanelFecha.setVisible(false);
                this.getUuidcomprobante().setValue("");
                this.getParamFecha1().resetValue();
                this.getParamFecha1().setValue("");
                this.getParamFecha2().resetValue();
                this.getParamFecha2().setValue("");


            } else if (opcion.equals("FECHA PAGO")) {
                this.criterioEmpleado.setVisible(false);
                bindPanelFecha.setVisible(true);
                this.getUuidcomprobante().setValue("");
                this.getParamFecha1().resetValue();
                this.getParamFecha1().setValue("");
                this.getParamFecha2().resetValue();
                this.getParamFecha2().setValue("");
            } else if (opcion == null) {
                opcion = "[ seleccione criterio de búsqueda ]";
            }
        } catch (Exception e) {
        }

    }


    public void llenaConsultaRfc() {
        if (listRecibos.size() > 0) {
            for (Recibos r : listRecibos) {
                //fechaCertificacio = r.getFechaCertificacio();
                //fechaEmision = r.getFechaEmision();
                fechaFinalPago = r.getFechaFinalPago();
                fechaInicialPago = r.getFechaInicialPago();
                fechaPago = r.getFechaPago();
                //idrecibos = r.getIdrecibos();
                //noDiasPagados = r.getNoDiasPagados();
                //reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                //totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                //totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                //totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                //totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                uuid = cifrado.Desencriptar(r.getUuid(), 10);
                selecc = false;
                estatus = r.getEstatus();
                version = "1.2";
                TablaRecibo tabla =
                    new TablaRecibo(fechaFinalPago, fechaInicialPago, fechaPago, uuid, selecc, estatus,version);
                listaTabla.add(tabla);
            }
        } else {
            listaTabla.clear();
        }

    }


    public void setChoiceBusquedas(RichSelectOneChoice choiceBusquedas) {
        this.choiceBusquedas = choiceBusquedas;
    }

    public RichSelectOneChoice getChoiceBusquedas() {
        return choiceBusquedas;
    }

    public void setCriterioEmpleado(RichPanelGroupLayout criterioEmpleado) {
        this.criterioEmpleado = criterioEmpleado;
    }

    public RichPanelGroupLayout getCriterioEmpleado() {
        return criterioEmpleado;
    }

    public void setUuidcomprobante(RichInputText uuidcomprobante) {
        this.uuidcomprobante = uuidcomprobante;
    }

    public RichInputText getUuidcomprobante() {
        return uuidcomprobante;
    }

    public void setBotonBuscarEmpleado(RichCommandButton botonBuscarEmpleado) {
        this.botonBuscarEmpleado = botonBuscarEmpleado;
    }

    public RichCommandButton getBotonBuscarEmpleado() {
        return botonBuscarEmpleado;
    }

    public void busquedaRecibo(ActionEvent actionEvent) {
        listRecibos.clear();
        listaRecibosTabla.clear();
        listaTabla.clear();
        if (opcion != null) {
            if (opcion.equals("UUID")) {
                if(!this.getCampouuidcomprobante().equals("")  && this.getCampouuidcomprobante() != null){
                System.out.println("entra    _______________________________________" +
                                   this.getCampouuidcomprobante() + "   " + this.getIdUsuario());
                listRecibos =
                        ejbRecibos.getConsultaRecibosUUID(cifrado.Encriptar(this.getCampouuidcomprobante(), 10), this.getIdUsuario(),
                                                          firstRow, rowsPerPage);


                System.out.println(listRecibos.size());
                
                if(listRecibos.size() > 0){
                this.setTotalRows(listRecibos.size());
                double val1 = ((double)this.getTotalRows() / this.getRowsPerPage());
                int totalPagesCal = (int)Math.ceil(val1);
                this.setTotalPages((totalPagesCal != 0) ? totalPagesCal : 1);
                //llenaConsultaRfc();

                fechaFinalPago = listRecibos.get(0).getFechaFinalPago();
                fechaInicialPago = listRecibos.get(0).getFechaInicialPago();
                fechaPago = listRecibos.get(0).getFechaPago();
                uuid = cifrado.Desencriptar(listRecibos.get(0).getUuid(), 10).toUpperCase();
                selecc = false;
                estatus = listRecibos.get(0).getEstatus();
                version = "1.2";
                TablaRecibo tabla =
                    new TablaRecibo(listRecibos.get(0).getFechaFinalPago(), listRecibos.get(0).getFechaInicialPago(),
                                    listRecibos.get(0).getFechaPago(),
                                    cifrado.Desencriptar(listRecibos.get(0).getUuid(), 10).toUpperCase(), selecc,
                                    estatus,version);
                
                listaTabla.add(tabla);
                }else{
                        AdfFacesContext.getCurrentInstance().addPartialTarget(uuidcomprobante);
                        FacesContext.getCurrentInstance().addMessage(uuidcomprobante.getClientId(FacesContext.getCurrentInstance()),
                                                                     new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                                                                                      "No se encontro UUID.".toUpperCase()));
                    
                    }
                }else{
                        AdfFacesContext.getCurrentInstance().addPartialTarget(uuidcomprobante);
                        FacesContext.getCurrentInstance().addMessage(uuidcomprobante.getClientId(FacesContext.getCurrentInstance()),
                                                                     new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                                                                                      "Debe ingresar un valor.".toUpperCase()));
                    }
            }
            if (opcion.equals("TODOS")) {
                firstRow = 0;
                listRecibos.clear();
                listaTabla.clear();
                this.criterioEmpleado.setVisible(false);
                if (admin != null) {
                    //desencriptaTabla(admin, firstRow, rowsPerPage);
                    System.out.println("---------ADMIN-----------" + admin.getIdempresa());
                    listRecibos =
                            ejbRecibos.getAllEmpresa(admin.getSucursalList().get(0).getIdsucursales(), firstRow, rowsPerPage);


                    listaTabla = new ArrayList<TablaRecibo>();
                    int tamRecibos = listRecibos.size();
                    System.out.println(",,,,, " + tamRecibos);
                    if (tamRecibos != 0) {
                        for (Recibos r : listRecibos) {
                            fechaCertificacio = r.getFechaCertificacio();
                            fechaEmision = r.getFechaEmision();
                            fechaFinalPago = r.getFechaFinalPago();
                            fechaInicialPago = r.getFechaInicialPago();
                            fechaPago = r.getFechaPago();
                            idrecibos = r.getIdrecibos();
                            noDiasPagados = r.getNoDiasPagados();
                            reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                            totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                            totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                            totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                            totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                            uuid = cifrado.Desencriptar(r.getUuid(), 10);
                            selecc = false;
                            estatus = r.getEstatus();
                            version = "1.2";

                            TablaRecibo tabla =
                                new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago,
                                                fechaPago, idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                                totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion,
                                                uuid, selecc, estatus,version);
                            listaTabla.add(tabla);


                        }
                        this.setTotalRows(rows);
                        double val1 = ((double)this.getTotalRows() / this.getRowsPerPage());
                        int totalPagesCal = (int)Math.ceil(val1);
                        this.setTotalPages((totalPagesCal != 0) ? totalPagesCal : 1);
                    }

                } else {
                    listRecibos =
                            ejbRecibos.getAllTrabajador(user.getIdtrabajador(), user.getTipo(), firstRow, rowsPerPage);
                    listaTabla = new ArrayList<TablaRecibo>();
                    int tamRecibos = listRecibos.size();
                    if (tamRecibos != 0) {
                        for (Recibos r : listRecibos) {
                            fechaCertificacio = r.getFechaCertificacio();
                            fechaEmision = r.getFechaEmision();
                            fechaFinalPago = r.getFechaFinalPago();
                            fechaInicialPago = r.getFechaInicialPago();
                            fechaPago = r.getFechaPago();
                            idrecibos = r.getIdrecibos();
                            noDiasPagados = r.getNoDiasPagados();
                            reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                            totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                            totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                            totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                            totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                            uuid = cifrado.Desencriptar(r.getUuid(), 10);
                            selecc = false;
                            estatus = r.getEstatus();
                            version = "1.2";
                            TablaRecibo tabla =
                                new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago,
                                                fechaPago, idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                                totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion,
                                                uuid, selecc, estatus,version);
                            listaTabla.add(tabla);
                            this.setCurrentPage(1);
                            this.setFirstRow(0);


                        }
                        this.setTotalRows(rows);
                        double val1 = ((double)this.getTotalRows() / this.getRowsPerPage());
                        int totalPagesCal = (int)Math.ceil(val1);
                        this.setTotalPages((totalPagesCal != 0) ? totalPagesCal : 1);
                    }

                }
            }
            if (opcion.equals("FECHA PAGO")) {
                System.out.println("---- > ");
                busquedaPorFecha(actionEvent);

            } else if (opcion.equals("[ seleccione criterio de búsqueda ]")) {
                AdfFacesContext.getCurrentInstance().addPartialTarget(choiceBusquedas);
                FacesContext.getCurrentInstance().addMessage(choiceBusquedas.getClientId(FacesContext.getCurrentInstance()),
                                                             new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                                                                              "Debe seleccionar una consulta".toUpperCase()));

            }
        } else {
            AdfFacesContext.getCurrentInstance().addPartialTarget(choiceBusquedas);
            FacesContext.getCurrentInstance().addMessage(choiceBusquedas.getClientId(FacesContext.getCurrentInstance()),
                                                         new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                                                                          "Debe seleccionar una consulta".toUpperCase()));

        }
    }


    public void busquedaPorFecha(ActionEvent at) {
        listRecibos.clear();
        listaRecibosTabla.clear();
        listaTabla.clear();
        firstRow = 0;
        rowsPerPage = 10;
        if (admin != null) {
            //desencriptaTabla(admin, firstRow, rowsPerPage);
            if (getMiFechaRango1() != null && getMiFechaRango2() != null) {
                if ((this.getMiFechaRango1().before(this.getMiFechaRango2()) ||
                     getMiFechaRango1().equals(getMiFechaRango2()))) {
                    if (getMiFechaRango1().equals(getMiFechaRango2())) {
                        Calendar calendarioUno = Calendar.getInstance();
                        calendarioUno.setTime(new Date(this.getMiFechaRango2().getTime()));
                        calendarioUno.add(Calendar.HOUR_OF_DAY, 24);
                        listRecibos =
                                ejbRecibos.getRecibosFechaEmpresa(admin.getIdempresa(), admin.getIdempresa(), new Timestamp(this.getMiFechaRango1().getTime()),
                                                                  new Timestamp((calendarioUno.getTime().getTime())),
                                                                  firstRow, rowsPerPage);


                    } else {
                        System.out.println(admin.getIdempresa() + "111111111111111111" + firstRow + "   " + rowsPerPage);
                        Calendar calendarioUno = Calendar.getInstance();
                        calendarioUno.setTime(new Date(this.getMiFechaRango2().getTime()));
                        calendarioUno.add(Calendar.HOUR_OF_DAY, 24);

                        listRecibos =
                                ejbRecibos.getRecibosFechaEmpresa(admin.getIdempresa(), admin.getIdempresa(), new Timestamp(this.getMiFechaRango1().getTime()),
                                                                  new Timestamp((calendarioUno.getTime().getTime())),
                                                                  firstRow, rowsPerPage);
                        System.out.println("...... " + listRecibos.size());
                    }
                    //this.listaAuxiliar()
                }

            }
            System.out.println("--> tamaño" + listRecibos.size());
            this.criterioEmpleado.setVisible(false);

            int tamRecibos = listRecibos.size();
            if (tamRecibos != 0) {
                for (Recibos r : listRecibos) {
                    fechaCertificacio = r.getFechaCertificacio();
                    fechaEmision = r.getFechaEmision();
                    fechaFinalPago = r.getFechaFinalPago();
                    fechaInicialPago = r.getFechaInicialPago();
                    fechaPago = r.getFechaPago();
                    idrecibos = r.getIdrecibos();
                    noDiasPagados = r.getNoDiasPagados();
                    reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                    totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                    totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                    totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                    totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                    uuid = cifrado.Desencriptar(r.getUuid(), 10);
                    selecc = false;
                    version = "1.2";
                    estatus = listRecibos.get(0).getEstatus();
                    TablaRecibo tabla =
                        new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago, fechaPago,
                                        idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                        totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion, uuid,
                                        selecc, estatus,version);
                    listaTabla.add(tabla);
                    this.setCurrentPage(1);
                    this.setFirstRow(0);


                }
                this.setTotalRows(rows);
                double val1 = ((double)this.getTotalRows() / this.getRowsPerPage());
                int totalPagesCal = (int)Math.ceil(val1);
                this.setTotalPages((totalPagesCal != 0) ? totalPagesCal : 1);
            }

        } else {

            if (getMiFechaRango1() != null && getMiFechaRango2() != null) {
                if ((this.getMiFechaRango1().before(this.getMiFechaRango2()) ||
                     getMiFechaRango1().equals(getMiFechaRango2()))) {
                    if (getMiFechaRango1().equals(getMiFechaRango2())) {
                        Calendar calendarioUno = Calendar.getInstance();
                        calendarioUno.setTime(new Date(this.getMiFechaRango2().getTime()));
                        calendarioUno.add(Calendar.HOUR_OF_DAY, 24);
                        listRecibos =
                                ejbRecibos.getRecibosFecha(idUsuario, new Timestamp(this.getMiFechaRango1().getTime()),
                                                           new Timestamp((calendarioUno.getTime().getTime())),
                                                           firstRow, rowsPerPage);


                    } else {
                        System.out.println(idUsuario + " " + firstRow + "   " + rowsPerPage);
                        Calendar calendarioUno = Calendar.getInstance();
                        calendarioUno.setTime(new Date(this.getMiFechaRango2().getTime()));
                        calendarioUno.add(Calendar.HOUR_OF_DAY, 24);

                        listRecibos =
                                ejbRecibos.getRecibosFecha(idUsuario, new Timestamp(this.getMiFechaRango1().getTime()),
                                                           new Timestamp((calendarioUno.getTime().getTime())),
                                                           firstRow, rowsPerPage);
                        System.out.println("...... " + listRecibos.size());
                    }
                    //this.listaAuxiliar()
                }

            }
            System.out.println("--> tamaño" + listRecibos.size());
            this.criterioEmpleado.setVisible(false);


            int tamRecibos = listRecibos.size();
            if (tamRecibos != 0) {
                for (Recibos r : listRecibos) {
                    fechaCertificacio = r.getFechaCertificacio();
                    fechaEmision = r.getFechaEmision();
                    fechaFinalPago = r.getFechaFinalPago();
                    fechaInicialPago = r.getFechaInicialPago();
                    fechaPago = r.getFechaPago();
                    idrecibos = r.getIdrecibos();
                    noDiasPagados = r.getNoDiasPagados();
                    reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                    totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                    totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                    totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                    totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                    uuid = cifrado.Desencriptar(r.getUuid(), 10);
                    selecc = false;
                    estatus = listRecibos.get(0).getEstatus();
                    version = "1.2";

                    TablaRecibo tabla =
                        new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago, fechaPago,
                                        idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                        totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion, uuid,
                                        selecc, estatus, version);
                    listaTabla.add(tabla);
                    this.setCurrentPage(1);
                    this.setFirstRow(0);


                }
                this.setTotalRows(rows);
                double val1 = ((double)this.getTotalRows() / this.getRowsPerPage());
                int totalPagesCal = (int)Math.ceil(val1);
                this.setTotalPages((totalPagesCal != 0) ? totalPagesCal : 1);
            }

        }


    }

    public void setPanelEmpleado(RichPanelGroupLayout panelEmpleado) {
        this.panelEmpleado = panelEmpleado;
    }

    public RichPanelGroupLayout getPanelEmpleado() {
        return panelEmpleado;
    }

    public void eventoRevisaExistenciaReceptor(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
    }

    public void setCajaTextoRFCEmpleado(RichInputText cajaTextoRFCEmpleado) {
        this.cajaTextoRFCEmpleado = cajaTextoRFCEmpleado;
    }

    public RichInputText getCajaTextoRFCEmpleado() {
        return cajaTextoRFCEmpleado;
    }

    public List getMiListaAutoSuggestNombreEmpleado() {
        // Add event code here...
        return null;
    }

    public List suggestedTextNombreEmpleado(String string) {
        // Add event code here...
        return null;
    }

    public void setPopNoSeleccionados(RichPopup popNoSeleccionados) {
        this.popNoSeleccionados = popNoSeleccionados;
    }

    public RichPopup getPopNoSeleccionados() {
        return popNoSeleccionados;
    }

    public void downLoadPdf(FacesContext facesContext, OutputStream out) {

        try {
            //this.sacaArchivoPdf();
            TablaRecibo seleccionado = (TablaRecibo)tablaRecibos.getSelectedRowData();
            EjecutaReporte ejecuta = new EjecutaReporte();
            String rutaXml = seleccionado.getUuid();
            System.out.println("ruta "+rutaXml);
            List<String> lisXml = new ArrayList<String>();
            lisXml.add(rutaXml);
            
            if(seleccionado.getVersion().equals("1.2")){
                    System.out.println("1.2");
                    ejecuta.generarReporte(lisXml, "web","1.2");    
            } else{
            ejecuta.generarReporte(lisXml, "web","1.1");    
            }
            
            
            if(user != null)
                //pistasAuditoria.eventosCliente("USUARIO", "Descargo CFDI "+rutaXml,user.getRfc() , "SELECT");
            

            //this.setCurrentPage(1);
            //this.setFirstRow(0);
            if (admin != null) {
                desencriptaTabla(admin, firstRow, rowsPerPage);
            } else {
                desencriptaTabla(user, firstRow, rowsPerPage);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    public String buscaArchivo(String nombre) {
        String path = "E://PDFDIF//";
        String ruta = "";
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {

            if (listOfFiles[i].isFile()) {
                files = listOfFiles[i].getName();
                if (files.endsWith(".pdf") || files.endsWith(".PDF")) {
                    if (listOfFiles[i].toString().contains(nombre))
                        ruta = listOfFiles[i].getAbsolutePath();
                }
            }
        }
        System.out.println("Fin");
        return ruta;
    }

    public void sacaArchivosPdfBD(List<String> nombreUuid, String tipo) {
        try {
            EjecutaReporte ejecuta = new EjecutaReporte();
            ejecuta.generarReporte(misUUIDS, tipo,"1.2");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setNombreArchivoPdf(String nombreArchivoPdf) {
        this.nombreArchivoPdf = nombreArchivoPdf;
    }

    public String getNombreArchivoPdf() {
        return nombreArchivoPdf;
    }

    public void downLoadXml(FacesContext facesContext, OutputStream out) throws FileNotFoundException, IOException {

        TablaRecibo seleccionado = (TablaRecibo)tablaRecibos.getSelectedRowData();
        if (seleccionado != null) {
            System.out.println("-->" + RUTA + seleccionado.getUuid().trim());
            File file = new File(RUTA + seleccionado.getUuid().trim() + ".xml");
            if (file.exists()) {
                if (file.isFile()) {

                    String contentType = "application/octet-stream"; //tipo de contenido para que sea de descarga
                    FileInputStream fdwd = null;
                    byte[] bt;
                    fdwd = new FileInputStream(file);
                    //if(user != null)
                      //  pistasAuditoria.eventosCliente("USUARIO", "Descargo xml "+seleccionado.getUuid(),user.getRfc() , "SELECT");
                    

                    HttpServletResponse response =
                        (HttpServletResponse)facesContext.getExternalContext().getResponse();
                    response.setContentType(contentType);
                    response.setHeader("Content-Disposition",
                                       "attachment;filename=\"" + seleccionado.getUuid() + ".xml"); //colocar nombre del documento asi como su extencion

                    int checkline;
                    while ((checkline = fdwd.available()) > 0) {
                        bt = new byte[checkline];
                        int rst = fdwd.read(bt);
                        out.write(bt, 0, rst);
                        if (rst == -1)
                            break;
                    }
                    out.flush();
                    out.close();
                }
            } else {
            }
        } else {
            ActionEvent action = new ActionEvent(botonBuscarEmpleado);
            anexo.showPopup(action, popNoSeleccionop);
        }
    }


    public void setNombreArchivoXml(String nombreArchivoXml) {
        this.nombreArchivoXml = nombreArchivoXml;
    }

    public String getNombreArchivoXml() {
        return nombreArchivoXml;
    }


    public void setTablaRecibos(RichTable tablaRecibos) {
        this.tablaRecibos = tablaRecibos;
    }

    public RichTable getTablaRecibos() {
        return tablaRecibos;
    }

    public void setPopNoSeleccionop(RichPopup popNoSeleccionop) {
        this.popNoSeleccionop = popNoSeleccionop;
    }

    public RichPopup getPopNoSeleccionop() {
        return popNoSeleccionop;
    }

    public void setCampouuidcomprobante(String campouuidcomprobante) {
        this.campouuidcomprobante = campouuidcomprobante;
    }

    public String getCampouuidcomprobante() {
        return campouuidcomprobante;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setSesionUser(String sesionUser) {
        this.sesionUser = sesionUser;
    }

    public String getSesionUser() {
        return sesionUser;
    }

    public void setPopMail(RichPopup popMail) {
        this.popMail = popMail;
    }

    public RichPopup getPopMail() {
        return popMail;
    }

    public void setCajaEmail(RichInputText cajaEmail) {
        this.cajaEmail = cajaEmail;
    }

    public RichInputText getCajaEmail() {
        return cajaEmail;
    }

    public void enviaMail(ActionEvent actionEvent) {
        sacaArchivoEmail(emailCliente);
        Anexo20 anexo = new Anexo20();
        anexo.cerrarPopUp(popMail);
        emailCliente = null;
        misUUIDS.clear();
        limpiaCorreo();

    }

    public void limpiaCorreo() {
        checkBoxCancelar = new RichSelectBooleanCheckbox();
        cajaEmail = new RichInputText();
        AdfFacesContext.getCurrentInstance().addPartialTarget(checkBoxCancelar);
        AdfFacesContext.getCurrentInstance().addPartialTarget(cajaEmail);
        this.checkBoxCancelar.resetValue();
        this.checkBoxCancelar.setValue(false);
        this.cajaEmail.resetValue();
        this.cajaEmail.setValue("");

    }


    public void sacaArchivoEmail(String correo) {
        List<String> rutasMail = new ArrayList<String>();
        EnviarCorreo enviaCorreo = new EnviarCorreo();
        List<String> uuidl = new ArrayList<String>();
        String userDir;
        String nombre = "";
        List<String> rutasPdf = new ArrayList<String>();
        try {
            if (misUUIDS.size() > 0) {
                if (misUUIDS.size() > 1) {
                    nombre = "Facturas";
                } else {
                    nombre = "" + misUUIDS.get(0);
                }


                tipo = "pdf";
                copiarArchivosXml(misUUIDS);

                userDir = System.getProperty("user.dir");
                String enviarArchivo = userDir + "\\" + "PDF" + "\\" + nombre + ".pdf";
                rutasPdf.add(enviarArchivo);
                for (String xml : misUUIDS) {
                    String enviarXml = userDir + "\\" + "PDF" + "\\" + xml + ".xml";
                    rutasPdf.add(enviarXml);
                }

                File filePdf = new File(enviarArchivo);
                if(user != null)
                    pistasAuditoria.eventosCliente("USUARIO", "Envio de correo PDF y XML ",user.getRfc() , "SELECT");
                

                sacaArchivosPdfBD(misUUIDS, tipo);
                enviaCorreo.enviaEmailSeparadosAux(correo, "Representacion Impresa y XML eQuadrum-cfdi",
                                                   "Estimado Cliente. \n" +
                        "\n\n Adjunto a este mensaje se anexan los comprobantes con las siguientes caracter\u00edsticas:" +
                        "\n\t Representaci\u00f3n impresa: (archivo con extensi\u00f3n .pdf)" +
                        "\n\t Archivo Fuente: (archivo con extensi\u00f3n .xml)" + "\n\n Saludos Cordiales" +
                        "\n\n Quadrum-cfdi" + "\n " + "\n contacto@quadrum.com.mx" +
                        "\n\n Este correo fue generado de forma automatica; por favor, no emita una respuesta al mismo.",
                        rutasMail, rutasPdf);
                ActionEvent evento = new ActionEvent(botonBuscarEmpleado);
                anexo.showPopup(evento, popMailEnviadoBien);
                eliminaArchivos(rutasPdf);
            }

        }

        catch (Exception e) {
            // ActionEvent evento=new ActionEvent(botonBuscar);
            // anexo.showPopup(evento, popAcuse);
            //enviar correo en caso de que truene
            e.printStackTrace();
            try {
                enviaCorreo.enviaEmailSeparados(correo, "Representacion Impresa y XML eQuadrum-cfdi",
                                                "Estimado Cliente," +
                                                "\n\nA continuaci\u00f3n \"Quadrum CFDI\" le hace entrega de los siguientes CFDIs: " +
                                                "\n " + uuidl.toString() +
                                                "\n\n Adjunto a este mensaje se anexan los comprobantes con las siguientes caracter\u00edsticas:" +
                                                "\n\t Representaci\u00f3n impresa: (archivo con extension .pdf)" +
                                                "\n\t Archivo Fuente: (archivo con extensi\u00f3n .xml)" +
                                                "\n\n Saludos Cordiales" + "\n\n Quadrum-cfdi" + "\n " +
                                                "\n contacto@quadrum.com.mx" +
                                                "\n\n Este correo fue generado de forma automatica; por favor, no emita una respuesta al mismo.",
                                                rutasMail, rutasPdf);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    public void eliminaArchivos(List<String> fileRecibido) {
        for (String eliminarArchivo : fileRecibido) {
            File fileDelete = new File(eliminarArchivo);
            if (fileDelete.delete()) {
                System.out.println(fileDelete + " eliminado correctamente");
            }
        }
    }


    public void seleccCheck(ValueChangeEvent valueChangeEvent) {
        String value = valueChangeEvent.getNewValue().toString();
        if (value.equals("true")) {
            try {
                cargaTabla();
                this.listaRecibosTabla.clear();

                if (this.getListaTabla().size() > 0) {

                    for (Recibos r : listRecibos) {
                        fechaCertificacio = r.getFechaCertificacio();
                        fechaEmision = r.getFechaEmision();
                        fechaFinalPago = r.getFechaFinalPago();
                        fechaInicialPago = r.getFechaInicialPago();
                        fechaPago = r.getFechaPago();
                        idrecibos = r.getIdrecibos();
                        noDiasPagados = r.getNoDiasPagados();
                        reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                        totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                        totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                        totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                        totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                        uuid = cifrado.Desencriptar(r.getUuid(), 10);
                        selecc = true;
                        version = "1.2";
                        estatus = listRecibos.get(0).getEstatus();
                        TablaRecibo tabla =
                            new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago,
                                            fechaPago, idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                            totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion,
                                            uuid, selecc, estatus,version);
                        listaTabla.add(tabla);
                    }
                } else {
                }
                //listaTabla.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void cierraPopMail(ActionEvent actionEvent) {

        Anexo20 anexo = new Anexo20();
        limpiaCorreo();
        listaTabla.clear();
        misUUIDS.clear();
        cargaTabla();
        anexo.cerrarPopUp(popMail);

    }

    public void setEmailCliente(String emailCliente) {
        this.emailCliente = emailCliente;
    }

    public String getEmailCliente() {
        return emailCliente;
    }

    public void setPopMailEnviadoBien(RichPopup popMailEnviadoBien) {
        this.popMailEnviadoBien = popMailEnviadoBien;
    }

    public RichPopup getPopMailEnviadoBien() {
        return popMailEnviadoBien;
    }

    public void setBindSelectAll(RichSelectBooleanCheckbox bindSelectAll) {
        this.bindSelectAll = bindSelectAll;
    }

    public RichSelectBooleanCheckbox getBindSelectAll() {
        return bindSelectAll;
    }

    public void setCheckBoxCancelar(RichSelectBooleanCheckbox checkBoxCancelar) {
        this.checkBoxCancelar = checkBoxCancelar;
    }

    public RichSelectBooleanCheckbox getCheckBoxCancelar() {
        return checkBoxCancelar;
    }

    public void setMisUUIDS(List<String> misUUIDS) {
        this.misUUIDS = misUUIDS;
    }

    public List<String> getMisUUIDS() {
        return misUUIDS;
    }


    public void setRfcImagen(String rfcImagen) {
        this.rfcImagen = rfcImagen;
    }

    public String getRfcImagen() {
        return rfcImagen;
    }

    public void setFirstRow(int firstRow) {
        this.firstRow = firstRow;
    }

    public int getFirstRow() {
        return firstRow;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setBindCancelar(RichCommandButton bindCancelar) {
        this.bindCancelar = bindCancelar;
    }

    public RichCommandButton getBindCancelar() {
        return bindCancelar;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdUsuario() {
        return idUsuario;
    }


    public void setArhivoPdf(String ArhivoPdf) {
        this.ArhivoPdf = ArhivoPdf;
    }

    public String getArhivoPdf() {
        return ArhivoPdf;
    }

    public void setBindPdfs(RichCommandButton bindPdfs) {
        this.bindPdfs = bindPdfs;
    }

    public RichCommandButton getBindPdfs() {
        return bindPdfs;
    }

    public void setListUuids(List<TablaRecibo> listUuids) {
        this.listUuids = listUuids;
    }

    public List<TablaRecibo> getListUuids() {
        return listUuids;
    }

    public void setPopAvisoCancela(RichPopup popAvisoCancela) {
        this.popAvisoCancela = popAvisoCancela;
    }

    public RichPopup getPopAvisoCancela() {
        return popAvisoCancela;
    }

    public void aceptaCancelar(DialogEvent dialogEvent) {

    }

    public void setTablaACancelar(RichTable tablaACancelar) {
        this.tablaACancelar = tablaACancelar;
    }

    public RichTable getTablaACancelar() {
        return tablaACancelar;
    }

    public void setBindClick(RichCommandButton bindClick) {
        this.bindClick = bindClick;
    }

    public RichCommandButton getBindClick() {
        return bindClick;
    }

    public void prepararBajar(ActionEvent actionEvent) {

        if (misUUIDS.size() > 0) {
            /**
         * You can add your business logic here to process the
         * user input, and based on the outcome you can decide
         * whether to continue the processing or not.
         */
            FacesContext context = FacesContext.getCurrentInstance();
            ExtendedRenderKitService erks = Service.getService(context.getRenderKit(), ExtendedRenderKitService.class);

            erks.addScript(context, "customHandler();");
            AdfFacesContext.getCurrentInstance().addPartialTarget(tablaRecibos);
            AdfFacesContext.getCurrentInstance().addPartialTarget(checkBoxCancelar);

            checkBoxCancelar.resetValue();
            checkBoxCancelar.setValue(false);
            //this.setFirstRow(0);
            //this.setCurrentPage(1);
            if(user != null)
                pistasAuditoria.eventosCliente("USUARIO", "Descargo mas de un CFDI ",user.getRfc() , "SELECT");
            
            try {
                //Thread.sleep(3000);
                listRecibos.clear();
                listaTabla.clear();

                if (admin != null) {
                    System.out.println("Admin---> first " + firstRow + " " + rowsPerPage);
                    desencriptaTabla(admin, firstRow, rowsPerPage);

                    //listRecibos = ejbRecibos.getAllEmpresa(admin.getIdempresa(), firstRow, rowsPerPage);
                    //listaTabla = new ArrayList<TablaRecibo>();
                    int tamRecibos = listRecibos.size();
                    /* if (tamRecibos != 0) {
                        for (Recibos r : listRecibos) {
                            fechaCertificacio = r.getFechaCertificacio();
                            fechaEmision = r.getFechaEmision();
                            fechaFinalPago = r.getFechaFinalPago();
                            fechaInicialPago = r.getFechaInicialPago();
                            fechaPago = r.getFechaPago();
                            idrecibos = r.getIdrecibos();
                            noDiasPagados = r.getNoDiasPagados();
                            reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                            totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                            totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                            totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                            totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                            uuid = cifrado.Desencriptar(r.getUuid(), 10).toUpperCase();
                            selecc = false;
                            //estatus = listRecibos.get(0).getEstatus();
                            TablaRecibo tabla =
                                new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago,
                                                fechaPago, idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                                totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion,
                                                uuid, selecc,estatus);
                            listaTabla.add(tabla);
                            this.setCurrentPage(1);
                            this.setFirstRow(0);

                        }
                    } */

                } else {
                    System.out.println("User---> first " + firstRow + " " + rowsPerPage);
                    desencriptaTabla(user, firstRow, rowsPerPage);


                    /* listRecibos =
                            ejbRecibos.getAllTrabajador(user.getIdtrabajador(), user.getTipo(), firstRow, rowsPerPage);
                    listaTabla = new ArrayList<TablaRecibo>();
                    int tamRecibos = listRecibos.size();
                    if (tamRecibos != 0) {
                        for (Recibos r : listRecibos) {
                            fechaCertificacio = r.getFechaCertificacio();
                            fechaEmision = r.getFechaEmision();
                            fechaFinalPago = r.getFechaFinalPago();
                            fechaInicialPago = r.getFechaInicialPago();
                            fechaPago = r.getFechaPago();
                            idrecibos = r.getIdrecibos();
                            noDiasPagados = r.getNoDiasPagados();
                            reciboXML = cifrado.Desencriptar(r.getReciboXML(), 10);
                            totalExcentoDeduccion = r.getTotalExcentoDeduccion();
                            totalExcentoPercepcion = r.getTotalExcentoPercepcion();
                            totalGravadoDeduccion = r.getTotalGravadoDeduccion();
                            totalGravadoPercepcion = r.getTotalExcentoPercepcion();
                            uuid = cifrado.Desencriptar(r.getUuid(), 10).toUpperCase();
                            selecc = false;
                            //estatus = listRecibos.get(0).getEstatus();

                            TablaRecibo tabla =
                                new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago,
                                                fechaPago, idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                                totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion,
                                                uuid, selecc,estatus);
                            listaTabla.add(tabla);


                        }
                    } */

                }

            } catch (Exception e) {
            }

            //misUUIDS.clear();
            //refreshPage();
        } else {
            AdfFacesContext.getCurrentInstance().addPartialTarget(checkBoxCancelar);
            FacesContext.getCurrentInstance().addMessage(checkBoxCancelar.getClientId(FacesContext.getCurrentInstance()),
                                                         new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                                                                          "Debe seleccionar al menos un Recibo".toUpperCase()));

        }
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setBindPanelFecha(RichPanelGroupLayout bindPanelFecha) {
        this.bindPanelFecha = bindPanelFecha;
    }

    public RichPanelGroupLayout getBindPanelFecha() {
        return bindPanelFecha;
    }

    public void setParamFecha1(RichInputDate paramFecha1) {
        this.paramFecha1 = paramFecha1;
    }

    public RichInputDate getParamFecha1() {
        return paramFecha1;
    }

    public void setParamFecha2(RichInputDate paramFecha2) {
        this.paramFecha2 = paramFecha2;
    }

    public RichInputDate getParamFecha2() {
        return paramFecha2;
    }

    public void setMiFechaRango1(Date miFechaRango1) {
        this.miFechaRango1 = miFechaRango1;
    }

    public Date getMiFechaRango1() {
        return miFechaRango1;
    }

    public void setMiFechaRango2(Date miFechaRango2) {
        this.miFechaRango2 = miFechaRango2;
    }

    public Date getMiFechaRango2() {
        return miFechaRango2;
    }

    public void validaCorreo(FacesContext facesContext, UIComponent uIComponent, Object object) {
        this.setEmailCliente(String.valueOf(object).trim());
        String regex =
            "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
        Pattern pat = Pattern.compile(regex);
        if (!pat.matcher(this.getEmailCliente()).matches()) {
            this.setEmailCliente(null);
            this.setEmailCliente("");
            throw new ValidatorException(new FacesMessage("La direccion de correo electrónico no cumple el patron correo@dominio.xx".toUpperCase()));
        }
    }

    private void copiarArchivosXml(List<String> listxml) {
        String userDir = System.getProperty("user.dir");
        for (String xml : listxml) {
            File fileXml = new File(RUTA + xml + ".xml");
            File destinoXml = new File(userDir + "\\PDF\\" + xml + ".xml");

            try {
                FileInputStream in = new FileInputStream(fileXml);
                FileOutputStream out = new FileOutputStream(destinoXml);

                int c;
                while ((c = in.read()) != -1)
                    out.write(c);

                in.close();
                out.close();

            } catch (Exception e) {
                System.err.println("Error durante el copiado" + e.getMessage());
            }
        }
    }

    public void cancelarCFDI(ActionEvent action) {
        TablaRecibo seleccionado = (TablaRecibo)tablaRecibos.getSelectedRowData();
        UuidCancela cancela = new UuidCancela();
        Integer codigoCancela = 0;

        System.out.println(seleccionado.getUuid());
        //codigoCancela = Integer.parseInt(cancela.can("SND7701134L0", misUUIDS, "Dns130177"));
        switch (codigoCancela) {
        case 201:
            System.out.println("Cancela correctamente");
            actualizaEstatus(misUUIDS);
            mensajeCancelacion = "Cancela correctamente";
            break;
        case 202:
            System.out.println("Cancelado anteriormente");
            actualizaEstatus(misUUIDS);
            mensajeCancelacion = "Cancelado anteriormente";
            break;
        case 203:
            System.out.println("No corresponde el RFC del Emisor y de quien solicita la cancelación");
            mensajeCancelacion = "No corresponde el RFC del Emisor y de quien solicita la cancelación";
            break;
        case 205:
            System.out.println("UUID no existe");
            mensajeCancelacion = "UUID no existe";
            break;
        case 303:
            System.out.println("Intentelo mas tarde");
            mensajeCancelacion = "Intentelo mas tarde";
            break;
        case 704:
            System.out.println("Error con la contraseña de la llave Privada");
            mensajeCancelacion = "Error con la contraseña de la llave Privada";
            break;
        case 708:
            System.out.println("No se pudo conectar al SAT");
            mensajeCancelacion = "No se pudo conectar al SAT";
            break;
        case 711:
            System.out.println("Error con el certificado al cancelar");
            mensajeCancelacion = "Error con el certificado al cancelar";
            break;

        default:
            System.out.println("No se pudo conectar al SAT");
            mensajeCancelacion = "No se pudo conectar al SAT";
        }
        anexo.showPopup(action, popupAvisoCancela);
        System.out.println(seleccionado.getUuid());
    }


    public boolean actualizaEstatus(List<String> uuid) {
        boolean bandera = false;
        listRecibos.clear();
        for (int i = 0; i < uuid.size(); i++) {
            listRecibos =
                    ejbRecibos.getConsultaRecibosUUID(cifrado.Encriptar(uuid.get(i), 10), this.getIdUsuario(), firstRow,
                                                      rowsPerPage);
            listRecibos.get(0).setEstatus("0");
            ejbRecibos.mergeRecibos(listRecibos.get(0));
            System.out.println(listRecibos.size());
        }


        return bandera;
    }

    public void setBotonCancelacion(RichCommandButton botonCancelacion) {
        this.botonCancelacion = botonCancelacion;
    }

    public RichCommandButton getBotonCancelacion() {
        return botonCancelacion;
    }

    public void setMensajeCancelacion(String mensajeCancelacion) {
        this.mensajeCancelacion = mensajeCancelacion;
    }

    public String getMensajeCancelacion() {
        return mensajeCancelacion;
    }

    public void setPopupAvisoCancela(RichPopup popupAvisoCancela) {
        this.popupAvisoCancela = popupAvisoCancela;
    }

    public RichPopup getPopupAvisoCancela() {
        return popupAvisoCancela;
    }

    public void dialogCancelar(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {

        }
    }


    public void setCheckBoxCancela(RichSelectBooleanCheckbox checkBoxCancela) {
        this.checkBoxCancela = checkBoxCancela;
    }

    public RichSelectBooleanCheckbox getCheckBoxCancela() {
        return checkBoxCancela;
    }

    public void listenerCancelar(ValueChangeEvent valueChangeEvent) {
        if ((Boolean)valueChangeEvent.getNewValue()) {
            TablaRecibo seleccionados = (TablaRecibo)tablaRecibos.getSelectedRowData();
            misUUIDS.add(seleccionados.getUuid());
            System.out.println("Size " + misUUIDS.size());
            listaRecibosTabla.add(seleccionados);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindPdfs);
            this.getBindPdfs().setDisabled(false);
        } else {
            TablaRecibo seleccionados = (TablaRecibo)tablaRecibos.getSelectedRowData();
            if (misUUIDS.size() > 0) {
                misUUIDS.remove(seleccionados.getUuid());
                System.out.println("Size " + misUUIDS.size());
            } else if (misUUIDS.size() == 0) {
                this.getBindPdfs().setDisabled(true);
            }
        }
    }

    public void setColumnaEstatus(RichColumn columnaEstatus) {
        this.columnaEstatus = columnaEstatus;
    }

    public RichColumn getColumnaEstatus() {
        return columnaEstatus;
    }

    public void setColumnaCancela(RichColumn columnaCancela) {
        this.columnaCancela = columnaCancela;
    }

    public RichColumn getColumnaCancela() {
        return columnaCancela;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
