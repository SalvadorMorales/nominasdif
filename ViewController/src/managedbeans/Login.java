package managedbeans;

import classjava.EnviarCorreo;
import entities.Empresa;
import entities.Permisos;
import entities.Trabajador;
import interfaces.SessionEmpresaRemote;
import interfaces.SessionPermisosRemote;
import interfaces.SessionTrabajadorRemote;
import interfaces.SessionUsuariosRemote;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.context.AdfFacesContext;
import org.apache.commons.codec.DecoderException;
import org.apache.myfaces.trinidad.event.AttributeChangeEvent;
import servicelocator.MyServiceLocator;
import util.CifradoCesar;
import util.MensajesPop;

public class Login
{
  private String usuario;
  private String password;
  private RichCommandButton botonAcceso;
  private RichPopup popCredencialesIncorrectas;
  private CifradoCesar cifrar;
  private Trabajador user;
  private Empresa empresa;
  private SessionUsuariosRemote ejbUsuario;
  private SessionTrabajadorRemote ejbTrabajador;
  private SessionEmpresaRemote ejbEmpresa;
  private Permisos permiso;
  private SessionPermisosRemote ejbPermisos;
  private Permisos permisos;
  private RichShowDetailItem pestania2;
  private String correoElectronico;
  private boolean flag = false;
  private RichCommandLink linkRecibos = new RichCommandLink();
  private RichCommandLink linkEmpleados = new RichCommandLink();
  private RichCommandLink linkSucursales = new RichCommandLink();
  private RichCommandLink linkCorreo = new RichCommandLink();
  private RichCommandLink linkDisenio = new RichCommandLink();
  private RichCommandLink linkRoles = new RichCommandLink();
  private RichInputText bindCorreo;
  private String curp;
  private String rfcImagen;
  private MensajesPop mensPop = null;
  private RichPopup popOk = new RichPopup();
  private RichCommandButton binbBotonAceptar;
  private String mensajeCorreo;
  private RichInputText bindSeguroSocial;
  private RichColumn columnaEstatus = new RichColumn();
  private RichColumn columnaCancela = new RichColumn();
  
  public Login()
  {
    this.ejbUsuario = MyServiceLocator.getUsuariosRemote();
    this.ejbTrabajador = MyServiceLocator.getTrabajadorRemote();
    this.ejbEmpresa = MyServiceLocator.getEmpresaRemote();
    this.ejbPermisos = MyServiceLocator.getPermisosRemote();
    this.cifrar = new CifradoCesar();
  }
  
  public String logueaUsuario()
  {
    if ((getUsuario() != null) && (getPassword() != null))
    {
      if ((!getUsuario().trim().equals("")) || (!getPassword().trim().equals("")))
      {
        this.user = new Trabajador();
        this.empresa = new Empresa();
        this.empresa = this.ejbEmpresa.getEmpresaFindEmpresa(getUsuario(), getPassword());
        System.out.println("User--- " + CifradoCesar.Encriptar(getUsuario(), 10));
        System.out.println("Pass--- " + CifradoCesar.Encriptar(getPassword(), 10));
        String var1 = CifradoCesar.Encriptar(getPassword(), 10);
        
        this.user = this.ejbTrabajador.getFindUsuario(CifradoCesar.Encriptar(getUsuario(), 10), CifradoCesar.Encriptar(getPassword(), 10));
        if (this.empresa != null)
        {
          sessionAdmin();
          obtienePermisos(this.empresa);
          
          setRfcImagen(this.empresa.getRfc());
          setUsuario(null);
          setPassword(null);
          return "/paginaPrincipal.jspx";
        }
        if (this.user != null)
        {
          sessionUser();
          obtienePermisos(this.user);
          setRfcImagen(this.user.getEmpresa1().getRfc());
          setUsuario(null);
          setPassword(null);
          return "/paginaPrincipal.jspx";
        }
        setUsuario(null);
        setPassword(null);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Usuario no registrado.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
      }
      else
      {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Los campos no pueden ir en blanco.");
        
        FacesContext.getCurrentInstance().addMessage(null, msg);
      }
    }
    else
    {
      FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Los campos no pueden ir en blanco.");
      
      FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    return null;
  }
  
  public void sessionUser()
  {
    FacesContext ctx = FacesContext.getCurrentInstance();
    HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
    HttpSession session = req.getSession(true);
    session.setAttribute("usuario", this.user);
  }
  
  public void sessionAdmin()
  {
    FacesContext ctx = FacesContext.getCurrentInstance();
    HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
    HttpSession session = req.getSession(true);
    session.setAttribute("admin", this.empresa);
  }
  
  public void obtienePermisos(Object objeto)
  {
    this.empresa = new Empresa();
    this.user = new Trabajador();
    this.permiso = new Permisos();
    String obj = objeto.getClass().toString();
    if (!obj.equals("class entities.Trabajador"))
    {
      this.empresa = ((Empresa)objeto);
      if (this.ejbPermisos.getFindPermisosEmpresa(this.empresa.getRfc()) != null)
      {
        this.permiso = this.ejbPermisos.getFindPermisosEmpresa(this.empresa.getRfc());
        if (this.permiso != null)
        {
          funciones(this.permiso);
          sessionPermiso();
        }
      }
    }
    else
    {
      this.user = ((Trabajador)objeto);
      System.out.println("user " + this.user.getUsuario());
      this.permiso = this.ejbPermisos.getFindPermisoTrabajadorSesion(this.user.getUsuario());
      funciones(this.permiso);
    }
  }
  
  public void sessionPermiso()
  {
    FacesContext ctx = FacesContext.getCurrentInstance();
    HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
    HttpSession session = req.getSession(true);
    session.setAttribute("permiso", this.permiso);
  }
  
  public void funciones(Object obj)
  {
    adfPart();
    if (obj != null)
    {
      Permisos permiso = (Permisos)obj;
      getLinkCorreo().setVisible(permiso.getCorreo());
      getLinkDisenio().setVisible(permiso.getDisenio());
      getLinkEmpleados().setVisible(permiso.getEmpleados());
      getLinkRecibos().setVisible(permiso.getRecibos());
      getLinkRoles().setVisible(permiso.getRoles());
      getLinkSucursales().setVisible(permiso.getSucursales());
    }
    else
    {
      getLinkCorreo().setVisible(false);
      getLinkDisenio().setVisible(false);
      getLinkEmpleados().setVisible(false);
      getLinkRecibos().setVisible(false);
      getLinkRoles().setVisible(false);
      getLinkSucursales().setVisible(false);
    }
  }
  
  public void adfPart()
  {
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.linkCorreo);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.linkDisenio);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.linkEmpleados);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.linkRecibos);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.linkRoles);
    AdfFacesContext.getCurrentInstance().addPartialTarget(this.linkSucursales);
  }
  
  public void validarCorreo(FacesContext facesContext, UIComponent uIComponent, Object object)
    throws DecoderException, Exception
  {
    setCorreoElectronico(String.valueOf(object).trim());
    String regex = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
    
    Pattern pat = Pattern.compile(regex);
    if (!pat.matcher(getCorreoElectronico()).matches())
    {
      setCorreoElectronico(null);
      setCorreoElectronico("");
      throw new ValidatorException(new FacesMessage("La direccion de correo electronico no cumple el patron correo@dominio.xx".toUpperCase()));
    }
  }
  
  public void OlvidoContrasenia(ActionEvent event)
  {
    ActionEvent action = new ActionEvent(this.binbBotonAceptar);
    this.mensPop = new MensajesPop();
    Trabajador trabajador = null;
    if ((getCorreoElectronico() != null) && (getCurp() != null))
    {
      if ((!getCorreoElectronico().equals("")) && (!getCurp().equals("")))
      {
        trabajador = this.ejbTrabajador.getFindCurp(CifradoCesar.Encriptar(this.curp, 10));
        if (trabajador != null)
        {
          mandaCorreo(getCorreoElectronico(), trabajador);
          AdfFacesContext.getCurrentInstance().addPartialTarget(getBindCorreo());
          AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSeguroSocial());
          getBindCorreo().resetValue();
          getBindCorreo().setValue("");
          getBindSeguroSocial().resetValue();
          getBindSeguroSocial().setValue("");
          this.mensPop.showPopup(event, this.popOk);
          this.mensajeCorreo = "jjjj";
        }
        else
        {
          FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Usuario no registrado.");
          FacesContext.getCurrentInstance().addMessage(null, msg);
        }
      }
      else
      {
        FacesContext.getCurrentInstance().addMessage(this.bindCorreo.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "El correo electr��nico no puede ir en blanco".toUpperCase()));
      }
    }
    else {
      FacesContext.getCurrentInstance().addMessage(this.bindCorreo.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "El correo electr��nico no puede ir en blanco".toUpperCase()));
    }
  }
  
  public void mandaCorreo(String correo, Trabajador trabajador)
  {
    EnviarCorreo enviaCorreo = new EnviarCorreo();
    String passTemporal = enviaCorreo.generaPassword();
    trabajador.setPassword(CifradoCesar.Encriptar(passTemporal, 10));
    this.ejbTrabajador.mergeTrabajador(trabajador);
    try
    {
      enviaCorreo.enviaEmails(correo, "Recuperacion de contrasenia.", "Estimado Cliente,\n\nA continuaci��n \"Quadrum CFDI\" le hace entrega de su Contrase��a: \n\nUsuario: " + CifradoCesar.Desencriptar(trabajador.getUsuario(), 10) + "\n\n" + "\n\nContrase��a: " + passTemporal + "\n\n" + "\n\n Saludos Cordiales" + "\n\n Quadrum-cfdi" + "\n " + "\n contacto@quadrum.com.mx" + "\n\n Este correo fue generado de forma automatica; por favor, no emita una respuesta al mismo.");
    }
    catch (MessagingException e)
    {
      e.printStackTrace();
    }
  }
  
  public boolean actualizaPass(Trabajador trabajador)
  {
    return true;
  }
  
  public String realizarAltaTrabajador()
  {
    return "/Trabajador/trabajadorAlta.jspx";
  }
  
  public String RecuperaPass()
  {
    return "/Password/cambioContrasenia.jspx";
  }
  
  public void setUsuario(String usuario)
  {
    this.usuario = usuario;
  }
  
  public String getUsuario()
  {
    return this.usuario;
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setBotonAcceso(RichCommandButton botonAcceso)
  {
    this.botonAcceso = botonAcceso;
  }
  
  public RichCommandButton getBotonAcceso()
  {
    return this.botonAcceso;
  }
  
  public void setPopCredencialesIncorrectas(RichPopup popCredencialesIncorrectas)
  {
    this.popCredencialesIncorrectas = popCredencialesIncorrectas;
  }
  
  public RichPopup getPopCredencialesIncorrectas()
  {
    return this.popCredencialesIncorrectas;
  }
  
  public void setPermiso(Permisos permiso)
  {
    this.permiso = permiso;
  }
  
  public Permisos getPermiso()
  {
    return this.permiso;
  }
  
  public void setPestania2(RichShowDetailItem pestania2)
  {
    this.pestania2 = pestania2;
  }
  
  public RichShowDetailItem getPestania2()
  {
    return this.pestania2;
  }
  
  public void setLinkRecibos(RichCommandLink linkRecibos)
  {
    this.linkRecibos = linkRecibos;
  }
  
  public RichCommandLink getLinkRecibos()
  {
    return this.linkRecibos;
  }
  
  public void setLinkEmpleados(RichCommandLink linkEmpleados)
  {
    this.linkEmpleados = linkEmpleados;
  }
  
  public RichCommandLink getLinkEmpleados()
  {
    return this.linkEmpleados;
  }
  
  public void setLinkSucursales(RichCommandLink linkSucursales)
  {
    this.linkSucursales = linkSucursales;
  }
  
  public RichCommandLink getLinkSucursales()
  {
    return this.linkSucursales;
  }
  
  public void setLinkCorreo(RichCommandLink linkCorreo)
  {
    this.linkCorreo = linkCorreo;
  }
  
  public RichCommandLink getLinkCorreo()
  {
    return this.linkCorreo;
  }
  
  public void setLinkDisenio(RichCommandLink linkDisenio)
  {
    this.linkDisenio = linkDisenio;
  }
  
  public RichCommandLink getLinkDisenio()
  {
    return this.linkDisenio;
  }
  
  public void setLinkRoles(RichCommandLink linkRoles)
  {
    this.linkRoles = linkRoles;
  }
  
  public RichCommandLink getLinkRoles()
  {
    return this.linkRoles;
  }
  
  public void setCorreoElectronico(String correoElectronico)
  {
    this.correoElectronico = correoElectronico;
  }
  
  public String getCorreoElectronico()
  {
    return this.correoElectronico;
  }
  
  public void setBindCorreo(RichInputText bindCorreo)
  {
    this.bindCorreo = bindCorreo;
  }
  
  public RichInputText getBindCorreo()
  {
    return this.bindCorreo;
  }
  
  public void setCurp(String curp)
  {
    this.curp = curp;
  }
  
  public String getCurp()
  {
    return this.curp;
  }
  
  public void validarLongitud(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String patronAux = "^[A-Z]{4}[0-9]{6}[H,M][A-Z]{5}[0-9]{2}$";
    Pattern pat = Pattern.compile(patronAux);
    String rfc = (String)object;
    if (!pat.matcher(rfc).matches()) {
      FacesContext.getCurrentInstance().addMessage(uIComponent.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "El CURP no cumple con un patron valido".toUpperCase()));
    }
  }
  
  public void popMailBien(AttributeChangeEvent attributeChangeEvent) {}
  
  public void setPopOk(RichPopup popOk)
  {
    this.popOk = popOk;
  }
  
  public RichPopup getPopOk()
  {
    return this.popOk;
  }
  
  public void setBinbBotonAceptar(RichCommandButton binbBotonAceptar)
  {
    this.binbBotonAceptar = binbBotonAceptar;
  }
  
  public RichCommandButton getBinbBotonAceptar()
  {
    return this.binbBotonAceptar;
  }
  
  public String cancelaPass()
  {
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindCorreo());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSeguroSocial());
    getBindCorreo().resetValue();
    getBindCorreo().setValue("");
    getBindSeguroSocial().resetValue();
    getBindSeguroSocial().setValue("");
    return "/inicioSesion.jspx";
  }
  
  public void setMensajeCorreo(String mensajeCorreo)
  {
    this.mensajeCorreo = mensajeCorreo;
  }
  
  public String getMensajeCorreo()
  {
    return this.mensajeCorreo;
  }
  
  public void setBindSeguroSocial(RichInputText bindSeguroSocial)
  {
    this.bindSeguroSocial = bindSeguroSocial;
  }
  
  public RichInputText getBindSeguroSocial()
  {
    return this.bindSeguroSocial;
  }
  
  public String cambioPass()
  {
    return "/Password/cambioPass.jspx";
  }
  
  public void setRfcImagen(String rfcImagen)
  {
    this.rfcImagen = rfcImagen;
  }
  
  public String getRfcImagen()
  {
    return this.rfcImagen;
  }
  
  public String paginaSucursales()
  {
    return "/Sucursal/sucursal.jspx";
  }
  
  public String getPaginaRecibos()
  {
    return "/Trabajador/AdminRecibos.jspx";
  }
  
  public String getPaginaNomina()
  {
    return "/paginaPrincipal";
  }
  
  public String regresaRoles()
  {
    return "/Roles/roles.jspx";
  }
}
