package managedbeans;

import classjava.EnviarCorreo;
import entities.Empresa;
import entities.Sucursal;
import entities.Trabajador;
import interfaces.SessionEmpresaRemote;
import interfaces.SessionSucursalRemote;
import interfaces.SessionTrabajadorRemote;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.mail.MessagingException;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichGoButton;
import servicelocator.MyServiceLocator;
import util.CifradoCesar;

public class altaTrabajadorClass
{
  private RichPanelGroupLayout altatrabajador;
  private int idtrabajador;
  private String nombre;
  private String apellidoP;
  private String apellidoM;
  private String rfc;
  private String curp;
  private String numSeguridadSocial;
  private String departamento;
  private String puesto;
  private String banco;
  private String noCuenta;
  private String clave;
  private String antiguedad;
  private Date fechaIniciRelacion;
  private String tipoContrato;
  private String tipoJornada;
  private BigDecimal salarioDiarioInt;
  private String usuario;
  private String password;
  private String password2;
  private String registroPatronal;
  private String correo;
  private String estatus;
  private String tipo;
  private int sucursal_idsucursal;
  private int empresa_idempresa;
  private String fecha;
  private SessionTrabajadorRemote ejbTrabajador;
  private SessionEmpresaRemote ejbEmpresa;
  private SessionSucursalRemote ejbSucursal;
  private CifradoCesar cifrar;
  private String empresaSeleccionada;
  private String sucursalSeleccionada;
  private List<SelectItem> listSucursal;
  private List<SelectItem> listEmpresa;
  List<SelectItem> sucursales = new ArrayList();
  private RichGoButton generarAltaTrabajador;
  private RichInputText beanUsuario;
  
  public altaTrabajadorClass()
  {
    this.ejbTrabajador = MyServiceLocator.getTrabajadorRemote();
    this.ejbEmpresa = MyServiceLocator.getEmpresaRemote();
    this.ejbSucursal = MyServiceLocator.getSucursalRemote();
    this.cifrar = new CifradoCesar();
    this.listEmpresa = llenarListaEmpresa();
  }
  
  public String registraTrabajador()
  {
    String link = null;
    if (camposLlenos())
    {
      if (validarContrasenia())
      {
        if (noExisteUsuario())
        {
          link = crearUsuario();
        }
        else
        {
          FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "No se puede dar  alta el registro ya existe un usuario con el mismo n��mero de seguridad social");
          FacesContext.getCurrentInstance().addMessage(null, msg);
          LimpiarCampos();
        }
      }
      else
      {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Las contrasenias deben ser iguales para poder darlo de Alta.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
      }
    }
    else
    {
      FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Falta informacinin para poder dar de alta al trabajador.");
      FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    return link;
  }
  
  public void LimpiarCampos()
  {
    setNombre(null);
    setApellidoP(null);
    setApellidoM(null);
    setRfc(null);
    setCurp(null);
    setNumSeguridadSocial(null);
    setPassword(null);
    setPassword2(null);
    setCorreo(null);
  }
  
  public boolean camposLlenos()
  {
    boolean composEstanLlenos = true;
    try
    {
      if ((getNombre().isEmpty()) || (getApellidoP().isEmpty()) || (getApellidoM().isEmpty()) || (getRfc().isEmpty()) || (getCurp().isEmpty()) || (getNumSeguridadSocial().isEmpty()) || (getPassword().isEmpty()) || (getCorreo().isEmpty()) || (getEmpresaSeleccionada().isEmpty()) || (getSucursalSeleccionada().isEmpty())) {
        composEstanLlenos = false;
      }
    }
    catch (Exception e)
    {
      composEstanLlenos = false;
    }
    return composEstanLlenos;
  }
  
  public List<SelectItem> llenarListaEmpresa()
  {
    List<SelectItem> empresas = new ArrayList();
    List<Empresa> list = this.ejbEmpresa.getEmpresaFindAll();
    for (Empresa e : list)
    {
      SelectItem var = new SelectItem();
      var.setLabel(CifradoCesar.Desencriptar(e.getRazonSocial(), 10));
      var.setValue(CifradoCesar.Desencriptar(e.getRazonSocial(), 10));
      empresas.add(var);
    }
    return empresas;
  }
  
  public List<SelectItem> llenarListaSucursal(String rfc)
  {
    List<Sucursal> list = new ArrayList();
    this.sucursales.clear();
    list = this.ejbSucursal.getSucursalFindAllxE(rfc);
    for (Sucursal e : list)
    {
      SelectItem var = new SelectItem();
      var.setLabel(CifradoCesar.Desencriptar(e.getRazonSocial(), 10));
      var.setValue(CifradoCesar.Desencriptar(e.getRazonSocial(), 10));
      this.sucursales.add(var);
    }
    return this.sucursales;
  }
  
  public String crearUsuario()
  {
    Trabajador trabajador = new Trabajador();
    Empresa empresa = new Empresa();
    Sucursal sucursal = new Sucursal();
    
    System.out.println("--Z " + getSucursalSeleccionada());
    System.out.println("... " + CifradoCesar.Encriptar(getSucursalSeleccionada(), 10));
    empresa = this.ejbEmpresa.getFindRazonSocial(CifradoCesar.Encriptar(getEmpresaSeleccionada(), 10));
    sucursal = this.ejbSucursal.getFindRazonSocial(CifradoCesar.Encriptar(getSucursalSeleccionada(), 10));
    setUsuario(getCurp());
    
    setTipo("0");
    setEstatus("1");
    verValoresTabla();
    String mail = getCorreo();
    
    trabajador.setNombre(CifradoCesar.Encriptar(getNombre(), 10));
    trabajador.setApellidos(CifradoCesar.Encriptar(getApellidoP() + "   " + getApellidoM(), 10));
    trabajador.setRfc(CifradoCesar.Encriptar(getRfc(), 10));
    trabajador.setCurp(CifradoCesar.Encriptar(getCurp(), 10));
    trabajador.setNumSeguridadSocial(CifradoCesar.Encriptar(getNumSeguridadSocial(), 10));
    trabajador.setDepartamento(getDepartamento());
    trabajador.setPuesto(getPuesto());
    trabajador.setBanco(getBanco());
    trabajador.setNoCuenta(getNoCuenta());
    trabajador.setClabe(getClave());
    trabajador.setAntiguedad(getAntiguedad());
    trabajador.setFechaInicioRelacion(getFechaIniciRelacion());
    trabajador.setTipoContrato(getTipoContrato());
    trabajador.setTipoJornada(getTipoJornada());
    trabajador.setSalarioDiarioInt(getSalarioDiarioInt());
    trabajador.setUsuario(CifradoCesar.Encriptar(getUsuario(), 10));
    trabajador.setPassword(CifradoCesar.Encriptar(getPassword(), 10));
    trabajador.setRegistroPatronal(getRegistroPatronal());
    trabajador.setSucursal1(sucursal);
    trabajador.setEmpresa1(empresa);
    trabajador.setCorreo(CifradoCesar.Encriptar(getCorreo(), 10));
    trabajador.setEstatus(getEstatus());
    trabajador.setTipo(getTipo());
    
    this.ejbTrabajador.persistTrabajador(trabajador);
    
    enviarCorreo(CifradoCesar.Desencriptar(trabajador.getCorreo(), 10), CifradoCesar.Desencriptar(trabajador.getCurp(), 10), CifradoCesar.Desencriptar(trabajador.getNombre(), 10), CifradoCesar.Desencriptar(trabajador.getPassword(), 10));
    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Trabajador dado de Alta. Se ha enviado un correo con los elementos de autenticacion al Sistema.");
    FacesContext.getCurrentInstance().addMessage(null, msg);
    LimpiarCampos();
    return "/inicioSesion.jspx";
  }
  
  public void enviarCorreo(String correo, String CURP, String nombre, String contrasenia)
  {
    EnviarCorreo enviaCorreo = new EnviarCorreo();
    try
    {
      String asunto = "Hola " + nombre + ", Su registro se realizo con Exito! ?";
      String mensaje = " Sus datos de autenticacion son:\n\n Usuario =  " + CURP + "\n Contrasenia = " + contrasenia + "\n\n Utilice esta informacion para poder acceder al portal Consulta Empleado. \n\nSaludos";
      enviaCorreo.enviaEmailAltaTrabajador(correo, asunto, mensaje);
    }
    catch (MessagingException e)
    {
      System.out.println("log : " + e.toString());
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  public void verValoresTabla() {}
  
  public boolean noExisteUsuario()
  {
    System.out.println("== Validad si Existe Usuario == Num Seguro Social: " + getNumSeguridadSocial());
    boolean noExiste = true;
    if (this.ejbTrabajador.getFindNumSC(CifradoCesar.Encriptar(getNumSeguridadSocial(), 10)) != null) {
      noExiste = false;
    }
    return noExiste;
  }
  
  public boolean validarContrasenia()
  {
    System.out.println("== Validad Contrase��a ==");
    boolean iguales = false;
    try
    {
      if ((getPassword().equals(getPassword2())) && (!getPassword().isEmpty())) {
        iguales = true;
      }
    }
    catch (Exception e) {}
    return iguales;
  }
  
  public Date getFechaIniciRelacion()
  {
    SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
    try
    {
      this.fechaIniciRelacion = ft.parse(this.fecha);
    }
    catch (Exception e)
    {
      System.out.println("Error de lectura de Fecha");
    }
    return this.fechaIniciRelacion;
  }
  
  public void empresaChangeListener(ValueChangeEvent valueChangeEvent)
  {
    try
    {
      this.empresaSeleccionada = ((String)valueChangeEvent.getNewValue());
      System.out.println("Empresa seleccionada  :: " + this.empresaSeleccionada);
      Empresa empresa = new Empresa();
      empresa = this.ejbEmpresa.getFindRazonSocial(CifradoCesar.Encriptar(this.empresaSeleccionada, 10));
      this.listSucursal = llenarListaSucursal(empresa.getRfc());
      System.out.println("bbb " + ((SelectItem)this.listSucursal.get(0)).getLabel());
    }
    catch (Exception e) {}
  }
  
  public void sucursalChangeListener(ValueChangeEvent valueChangeEvent)
  {
    this.sucursalSeleccionada = ((String)valueChangeEvent.getNewValue());
    System.out.println(" SUCURSAL SELECCIONADA " + this.sucursalSeleccionada);
  }
  
  public String cancelarAltaTrabajador()
  {
    LimpiarCampos();
    return "/inicioSesion.jspx";
  }
  
  public String crearAltaTrabajadorLink()
  {
    return "/trabajadorAlta.jspx";
  }
  
  public String adminTrabajadorLink()
  {
    return "/Trabajador/AdminTrabajador.jspx";
  }
  
  public String logoEmpresaLink()
  {
    return "/Trabajador/logoEmpresa.jspx";
  }
  
  public void setSucursal_idsucursal(int idsucursal)
  {
    Sucursal obj = null;
    
    this.sucursal_idsucursal = obj.getIdsucursales();
  }
  
  public void setEmpresa_idempresa(int idempresa)
  {
    Empresa obj = null;
    obj = this.ejbEmpresa.getEmpresa(idempresa);
    System.out.println("ID : " + obj.getIdempresa());
    this.empresa_idempresa = obj.getIdempresa();
  }
  
  public void setTipoContrato(String tipoContrato)
  {
    this.tipoContrato = tipoContrato;
  }
  
  public String getTipoContrato()
  {
    return this.tipoContrato;
  }
  
  public void setTipoJornada(String tipoJornada)
  {
    this.tipoJornada = tipoJornada;
  }
  
  public String getTipoJornada()
  {
    return this.tipoJornada;
  }
  
  public void setUsuario(String usuario)
  {
    this.usuario = getCurp();
  }
  
  public String getUsuario()
  {
    this.usuario = getCurp();
    return this.usuario;
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setPassword2(String password2)
  {
    this.password2 = password2;
  }
  
  public String getPassword2()
  {
    return this.password2;
  }
  
  public void setRegistroPatronal(String registroPatronal)
  {
    this.registroPatronal = registroPatronal;
  }
  
  public String getRegistroPatronal()
  {
    return this.registroPatronal;
  }
  
  public void setFecha(String fecha)
  {
    this.fecha = fecha;
  }
  
  public String getFecha()
  {
    return this.fecha;
  }
  
  public void setSalarioDiarioInt(BigDecimal salarioDiarioInt)
  {
    this.salarioDiarioInt = salarioDiarioInt;
  }
  
  public BigDecimal getSalarioDiarioInt()
  {
    return this.salarioDiarioInt;
  }
  
  public int getSucursal_idsucursal()
  {
    return this.sucursal_idsucursal;
  }
  
  public int getEmpresa_idempresa()
  {
    return this.empresa_idempresa;
  }
  
  public void setEmpresaSeleccionada(String empresaSeleccionada)
  {
    this.empresaSeleccionada = empresaSeleccionada;
  }
  
  public String getEmpresaSeleccionada()
  {
    return this.empresaSeleccionada;
  }
  
  public void setListEmpresa(List<SelectItem> listEmpresa)
  {
    this.listEmpresa = listEmpresa;
  }
  
  public List<SelectItem> getListEmpresa()
  {
    return this.listEmpresa;
  }
  
  public void setListSucursal(List<SelectItem> listSucursal)
  {
    this.listSucursal = listSucursal;
  }
  
  public List<SelectItem> getListSucursal()
  {
    return this.listSucursal;
  }
  
  public void setCorreo(String correo)
  {
    this.correo = correo;
  }
  
  public String getCorreo()
  {
    return this.correo;
  }
  
  public void setSucursalSeleccionada(String sucursalSeleccionada)
  {
    this.sucursalSeleccionada = sucursalSeleccionada;
  }
  
  public String getSucursalSeleccionada()
  {
    return this.sucursalSeleccionada;
  }
  
  public void setAltatrabajador(RichPanelGroupLayout altatrabajador)
  {
    this.altatrabajador = altatrabajador;
  }
  
  public RichPanelGroupLayout getAltatrabajador()
  {
    return this.altatrabajador;
  }
  
  public void setIdtrabajador(int idtrabajador)
  {
    this.idtrabajador = idtrabajador;
  }
  
  public int getIdtrabajador()
  {
    return this.idtrabajador;
  }
  
  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }
  
  public String getNombre()
  {
    return this.nombre;
  }
  
  public void setApellidoP(String apellidoP)
  {
    this.apellidoP = apellidoP;
  }
  
  public String getApellidoP()
  {
    return this.apellidoP;
  }
  
  public void setApellidoM(String apellidoM)
  {
    this.apellidoM = apellidoM;
  }
  
  public String getApellidoM()
  {
    return this.apellidoM;
  }
  
  public void setRfc(String rfc)
  {
    this.rfc = rfc;
  }
  
  public String getRfc()
  {
    return this.rfc;
  }
  
  public void setCurp(String curp)
  {
    this.curp = curp;
    setUsuario(this.curp);
  }
  
  public String getCurp()
  {
    return this.curp;
  }
  
  public void setNumSeguridadSocial(String numSeguridadSocial)
  {
    this.numSeguridadSocial = numSeguridadSocial;
  }
  
  public String getNumSeguridadSocial()
  {
    return this.numSeguridadSocial;
  }
  
  public void setDepartamento(String departamento)
  {
    this.departamento = departamento;
  }
  
  public String getDepartamento()
  {
    return this.departamento;
  }
  
  public void setPuesto(String puesto)
  {
    this.puesto = puesto;
  }
  
  public String getPuesto()
  {
    return this.puesto;
  }
  
  public void setBanco(String banco)
  {
    this.banco = banco;
  }
  
  public String getBanco()
  {
    return this.banco;
  }
  
  public void setNoCuenta(String noCuenta)
  {
    this.noCuenta = noCuenta;
  }
  
  public String getNoCuenta()
  {
    return this.noCuenta;
  }
  
  public void setClave(String clave)
  {
    this.clave = clave;
  }
  
  public String getClave()
  {
    return this.clave;
  }
  
  public void setAntiguedad(String antiguedad)
  {
    this.antiguedad = antiguedad;
  }
  
  public String getAntiguedad()
  {
    return this.antiguedad;
  }
  
  public void setFechaIniciRelacion(String fechaIniciRelacion) {}
  
  public void setGenerarAltaTrabajador(RichGoButton generarAltaTrabajador)
  {
    this.generarAltaTrabajador = generarAltaTrabajador;
  }
  
  public void copyNSCtoUser(ValueChangeEvent valueChangeEvent)
  {
    System.out.println("Entro asignar Usuario  " + getNumSeguridadSocial());
    setUsuario(getCurp());
  }
  
  public void setEstatus(String status)
  {
    this.estatus = status;
  }
  
  public String getEstatus()
  {
    return this.estatus;
  }
  
  public void setTipo(String tipo)
  {
    this.tipo = tipo;
  }
  
  public String getTipo()
  {
    return this.tipo;
  }
  
  public void setBeanUsuario(RichInputText beanUsuario)
  {
    this.beanUsuario = beanUsuario;
  }
  
  public RichInputText getBeanUsuario()
  {
    return this.beanUsuario;
  }
  
  public void actualizaUsuario(ValueChangeEvent valueChangeEvent)
  {
    this.beanUsuario.setValue(getCurp());
  }
}
