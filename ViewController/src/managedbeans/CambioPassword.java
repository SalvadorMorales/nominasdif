package managedbeans;

import classjava.EnviarCorreo;
import entities.Trabajador;
import interfaces.SessionTrabajadorRemote;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.mail.MessagingException;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import org.apache.commons.codec.DecoderException;
import servicelocator.MyServiceLocator;
import util.CifradoCesar;
import util.MensajesPop;

public class CambioPassword
{
  private String noSeguroSocial;
  private String correo;
  private String contrasenia;
  private String pass1;
  private String pass2;
  private RichInputText bindSeguroSocial = new RichInputText();
  private RichInputText bindPassword = new RichInputText();
  private RichInputText bindpass1 = new RichInputText();
  private RichInputText bindpass2 = new RichInputText();
  private Trabajador trabajador;
  private SessionTrabajadorRemote ejbTrabajador;
  private CifradoCesar cifrado;
  private RichInputText bindCorreo = new RichInputText();
  private RichPopup popOk;
  private MensajesPop mensPop;
  
  public CambioPassword()
  {
    this.ejbTrabajador = MyServiceLocator.getTrabajadorRemote();
  }
  
  public void enviarContrasenia(ActionEvent actionEvent)
  {
    this.trabajador = new Trabajador();
    this.mensPop = new MensajesPop();
    if ((getNoSeguroSocial() != null) && (getCorreo() != null) && (getContrasenia() != null) && (getPass1() != null) && (getPass2() != null))
    {
      if ((!getNoSeguroSocial().trim().equals("")) || (!getCorreo().trim().equals("")) || (!getContrasenia().trim().equals("")) || (!getPass1().trim().equals("")) || (!getPass2().trim().equals("")))
      {
        this.trabajador = this.ejbTrabajador.getFindUsuario(CifradoCesar.Encriptar(this.noSeguroSocial, 10));
        if (this.trabajador != null)
        {
          if (this.trabajador.getPassword().equals(CifradoCesar.Encriptar(this.contrasenia, 10)))
          {
            if (this.pass1.compareTo(this.pass2) == 0)
            {
              mandaCorreo(this.correo, this.trabajador, this.pass1);
              limpiaVariables();
              this.mensPop.showPopup(actionEvent, this.popOk);
            }
            else
            {
              FacesContext.getCurrentInstance().addMessage(this.bindpass1.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Las contrasenias nuevas no son iguales. Intente de nuevo".toUpperCase()));
            }
          }
          else {
            FacesContext.getCurrentInstance().addMessage(this.bindPassword.getClientId(FacesContext.getCurrentInstance()), new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "El password no coincide con el trabajador. Intente de nuevo".toUpperCase()));
          }
        }
        else
        {
          FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Usuario no registrado.");
          
          FacesContext.getCurrentInstance().addMessage(null, msg);
        }
      }
      else
      {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Los campos no pueden ir en blanco.");
        
        FacesContext.getCurrentInstance().addMessage(null, msg);
      }
    }
    else
    {
      FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Los campos no pueden ir en blanco.");
      
      FacesContext.getCurrentInstance().addMessage(null, msg);
    }
  }
  
  public void validarLongitud(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    setNoSeguroSocial(String.valueOf(object).trim());
    String regex = "[0-9]{11,11}";
    Pattern pat = Pattern.compile(regex);
    if (!pat.matcher(getNoSeguroSocial()).matches())
    {
      setNoSeguroSocial(null);
      setNoSeguroSocial("");
      throw new ValidatorException(new FacesMessage("El No. del Seguro Social no coincide con el patr��n".toUpperCase()));
    }
  }
  
  public void limpiaVariables()
  {
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindCorreo());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindSeguroSocial());
    
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindPassword());
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindpass1());
    
    AdfFacesContext.getCurrentInstance().addPartialTarget(getBindpass2());
    this.bindCorreo.resetValue();
    this.bindCorreo.setValue("");
    this.bindPassword.resetValue();
    this.bindPassword.setValue("");
    this.bindSeguroSocial.resetValue();
    this.bindSeguroSocial.setValue("");
    this.bindpass1.resetValue();
    this.bindpass1.setValue("");
    this.bindpass2.resetValue();
    this.bindpass2.setValue("");
  }
  
  public void validarCorreo(FacesContext facesContext, UIComponent uIComponent, Object object)
    throws DecoderException, Exception
  {
    setCorreo(String.valueOf(object).trim());
    String regex = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
    
    Pattern pat = Pattern.compile(regex);
    if (!pat.matcher(getCorreo()).matches())
    {
      setCorreo(null);
      setCorreo("");
      throw new ValidatorException(new FacesMessage("La direccion de correo electr��nico no cumple el patron correo@dominio.xx".toUpperCase()));
    }
  }
  
  public String cancelaEnvioPass()
  {
    limpiaVariables();
    return "/inicioSesion.jspx";
  }
  
  public void validaNuevoPasswordUno(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String nuevo = String.valueOf(object);
    if (nuevo.length() < 6)
    {
      this.bindpass1.resetValue();
      throw new ValidatorException(new FacesMessage(null, "La contrasenia no cumple con la longitud minima requerida".toUpperCase()));
    }
    if (nuevo.length() > 15) {
      throw new ValidatorException(new FacesMessage(null, "La contrasenia EXCEDE LA LONGITUD de 15 CARACTERES".toUpperCase()));
    }
    String regexMayuscula = "(?=.*[A-Z]).*$";
    Pattern patMayusucula = Pattern.compile(regexMayuscula);
    if (!patMayusucula.matcher(nuevo).matches())
    {
      this.bindpass1.resetValue();
      throw new ValidatorException(new FacesMessage(null, "La contrasenia no incluye por lo menos una mayuscula".toUpperCase()));
    }
    String regexDigito = "(?=.*[0-9]).*$";
    Pattern patDigito = Pattern.compile(regexDigito);
    if (!patDigito.matcher(nuevo).matches())
    {
      this.bindpass1.resetValue();
      throw new ValidatorException(new FacesMessage(null, "La contrasenia no incluye por lo menos un digito".toUpperCase()));
    }
    String regexSpacio = "(?=.*[^\\S]).*$";
    Pattern patSpacio = Pattern.compile(regexSpacio);
    if (patSpacio.matcher(nuevo).matches())
    {
      this.bindpass1.resetValue();
      throw new ValidatorException(new FacesMessage(null, "La contrasenia no debe incluir espacios".toUpperCase()));
    }
  }
  
  public void validaNuevoPasswordDos(FacesContext facesContext, UIComponent uIComponent, Object object)
  {
    String nuevo = String.valueOf(object);
    if (nuevo.length() < 6)
    {
      this.bindpass2.resetValue();
      throw new ValidatorException(new FacesMessage(null, "La contrasenia no cumple con la longitud minima requerida".toUpperCase()));
    }
    String regexMayuscula = "(?=.*[A-Z]).*$";
    Pattern patMayusucula = Pattern.compile(regexMayuscula);
    if (!patMayusucula.matcher(nuevo).matches())
    {
      this.bindpass2.resetValue();
      throw new ValidatorException(new FacesMessage(null, "La contrasenia no incluye por lo menos una mayuscula".toUpperCase()));
    }
    String regexDigito = "(?=.*[0-9]).*$";
    Pattern patDigito = Pattern.compile(regexDigito);
    if (!patDigito.matcher(nuevo).matches())
    {
      this.bindpass2.resetValue();
      throw new ValidatorException(new FacesMessage(null, "La contrasenia no incluye por lo menos un digito".toUpperCase()));
    }
    String regexSpacio = "(?=.*[^\\S]).*$";
    Pattern patSpacio = Pattern.compile(regexSpacio);
    if (patSpacio.matcher(nuevo).matches())
    {
      this.bindpass2.resetValue();
      throw new ValidatorException(new FacesMessage(null, "La contrasenia no debe incluir espacios".toUpperCase()));
    }
  }
  
  public void mandaCorreo(String correo, Trabajador trabajador, String pass1)
  {
    EnviarCorreo enviaCorreo = new EnviarCorreo();
    String pass = pass1;
    trabajador.setPassword(CifradoCesar.Encriptar(pass, 10));
    this.ejbTrabajador.mergeTrabajador(trabajador);
    try
    {
      enviaCorreo.enviaEmails(correo, "Recuperacion de contrasenia.", "Estimado Cliente,\n\nA continuaci��n \"Quadrum CFDI\" le hace entrega de su Contrase��a: \n\nUsuario: " + CifradoCesar.Desencriptar(trabajador.getCurp(), 10) + "\n\n" + "\n\nContrase��a: " + pass + "\n\n" + "\n\n Saludos Cordiales" + "\n\n Quadrum-cfdi" + "\n " + "\n contacto@quadrum.com.mx" + "\n\n Este correo fue generado de forma automatica; por favor, no emita una respuesta al mismo.");
    }
    catch (MessagingException e)
    {
      e.printStackTrace();
    }
  }
  
  public void setNoSeguroSocial(String noSeguroSocial)
  {
    this.noSeguroSocial = noSeguroSocial;
  }
  
  public String getNoSeguroSocial()
  {
    return this.noSeguroSocial;
  }
  
  public void setCorreo(String correo)
  {
    this.correo = correo;
  }
  
  public String getCorreo()
  {
    return this.correo;
  }
  
  public void setContrasenia(String contrasenia)
  {
    this.contrasenia = contrasenia;
  }
  
  public String getContrasenia()
  {
    return this.contrasenia;
  }
  
  public void setPass1(String pass1)
  {
    this.pass1 = pass1;
  }
  
  public String getPass1()
  {
    return this.pass1;
  }
  
  public void setPass2(String pass2)
  {
    this.pass2 = pass2;
  }
  
  public String getPass2()
  {
    return this.pass2;
  }
  
  public void setBindSeguroSocial(RichInputText bindSeguroSocial)
  {
    this.bindSeguroSocial = bindSeguroSocial;
  }
  
  public RichInputText getBindSeguroSocial()
  {
    return this.bindSeguroSocial;
  }
  
  public void setBindPassword(RichInputText bindPassword)
  {
    this.bindPassword = bindPassword;
  }
  
  public RichInputText getBindPassword()
  {
    return this.bindPassword;
  }
  
  public void setBindpass1(RichInputText bindpass1)
  {
    this.bindpass1 = bindpass1;
  }
  
  public RichInputText getBindpass1()
  {
    return this.bindpass1;
  }
  
  public void setBindpass2(RichInputText bindpass2)
  {
    this.bindpass2 = bindpass2;
  }
  
  public RichInputText getBindpass2()
  {
    return this.bindpass2;
  }
  
  public void setBindCorreo(RichInputText bindCorreo)
  {
    this.bindCorreo = bindCorreo;
  }
  
  public RichInputText getBindCorreo()
  {
    return this.bindCorreo;
  }
  
  public void setPopOk(RichPopup popOk)
  {
    this.popOk = popOk;
  }
  
  public RichPopup getPopOk()
  {
    return this.popOk;
  }
}
