package managedbeans;


import cargaxmlreporte.CargaXml;

import classjava.Anexo20;
import classjava.EjecutaReporte;
import classjava.TablaConsultas;
import classjava.TablaRecibo;

import classjava.UuidCancela;

import entities.Empresa;
import entities.Recibos;
import entities.Trabajador;

import facturareporte.Reporte;

import generadorqr.QRZxing;

import interfaces.SessionRecibosRemote;
import interfaces.SessionTrabajadorRemote;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.InputStream;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import javax.faces.validator.ValidatorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;


import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;
import oracle.adf.view.rich.component.rich.output.RichOutputLabel;

import java.util.Date;

import java.text.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

import java.util.HashMap;
import java.util.Map;

import javax.faces.context.ExternalContext;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JasperPrint;

import numeroConLetra.NumberToLetterConverter;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.DialogEvent;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

import servicelocator.MyServiceLocator;

import util.CifradoCesar;

public class SuggestionsBean //
{

    private Trabajador user; //
    private Empresa admin;
    private List<Trabajador> lisDesnc;
    private List<Trabajador> listTrabajadores;
    private List<Recibos> lisDesncR;
    private List<Recibos> lisDesncRT;
    private Recibos recibo;
    private List<TablaConsultas> MuestraConsulta;
    private String patronBuscar;
    private List<Integer> numEmpleados;
    private SessionTrabajadorRemote trabajadorFacade;
    private SessionRecibosRemote recibosFacade;
    private String fileName;

    private Trabajador objT;
    private int numTrab;

    private String campoBuscar;
    private String UUID;
    private String Empleado;
    private String NumSS;
    private String Curp;
    private String Rfc;
    private String Finicial;
    private String Ffinal;


    private int tipoDato;

    CifradoCesar convert;
    private List<SelectItem> suggestions = new ArrayList<SelectItem>();
    private RichInputText it1_validator;
    private RichInputText beanUUID;
    private RichInputText beanEmpleado;
    private RichInputDate beanFinicial;
    private RichInputDate beanFfinal;
    private RichInputText beanNSS;
    private RichInputText beanCurp;
    private RichInputText beanRFC;
    private RichOutputLabel beanlebelFecha;
    private RichCommandButton bottonBuscar;
    private RichTable tablaConsulta;
    private String nombreArchivoPdf;
    private RichOutputLabel labelRecibosEncontradosV;
    private RichCommandButton exportarExcel;
    private RichCommandButton bottonBuscarFecha;

    boolean entroAdmin;
    boolean entroUser;
    private RichCommandButton exportarPDF;

    private int firstRow = 0;
    private int rowsPerPage = 30;
    private int totalRows;
    private int totalPages;
    private int currentPage = 1;
    private int rows;


    private boolean selecc;
    private String estatus;
    private RichColumn columnaEstatus;
    private RichSelectBooleanCheckbox checkBoxCancela;

    private List<TablaConsultas> listaRecibosTabla = new ArrayList<TablaConsultas>();
    private RichCommandButton botonCancelacion;
    private String mensajeCancelacion = "";

    private Anexo20 anexo;
    private RichPopup popupAvisoCancela;

    private CifradoCesar cifrado;

    private String version;
    
    private String fechaCertificacio;
    private String fechaEmision;
    private Date fechaFinalPago;
    private Date fechaInicialPago;
    private Date fechaPago;
    private int idrecibos;
    private long noDiasPagados;
    private String reciboXML;
    private BigDecimal totalExcentoDeduccion;
    private BigDecimal totalExcentoPercepcion;
    private BigDecimal totalGravadoDeduccion;
    private BigDecimal totalGravadoPercepcion;
    private String uuid;

    private List<TablaRecibo> listaTabla = new ArrayList<TablaRecibo>();
    private List<Recibos> listRecibos = new ArrayList<Recibos>();

    private String ArhivoPdf = "";
    private static String RUTA = "E:\\redringquadrum\\histenviados\\";
    private List<String> misUUIDS = new ArrayList<String>();
    
    public SuggestionsBean() {
        super();
        anexo = new Anexo20();
        cifrado = new CifradoCesar();
        trabajadorFacade = MyServiceLocator.getTrabajadorRemote();
        recibosFacade = MyServiceLocator.getSessionRecibosRemote();
        obtieneSesion();
        lisDesnc = new ArrayList<Trabajador>();
        lisDesncR = new ArrayList<Recibos>();
        lisDesncRT = new ArrayList<Recibos>();
        listTrabajadores = new ArrayList<Trabajador>();
        MuestraConsulta = new ArrayList<TablaConsultas>();
        numEmpleados = new ArrayList<Integer>();
        campoBuscar = "";
        MuestraConsulta.clear();
        objT = new Trabajador();
        llenarListaSuggestios(1);
    }

    public void llenarListaSuggestios(int opc) {
        /* Opciones
         *  1 = Nombre
         *  2 = RFC
         *  3 = CURP
         *  4 = Numero de Seguridad Social
         *  5 = UUID
         */
        entroAdmin = false;
        entroUser = false;
        boolean existeElmento = false;
        try {
            suggestions.clear();

            if (admin.getIdempresa() != 0) {
                entroAdmin = true;
                lisDesnc =
                        trabajadorFacade.getfindListXEpresa(admin.getIdempresa()); //trae todos los trabajadores de una empresa y sus sucursales
            }
            if (user.getIdtrabajador() != 0) {
                entroUser = true;
                lisDesnc =
                        trabajadorFacade.getfindListXSucursal(user.getSucursal1().getIdsucursales()); //trae todos los trabajadores de una sucursal
            }
        } catch (Exception e) {
        }
        ;
        if (opc == 5) {
            if (entroAdmin)
                lisDesncR =
                        recibosFacade.getXSecursales(admin.getIdempresa()); //Trae todos los recibos de una Empresa y sus Sucursales

            if (entroUser)
                lisDesncR =
                        recibosFacade.getXSecursal(user.getSucursal1().getIdsucursales()); //Trae solo los recibos de una sucursal


            String uuid;
            for (Recibos t : lisDesncR) {
                uuid = convert.Desencriptar(t.getUuid(), 10);
                suggestions.add(new SelectItem(uuid));
            }
        } else
            for (Trabajador t : lisDesnc) {
                String nombre = "";
                switch (opc) {
                case 1: //Nombre
                    nombre = convert.Desencriptar(t.getNombre(), 10);
                    nombre += " " + convert.Desencriptar(t.getApellidos(), 10);
                    if (!existeItem(nombre))
                        suggestions.add(new SelectItem(nombre));
                    break;
                case 2: //RFC
                    nombre = convert.Desencriptar(t.getRfc(), 10);
                    suggestions.add(new SelectItem(nombre));
                    break;
                case 3: //CURP
                    nombre = convert.Desencriptar(t.getCurp(), 10);
                    suggestions.add(new SelectItem(nombre));
                    break;
                case 4: //Numero de seguridad Social
                    nombre = convert.Desencriptar(t.getNumSeguridadSocial(), 10);
                    suggestions.add(new SelectItem(nombre));
                    break;
                }
            }
    }

    public boolean existeItem(String dato) {
        boolean existeElmento = false;
        for (SelectItem item : suggestions)
            if (item.getLabel().compareTo(dato) == 0)
                existeElmento = true;
        return existeElmento;
    }

    public void obtieneSesion() {
        user = new Trabajador();
        admin = new Empresa();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
        HttpSession session = req.getSession(true);
        if (session.getAttribute("usuario") != null) {
            System.out.println("usuario");
            user = (Trabajador)session.getAttribute("usuario");
        } else {
            System.out.println("admin");
            admin = (Empresa)session.getAttribute("admin");
        }
    }


    public List getSuggestions(String input) {
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (SelectItem item : suggestions) {
            if (item.getLabel().toUpperCase().contains(input.toUpperCase())) {
                items.add(item);
            }
        }
        return items;
    }


    public void setIt1_validator(RichInputText it1_validator) {
        this.it1_validator = it1_validator;
    }

    public RichInputText getIt1_validator() {
        return it1_validator;
    }

    public List it1_validator(String string) {
        return null;
    }

    public void buscarX(ValueChangeEvent valueChangeEvent) {
        // actualizar tipo de busqueda
        String patron = (String)valueChangeEvent.getNewValue();
        System.out.println(" PATRON :: " + patron);
        if (patron != null) {
            llenarListaSuggestios(Integer.parseInt(patron));
            this.setTipoDato(Integer.parseInt(patron));
        } else {
            this.setTipoDato(-1);
        }
        enebleInputText();
    }

    public void enebleInputText() {
        beanUUID.setVisible(false);
        beanEmpleado.setVisible(false);
        beanFinicial.setVisible(false);
        beanFfinal.setVisible(false);
        beanNSS.setVisible(false);
        beanCurp.setVisible(false);
        beanRFC.setVisible(false);
        beanlebelFecha.setVisible(false);
        if (this.getTipoDato() > 0)
            bottonBuscar.setVisible(true);
        else
            bottonBuscar.setVisible(false);
        bottonBuscarFecha.setVisible(false);
        switch (this.getTipoDato()) {
        case 1: //  1 = Nombre
            beanEmpleado.setVisible(true);
            break;
        case 2: // 2 = RFC
            beanRFC.setVisible(true);
            break;
        case 3: // 3 = CURP
            beanCurp.setVisible(true);
            break;
        case 4: // 4 = Numero de Seguridad Social
            beanNSS.setVisible(true);
            break;
        case 5: // 5 = UUID
            beanUUID.setVisible(true);
            break;
        case 6: // 6 = Fecha
            beanlebelFecha.setVisible(true);
            beanFinicial.setVisible(true);
            beanFfinal.setVisible(true);
            bottonBuscarFecha.setVisible(true);
            bottonBuscar.setVisible(false);
            break;
        }

    }

    public void LimpiarCampos() {
        this.setUUID("");
        this.setEmpleado("");
        this.setNumSS("");
        this.setCurp("");
        this.setFfinal("");
        this.setFinicial("");
        this.setRfc("");
        this.setCampoBuscar("");
        this.setCampoBuscar("");
        if (MuestraConsulta.size() >= 1) {
            exportarExcel.setVisible(true);
            exportarPDF.setVisible(false);
        } else {
            exportarExcel.setVisible(false);
            exportarPDF.setVisible(false);
        }
    }

    public String getNombre(String nombre) {
        String Nombre = "";
        for (int x = 0; x < nombre.length(); x++)
            if (nombre.charAt(x) != ' ') {
                Nombre += nombre.charAt(x);
            } else {
                break;
            }

        return Nombre;
    }

    public String getApellido(String nombre) {
        String leer = "";
        String Apellido = "";
        for (int x = nombre.length() - 1; x >= 0; x--)
            if (nombre.charAt(x) != ' ')
                leer += nombre.charAt(x);
            else {
                break;
            }

        for (int x = leer.length() - 1; x >= 0; x--)
            Apellido += leer.charAt(x);

        return Apellido;
    }

    public String cb1_actionConsulta() {
        /* Opciones 1-4 obtiene trabajdor trae recibos asociados
         *  1 = Nombre                          findByName(String nombre)
         *  2 = RFC                             getfindXRFC(String rfc)
         *  3 = CURP                            getfindXCURP(String curp)
         *  4 = Numero de Seguridad Social      getfindXnsg(String nsg)
         *  En el caso sinco traer Recibo y optener Trabajador
         *  5 = UUID
         *  6 = Fecha Pago
         */
        try {
            numTrab = 0;
            lisDesncRT.clear();
            MuestraConsulta.clear();
            numEmpleados.clear();
            listTrabajadores.clear();
        } catch (Exception e) {
        }
        setFileName("Reporte");
        if (this.getTipoDato() == 7 || this.getTipoDato() == 8 || this.getTipoDato() == 9 || this.getTipoDato() == 10)
            this.setCampoBuscar("Traer Todos");
        Recibos objR = new Recibos();
        Trabajador objT = new Trabajador();
        if (!getCampoBuscar().isEmpty()) {
            switch (this.getTipoDato()) {
            case 1: //Nombre
                rows = 0;
                firstRow = 0;
                rowsPerPage = 30;
                String nombre = getNombre(this.getEmpleado());
                nombre = convert.Encriptar(nombre, 10);
                nombre = nombre + "%";
                String apellido = getApellido(this.getEmpleado());
                apellido = convert.Encriptar(apellido, 10);
                apellido = "%" + apellido;
                if (entroAdmin)
                    listTrabajadores =
                            (List<Trabajador>)trabajadorFacade.findByNameXEmpresa(nombre, apellido, admin.getIdempresa()); //Trae trabajadores de una Empresa y sus sucursales
                if (entroUser)
                    listTrabajadores =
                            trabajadorFacade.findByNameXSucursal(nombre, apellido, user.getSucursal1().getIdsucursales()); // Trae trabajadores solo de 1 sucursal
                break;
            case 2: //RFC  *****************
                rows = 0;
                firstRow = 0;
                rowsPerPage = 30;
                if (entroAdmin)
                    listTrabajadores =
                            trabajadorFacade.getfindXRFC(convert.Encriptar(this.getRfc(), 10), admin.getIdempresa());
                if (entroUser)
                    listTrabajadores =
                            trabajadorFacade.getfindXRFCySucursal(convert.Encriptar(this.getRfc(), 10), user.getSucursal1().getIdsucursales());
                break;
            case 3: //CURP
                rows = 0;
                firstRow = 0;
                rowsPerPage = 30;
                if (entroAdmin)
                    listTrabajadores =
                            trabajadorFacade.getfindXCURP(convert.Encriptar(this.getCurp(), 10), admin.getIdempresa());
                if (entroUser)
                    listTrabajadores =
                            trabajadorFacade.getfindXCURP(convert.Encriptar(this.getCurp(), 10), user.getSucursal1().getIdsucursales());
                break;
            case 4: //NSS
                rows = 0;
                firstRow = 0;
                rowsPerPage = 30;
                if (entroAdmin)
                    listTrabajadores =
                            trabajadorFacade.getfindXnsg(convert.Encriptar(this.getNumSS(), 10), admin.getIdempresa());
                if (entroUser)
                    listTrabajadores =
                            trabajadorFacade.getfindXnsgySucursal(convert.Encriptar(this.getNumSS(), 10), user.getSucursal1().getIdsucursales());
                break;
            case 5: //UUID
                rows = 0;
                firstRow = 0;
                rowsPerPage = 30;
                if (entroAdmin)
                    recibo =
                            recibosFacade.getfindXUIDD(convert.Encriptar(this.getUUID(), 10), admin.getIdempresa());
                if (entroUser)
                    lisDesncRT =
                            recibosFacade.getfindXUIDDySucursal(convert.Encriptar(this.getUUID(), 10), user.getSucursal1().getIdsucursales());

                break;
            case 6:
                break;
            case 7: //Trae Todos
                if (entroAdmin) {
                    rows = 0;
                    firstRow = 0;
                    rowsPerPage = 30;
                    rows = recibosFacade.recibosTotalEmpresa(admin.getSucursalList().get(0).getIdsucursales());
                    this.setTotalRows(rows);
                    double val1 = ((double)this.getTotalRows() / this.getRowsPerPage());
                    int totalPagesCal = (int)Math.ceil(val1);
                    this.setTotalPages((totalPagesCal != 0) ? totalPagesCal : 1);
                    System.out.println("-------------------------" + admin.getSucursalList().get(0).getIdsucursales());
                    lisDesncRT =
                            recibosFacade.getAllEmpresa(admin.getSucursalList().get(0).getIdsucursales(), firstRow,
                                                        rowsPerPage);
                }
                if (entroUser) {
                    lisDesncRT = recibosFacade.getXSecursal(user.getSucursal1().getIdsucursales());
                }
                break;
            case 8: //recibos activos
                rows = 0;
                firstRow = 0;
                rowsPerPage = 30;
                if (entroAdmin)
                    lisDesncRT = recibosFacade.getXRecibosActivos(admin.getIdempresa());
                if (entroUser)
                    lisDesncRT = recibosFacade.getXRecibosActivosXsucursal(user.getSucursal1().getIdsucursales());
                break;
            case 9: //recibos cancelados
                rows = 0;
                firstRow = 0;
                rowsPerPage = 30;
                if (entroAdmin)
                    lisDesncRT = recibosFacade.getXRecibosInactivos(admin.getIdempresa());
                if (entroUser)
                    lisDesncRT = recibosFacade.getXRecibosInactivosXsucursal(user.getSucursal1().getIdsucursales());
                break;
            case 10: //trabajadores dados de baja
                rows = 0;
                firstRow = 0;
                rowsPerPage = 30;
                if (entroAdmin)
                    listTrabajadores = trabajadorFacade.getfinEliminados(admin.getIdempresa());
                if (entroUser)
                    listTrabajadores = trabajadorFacade.getfindElimXsucursal(user.getSucursal1().getIdsucursales());

                break;
            }

            //Carga resultado de seleccion
            try {

                if (this.getTipoDato() == 5) // Apartir de un recibo se optiene el Trabajador - No existe Lista recibos
                { //Desplegar recibo y trabajador
                    numTrab = 1;
                    if (recibo != null){
                        //listTrabajadores.add(trabajadorFacade.getfindXid(recibo.getTrabajador().getIdtrabajador()));
                        recibo.setUuid(convert.Desencriptar(recibo.getUuid(), 10));
                        MuestraConsulta.add(new TablaConsultas(desencriptarTrabajador(trabajadorFacade.getfindXid(recibo.getTrabajador().getIdtrabajador())),recibo));    
                    /* if (lisDesncRT.size() > 0 && listTrabajadores.size() > 0) {
                        lisDesncRT.clear();
                        for (Trabajador obT : listTrabajadores) {
                            obT = desencriptarTrabajador(obT);
                            lisDesncRT = recibosFacade.findXIDTrab(obT.getIdtrabajador());
                            for (Recibos obj : lisDesncRT) {
                                obj.setUuid(convert.Desencriptar(obj.getUuid(), 10));
                                MuestraConsulta.add(new TablaConsultas(obT, obj));
                            }
                        }*/
                    } else {
                        objT = new Trabajador();
                        FacesMessage msg =
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "No se encontró información con el patrón de búsqueda seleccionado intente una vez más.");
                        FacesContext.getCurrentInstance().addMessage(null, msg);
                    } 
                } else if (this.getTipoDato() == 6) {
                    if (!(this.getFinicial().isEmpty() || this.getFfinal().isEmpty())) {
                        if (entroAdmin)
                            lisDesncRT =
                                    recibosFacade.getAllXPago(obtenerFormatoFecha(this.getFinicial()), obtenerFormatoFecha(this.getFfinal()),
                                                              admin.getIdempresa());
                        if (entroUser)
                            lisDesncRT =
                                    recibosFacade.getAllXPagoSucursal(obtenerFormatoFecha(this.getFinicial()), obtenerFormatoFecha(this.getFfinal()),
                                                                      user.getSucursal1().getIdsucursales());

                        for (Recibos obj : lisDesncRT) {
                            boolean existe = true;
                            for (int x = 0; x < numEmpleados.size(); x++)
                                if (numEmpleados.get(x) == obj.getTrabajador().getIdtrabajador())
                                    existe = false;

                            if (existe)
                                numEmpleados.add(obj.getTrabajador().getIdtrabajador());

                            objT = trabajadorFacade.getfindXid(obj.getTrabajador().getIdtrabajador());
                            objT = desencriptarTrabajador(objT);
                            obj.setUuid(convert.Desencriptar(obj.getUuid(), 10));
                            MuestraConsulta.add(new TablaConsultas(objT, obj));
                        }
                    } else {
                        FacesMessage msg =
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "No se encontró información con el patrón de búsqueda seleccionado intente una vez más. Verifique que ha seleccionado  una fecha inicial y final. ");
                        FacesContext.getCurrentInstance().addMessage(null, msg);
                    }
                } else if (this.getTipoDato() == 7 || this.getTipoDato() == 8 ||
                           this.getTipoDato() == 9) { //Trae todos los trabajadores de la empresa
                    if (lisDesncRT.size() > 0) {
                        for (Recibos r : lisDesncRT) {
                            boolean existe = true;
                            for (int x = 0; x < numEmpleados.size(); x++)
                                if (numEmpleados.get(x) == r.getTrabajador().getIdtrabajador())
                                    existe = false;

                            if (existe)
                                numEmpleados.add(r.getTrabajador().getIdtrabajador());

                            Trabajador t = new Trabajador();
                            t = trabajadorFacade.getfindXid(r.getTrabajador().getIdtrabajador());
                            t = desencriptarTrabajador(t);
                            r.setUuid(convert.Desencriptar(r.getUuid(), 10));
                            MuestraConsulta.add(new TablaConsultas(t, r));
                        }
                    } else {
                        objT = new Trabajador();
                        FacesMessage msg =
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "No se encontró información de recibos por el patrón de búsqueda.");
                        FacesContext.getCurrentInstance().addMessage(null, msg);
                    }
                } else if (this.getTipoDato() == 2 || this.getTipoDato() == 3 ||
                           this.getTipoDato() == 4) { //Llenar lista de Recibos de trabajador seleccionado

                    for (Trabajador oT : listTrabajadores) {
                        oT = desencriptarTrabajador(oT);
                        numTrab = 1;
                        lisDesncRT = recibosFacade.findXIDTrab(oT.getIdtrabajador());
                        for (Recibos obj : lisDesncRT) {
                            obj.setUuid(convert.Desencriptar(obj.getUuid(), 10));
                            MuestraConsulta.add(new TablaConsultas(oT, obj));
                        }
                    }
                } else if (this.getTipoDato() == 1 || this.getTipoDato() == 10) { //Desplegar trabajador y recibos

                    if (listTrabajadores.size() > 0) {
                        for (Trabajador t : listTrabajadores) {
                            t = desencriptarTrabajador(t);
                            lisDesncRT = recibosFacade.findXIDTrab(t.getIdtrabajador());
                            for (Recibos r : lisDesncRT) {

                                boolean existe = true;
                                for (int x = 0; x < numEmpleados.size(); x++)
                                    if (numEmpleados.get(x) == r.getTrabajador().getIdtrabajador())
                                        existe = false;

                                if (existe)
                                    numEmpleados.add(r.getTrabajador().getIdtrabajador());

                                r.setUuid(convert.Desencriptar(r.getUuid(), 10));
                                MuestraConsulta.add(new TablaConsultas(t, r));
                            }
                        }

                        if (MuestraConsulta.size() == 0) {
                            FacesMessage msg =
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "No se encontraron recibos pertenecientes al empleado seleccionado.");
                            FacesContext.getCurrentInstance().addMessage(null, msg);
                        }

                    } else {
                        objT = new Trabajador();
                        FacesMessage msg =
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "No se encontró información con el patrón de búsqueda seleccionado intente una vez más.");
                        FacesContext.getCurrentInstance().addMessage(null, msg);

                    }
                }
            } catch (Exception e) {
                System.out.println("Error al construir tabla recibos");
                e.printStackTrace();
            }
        } else {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Introduzca Información a buscar, por favor. ");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        LimpiarCampos();
        elementosEncontrados();
        return null;
    }

    public void elementosEncontrados() {
        String total;
        if (numEmpleados.size() > 1)
            total =
                    "  " + numEmpleados.size() + " TRABAJADORES  :: " + MuestraConsulta.size() + " RECIBOS  ENCONTRADOS ";
        else if (numEmpleados.size() == 1)
            total = "  1 TRABAJADOR  :: " + MuestraConsulta.size() + " RECIBOS  ENCONTRADOS ";
        else if (numTrab == 1 && MuestraConsulta.size() > 1)
            total = "  1 TRABAJADOR  :: " + MuestraConsulta.size() + " RECIBOS  ENCONTRADOS ";
        else
            total = " " + numTrab + " TRABAJADOR  :: " + MuestraConsulta.size() + " RECIBO  ENCONTRADO ";
        labelRecibosEncontradosV.setValue(total);
    }

    public Trabajador desencriptarTrabajador(Trabajador trabajador) {
        try {
            trabajador.setNombre(convert.Desencriptar(trabajador.getNombre(), 10) + " " +
                                 convert.Desencriptar(trabajador.getApellidos(), 10));
            trabajador.setApellidos(convert.Desencriptar(trabajador.getApellidos(), 10));
            trabajador.setRfc(convert.Desencriptar(trabajador.getRfc(), 10));
            trabajador.setCurp(convert.Desencriptar(trabajador.getCurp(), 10));
            trabajador.setNumSeguridadSocial(convert.Desencriptar(trabajador.getNumSeguridadSocial(), 10));
            trabajador.setUsuario(convert.Desencriptar(trabajador.getUsuario(), 10));
            trabajador.setCorreo(convert.Desencriptar(trabajador.getCorreo(), 10));
        } catch (Exception e) {
            return null;
        }
        return trabajador;
    }

    public Trabajador encriptarTrabajdor(Trabajador trabajador) {
        try {
            trabajador.setNombre(convert.Encriptar(trabajador.getNombre(), 10));
            trabajador.setApellidos(convert.Encriptar(trabajador.getApellidos(), 10));
            trabajador.setRfc(convert.Encriptar(trabajador.getRfc(), 10));
            trabajador.setCurp(convert.Encriptar(trabajador.getCurp(), 10));
            trabajador.setNumSeguridadSocial(convert.Encriptar(trabajador.getNumSeguridadSocial(), 10));
            trabajador.setUsuario(convert.Encriptar(trabajador.getUsuario(), 10));
            trabajador.setCorreo(convert.Encriptar(trabajador.getCorreo(), 10));
        } catch (Exception e) {
            return null;
        }
        return trabajador;
    }

    public Recibos desifraRecibos(Recibos r) {
        return null;
    }

    public Recibos encriptaRecibos(Recibos r) {
        return null;
    }

    public String obtenerFormatoFecha(String fecha) {
        String[] Fecha = fecha.split("/");
        return Fecha[2] + "-" + Fecha[1] + "-" + Fecha[0];
    }

    public void setCampoBuscar(String campoBuscar) {
        this.campoBuscar = campoBuscar;
        enebleInputText();
    }

    public String getCampoBuscar() {
        return campoBuscar;
    }

    public void setTipoDato(int tipoDato) {
        this.tipoDato = tipoDato;
    }

    public int getTipoDato() {
        return tipoDato;
    }


    public void setLisDesncRT(List<Recibos> lisDesncRT) {
        this.lisDesncRT = lisDesncRT;
    }

    public List<Recibos> getLisDesncRT() {
        return lisDesncRT;
    }

    public void setObjT(Trabajador objT) {
        this.objT = objT;
    }

    public Trabajador getObjT() {
        return objT;
    }

    public void validaBuscar(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String nombre = (String)object;
        if ((nombre == null) || nombre.length() == 0) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "Introduzca un patrón de Búsqueda.", null));
        }
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
        this.setCampoBuscar(this.UUID);
    }

    public String getUUID() {
        return UUID;
    }

    public void setEmpleado(String Empleado) {
        this.Empleado = Empleado;
        this.setCampoBuscar(this.Empleado);
    }

    public String getEmpleado() {
        return Empleado;
    }

    public void setNumSS(String NumSS) {
        this.NumSS = NumSS;
        this.setCampoBuscar(this.NumSS);
    }

    public String getNumSS() {
        return NumSS;
    }

    public void setCurp(String Curp) {
        this.Curp = Curp;
        this.setCampoBuscar(this.Curp);
    }

    public String getCurp() {
        return Curp;
    }

    public void setRfc(String Rfc) {
        this.Rfc = Rfc;
        this.setCampoBuscar(this.Rfc);
    }

    public String getRfc() {
        return Rfc;
    }

    public void setFinicial(String Finicial) {
        this.Finicial = Finicial;
        this.setCampoBuscar(this.Finicial);
    }

    public String getFinicial() {
        return Finicial;
    }

    public void setFfinal(String Ffinal) {
        this.Ffinal = Ffinal;
        this.setCampoBuscar(this.Ffinal);
    }

    public String getFfinal() {
        return Ffinal;
    }

    public void setMuestraConsulta(List<TablaConsultas> MuestraConsulta) {
        this.MuestraConsulta = MuestraConsulta;
    }

    public List<TablaConsultas> getMuestraConsulta() {
        return MuestraConsulta;
    }

    public void setPatronBuscar(String patronBuscar) {
        this.patronBuscar = patronBuscar;
    }

    public String getPatronBuscar() {
        return patronBuscar;
    }


    public void setBeanUUID(RichInputText beanUUID) {
        this.beanUUID = beanUUID;
    }

    public RichInputText getBeanUUID() {
        return beanUUID;
    }

    public void setBeanEmpleado(RichInputText beanEmpleado) {
        this.beanEmpleado = beanEmpleado;
    }

    public RichInputText getBeanEmpleado() {
        return beanEmpleado;
    }

    public void setBeanFinicial(RichInputDate beanFinicial) {
        this.beanFinicial = beanFinicial;
    }

    public RichInputDate getBeanFinicial() {
        return beanFinicial;
    }

    public void setBeanFfinal(RichInputDate beanFfinal) {
        this.beanFfinal = beanFfinal;
    }

    public RichInputDate getBeanFfinal() {
        return beanFfinal;
    }

    public void setBeanNSS(RichInputText beanNSS) {
        this.beanNSS = beanNSS;
    }

    public RichInputText getBeanNSS() {
        return beanNSS;
    }

    public void setBeanCurp(RichInputText beanCurp) {
        this.beanCurp = beanCurp;
    }

    public RichInputText getBeanCurp() {
        return beanCurp;
    }

    public void setBeanRFC(RichInputText beanRFC) {
        this.beanRFC = beanRFC;
    }

    public RichInputText getBeanRFC() {
        return beanRFC;
    }

    public void setBeanlebelFecha(RichOutputLabel beanlebelFecha) {
        this.beanlebelFecha = beanlebelFecha;
    }

    public RichOutputLabel getBeanlebelFecha() {
        return beanlebelFecha;
    }

    public void setBottonBuscar(RichCommandButton bottonBuscar) {
        this.bottonBuscar = bottonBuscar;
    }

    public RichCommandButton getBottonBuscar() {
        return bottonBuscar;
    }

    public void setTablaConsulta(RichTable tablaConsulta) {
        this.tablaConsulta = tablaConsulta;
    }

    public RichTable getTablaConsulta() {
        return tablaConsulta;
    }

    public void setNombreArchivoPdf(String nombreArchivoPdf) {
        this.nombreArchivoPdf = nombreArchivoPdf;
    }

    public String getNombreArchivoPdf() {
        return nombreArchivoPdf;
    }

    public void setLabelRecibosEncontradosV(RichOutputLabel labelRecibosEncontradosV) {
        this.labelRecibosEncontradosV = labelRecibosEncontradosV;
    }

    public RichOutputLabel getLabelRecibosEncontradosV() {
        return labelRecibosEncontradosV;
    }


    public void setExportarExcel(RichCommandButton exportarExcel) {
        this.exportarExcel = exportarExcel;
    }

    public RichCommandButton getExportarExcel() {
        return exportarExcel;
    }

    public void setFileName(String fileName) {
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy_k-m-sa");
        this.fileName = fileName + ft.format(date) + ".xls";
    }

    public String getFileName() {
        return fileName;
    }

    public void setBottonBuscarFecha(RichCommandButton bottonBuscarFecha) {
        this.bottonBuscarFecha = bottonBuscarFecha;
    }

    public RichCommandButton getBottonBuscarFecha() {
        return bottonBuscarFecha;
    }


    


    public String retornaFecha() {
        Calendar fecha = new GregorianCalendar();
        //Obtenemos el valor del año, mes, día,
        //hora, minuto y segundo del sistema
        //usando el método get y el parámetro correspondiente
        int anio = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        return dia + "-" + (mes + 1) + "-" + anio + "--" + hora + "-" + minuto + "-" + segundo;
    }


   /* public void descargarPDFs(FacesContext facesContext, OutputStream out) {

        ArhivoPdf = "RecibosNomina" + retornaFecha();

        List<CargaXml> listXml = new ArrayList<CargaXml>();
        CargaXml xml = null;

        for(TablaConsultas reg: MuestraConsulta)
        {

            try{

             File f= new File(RUTA + reg.getRecibo().getUuid() + ".xml");

             if(f.exists() ){
                 xml = new CargaXml(RUTA + reg.getRecibo().getUuid() + ".xml");
                 reg.setDescargar(true);
                 misUUIDS.add(reg.getRecibo().getUuid());
                 listXml.add(xml);
             }else{
                       System.out.println("No es posible obtener datos no existe archivo xml");
                 }
            }catch(Exception e){}

        }

        listaTabla.clear();
        if (misUUIDS.size() > 0)
        {
            for(TablaConsultas reg: MuestraConsulta)
            {
               if(reg.isDescargar()) {
                    fechaCertificacio = reg.getRecibo().getFechaCertificacio();
                    fechaEmision = reg.getRecibo().getFechaEmision();
                    fechaFinalPago = reg.getRecibo().getFechaFinalPago();
                    fechaInicialPago = reg.getRecibo().getFechaInicialPago();
                    fechaPago = reg.getRecibo().getFechaPago();
                    idrecibos = reg.getRecibo().getIdrecibos();
                    noDiasPagados = reg.getRecibo().getNoDiasPagados();
                    reciboXML = convert.desencriptar(reg.getRecibo().getReciboXML(), 10);
                    totalExcentoDeduccion = reg.getRecibo().getTotalExcentoDeduccion();
                    totalExcentoPercepcion = reg.getRecibo().getTotalExcentoPercepcion();
                    totalGravadoDeduccion = reg.getRecibo().getTotalGravadoDeduccion();
                    totalGravadoPercepcion = reg.getRecibo().getTotalExcentoPercepcion();
                    uuid = reg.getRecibo().getUuid().toUpperCase();
                    
                    version = reg.getRecibo().getVersion();
                    
                    //selecc = false;
                    TablaRecibo tabla =
                        new TablaRecibo(fechaCertificacio, fechaEmision, fechaFinalPago, fechaInicialPago,
                                        fechaPago, idrecibos, noDiasPagados, reciboXML, totalExcentoDeduccion,
                                        totalExcentoPercepcion, totalGravadoDeduccion, totalGravadoPercepcion,
                                        uuid, false,"",version);
                    listaTabla.add(tabla);
                }
            }

        }




        if (misUUIDS.size() > 0 && listXml.size()>0) {
                        EjecutaReporte ejecuta = new EjecutaReporte();
                        try {

                            if (ejecuta.generarReporte(misUUIDS,"web", "pdf")) {
                                File file = new File(System.getProperty("user.dir") + "/PDF/" + ArhivoPdf + ".pdf");

                                if (file.isFile()) {
                                    HttpServletResponse response =
                                        (HttpServletResponse)facesContext.getExternalContext().getResponse();
                                    String contentType = "application/octet-stream";
                                    FileInputStream fdwd;
                                    byte[] bt;
                                    try {
                                        fdwd = new FileInputStream(file);
                                        int checkline;
                                        while ((checkline = fdwd.available()) > 0) {
                                            bt = new byte[checkline];
                                            response.setContentType(contentType);
                                            response.setHeader("Content-Disposition",
                                                               "attachment;filename=\"" + ArhivoPdf + ".pdf");
                                            int rst = fdwd.read(bt);
                                            out.write(bt, 0, bt.length);
                                            if (rst == -1)
                                                break;
                                        }
                                        out.flush();

                                        out.close();
                                        fdwd.close();

                                        file.deleteOnExit();
                                        file.delete();

                                    } catch (IOException e) {
                                                    System.out.println("Erro al construir PDF");
                                    }
                                }
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        //refreshPage();
                        listXml.clear();

                        misUUIDS.clear();
                    } else {
                        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,"","No se encontraron los archivos xml");
                        FacesContext.getCurrentInstance().addMessage(null, msg);

                    }
        //this.sacaArchivoPdf();

    }  */


    /* public void descargarPDFxRec(List<String>  misUUIDS) {
       if (misUUIDS.size() > 0)
       {
           for(String uudi:misUUIDS)
           {
               EjecutaReporte reporte = new EjecutaReporte();
               List<String> rutaXML = new ArrayList<String>();
               rutaXML.add(RUTA + uudi + ".xml");
               List<String> uuid = new ArrayList<String>();
               uuid.add(uudi);
               try {
                   reporte.generaPDFs(rutaXML, uuid);

               } catch (Exception e) {
                   e.printStackTrace();

               }
           }
       } else {
           return;
       }

   } */

    public void setArhivoPdf(String ArhivoPdf) {
        this.ArhivoPdf = ArhivoPdf;
    }

    public String getArhivoPdf() {
        return ArhivoPdf;
    }

    public void setExportarPDF(RichCommandButton exportarPDF) {
        this.exportarPDF = exportarPDF;
    }

    public RichCommandButton getExportarPDF() {
        return exportarPDF;
    }


    public void firstActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(1);
        this.setFirstRow(0);
    }

    public void previousActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(this.getCurrentPage() - 1);
        this.setFirstRow(this.getFirstRow() - this.getRowsPerPage());
        if (admin != null) {
            desencriptaTabla(admin, firstRow, rowsPerPage);
        } else {
            desencriptaTabla(user, firstRow, rowsPerPage);
        }
    }

    public void nextActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(this.getCurrentPage() + 1);
        this.setFirstRow(this.getFirstRow() + this.getRowsPerPage());
        if (admin != null) {
            desencriptaTabla(admin, firstRow, rowsPerPage);
        } else {
            desencriptaTabla(user, firstRow, rowsPerPage);
        }
    }

    public void lastActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(this.getTotalPages());
        this.setFirstRow(this.getTotalRows() -
                         ((this.getTotalRows() % this.getRowsPerPage() != 0) ? this.getTotalRows() %
                          this.getRowsPerPage() : this.getRowsPerPage()));
        if (admin != null) {
            desencriptaTabla(admin, firstRow, rowsPerPage);
        } else {
            desencriptaTabla(user, firstRow, rowsPerPage);
        }
    }

    public boolean isBeforeDisabled() {
        return this.getFirstRow() == 0;
    }

    public boolean isAfterDisabled() {
        return this.getFirstRow() >= this.getTotalRows() - this.getRowsPerPage();
    }


    public void setFirstRow(int firstRow) {
        this.firstRow = firstRow;
    }

    public int getFirstRow() {
        return firstRow;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void desencriptaTabla(Object obj, int inicio, int fin) {
        String tipo = obj.getClass().toString();
        System.out.println("--Inicio-- " + inicio + " --Fin- " + fin);
        MuestraConsulta.clear();
        listRecibos.clear();
        listaTabla.clear();
        System.out.println("      " + MuestraConsulta.size());

        if (tipo.equals("class entities.Empresa")) {
            Empresa empr = (Empresa)obj;
            listRecibos = recibosFacade.getAllEmpresa(empr.getSucursalList().get(0).getIdsucursales(), inicio, fin);
            //listaTabla = new ArrayList<TablaRecibo>();
            int tamRecibos = listRecibos.size();
            if (tamRecibos != 0) {
                for (Recibos r : listRecibos) {
                    fechaFinalPago = r.getFechaFinalPago();
                    fechaInicialPago = r.getFechaInicialPago();
                    fechaPago = r.getFechaPago();
                    uuid = convert.Desencriptar(r.getUuid(), 10).toUpperCase();
                    selecc = false;
                    estatus = r.getEstatus();


                    boolean existe = true;
                    for (int x = 0; x < numEmpleados.size(); x++)
                        if (numEmpleados.get(x) == r.getTrabajador().getIdtrabajador())
                            existe = false;

                    if (existe)
                        numEmpleados.add(r.getTrabajador().getIdtrabajador());

                    Trabajador t = new Trabajador();
                    t = trabajadorFacade.getfindXid(r.getTrabajador().getIdtrabajador());
                    t = desencriptarTrabajador(t);
                    r.setUuid(convert.Desencriptar(r.getUuid(), 10));
                    MuestraConsulta.add(new TablaConsultas(t, r));


                    //refreshPage();
                }
            }

        }
    }

    public void setColumnaEstatus(RichColumn columnaEstatus) {
        this.columnaEstatus = columnaEstatus;
    }

    public RichColumn getColumnaEstatus() {
        return columnaEstatus;
    }

    public void listenerCancelar(ValueChangeEvent valueChangeEvent) {
        if ((Boolean)valueChangeEvent.getNewValue()) {
            TablaConsultas seleccionados = (TablaConsultas)tablaConsulta.getSelectedRowData();
            misUUIDS.add(seleccionados.getRecibo().getUuid());
            System.out.println("Size " + misUUIDS.size());
            listaRecibosTabla.add(seleccionados);
        } else {
            TablaConsultas seleccionados = (TablaConsultas)tablaConsulta.getSelectedRowData();
            if (misUUIDS.size() > 0) {
                misUUIDS.remove(seleccionados.getRecibo().getUuid());
                System.out.println("Size " + misUUIDS.size());
            }
        }
    }

    public void setCheckBoxCancela(RichSelectBooleanCheckbox checkBoxCancela) {
        this.checkBoxCancela = checkBoxCancela;
    }

    public RichSelectBooleanCheckbox getCheckBoxCancela() {
        return checkBoxCancela;
    }

    public void cancelarCFDI(ActionEvent actionEvent) {
        UuidCancela cancela = new UuidCancela();
        Integer codigoCancela = 0;

        // codigoCancela = Integer.parseInt(cancela.can("SND7701134L0", misUUIDS, "Dns130177"));
        switch (codigoCancela) {
        case 201:
            System.out.println("Cancela correctamente");
            actualizaEstatus(misUUIDS);
            mensajeCancelacion = "Cancela correctamente";
            break;
        case 202:
            System.out.println("Cancelado anteriormente");
            actualizaEstatus(misUUIDS);
            mensajeCancelacion = "Cancelado anteriormente";
            break;
        case 203:
            System.out.println("No corresponde el RFC del Emisor y de quien solicita la cancelación");
            mensajeCancelacion = "No corresponde el RFC del Emisor y de quien solicita la cancelación";
            break;
        case 205:
            System.out.println("UUID no existe");
            mensajeCancelacion = "UUID no existe";
            break;
        case 303:
            System.out.println("Intentelo mas tarde");
            mensajeCancelacion = "Intentelo mas tarde";
            break;
        case 704:
            System.out.println("Error con la contraseña de la llave Privada");
            mensajeCancelacion = "Error con la contraseña de la llave Privada";
            break;
        case 708:
            System.out.println("No se pudo conectar al SAT");
            mensajeCancelacion = "No se pudo conectar al SAT";
            break;
        case 711:
            System.out.println("Error con el certificado al cancelar");
            mensajeCancelacion = "Error con el certificado al cancelar";
            break;

        default:
            System.out.println("No se pudo conectar al SAT");
            mensajeCancelacion = "No se pudo conectar al SAT";
        }
        anexo.showPopup(actionEvent, popupAvisoCancela);
    }

    public boolean actualizaEstatus(List<String> uuid) {
        boolean bandera = false;
        listRecibos.clear();
        for (int i = 0; i < uuid.size(); i++) {
            listRecibos =
                    recibosFacade.getConsultaRecibosUUID(cifrado.Encriptar(uuid.get(i), 10), this.admin.getIdempresa(),
                                                         firstRow, rowsPerPage);
            listRecibos.get(0).setEstatus("0");
            recibosFacade.mergeRecibos(listRecibos.get(0));
            System.out.println(listRecibos.size());
        }


        return bandera;
    }

    public void setBotonCancelacion(RichCommandButton botonCancelacion) {
        this.botonCancelacion = botonCancelacion;
    }

    public RichCommandButton getBotonCancelacion() {
        return botonCancelacion;
    }

    public void setPopupAvisoCancela(RichPopup popupAvisoCancela) {
        this.popupAvisoCancela = popupAvisoCancela;
    }

    public RichPopup getPopupAvisoCancela() {
        return popupAvisoCancela;
    }

    public void dialogCancelar(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {

        }
    }

    public void setMensajeCancelacion(String mensajeCancelacion) {
        this.mensajeCancelacion = mensajeCancelacion;
    }

    public String getMensajeCancelacion() {
        return mensajeCancelacion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
