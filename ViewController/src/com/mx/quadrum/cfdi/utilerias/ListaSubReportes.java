package com.mx.quadrum.cfdi.utilerias;

import java.util.ArrayList;
import java.util.List;

public class ListaSubReportes {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4807229497011669320L;

	private List<ConceptoReporte> conceptosList = new ArrayList<ConceptoReporte>();

	public List<ConceptoReporte> getConceptosList() {
		return conceptosList;
	}

	public void setConceptosList(List<ConceptoReporte> conceptosList) {
		this.conceptosList = conceptosList;
	}

}
