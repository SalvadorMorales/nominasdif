package com.mx.quadrum.cfdi.utilerias;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import com.mx.quadrum.cfdi.complementos.leyendasFiscales.schema.LeyendasFiscales;
import com.mx.quadrum.cfdi.complementos.timbrefiscaldigital.v01.schema.TimbreFiscalDigital;
import com.mx.quadrum.cfdi.v32.schema.Comprobante;

public class PdfGenerator {

	/**
	 * Metodo encargado de crear una lista de la clase ComprobanteWrapper
	 * 
	 * @param listaDirecciones
	 * @return
	 * @throws Exception
	 */
	public static List<ComprobanteWrapper> obtenerListaCfdi(
			List<String> listaDirecciones) throws Exception {
		List<ComprobanteWrapper> cfdis = null;
		cfdis = new ArrayList<ComprobanteWrapper>();
		for (String direccion : listaDirecciones) {
			ComprobanteWrapper wrapper = null;
			wrapper = new ComprobanteWrapper();
			Comprobante cfdi = null;
			Class clases[] = { Comprobante.class, TimbreFiscalDigital.class,
					LeyendasFiscales.class };
			cfdi = UtileriasCfdi.convertirAComprobante(new File(direccion),
					clases);

			wrapper.setComprobante(cfdi);
			wrapper.setPathComprobante(direccion);
			cfdis.add(wrapper);
		}
		return cfdis;
	}

	/**
	 * Metodo encargado de crear un archivo pdf y retornar un arreglo de bytes
	 * del archivo creado
	 * 
	 * @param listaCfdi
	 * @param tipo
	 * @param plantillasService
	 * @param emisorService
	 * @return
	 */
	public static byte[] creaPDFByteArray(List<ComprobanteWrapper> listaCfdi,
			String tipo) {
		byte respuesta[] = null;
		List<PojoReporte> pam = new ArrayList<PojoReporte>();
		try {
			for (ComprobanteWrapper cfdi : listaCfdi) {
				Map<String, Object> map = null;
				PojoReporte padre = null;
				padre = new PojoReporte();
				map = crearMapaParametros(cfdi.getComprobante());
				List<ConceptoReporte> listaReporte = new ArrayList<ConceptoReporte>();
				convertirConceptos(cfdi.getComprobante().getConceptos()
						.getConcepto(), listaReporte);
				ListaSubReportes listaSubreportes = new ListaSubReportes();
				listaSubreportes.setConceptosList(listaReporte);
				padre.addObjeto(listaSubreportes);
				asignarUrlReporteSubReporte(padre, map);
				padre.setDatos(map);
				pam.add(padre);
			}
			JasperReport reporte = (JasperReport) JRLoader
					.loadObjectFromFile("/data/cnbv/archivos/reportePadre.jasper");
			JasperPrint jasperPrint = null;
			jasperPrint = JasperFillManager.fillReport(reporte, null,
					new JRBeanCollectionDataSource(pam));

			if (tipo != null && tipo.equalsIgnoreCase("crearPdf")) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JRExporter exporter = new JRPdfExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT,
						jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
				exporter.exportReport();
				respuesta = baos.toByteArray();
			}
		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println(GlobalUtil.imprimeStackError(e.getStackTrace()));
			// logger.error(GlobalUtil.imprimeStackError(e.getStackTrace()));
		}
		return respuesta;
	}

	/**
	 * Metodo encargado de crear un mapa de objetos el cual sera enviado a una
	 * plantilla .jasper, el mapa sera utilizado para facturas normales
	 * 
	 * @param comprobante
	 * @return
	 * @throws Exception
	 */
	private static Map<String, Object> crearMapaParametros(
			Comprobante comprobante) throws Exception {
		Map<String, Object> mapa = null;
		mapa = new HashMap<String, Object>();
		String calleReceptor = "";
		String noextReceptor = "";
		String nointReceptor = "";
		String colonia = "";
		String municipio = "";
		String estado = "";
		String cp = "";
		String dir = "";
		byte pathImgQr[] = null;
		TimbreFiscalDigital fiscalDigital = (TimbreFiscalDigital) UtileriasCfdi
				.getObjectoComplemento(comprobante.getComplemento().getAny(),
						TimbreFiscalDigital.class);
		pathImgQr = crearQrByte(comprobante, fiscalDigital);
		if (pathImgQr != null) {
			mapa.put("QR", pathImgQr);
		}

		LeyendasFiscales fiscales = (LeyendasFiscales) UtileriasCfdi
				.getObjectoComplemento(comprobante.getComplemento().getAny(),
						LeyendasFiscales.class);
		if (fiscales != null) {
			List<LeyendasFiscales.Leyenda> leyenda = fiscales.getLeyenda();
			if (leyenda != null && !leyenda.isEmpty()) {
				mapa.put("disposicionFiscal", leyenda.get(0)
						.getDisposicionFiscal());
				mapa.put("textoLeyenda", leyenda.get(0).getTextoLeyenda());
			}
		}
		mapa.put("timbreFiscalDigital.selloSAT",
				fiscalDigital.getNoCertificadoSAT());
		mapa.put("comprobante.fecha",
				UtileriasCfdi.YYYYMMDD_HHMMSS.format(comprobante.getFecha()));
		mapa.put("fechaCertificacion", UtileriasCfdi.YYYYMMDD_HHMMSS
				.format(fiscalDigital.getFechaTimbrado()));
		mapa.put("follioFiscal", fiscalDigital.getUUID());
		mapa.put("emisor.emisor", comprobante.getEmisor().getNombre());
		mapa.put("emisor.regimenFiscal", comprobante.getEmisor()
				.getRegimenFiscal().get(0).getRegimen());
		mapa.put("emisor.rfc", comprobante.getEmisor().getRfc());
		mapa.put("noCertificadoSAT", fiscalDigital.getNoCertificadoSAT());
		mapa.put("certificadoEmisor", comprobante.getNoCertificado());
		mapa.put("nota", comprobante.getSerie());

		String domicilioEmisor = null;
		domicilioEmisor = crearDomicilioEmisor(comprobante);
		mapa.put("emisor.domicilioEmisors", domicilioEmisor);
		mapa.put("comprobante.folio", comprobante.getFolio());
		mapa.put("lugar_fecha", comprobante.getLugarExpedicion() + " "
				+ UtileriasCfdi.YYYYMMDD_HHMMSS.format(comprobante.getFecha()));
		// String emisor_calle = "";
		// if (comprobante.getEmisor().getDomicilioFiscal().getCalle() != null)
		// {
		// emisor_calle =
		// comprobante.getEmisor().getDomicilioFiscal().getCalle();
		// }
		// if (comprobante.getEmisor().getDomicilioFiscal().getNoExterior() !=
		// null) {
		// emisor_calle = emisor_calle + " " +
		// comprobante.getEmisor().getDomicilioFiscal().getNoExterior();
		// }
		// if (comprobante.getEmisor().getDomicilioFiscal().getCalle() != null)
		// {
		// emisor_calle = emisor_calle + " " +
		// comprobante.getEmisor().getDomicilioFiscal().getNoExterior()+"
		// "+comprobante.getEmisor().getDomicilioFiscal().getCalle();
		// }
		// if (comprobante.getEmisor().getDomicilioFiscal().getCalle() != null)
		// {
		// emisor_calle = emisor_calle + " " +
		// comprobante.getEmisor().getDomicilioFiscal().getNoExterior()+"
		// "+comprobante.getEmisor().getDomicilioFiscal().getCalle();
		// }
		// if (comprobante.getEmisor().getDomicilioFiscal().getCodigoPostal() !=
		// null) {
		// emisor_calle = emisor_calle + " " +
		// comprobante.getEmisor().getDomicilioFiscal().getNoExterior()+"
		// "+comprobante.getEmisor().getDomicilioFiscal().getCalle()+"
		// "+comprobante.getEmisor().getDomicilioFiscal().getCodigoPostal();
		// }
		// if (comprobante.getEmisor().getDomicilioFiscal().getColonia() !=
		// null) {
		// emisor_calle = emisor_calle + " " +
		// comprobante.getEmisor().getDomicilioFiscal().getNoExterior()+"
		// "+comprobante.getEmisor().getDomicilioFiscal().getCalle()+"
		// "+comprobante.getEmisor().getDomicilioFiscal().getCodigoPostal()+"
		// "+comprobante.getEmisor().getDomicilioFiscal().getColonia() ;
		// }

		// mapa.put("calle_emisor", emisor_calle);
		// mapa.put("cp_emisor",
		// comprobante.getEmisor().getDomicilioFiscal().getCodigoPostal());
		// mapa.put("colonia_emisor",
		// comprobante.getEmisor().getDomicilioFiscal().getColonia());
		// mapa.put("municipio_emisor",
		// comprobante.getEmisor().getDomicilioFiscal().getMunicipio());
		// mapa.put("estado_emisor",
		// comprobante.getEmisor().getDomicilioFiscal().getEstado());
		if (comprobante.getReceptor().getNombre() != null) {
			mapa.put("receptor.nombre", comprobante.getReceptor().getNombre());
		}

		if (comprobante.getReceptor().getDomicilio().getCalle() != null) {
			calleReceptor = comprobante.getReceptor().getDomicilio().getCalle();
		}
		if (comprobante.getReceptor().getDomicilio().getNoExterior() != null) {
			noextReceptor = ", No. ext. "
					+ comprobante.getReceptor().getDomicilio().getNoExterior();
		}
		if (comprobante.getReceptor().getDomicilio().getNoInterior() != null) {
			nointReceptor = ", No. int. "
					+ comprobante.getReceptor().getDomicilio().getNoInterior();
		}
		if (comprobante.getReceptor().getDomicilio().getCodigoPostal() != null) {
			cp = ", Codigo postal. "
					+ comprobante.getReceptor().getDomicilio()
							.getCodigoPostal();
		}
		if (comprobante.getReceptor().getDomicilio().getColonia() != null) {
			colonia = ", Colonia. "
					+ comprobante.getReceptor().getDomicilio().getColonia();
		}
		if (comprobante.getReceptor().getDomicilio().getMunicipio() != null) {
			municipio = ", Municipio. "
					+ comprobante.getReceptor().getDomicilio().getMunicipio();
		}
		if (comprobante.getReceptor().getDomicilio().getEstado() != null) {
			estado = ", Estado. "
					+ comprobante.getReceptor().getDomicilio().getEstado();
		}
		dir = calleReceptor + noextReceptor + nointReceptor + cp + colonia
				+ municipio + estado;
		mapa.put("receptor.rfc", comprobante.getReceptor().getRfc());
		mapa.put("receptor.domicilioRecepors", dir.toUpperCase());
		// mapa.put("cp_receptor",
		// comprobante.getReceptor().getDomicilio().getCodigoPostal());
		// mapa.put("colonia_receptor",
		// comprobante.getReceptor().getDomicilio().getColonia());
		// mapa.put("municipio_receptor",
		// comprobante.getReceptor().getDomicilio().getMunicipio());
		// mapa.put("estado_receptor",
		// comprobante.getReceptor().getDomicilio().getEstado());
		mapa.put(
				"comprobante.subtotal",
				"$"
						+ UtileriasCfdi.formateador.format(comprobante
								.getSubTotal().doubleValue()));
		mapa.put(
				"comprobante.total",
				"$"
						+ UtileriasCfdi.formateador.format(comprobante
								.getTotal().doubleValue()));
		mapa.put("comprobante.moneda", "MXN");
		mapa.put("comprobante.tipoCambio", "1");
		mapa.put("comprobante.formaDePago", comprobante.getFormaDePago());
		agregarImpuestos(mapa, comprobante.getImpuestos());
		mapa.put("timbreFiscalDigital.selloCFD", fiscalDigital.getSelloCFD());
		mapa.put("timbreFiscalDigital.selloSAT", fiscalDigital.getSelloSAT());
		mapa.put(
				"cadenaOriginal",
				"||"
						+ comprobante.getVersion()
						+ "|"
						+ fiscalDigital.getUUID()
						+ "|"
						+ UtileriasCfdi.YYYYMMDD_HHMMSS.format(fiscalDigital
								.getFechaTimbrado()) + "|"
						+ fiscalDigital.getSelloCFD() + "|"
						+ fiscalDigital.getNoCertificadoSAT() + "||");

		mapa.put("noCertificado", fiscalDigital.getNoCertificadoSAT());
		if (comprobante.getCondicionesDePago() != null) {
			mapa.put("condicionesDePago", comprobante.getCondicionesDePago()
					.toUpperCase());
		}

		mapa.put("formaDePago", comprobante.getFormaDePago());
		mapa.put("NumCtaPago", comprobante.getNumCtaPago());

		mapa.put("comprobante.metodoDePago", comprobante.getMetodoDePago()
				.toUpperCase());
		mapa.put("comprobante.tipoDeComprobante", comprobante
				.getTipoDeComprobante().toUpperCase());

		mapa.put("comprobante.numCtaPago", "No Identificado".toUpperCase());
		String expedidoEn = null;
		expedidoEn = crearDomicilio(comprobante);
		mapa.put("comprobante.lugarExpedicion", expedidoEn.toUpperCase());
		mapa.put(
				"TotalEnLetra",
				ConvertidorLetras.convertir(""
						+ comprobante.getTotal().doubleValue(), true));
		return mapa;
	}

	/**
	 * Metodo encargado de convertir y agregar objetos del tipo
	 * Comprobante_Cfdi.Conceptos.Concepto a ConceptoReporte
	 * 
	 * @param conceptos
	 * @param listaReporte
	 */
	public static void convertirConceptos(
			List<Comprobante.Conceptos.Concepto> conceptos,
			List<ConceptoReporte> listaReporte) {
		for (Comprobante.Conceptos.Concepto concepto : conceptos) {
			ConceptoReporte conceptoReporte = new ConceptoReporte();
			conceptoReporte.setCantidad("" + concepto.getCantidad());
			conceptoReporte.setUnidad(concepto.getUnidad());
			conceptoReporte.setDescripcion(concepto.getDescripcion());
			conceptoReporte.setValorUnitario("" + concepto.getValorUnitario());
			conceptoReporte.setImporte("" + concepto.getImporte());
			conceptoReporte.setNoIdentificacion(""
					+ concepto.getNoIdentificacion());
			listaReporte.add(conceptoReporte);
		}
	}

	/**
	 * Metodo encargado de buscar si un emisor tiene una plantilla y
	 * subplatnilla .jasper, en caso de no tener alguna le asigna una por
	 * default
	 * 
	 * @param emisorPdf
	 * @param pojoReporte
	 * @param map
	 * @param plantillasService
	 * @param hasNomina
	 * @throws Exception
	 */
	public static void asignarUrlReporteSubReporte(PojoReporte pojoReporte,
			Map<String, Object> map) throws Exception {
		pojoReporte
				.setUrlReporte("/data/cnbv/archivos/factura_bancaria.jasper");
		map.put("dirSubreporte", "/data/cnbv/archivos/");
		try {
			Path path = Paths.get("/data/cnbv/archivos/cnbv.png");
			byte[] data = Files.readAllBytes(path);
			map.put("logo", data);
		} catch (Exception e) {
		}

	}

	/**
	 * Metodo encargado de crear una Imagen QR .png en un arreglo de bytes
	 * 
	 * @param comprobante
	 * @param fiscalDigital
	 * @return
	 * @throws Exception
	 */
	private static byte[] crearQrByte(Comprobante comprobante,
			TimbreFiscalDigital fiscalDigital) throws Exception {
		byte respuesta[] = null;
		String cadena = null;

		if (comprobante.getEmisor().getRfc() != null
				&& comprobante.getReceptor().getRfc() != null
				&& comprobante.getTotal() != null
				&& fiscalDigital.getUUID() != null) {
			cadena = "?re="
					+ comprobante.getEmisor().getRfc()
					+ "&rr="
					+ comprobante.getReceptor().getRfc()
					+ "&tt="
					+ UtileriasCfdi.formatoQr.format(
							comprobante.getTotal().doubleValue()).toString()
					+ "&id=" + fiscalDigital.getUUID();
		}
		if (cadena != null) {
			respuesta = QrGenerator.generateQrByteArray(cadena, 140, 140);
		}
		return respuesta;
	}

	public static String crearDomicilioEmisor(Comprobante comprobante) {
		String domicilio = "";
		String calle = "";
		String numeroInter = "";
		String numeroext = "";
		String colonia = "";
		String cp = "";
		String municipio = "";
		String estado = "";
		String pais = "";
		if (comprobante.getEmisor().getExpedidoEn() != null) {
			if (comprobante.getEmisor().getExpedidoEn().getCalle() != null) {
				calle = comprobante.getEmisor().getExpedidoEn().getCalle()
						+ ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getNoInterior() != null) {
				numeroInter = "No. int. "
						+ comprobante.getEmisor().getExpedidoEn()
								.getNoInterior() + ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getNoExterior() != null) {
				numeroext = "No. ext. "
						+ comprobante.getEmisor().getExpedidoEn()
								.getNoExterior() + ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getColonia() != null) {
				colonia = comprobante.getEmisor().getExpedidoEn().getColonia()
						+ ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getCodigoPostal() != null) {
				cp = "C. P. "
						+ comprobante.getEmisor().getExpedidoEn()
								.getCodigoPostal() + ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getMunicipio() != null) {
				municipio = comprobante.getEmisor().getExpedidoEn()
						.getMunicipio()
						+ ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getEstado() != null) {
				estado = comprobante.getEmisor().getExpedidoEn().getEstado()
						+ ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getPais() != null) {
				pais = comprobante.getEmisor().getExpedidoEn().getPais() + ".";
			}
			domicilio = calle + numeroext + numeroInter + colonia + cp
					+ municipio + estado + pais;
		}
		return domicilio;
	}

	/**
	 * Metodo encargado de agreagar los respectivos impuestos Traslados y
	 * Retenciones al mapa de objetos
	 * 
	 * @param mapa
	 * @param impuestos
	 * @throws Exception
	 */
	public static void agregarImpuestos(Map<String, Object> mapa,
			Comprobante.Impuestos impuestos) throws Exception {
		double ivaRetenido = 0;
		double isrRetenido = 0;
		double ivaTrasladado = 0;
		if (impuestos.getRetenciones() != null) {
			List<Comprobante.Impuestos.Retenciones.Retencion> retenciones = impuestos
					.getRetenciones().getRetencion();
			if (retenciones != null && !retenciones.isEmpty()) {
				for (Comprobante.Impuestos.Retenciones.Retencion retencion : retenciones) {
					if (retencion.getImpuesto() != null
							&& retencion.getImpuesto().equalsIgnoreCase("IVA")) {
						ivaRetenido = ivaRetenido
								+ retencion.getImporte().doubleValue();
					} else if (retencion.getImporte() != null
							&& retencion.getImpuesto().equalsIgnoreCase("ISR")) {
						isrRetenido = isrRetenido
								+ retencion.getImporte().doubleValue();
					}
				}
			}
		}
		if (impuestos.getTraslados() != null) {
			List<Comprobante.Impuestos.Traslados.Traslado> traslados = impuestos
					.getTraslados().getTraslado();
			if (traslados != null && !traslados.isEmpty()) {
				for (Comprobante.Impuestos.Traslados.Traslado traslado : traslados) {
					ivaTrasladado = ivaTrasladado
							+ traslado.getImporte().doubleValue();
				}
			}
		}
		// mapa.put("Comprobante_Ivatrasladado", "$" +
		// Utils.formateador.format(ivaTrasladado));
		// mapa.put("Comprobante_Ivaretenido", "$" +
		// Utils.formateador.format(ivaRetenido));
		// mapa.put("Comprobante_IsrRetenido", "$" +
		// Utils.formateador.format(isrRetenido));
		mapa.put("ivatrasladado",
				"$" + UtileriasCfdi.formateador.format(ivaTrasladado));
		mapa.put("ivaretenido",
				"$" + UtileriasCfdi.formateador.format(ivaRetenido));
		mapa.put("isrRetenido",
				"$" + UtileriasCfdi.formateador.format(isrRetenido));
	}

	/**
	 * Metodo encargado de crear un String con el domicilio de un Emisor
	 * 
	 * @param comprobante
	 * @return
	 */
	private static String crearDomicilio(Comprobante comprobante) {
		String expedidoEn = "";
		String calle = "";
		String numeroInter = "";
		String numeroext = "";
		String colonia = "";
		String cp = "";
		String municipio = "";
		String estado = "";
		String pais = "";
		if (comprobante.getEmisor().getExpedidoEn() != null) {
			if (comprobante.getEmisor().getExpedidoEn().getCalle() != null) {
				calle = comprobante.getEmisor().getExpedidoEn().getCalle()
						+ ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getNoInterior() != null) {
				numeroInter = "No. int. "
						+ comprobante.getEmisor().getExpedidoEn()
								.getNoInterior() + ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getNoExterior() != null) {
				numeroext = "No. ext. "
						+ comprobante.getEmisor().getExpedidoEn()
								.getNoExterior() + ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getColonia() != null) {
				colonia = comprobante.getEmisor().getExpedidoEn().getColonia()
						+ ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getCodigoPostal() != null) {
				cp = "C. P. "
						+ comprobante.getEmisor().getExpedidoEn()
								.getCodigoPostal() + ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getMunicipio() != null) {
				municipio = comprobante.getEmisor().getExpedidoEn()
						.getMunicipio()
						+ ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getEstado() != null) {
				estado = comprobante.getEmisor().getExpedidoEn().getEstado()
						+ ", ";
			}
			if (comprobante.getEmisor().getExpedidoEn().getPais() != null) {
				pais = comprobante.getEmisor().getExpedidoEn().getPais() + ".";
			}
			expedidoEn = calle + numeroext + numeroInter + colonia + cp
					+ municipio + estado + pais;
		} else {
			if (comprobante.getEmisor().getDomicilioFiscal().getCalle() != null) {
				calle = comprobante.getEmisor().getDomicilioFiscal().getCalle()
						+ ", ";
			}
			if (comprobante.getEmisor().getDomicilioFiscal().getNoInterior() != null) {
				numeroInter = "No. int. "
						+ comprobante.getEmisor().getDomicilioFiscal()
								.getNoInterior() + ", ";
			}
			if (comprobante.getEmisor().getDomicilioFiscal().getNoExterior() != null) {
				numeroext = "No. ext. "
						+ comprobante.getEmisor().getDomicilioFiscal()
								.getNoExterior() + ", ";
			}
			if (comprobante.getEmisor().getDomicilioFiscal().getColonia() != null) {
				colonia = comprobante.getEmisor().getDomicilioFiscal()
						.getColonia()
						+ ", ";
			}
			if (comprobante.getEmisor().getDomicilioFiscal().getCodigoPostal() != null) {
				cp = "C. P. "
						+ comprobante.getEmisor().getDomicilioFiscal()
								.getCodigoPostal() + ", ";
			}
			if (comprobante.getEmisor().getDomicilioFiscal().getMunicipio() != null) {
				municipio = comprobante.getEmisor().getDomicilioFiscal()
						.getMunicipio()
						+ ", ";
			}
			if (comprobante.getEmisor().getDomicilioFiscal().getEstado() != null) {
				estado = comprobante.getEmisor().getDomicilioFiscal()
						.getEstado()
						+ ", ";
			}
			if (comprobante.getEmisor().getDomicilioFiscal().getPais() != null) {
				pais = comprobante.getEmisor().getDomicilioFiscal().getPais()
						+ ".";
			}
			expedidoEn = calle + numeroext + numeroInter + colonia + cp
					+ municipio + estado + pais;
		}
		return expedidoEn;
	}

}
