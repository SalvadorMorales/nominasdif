package com.mx.quadrum.cfdi.utilerias;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PojoReporte {
	private Map<String, Object> datos = new HashMap<String, Object>();
	private String urlReporte;
	private List<Object> listaObjetos = new ArrayList<Object>();

	public void setDatos(Map<String, Object> datos) {
		this.datos = datos;
	}

	public Map<String, Object> getDatos() {
		return datos;
	}

	public void setUrlReporte(String urlReporte) {
		this.urlReporte = urlReporte;
	}

	public String getUrlReporte() {
		return urlReporte;
	}

	public List<Object> getListaObjetos() {
		return listaObjetos;
	}

	public void setListaObjetos(List<Object> listaObjetos) {
		this.listaObjetos = listaObjetos;
	}

	public void addObjeto(Object FC) {
		this.listaObjetos.add(FC);
	}

}
