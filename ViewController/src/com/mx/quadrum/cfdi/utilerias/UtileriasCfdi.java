package com.mx.quadrum.cfdi.utilerias;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.poi.ss.usermodel.Workbook;

public class UtileriasCfdi {

	/**
	 * Atributo estatico para la codificacion UTF-8
	 */
	public static final String UTF8 = "UTF-8";

	/**
	 * Atributo estatico para la extencion de un archivo .xml
	 */
	public static final String EXTENCION_XML = ".xml";

	public static final String EXTENCION_PDF = ".pdf";

	public static final String DIAGONAL = "/";

	public static final String SALTO_LINEA = "\n";

	public static final String SEPARADOR_LAYOUT_NOMINA = "=====NuevoNomina12=====";

	public static final String TIPO_CONTENIDO_ZIP = "application/zip";

	public static final DecimalFormat FORMATO_QR = new DecimalFormat(
			"###########0000000000.000000");
	public static final SimpleDateFormat FORMATOFECHA = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	/**
	 * ###,###.00.
	 */
	public static DecimalFormat formateador = new DecimalFormat("###,###.00");

	/**
	 * yyyy-MM-dd HH:mm:ss.
	 */
	public static DateFormat YYYYMMDD_HHMMSS = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss", new Locale("es", "MX"));

	public static DateFormat YYYYMMDDTHHMMSS = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss", new Locale("es", "MX"));

	/**
	 * ###########0000000000.000000
	 */
	public static DecimalFormat formatoQr = new DecimalFormat(
			"###########0000000000.000000");

	/**
	 * Metodo encargado de convertir un arreglo de byte provediente de un
	 * archivo en formato XML a un objeto, el cual este usando JAXB
	 * 
	 * @param arrayXml
	 * @param clases
	 * @return
	 * @throws Exception
	 */

	public static final String VERSION_10 = "1.0";
	public static final String VERSION_11 = "1.1";
	public static final String VERSION_32 = "3.2";
	public static final String VERSION_33 = "3.3";

	public static <T> T convertirAComprobante(final byte[] arrayXml,
			final Class[] clases) throws Exception {
		if (arrayXml == null) {
			throw new NullPointerException(
					"El arreglo de bytes obtenido de la cadena XML o Archivo son nullos");
		}
		if (arrayXml.length == 0) {
			throw new Exception("El xml es vacio");
		}
		T comprobante = null;
		InputStream stream = new ByteArrayInputStream(arrayXml);
		JAXBContext context = JAXBContext.newInstance(clases);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		comprobante = (T) unmarshaller.unmarshal(stream);
		return comprobante;

	}

	/**
	 * Metodo encargado de convertir una cadena XML a un objeto, siempre y
	 * cuando este usando JAXB
	 * 
	 * @param xml
	 * @param clases
	 * @return
	 * @throws Exception
	 */
	public static <T> T convertirAComprobante(final String xml,
			final Class[] clases) throws Exception {
		if (xml == null) {
			throw new NullPointerException("El String Xml es nullo");
		}
		return convertirAComprobante(xml.getBytes(UTF8), clases);
	}

	/**
	 * Metodo encargado de leer un archivo XML y obtener sus bytes para
	 * convertirlo en un objeto JAXB
	 * 
	 * @param fileXml
	 * @param clases
	 * @return
	 * @throws Exception
	 */
	public static <T> T convertirAComprobante(final File xmlFile,
			final Class[] clases) throws Exception {
		return convertirAComprobante(
				obtenerArregloBytesArchivo(xmlFile, ".xml"), clases);
	}

	/**
	 * Metodo encargado de buscar un objeto dentro del Nodo cfdi:Complemento
	 * puede retornar el complemento nomina:Nomina, tfd:TimbreFiscalDigital
	 * 
	 * @param any
	 * @param class1
	 * @return
	 * @throws Exception
	 */
	public static <T> T getObjectoComplemento(List<T> any, Class<?> class1)
			throws Exception {
		for (T object : any) {
			if (class1.isInstance(object)) {
				return object;
			}
		}
		return null;
	}

	/**
	 * Obtiene el arreglo de bytes de un archivo, valida la extencion del
	 * archivo
	 * 
	 * @param xmlFile
	 * @param extencion
	 * @return
	 * @throws Exception
	 */
	public static byte[] obtenerArregloBytesArchivo(final File xmlFile,
			final String extencion) throws Exception {
		if (xmlFile == null) {
			throw new NullPointerException("El archivo es nullo");
		}
		if (!xmlFile.exists()) {
			throw new FileNotFoundException("El archivo " + xmlFile.getPath()
					+ " no existe");
		}
		if (!xmlFile.getName().endsWith(extencion)) {
			throw new Exception("El archivo no es un "
					+ extencion.toUpperCase());
		}
		if (extencion == null || extencion.isEmpty()) {
			throw new Exception(
					"La extencion del archivo no debe de ser nula o vacia");
		}
		byte[] bytesArray = new byte[(int) xmlFile.length()];
		FileInputStream fis = new FileInputStream(xmlFile);
		fis.read(bytesArray);
		fis.close();

		return bytesArray;
	}

	/**
	 * Metodo encargado de armar una direccion del sistema operativo en la cual
	 * se guardara un archivo .xml <br/>
	 * La estructura seria la siguiente: <br/>
	 * /raizDirectorio/rfcEmisorXml/anio_de_la_fecha_enviada/
	 * mes_de_la_fecha_enviada/dia_de_la_fecha_enviada/hora_de_la_fecha_enviada/
	 * uuidXml.xml
	 * 
	 * @param raizDirectorio
	 * @param fechaCreacionXml
	 * @param rfcEmisorXml
	 * @param uuidXml
	 * @return
	 * @throws Exception
	 */
	public static String crearDireccionGuardadoXml(String raizDirectorio,
			Date fechaCreacionXml, String rfcEmisorXml, String uuidXml)
			throws Exception {
		DateFormat formato = new SimpleDateFormat("dd-MMMM-yyyy-HH",
				new Locale("es", "MX"));
		String arregloFecha[] = formato.format(fechaCreacionXml).split("-");
		String ruta = raizDirectorio + rfcEmisorXml + "/" + arregloFecha[2]
				+ "/" + arregloFecha[1] + "/" + arregloFecha[0] + "/"
				+ arregloFecha[3] + "/" + uuidXml + EXTENCION_XML;
		File rutaFile = new File(ruta);
		if (rutaFile.exists()) {

		}
		return ruta;
	}

	/**
	 * Metodo encargado de guardar un archivo en el S.O., recibe el path del
	 * archivo
	 * 
	 * @param pathArchivo
	 * @param contenidoXML
	 */
	public static void guardarArchivoXml(String pathArchivo, String contenidoXML) {
		try {
			crearDireccionCarpetas(pathArchivo);
			Writer write = null;
			write = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(pathArchivo), UTF8));
			write.write(contenidoXML);
			write.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void crearDireccionCarpetas(String pathArchivo)
			throws Exception {
		if (pathArchivo != null) {
			StringBuilder builder = new StringBuilder();
			builder.append(DIAGONAL);
			String arregloPath[] = pathArchivo.trim().split(DIAGONAL);
			for (String string : arregloPath) {
				if (!string.endsWith(EXTENCION_XML)) {
					builder.append(string + DIAGONAL);
				}
			}
			crearCarpetas(builder.toString());
		}
	}

	/**
	 * Metodo que sirve para crear carpetas , recibe una direccion y el nombre
	 * de la carpeta, si existe no hace nada de lo contrario la cre
	 */
	public static void crearCarpetas(String direccion) {
		try {
			File folder = new File(direccion);
			if (!folder.exists()) {
				folder.mkdirs();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que se encarga de crear un archivo zip apartir de una lista de
	 * objetos con la siguiente estructura
	 * <ul>
	 * <li>String nombre Archivo que es el nombre del archivo (.xml,.txt.doc,
	 * etc)</li>
	 * <li>byte[] Bytes del Archivo que es el arreglo de bytes del archivo</li>
	 * </ul>
	 * 
	 * 
	 * @since version 1
	 * @param list
	 * @return Arreglo de bytes (.zip)
	 * @throws IOException
	 */
	public static byte[] crearZipListaBytesArchivo(List<BytesArchivo> list)
			throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(baos);
		for (BytesArchivo xmlBytes : list) {
			ZipEntry entry = new ZipEntry(xmlBytes.getNombreArchivo());
			entry.setSize(xmlBytes.getBytesArchivo().length);
			zos.putNextEntry(entry);
			zos.write(xmlBytes.getBytesArchivo());
		}
		zos.closeEntry();
		zos.close();
		return baos.toByteArray();
	}

	/**
	 * 
	 * @param workbook
	 * @return
	 * @throws Exception
	 */
	public static byte[] exelByteArray(Workbook workbook) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		workbook.write(baos);
		baos.flush();
		baos.close();
		return baos.toByteArray();
	}

	/**
	 * Metodo que imprime el Stack de una Exception
	 * 
	 * @param stackTraceElements
	 *            parametro que trae el Stack de la Exception
	 * @return builder retorna el valor en un String
	 * @since Incluido desde la version 1
	 */
	public static String imprimeStackError(
			StackTraceElement[] stackTraceElements) {
		StringBuilder builder = new StringBuilder();
		for (StackTraceElement line : stackTraceElements) {
			builder.append(line.toString());
		}
		return builder.toString();
	}

}
