package com.mx.quadrum.cfdi.utilerias;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "cfdi:Concepto")
public class ConceptoReporte implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String cantidad;
	private String unidad;
	private String noIdentificacion;
	private String descripcion;
	private String valorUnitario;
	private String importe;
	private double impuestoIva;
	private String numero;
	private String fecha;
	private String aduana;

	@XmlAttribute
	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	@XmlAttribute
	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	@XmlAttribute
	public String getNoIdentificacion() {
		return noIdentificacion;
	}

	public void setNoIdentificacion(String noIdentificacion) {
		this.noIdentificacion = noIdentificacion;
	}

	@XmlAttribute
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@XmlAttribute
	public String getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(String valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	@XmlAttribute
	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	@XmlAttribute
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@XmlAttribute
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	@XmlAttribute
	public String getAduana() {
		return aduana;
	}

	public void setAduana(String aduana) {
		this.aduana = aduana;
	}

	@XmlTransient
	public double getImpuestoIva() {
		return impuestoIva;
	}

	public void setImpuestoIva(double impuestoIva) {
		this.impuestoIva = impuestoIva;
	}

}