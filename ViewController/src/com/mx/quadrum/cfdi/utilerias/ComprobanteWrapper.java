package com.mx.quadrum.cfdi.utilerias;

import com.mx.quadrum.cfdi.v32.schema.Comprobante;

public class ComprobanteWrapper {
	/**
	 * 
	 */
	private static final long serialVersionUID = 154825139917535603L;
	private String pathComprobante;
	private Comprobante comprobante;

	public String getPathComprobante() {
		return pathComprobante;
	}

	public void setPathComprobante(String pathComprobante) {
		this.pathComprobante = pathComprobante;
	}

	public Comprobante getComprobante() {
		return comprobante;
	}

	public void setComprobante(Comprobante comprobante) {
		this.comprobante = comprobante;
	}

}
