//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.01.05 a las 02:29:55 PM CST 
//

package com.mx.quadrum.cfdi.v33.schema;

import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter1 extends XmlAdapter<String, Date> {

	public Date unmarshal(String value) {
		return (com.mx.quadrum.cfdi.binder.DateTimeCustomBinder.parseDateTime(value));
	}

	public String marshal(Date value) {
		return (com.mx.quadrum.cfdi.binder.DateTimeCustomBinder.printDateTime(value));
	}

}
