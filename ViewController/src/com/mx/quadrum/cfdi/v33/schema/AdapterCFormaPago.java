package com.mx.quadrum.cfdi.v33.schema;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import com.mx.quadrum.cfdi.catalogos.catcfdi.CFormaPago;

public class AdapterCFormaPago extends XmlAdapter<String, CFormaPago> {

	@Override
	public CFormaPago unmarshal(String v) throws Exception {
		CFormaPago cFormaPago = null;
		try {
			cFormaPago = CFormaPago.getByName(v);
		} catch (Exception e) {
			cFormaPago = null;
		}
		if (null == cFormaPago) {
			cFormaPago = CFormaPago.NOTFOUND;
		}
		return cFormaPago;
	}

	@Override
	public String marshal(CFormaPago v) throws Exception {
		if (null == v) {
			return null;
		} else {
			return CFormaPago.getValueByName(v.getValue());
		}

	}

}
