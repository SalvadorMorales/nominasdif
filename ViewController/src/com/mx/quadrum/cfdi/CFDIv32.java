package com.mx.quadrum.cfdi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.ssl.PKCS8Key;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mx.quadrum.cfdi.common.NamespacePrefixMapperImpl;
import com.mx.quadrum.cfdi.common.URIResolverImpl;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.v32.schema.Comprobante;
import com.mx.quadrum.cfdi.v32.schema.Comprobante.Complemento;
import com.mx.quadrum.cfdi.validador.JaxbValidador;
import com.mx.quadrum.cfdi.validador.SchemaValidatorXsd;

public class CFDIv32 {

	
	private static final String XSLT = "/xslt/v32/cadenaoriginal_3_2.xslt";
	private static final String XSD = "xsd/v32/cfdv32.xsd";
	private final Comprobante documento;
	private final byte[] archivoCertificadoEmisorByteArray;
	private final byte[] archivoKeyEmisorByteArray;
	private final String clavePrivada;

	private static final Class CLASE_BASE = Comprobante.class;
	private final JAXBContext contexto;
	private final Map<String, String> prefijosLocales = Maps
			.newHashMap(PREFIXES);

	private static final ImmutableMap<String, String> PREFIXES = ImmutableMap
			.of("http://www.w3.org/2001/XMLSchema-instance", "xsi",
					"http://www.sat.gob.mx/cfd/3", "cfdi",
					"http://www.sat.gob.mx/TimbreFiscalDigital", "tfd");

	public CFDIv32(Comprobante comprobante,
			byte[] archivoCertificadoEmisorByteArray,
			byte[] archivoKeyEmisorByteArray, String clavePrivada)
			throws Exception {
		this.documento = comprobante;
		this.archivoCertificadoEmisorByteArray = archivoCertificadoEmisorByteArray;
		this.archivoKeyEmisorByteArray = archivoKeyEmisorByteArray;
		this.clavePrivada = clavePrivada;
		this.contexto = obtenerContextos(comprobante);
	}

	public CFDIv32(Comprobante comprobante) throws Exception {
		this.documento = comprobante;
		this.archivoCertificadoEmisorByteArray = null;
		this.archivoKeyEmisorByteArray = null;
		this.clavePrivada = null;
		this.contexto = obtenerContextos(comprobante);
	}

	/**
	 * Obtiene las objetos del elemento any de la clase Complemento, recorre el
	 * objeto any y obtiene las clases de cada unos de los complementos
	 * agregados al CFDI
	 * 
	 * @param comprobante
	 * @return
	 * @throws Exception
	 */
	private static JAXBContext obtenerContextos(Comprobante comprobante)
			throws Exception {
		Complemento complemento = comprobante.getComplemento();
		List<Class> clases = new ArrayList<Class>();
		if (complemento != null) {
			List<Object> compementos = complemento.getAny();
			for (Object object : compementos) {
				clases.add(object.getClass());
			}
		}
		Class arrayClasses[] = new Class[clases.size()];
		arrayClasses = clases.toArray(arrayClasses);
		return getContext(arrayClasses);
	}

	/**
	 * Constructor utilizado solo para crear una instancia, al crear una
	 * instancia de este constructor solo podras utilizar los metodos de
	 * timbrado y de cancelacion, siempre y cuando el XML ya haya sido validado
	 * y este sellado, de lo contrario te retornara un error de timbrado
	 */
	public CFDIv32() {
		this.documento = null;
		this.archivoCertificadoEmisorByteArray = null;
		this.archivoKeyEmisorByteArray = null;
		this.clavePrivada = null;
		this.contexto = null;
	}

	public void addNamespace(String uri, String prefix) {
		prefijosLocales.put(uri, prefix);
	}

	private void validarContraXSD() throws Exception {
		StringBuilder stringBuilder =new StringBuilder();
		SchemaValidatorXsd.validacionContraXsd(XSD).validate(new JAXBSource(contexto, documento),stringBuilder);
		if(stringBuilder.length()>0){
			throw new Exception("Error de  validacion contra XSD: " + stringBuilder.toString());
		}
	}

	private void validarElementosRequeridos() throws Exception {
		StringBuilder errors = new StringBuilder();
		JaxbValidador.validarRequeridos(documento, errors);
		if (errors.length() > 0) {
			throw new Exception("Elementos requeridos: " + errors.toString());
		}
	}

	public String sellarObtenerComprobanteEnString() throws Exception {
		String respuesta = null;
		asignarCertificadoNumeroCertificado();
		String xmlSinsellar = null;
		xmlSinsellar = convertirComprobanteEnString();
		InputStream inputXmlSinSellar = null;
		inputXmlSinSellar = stringToUTF8InputStream(xmlSinsellar);
		byte[] cadenaOriginal = null;
		cadenaOriginal = formarCadenaOriginalByteArray(inputXmlSinSellar);
		generarSelloDigital(cadenaOriginal);
		validarElementosRequeridos();
		validarContraXSD();
		respuesta = convertirComprobanteEnString();
		return respuesta;
	}

	/**
	 * 
	 * Metodo encargado de convertir un Comprobante a un String en formato XML
	 * 
	 * @author marco antonio carrasco comonfort
	 * @param object
	 * @param klass
	 * @return
	 */
	public String convertirComprobanteEnString() throws Exception {
		StringWriter stringWriter = new StringWriter();
		final Marshaller m = contexto.createMarshaller();
		m.setProperty("com.sun.xml.bind.namespacePrefixMapper",
				new NamespacePrefixMapperImpl(prefijosLocales));
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty(
				Marshaller.JAXB_SCHEMA_LOCATION,
				"http://www.sat.gob.mx/cfd/3  "
						+ "http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd");
		m.marshal(documento, stringWriter);
		return stringWriter.toString();
	}

	private void asignarCertificadoNumeroCertificado() throws Exception {
		CertificateFactory cf = CertificateFactory.getInstance("X509");
		X509Certificate c = (X509Certificate) cf
				.generateCertificate(new ByteArrayInputStream(
						archivoCertificadoEmisorByteArray));
		String certificadoString = "";
		Base64 b64 = new Base64(-1);
		certificadoString = b64.encodeToString(c.getEncoded());
		BigInteger byteArray = c.getSerialNumber();
		documento.setNoCertificado(new String(byteArray.toByteArray()));
		documento.setCertificado(certificadoString);
	}

	/**
	 * M�todo que genera la cadena original del XML utilizando la hoja de
	 * transformaci�n dada por el SAT
	 *
	 * @param xml
	 *            Documento XML
	 * @param xslt
	 *            Hoja de transformaci�n
	 * @return Cadena original del documento XML
	 */
	private byte[] formarCadenaOriginalByteArray(InputStream xml)
			throws Exception {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		StreamSource xmlFile = new StreamSource(xml);
		TransformerFactory xsltFactory = TransformerFactory.newInstance();
		xsltFactory.setURIResolver(new URIResolverImpl());
		Transformer transformer = xsltFactory.newTransformer(new StreamSource(
				this.getClass().getResourceAsStream(XSLT)));
		transformer.transform(xmlFile, new StreamResult(output));
		return output.toByteArray();
	}

	/**
	 * Convierte una cadena a un InputStream, codificado en UTF-8
	 * 
	 * @param cadena
	 * @return
	 */
	private InputStream stringToUTF8InputStream(String cadena) throws Exception {
		return new ByteArrayInputStream(cadena.getBytes(UtileriasCfdi.UTF8));
	}

	/**
	 * M�todo que genera el sello digital implementando la firma SHA1withRSA
	 *
	 * @return Sello generado para el comprobante
	 */
	private void generarSelloDigital(byte[] cadenaOriginal) throws Exception {
		PKCS8Key pkcs8 = new PKCS8Key(archivoKeyEmisorByteArray,
				clavePrivada.toCharArray());
		PrivateKey privateKey = pkcs8.getPrivateKey();
		Signature firma = Signature.getInstance("SHA1withRSA");
		firma.initSign(privateKey);
		firma.update(cadenaOriginal);
		byte[] signed = firma.sign();
		Base64 base64 = new Base64(-1);
		documento.setSello(base64.encodeToString(signed));
	}

	private static JAXBContext getContext(Class... contexts) throws Exception {
		List<Class> ctx = Lists.asList(CLASE_BASE, contexts);
		Class[] arregloFinal = ctx.toArray(new Class[ctx.size()]);
		return JAXBContext.newInstance(arregloFinal);
	}

}
