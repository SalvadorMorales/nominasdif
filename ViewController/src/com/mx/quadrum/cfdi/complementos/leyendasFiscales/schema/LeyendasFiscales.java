//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.01.25 a las 03:40:57 PM CST 
//


package com.mx.quadrum.cfdi.complementos.leyendasFiscales.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Leyenda" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="disposicionFiscal">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;whiteSpace value="collapse"/>
 *                       &lt;minLength value="1"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="norma">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;whiteSpace value="collapse"/>
 *                       &lt;minLength value="1"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="textoLeyenda" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;minLength value="1"/>
 *                       &lt;whiteSpace value="collapse"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" fixed="1.0" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "leyenda"
})
@XmlRootElement(name = "LeyendasFiscales")
public class LeyendasFiscales {

    @XmlElement(name = "Leyenda", required = true)
    protected List<LeyendasFiscales.Leyenda> leyenda;
    @XmlAttribute(name = "version", required = true)
    protected String version;

    /**
     * Gets the value of the leyenda property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the leyenda property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLeyenda().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LeyendasFiscales.Leyenda }
     * 
     * 
     */
    public List<LeyendasFiscales.Leyenda> getLeyenda() {
        if (leyenda == null) {
            leyenda = new ArrayList<LeyendasFiscales.Leyenda>();
        }
        return this.leyenda;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        if (version == null) {
            return "1.0";
        } else {
            return version;
        }
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="disposicionFiscal">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;whiteSpace value="collapse"/>
     *             &lt;minLength value="1"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="norma">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;whiteSpace value="collapse"/>
     *             &lt;minLength value="1"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="textoLeyenda" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;minLength value="1"/>
     *             &lt;whiteSpace value="collapse"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Leyenda {

        @XmlAttribute(name = "disposicionFiscal")
        protected String disposicionFiscal;
        @XmlAttribute(name = "norma")
        protected String norma;
        @XmlAttribute(name = "textoLeyenda", required = true)
        protected String textoLeyenda;

        /**
         * Obtiene el valor de la propiedad disposicionFiscal.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisposicionFiscal() {
            return disposicionFiscal;
        }

        /**
         * Define el valor de la propiedad disposicionFiscal.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisposicionFiscal(String value) {
            this.disposicionFiscal = value;
        }

        /**
         * Obtiene el valor de la propiedad norma.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNorma() {
            return norma;
        }

        /**
         * Define el valor de la propiedad norma.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNorma(String value) {
            this.norma = value;
        }

        /**
         * Obtiene el valor de la propiedad textoLeyenda.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTextoLeyenda() {
            return textoLeyenda;
        }

        /**
         * Define el valor de la propiedad textoLeyenda.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTextoLeyenda(String value) {
            this.textoLeyenda = value;
        }

    }

}
