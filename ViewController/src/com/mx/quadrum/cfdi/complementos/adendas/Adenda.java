package com.mx.quadrum.cfdi.complementos.adendas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Addenda")
@XmlAccessorType(XmlAccessType.FIELD)
public class Adenda {
    
}
