@javax.xml.bind.annotation.XmlSchema (namespace="http://www.example.org/Schema-Ejemplo",              
    xmlns = {
            @javax.xml.bind.annotation.XmlNs(prefix = "Addenda", 
                     namespaceURI="http://www.example.org/Schema-Ejemplo")           
           },        
    elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED        
)
package com.mx.quadrum.cfdi.complementos.adendas;