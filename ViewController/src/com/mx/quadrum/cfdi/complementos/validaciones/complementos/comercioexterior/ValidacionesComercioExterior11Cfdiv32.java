package com.mx.quadrum.cfdi.complementos.validaciones.complementos.comercioexterior;

import java.util.ArrayList;
import java.util.List;

import com.mx.quadrum.cfdi.catalogos.catcfdi.CMoneda;
import com.mx.quadrum.cfdi.v32.schema.Comprobante.Conceptos.Concepto;

public class ValidacionesComercioExterior11Cfdiv32 {

	/**
	 * Metodo encargado de hacer las respectivas validaciones con respecto al
	 * complemento Comercio Exterior asi como las validaciones de relacion entre
	 * Cfdiv32 y el Complemento Comercio Exterior 1.1
	 * 
	 * @param cfdi
	 * @param complementos
	 * @param builder
	 */
	public static void validacionesComercioExterior(
			final com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi,
			final List<Object> complementos, final StringBuilder builder) {
		boolean respuesta = true;
		int contador = 1;
		while (respuesta) {
			respuesta = reglasValidacionComplementoComercioExterior(cfdi,
					complementos, contador, builder);
			contador++;
		}
	}

	private static boolean reglasValidacionComplementoComercioExterior(
			final com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi,
			final List<Object> complementos, int opcion, StringBuilder builder) {
		boolean respuesta = false;
		switch (opcion) {
		case 1:
			respuesta = validarCfdiv32ComercioExterior11(cfdi, builder);
			break;

		case 2:
			respuesta = validarSoloUnComplementoComercioExterior(complementos,
					builder);
			break;

		default:
			// respuesta=true;
			break;
		}
		return respuesta;
	}

	private static boolean validarCfdiv32ComercioExterior11(
			final com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi,
			final StringBuilder builder) {
		boolean respuesta = false;
		int contador = 1;
		while (!respuesta) {
			respuesta = reglasValidacionCfdiv32ComplementoComercioExterior11(
					cfdi, builder, contador);
			contador++;
		}
		return respuesta;
	}

	public static boolean reglasValidacionCfdiv32ComplementoComercioExterior11(
			final com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi,
			final StringBuilder builder, final int caso) {
		boolean respuesta = false;
		switch (caso) {
		case 1:
			respuesta = validarFechaComprobante(cfdi, builder);
			break;

		case 2:
			respuesta = validarSubtotal(cfdi, builder);
			break;

		case 3:
			respuesta = validarMoneda(cfdi, builder);
			break;
		default:
			respuesta=true;
			break;
		}
		return respuesta;
	}

	/**
	 * 2 El atributo cfdi:Comprobante:fecha debe cumplir con el patrón
	 * [0-9]{4}-(
	 * 0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5
	 * ][0-9]:[0-5][0-9]) y debe corresponder con la hora local donde se expide
	 * el comprobante. CCE102 El atributo cfdi:Comprobante:fecha no cumple con
	 * el formato requerido.
	 * 
	 * @return
	 */
	private static boolean validarFechaComprobante(
			final com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi,
			final StringBuilder builder) {
		boolean respuesta = false;
		if (cfdi.getFecha() == null) {
			respuesta = true;
			builder.append("El atributo cfdi:Comprobante:fecha debe cumplir con el patrón [0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]) \n");
		}
		return respuesta;
	}

	/**
	 * 3 El atributo cfdi:Comprobante:subtotal debe ser igual a la suma de los
	 * atributos importe por cada nodo Concepto ubicado en el nodo
	 * cfdi:Comprobante:Conceptos. CCE103 El atributo cfdi:Comprobante:subtotal
	 * no coincide con la suma de los atributos importe de los nodos Concepto.
	 * 
	 * @return
	 */
	public static boolean validarSubtotal(
			final com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi,
			final StringBuilder builder) {
		boolean respuesta = false;
		if (cfdi.getConceptos() != null) {
			if (cfdi.getConceptos().getConcepto() != null
					&& cfdi.getConceptos().getConcepto() != null
					&& !cfdi.getConceptos().getConcepto().isEmpty()) {
				double subtotal = 0;
				for (Concepto concepto : cfdi.getConceptos().getConcepto()) {
					subtotal = subtotal + concepto.getImporte().doubleValue();
				}
				if (cfdi.getSubTotal().doubleValue() != subtotal) {
					respuesta = true;
					builder.append("El atributo cfdi:Comprobante:subtotal debe ser igual a la suma de los atributos importe por cada nodo Concepto ubicado en el nodo cfdi:Comprobante:Conceptos \n");
				}
			}
		}
		return respuesta;
	}

	/**
	 * 4 El atributo cfdi:Comprobante:Moneda es requerido. CCE104 El atributo
	 * cfdi:Comprobante:Moneda se debe registrar
	 * 
	 * 5 El atributo cfdi:Comprobante:Moneda, debe contener un valor del
	 * catálogo catCFDI:c_Moneda. CCE105 El atributo cfdi:Comprobante:Moneda no
	 * contiene un valor del catálogo catCFDI:c_Moneda.
	 * 
	 * @return
	 */
	public static boolean validarMoneda(
			final com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi,
			final StringBuilder builder) {
		boolean respuesta = false;
		if (cfdi.getMoneda() == null || cfdi.getMoneda().isEmpty()) {
			respuesta = true;
			builder.append("El atributo cfdi:Comprobante:Moneda se debe registrar \n");
		}

		try {
			CMoneda.fromValue(cfdi.getMoneda());			
		} catch (Exception e) {
			respuesta = true;
			builder.append("El atributo cfdi:Comprobante:Moneda no contiene un valor del catálogo catCFDI:c_Moneda. \n");
		}
		return respuesta;
	}

	/**
	 * 53 El nodo cce11:ComercioExterior solo debe registrarse una vez. CCE153
	 * El nodo cce11:ComercioExterior no puede registrarse mas de una vez.
	 */
	private static boolean validarSoloUnComplementoComercioExterior(
			final List<Object> complementos, final StringBuilder builder) {
		boolean respuesta = false;
		List<com.mx.quadrum.cfdi.complementos.comercioExterior.v11.schema.ComercioExterior> listaComercioExterior = new ArrayList<com.mx.quadrum.cfdi.complementos.comercioExterior.v11.schema.ComercioExterior>();
		for (Object object : complementos) {
			if (object instanceof com.mx.quadrum.cfdi.complementos.comercioExterior.v11.schema.ComercioExterior) {
				listaComercioExterior
						.add((com.mx.quadrum.cfdi.complementos.comercioExterior.v11.schema.ComercioExterior) object);
			}
		}
		if (listaComercioExterior.size() > 1) {
			respuesta = true;
			builder.append("No deben de existir mas de 1 complemento comercio exterior en el CFDI \n");
		}
		return respuesta;
	}
}
