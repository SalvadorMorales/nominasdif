package com.mx.quadrum.cfdi.complementos.validaciones;

import java.util.List;

import com.mx.quadrum.cfdi.complementos.validaciones.complementos.comercioexterior.ValidacionesComercioExterior11Cfdiv32;

public class ValidacionComplementosCfdi {

	public static <T> boolean validarSemanticaCfdi(final T comprobante)
			throws Exception {
		Boolean respuesta = false;
		StringBuilder builder = new StringBuilder();
		obtenerComprobante(comprobante, builder);
		if (!builder.toString().isEmpty()) {
			throw new Exception("Error: " + builder.toString());
		}
		return respuesta;
	}

	private static <T> void obtenerComprobante(final T comprobante,
			final StringBuilder builder) {
		if (comprobante instanceof com.mx.quadrum.cfdi.v32.schema.Comprobante) {
			validacionesCFDIv32(comprobante, builder);

		}
		if (comprobante instanceof com.mx.quadrum.cfdi.v33.schema.Comprobante) {
			validacionesCFDIv33(comprobante);
		}
	}

	private static <T> void validacionesCFDIv32(final T comprobante,
			final StringBuilder builder) {
		com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi = (com.mx.quadrum.cfdi.v32.schema.Comprobante) comprobante;
		validarComplementos32(cfdi, builder);
	}

	private static <T> void validacionesCFDIv33(final T comprobante) {
		com.mx.quadrum.cfdi.v33.schema.Comprobante cfdi = (com.mx.quadrum.cfdi.v33.schema.Comprobante) comprobante;
	}

	private static void validarComplementos32(
			final com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi,
			final StringBuilder builder) {
		List<Object> complementos = cfdi.getComplemento().getAny();
		if (complementos != null && !complementos.isEmpty()) {
			for (Object complemento : complementos) {
				validarComplemento(cfdi, complemento, builder, complementos);
			}
		}
	}

	private static void validarComplemento(
			final com.mx.quadrum.cfdi.v32.schema.Comprobante cfdi,
			final Object complemento, final StringBuilder builder,
			final List<Object> complementos) {
		String clase = complemento.getClass().getName();
		switch (clase) {
		case "com.mx.quadrum.cfdi.complementos.comercioExterior.v11.schema.ComercioExterior":
			ValidacionesComercioExterior11Cfdiv32.validacionesComercioExterior(
					cfdi, complementos, builder);
			break;

		default:
			break;
		}
	}

}
