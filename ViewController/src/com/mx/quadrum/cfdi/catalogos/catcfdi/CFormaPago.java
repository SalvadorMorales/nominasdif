package com.mx.quadrum.cfdi.catalogos.catcfdi;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "c_FormaPago")
@XmlEnum
public enum CFormaPago {
	V01("01"), V02("02"), V03("03"), V04("04"), V05("05"), V06("06"), V08("08"), V12("12"), V13("13"), V14("14"), V15(
			"15"), V17("17"), V23("23"), V24(
					"24"), V25("25"), V26("26"), V27("27"), V28("28"), V29("29"), V99("99"), NOTFOUND("not found");

	private final String value;

	CFormaPago(final String newValue) {
		value = newValue;
	}

	public String getValue() {
		return value;
	}

	// @Override
	// public String toString() {
	// String preValor = getValueByName(value);
	// return preValor;
	// }

	public static String getValueByName(String aName) {
		if (aName.equalsIgnoreCase("not found")) {
			return NOTFOUND.getValue();
		} else {
			return valueOf("V" + aName).getValue();
		}
	}

	public static CFormaPago getByName(String aName) {
		return valueOf("V" + aName);
	}
}
