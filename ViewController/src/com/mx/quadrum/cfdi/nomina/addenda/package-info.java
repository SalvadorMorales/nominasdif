/**
 * 
 */
/**
 * @author salvador
 *
 */

@javax.xml.bind.annotation.XmlSchema (namespace="http://www.example.org/Schema-Ejemplo",              
    xmlns = {
            @javax.xml.bind.annotation.XmlNs(prefix = "ns4", 
                     namespaceURI="http://www.example.org/Schema-Ejemplo")           
           },        
    elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED        
)

package com.mx.quadrum.cfdi.nomina.addenda;