package com.mx.quadrum.cfdi.resolver;

import java.io.InputStream;

import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

public class ResourceResolver implements LSResourceResolver {

	public LSInput resolveResource(String type, String namespaceURI,
			String publicId, String systemId, String baseURI) {
		InputStream resourceAsStream = this.getClass().getResourceAsStream(systemId);
		return new Input(publicId, systemId, resourceAsStream);
	}

}
