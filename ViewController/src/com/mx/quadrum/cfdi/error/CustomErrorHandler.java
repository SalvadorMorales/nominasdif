package com.mx.quadrum.cfdi.error;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class CustomErrorHandler implements ErrorHandler {

	private StringBuilder builder = new StringBuilder();

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		builder.append("*********   warning   **************\n".toUpperCase());
		builder.append("Mensaje warning: " + exception.getMessage() + " \n");
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		builder.append("*********   error   **************\n".toUpperCase());
		builder.append("Mensaje error: " + exception.getMessage() + " \n");
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		builder.append("*********   fatalError   **************\n"
				.toUpperCase());
		builder.append("Mensaje fatalError: " + exception.getMessage() + " \n");
	}

	public StringBuilder getBuilder() {
		return builder;
	}

	public void setBuilder(StringBuilder builder) {
		this.builder = builder;
	}

}
