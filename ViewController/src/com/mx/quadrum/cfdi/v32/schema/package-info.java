//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.01.21 a las 09:26:04 AM CST 
//

//@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.sat.gob.mx/cfd/3", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
@javax.xml.bind.annotation.XmlSchema (namespace="http://www.sat.gob.mx/cfd/3",              
    xmlns = {
            @javax.xml.bind.annotation.XmlNs(prefix = "cfdi", 
                     namespaceURI="http://www.sat.gob.mx/cfd/3")           
           },        
    elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED        
)
package com.mx.quadrum.cfdi.v32.schema;
