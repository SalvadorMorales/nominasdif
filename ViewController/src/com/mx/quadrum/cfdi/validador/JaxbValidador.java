package com.mx.quadrum.cfdi.validador;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class JaxbValidador {

	// public static class ValidationException extends Exception {
	// public ValidationException(String message, Throwable cause) {
	// super(message, cause);
	// }
	//
	// public ValidationException(String message) {
	// super(message);
	// }
	// }

	/**
	 * Metodo encargado de validar los elementos requeridos de un objeto que
	 * utilice las anotaciones @XmlElement y @XmlAttribute en caso de ser
	 * requridas asignara los errores a el elemento de tipo StringBuilder
	 * 
	 * @param objeto
	 * @param errors
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static void validarRequeridos(Object objeto, StringBuilder errors)
			throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = objeto.getClass().getDeclaredFields();
		for (Field field : fields) {
			validarRequeridosAnotaciones(errors, objeto, field);
		}

		// if (errors.length() != 0) {
		// throw new ValidationException(errors.toString());
		// }		
	}

	/**
	 * Se encarga de validar si las anotaciones son @XmlElement o @XmlAttribute
	 * asi mismo utiliza la recursividad para realizar una busqueda mas a fondo
	 * 
	 * @param errors
	 * @param target
	 * @param field
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	private static void validarRequeridosAnotaciones(StringBuilder errors, Object target, Field field)
			throws IllegalArgumentException, IllegalAccessException {
		Annotation annotations[] = field.getAnnotations();
		for (Annotation annotation : annotations) {
			Class clase = annotation.annotationType();
			Object anotacion = field.getAnnotation(clase);
			if (anotacion != null) {
				if (anotacion instanceof XmlElement) {
					XmlElement element = (XmlElement) anotacion;
					if (element != null && element.required()) {
						field.setAccessible(true);
						if (field.get(target) == null) {
							String message = String.format("%s: El elemento requerido '%s' tiene valor null. \n",
									target.getClass().getSimpleName(), field.getName());
							errors.append(message);

						} else {
							Object fieldValue = field.get(target);
							validarRequeridos(fieldValue, errors);
						}

					} else {
						field.setAccessible(true);
						Object fieldValue = field.get(target);
						if (fieldValue != null) {
							validarRequeridos(fieldValue, errors);
						}
					}
				}
				if (anotacion instanceof XmlAttribute) {
					XmlAttribute element = (XmlAttribute) anotacion;
					if (element != null && element.required()) {
						field.setAccessible(true);
						if (field.get(target) == null) {
							String message = String.format("%s: El atributo requerido '%s' tiene valor null. \n",
									target.getClass().getSimpleName(), field.getName());
							errors.append(message);
						}
					}
				}
			}
		}

	}
}
