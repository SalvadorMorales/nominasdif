package com.mx.quadrum.cfdi.validador;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import com.mx.quadrum.cfdi.error.CustomErrorHandler;
import com.mx.quadrum.cfdi.resolver.ResourceResolver;

public class SchemaValidatorXsd {

	private static SchemaValidatorXsd schemaValidatorXsd;

	private static String schema_path;
	private static final Map schemaPool = new Hashtable();

	private SchemaValidatorXsd(String schema_path) {
		this.schema_path = schema_path;
	}

	public static SchemaValidatorXsd validacionContraXsd(String schema_path) {
		if (schemaValidatorXsd == null) {
			synchronized (SchemaValidatorXsd.class) {
				if (schemaValidatorXsd == null) {
					System.out.println("Creo la instancia --> B)");
					schemaValidatorXsd = new SchemaValidatorXsd(schema_path);
				}
			}
		}
		return schemaValidatorXsd;
	}

	/**
	 * Validar XMl contra esquema XSD
	 *
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public void validate(Source source, StringBuilder stringBuilder)
			throws Exception {
		Schema schema = compilarEsquema(schema_path);
		Validator validator = schema.newValidator();
		CustomErrorHandler customErrorHandler = new CustomErrorHandler();
		validator.setErrorHandler(customErrorHandler);
		validator.validate(source);
		stringBuilder.append(customErrorHandler.getBuilder().toString());
	}

	private static Schema compilarEsquema(String schemaFile) throws Exception {
		if (!schemaPool.containsKey(schemaFile)) {
			SchemaFactory factory = SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			factory.setResourceResolver(new ResourceResolver());
			factory.setErrorHandler(new CustomErrorHandler());

			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			URL url = loader.getResource(schemaFile);
			if (url == null) {
				throw new RuntimeException("Could not find Schema file: "
						+ schemaFile);
			}
			InputStream is = loader.getResourceAsStream(schemaFile);
			Source schemasrc = new StreamSource(is, url.getPath());
			Schema schema = factory.newSchema(schemasrc);
			schemaPool.put(schemaFile, schema);
		}
		return (Schema) schemaPool.get(schemaFile);
	}

	// public static synchronized void removeFromPool(String schemaFile) {
	// schemaPool.remove(schemaFile);
	// }
	//
	// public static synchronized void clearPool() {
	// schemaPool.clear();
	// }

}
