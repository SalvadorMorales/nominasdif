package com.mx.quadrum.cfdi.binder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeCustomBinder {

	public static Date parseDateTime(String s) {
		try {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			return formatter.parse(s);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public static String printDateTime(Date dt) {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return (dt == null) ? null : formatter.format(dt);
	}
	
	public static Date parseDateYYYYMMDD(String s) {
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.parse(s);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String printDateTimeYYYYMMDD(Date dt) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return (dt == null) ? null : formatter.format(dt);
    }
}
