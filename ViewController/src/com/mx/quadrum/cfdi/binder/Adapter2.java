package com.mx.quadrum.cfdi.binder;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter2 extends XmlAdapter<String, Date>{

    public Date unmarshal(String value) {
        return (com.mx.quadrum.cfdi.binder.DateTimeCustomBinder.parseDateYYYYMMDD(value));
    }

    public String marshal(Date value) {
        return (com.mx.quadrum.cfdi.binder.DateTimeCustomBinder.printDateTimeYYYYMMDD(value));
    }
}
