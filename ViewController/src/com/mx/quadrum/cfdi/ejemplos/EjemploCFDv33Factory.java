package com.mx.quadrum.cfdi.ejemplos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.mx.quadrum.cfdi.catalogos.catcfdi.CFormaPago;
import com.mx.quadrum.cfdi.catalogos.catcfdi.CMetodoPago;
import com.mx.quadrum.cfdi.catalogos.catcfdi.CMoneda;
import com.mx.quadrum.cfdi.catalogos.catcfdi.CTipoDeComprobante;
import com.mx.quadrum.cfdi.catalogos.catcfdi.CTipoFactor;
import com.mx.quadrum.cfdi.catalogos.catcfdi.CUsoCFDI;
import com.mx.quadrum.cfdi.v33.schema.Comprobante;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Complemento;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Conceptos;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Conceptos.Concepto;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Emisor;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Impuestos;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Impuestos.Traslados;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Impuestos.Traslados.Traslado;
import com.mx.quadrum.cfdi.v33.schema.Comprobante.Receptor;
import com.mx.quadrum.cfdi.v33.schema.ObjectFactory;

public class EjemploCFDv33Factory {

	public static Comprobante createComprobante() throws Exception {
		ObjectFactory of = new ObjectFactory();
		Comprobante comp = of.createComprobante();
		comp.setVersion("3.3");
		comp.setMoneda(CMoneda.MXN);
		Date date = new Date();
		comp.setFecha(date);
		comp.setFormaPago(CFormaPago.V01);
		comp.setSubTotal(new BigDecimal("466.43"));
		comp.setTotal(new BigDecimal("488.50"));
		CTipoDeComprobante cTipoDeComprobante = CTipoDeComprobante.E;
		comp.setTipoDeComprobante(cTipoDeComprobante);
		CMetodoPago cMetodoPago = CMetodoPago.PIP;
		comp.setMetodoPago(cMetodoPago);
		comp.setLugarExpedicion("01000");
		comp.setEmisor(createEmisor(of));
		comp.setReceptor(createReceptor(of));
		comp.setConceptos(createConceptos(of));
		comp.setImpuestos(createImpuestos(of));
		comp.setComplemento(createComplemento(of));		
		return comp;
	}

	private static Complemento createComplemento(ObjectFactory of) {
		Complemento complemento = new Complemento();

		return complemento;
	}

	private static Emisor createEmisor(ObjectFactory of) {
		Emisor emisor = of.createComprobanteEmisor();
		emisor.setNombre("PHARMA PLUS SA DE CV");
		emisor.setRfc("PPL961114GZ1");
		emisor.setRegimenFiscal("regimen fiscal");
		return emisor;
	}

	private static Receptor createReceptor(ObjectFactory of) {
		Receptor receptor = of.createComprobanteReceptor();
		receptor.setNombre("JUAN PEREZ PEREZ");
		receptor.setRfc("PEPJ8001019Q8");
		receptor.setUsoCFDI(CUsoCFDI.D_01);
		return receptor;
	}

	private static Conceptos createConceptos(ObjectFactory of) {
		Conceptos cps = of.createComprobanteConceptos();
		List<Concepto> list = cps.getConcepto();
		Concepto c1 = of.createComprobanteConceptosConcepto();
		c1.setUnidad("CAPSULAS");
		c1.setImporte(new BigDecimal("244.00"));
		c1.setCantidad(new BigDecimal("1.0"));
		c1.setDescripcion("VIBRAMICINA 100MG 10");
		c1.setValorUnitario(new BigDecimal("244.00"));
		list.add(c1);
		Concepto c2 = of.createComprobanteConceptosConcepto();
		c2.setUnidad("BOTELLA");
		c2.setImporte(new BigDecimal("137.93"));
		c2.setCantidad(new BigDecimal("1.0"));
		c2.setDescripcion("CLORUTO 500M");
		c2.setValorUnitario(new BigDecimal("137.93"));
		list.add(c2);
		Concepto c3 = of.createComprobanteConceptosConcepto();
		c3.setUnidad("TABLETAS");
		c3.setImporte(new BigDecimal("84.50"));
		c3.setCantidad(new BigDecimal("1.0"));
		c3.setDescripcion("SEDEPRON 250MG 10");
		c3.setValorUnitario(new BigDecimal("84.50"));
		list.add(c3);
		return cps;
	}

	private static Impuestos createImpuestos(ObjectFactory of) {
		Impuestos imps = of.createComprobanteImpuestos();
		Traslados trs = of.createComprobanteImpuestosTraslados();
		List<Traslado> list = trs.getTraslado();
		Traslado t1 = of.createComprobanteImpuestosTrasladosTraslado();
		t1.setImporte(new BigDecimal("0.00"));
		t1.setImpuesto("IVA");
		t1.setTipoFactor(CTipoFactor.CUOTA);
		list.add(t1);
		Traslado t2 = of.createComprobanteImpuestosTrasladosTraslado();
		t2.setImporte(new BigDecimal("22.07"));
		t2.setImpuesto("IVA");
		t2.setTipoFactor(CTipoFactor.CUOTA);
		list.add(t2);
		imps.setTraslados(trs);
		return imps;
	}
}
