package com.mx.quadrum.cfdi.timbrado;

public class UtilsTimbrado {

	/**
	 * variable para timeout-timbrar ws
	 */
	public static final int TIMEOUT_TIMBRAR = 5000;

	/**
	 * Parametro de respuesta del WS cuando el xml es timbrado con exito
	 */
	public static final String TIMBRADO_SATISFACTORIO = "Comprobante timbrado satisfactoriamente";

	/**
	 * Parametro de URL de pruebas del WS de timbrado
	 */
	public static final String URL_TIMBRADO_PRUEBAS = "http://dev.ws.cfdiquadrum.com.mx/timbrar";
	/**
	 * Parametro de URL de pruebas del WS de cancelacion
	 */
	public static final String URL_CANCELACION_PRUEBAS = "http://dev.ws.cfdiquadrum.com.mx/cancelar";

	/**
	 * Parametro de URL de produccion del WS de timbrado
	 */
	public static final String URL_TIMBRADO_PRODUCCION = "https://ws.cfdiquadrum.com.mx/timbrar";
	/**
	 * Parametro de URL de produccion del WS de cancelacion
	 */
	public static final String URL_CANCELACION_PRODUCCION = "https://ws.cfdiquadrum.com.mx/cancelar";
}
