package generadorqr;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.awt.image.BufferedImage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;


/**
 * Clase que crea el QR que se enviara a la representación impresa
 */

public class QRZxing {
    private static final int ancho = 114;
    private static final int alto = 114;
    private static final int param = 19;
    private static final int paramAlto = 76;
    private static final int paramAncho = 76;
    //La informacion que contendra el codigo QR
    private String rfcEmisor;
    private String rfcReceptor;
    private String totalComprobante;
    private String folioFiscal;
    private String datos;

    private InputStream miStream;

    // ********* Constructores ***********

    public QRZxing() {
        this.obtieneQR();
    }

    public QRZxing(String rfcEmisor, String rfcReceptor, String totalComprobante, String folioFiscal) {
        super();
        this.rfcEmisor = rfcEmisor;
        this.rfcReceptor = rfcReceptor;
        this.totalComprobante = totalComprobante;
        this.folioFiscal = folioFiscal;
        this.datos =
                "?re=" + this.rfcEmisor + "&rr=" + this.rfcReceptor + "&tt=" + this.totalComprobante + "&id=" + this.folioFiscal;
        this.obtieneQR();
    }
    // ************************************************

    /**
     * Método que genera el QR con los datos necesarios para el SAT
     */

    public void obtieneQR() {
        BitMatrix bm;
        Writer writer = new QRCodeWriter();
        try {

            bm = writer.encode(datos, BarcodeFormat.QR_CODE, ancho, alto);
            // Crear un buffer para escribir la imagen
            BufferedImage image = new BufferedImage(ancho, alto, BufferedImage.TYPE_INT_RGB);
            BufferedImage imageDef = new BufferedImage(paramAncho, paramAlto, BufferedImage.TYPE_INT_RGB);

            // Iterar sobre la matriz para dibujar los pixeles
            for (int y = 0; y < ancho; y++) {
                for (int x = 0; x < alto; x++) {
                    int grayValue = (bm.get(x, y) ? 1 : 0) & 0xff;
                    image.setRGB(x, y, (grayValue == 0 ? 0 : 0xFFFFFF));
                }
            }
            int y;
            for (y = param; y < ancho - param; y++) {

                for (int x = param; x < alto - param; x++) {
                    imageDef.setRGB(x - param, y - param, (image.getRGB(x, y)));
                }
            }

            //Escribir la imagen al archivo
            imageDef = invertirColores(imageDef);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(imageDef, "png", baos);
            InputStream imageStream = new ByteArrayInputStream(baos.toByteArray());

            setMiStream(imageStream);

        } catch (Exception e) {
            e.printStackTrace(System.err);
            return;
        }

    }

    /**
     * Mètodo que corrige un bug del API que genera los QR con colores invertidos
     *
     * @param imagen    QR generado
     * @return          QR corregido del bug de la API
     */

    private static BufferedImage invertirColores(BufferedImage imagen) {
        for (int x = 0; x < paramAncho; x++) {
            for (int y = 0; y < paramAlto; y++) {
                int rgb = imagen.getRGB(x, y);
                if (rgb == -16777216) {
                    imagen.setRGB(x, y, -1);
                } else {
                    imagen.setRGB(x, y, -16777216);
                }
            }
        }
        return imagen;
    }

    // ********* SETTER Y GETTER **********

    public void setMiStream(InputStream miStream) {
        this.miStream = miStream;
    }

    public InputStream getMiStream() {
        return miStream;
    }
}
