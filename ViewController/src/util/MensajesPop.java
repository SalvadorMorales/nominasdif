package util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.RichPopup;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class MensajesPop {
    public MensajesPop() {
        super();
    }

    public void showPopup(ActionEvent event, RichPopup popup1) {
        FacesContext context = FacesContext.getCurrentInstance();
        UIComponent source = (UIComponent)event.getSource();
        String alignId = source.getClientId(context);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup1.show(hints);
    }

    /**
     * Método que oculta componente PopUp
     *
     * @param popup     PopUp a ocultar
     */

    public void cerrarPopUp(RichPopup popup) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIComponent component = fctx.getViewRoot().findComponent(popup.getClientId());
        ExtendedRenderKitService service = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
        StringBuffer strbuffer = new StringBuffer();
        strbuffer.append("popup=AdfPage.PAGE.findComponent('");
        strbuffer.append(component.getClientId(fctx));
        strbuffer.append("');popup.hide();");
        service.addScript(fctx, strbuffer.toString());
    }
}
