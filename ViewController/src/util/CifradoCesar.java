package util;

import java.io.BufferedReader;

import java.io.IOException;

import java.io.InputStreamReader;


public class CifradoCesar {

    private static String tabla =
        "ÁÉÍÓÚáéíóúABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzÑñ1234567890@ .,;:-+*/$#¿?!¡=()[]{}<>\\\"\"";

    private static int TAM = 10;


    public CifradoCesar() {
    }

    public static void main(String[] args) {
       
    
        System.out.println("Usuario:" + Encriptar("81D6534E-3B08-46D4-A776-8470070B66F1", 10) + "<<clave: " +
                           Encriptar("pedro", 10));

        
        System.out.println("UUID:" + Encriptar("81D6534E-3B08-46D4-A776-8470070B66F1", 10));
        System.out.println("UUID:" + Desencriptar(Encriptar("81D6534E-3B08-46D4-A776-8470070B66F1", 10), 10));
        
        System.out.println(Desencriptar("+@N:;.,OÂ.L/+Â,:N,ÂK--:Â+,-//-/L::P@",10));
        System.out.println("UUID2:" + Encriptar("81D6534E$3B08$46D4$A776$8470070B66F1", 10));
        System.out.println("d UUID " + desencriptar("+@N:;.,O).L/+),:N,)K--:)+,-//-/L::P@",10));
        
         System.out.println("LEME: "+Desencriptar("L-M,ML  =/PM:=,. O=K.@+=@M, ,O;KM.@/", 10));
     
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }


    public static String Encriptar(String entrada, int desp) {
        String salida = "";
        for (int i = 0; i < entrada.length(); i++) {

            if ((tabla.indexOf(entrada.charAt(i)) != -1) || (tabla.indexOf(entrada.charAt(i)) != -1)) {
                salida +=
                        (tabla.indexOf(entrada.charAt(i)) != -1) ? tabla.charAt(((tabla.indexOf(entrada.charAt(i))) + desp) %
                                                                                tabla.length()) :
                        tabla.charAt((tabla.indexOf(entrada.charAt(i)) + desp) % tabla.length());
            } else {
                salida += entrada.charAt(i);
            }

        }

        return salida;

    }

    public static String Desencriptar(String text, int key) {
        
        
        if (text != null) {
            String texto = text;

            String res = "";
            for (int i = 0; i < texto.length(); i++) {
                
                int pos = tabla.indexOf(texto.charAt(i));
                
                
                if ((pos - key) < 0) {
                   
                    res = res + tabla.charAt((pos - key) + tabla.length());
                } else {
                    //System.out.println("LET"+(texto.charAt(i))+"SUM"+tabla.charAt((pos - key) ));
                  
                    res = res + tabla.charAt(pos - key);
                }
            }
            //System.out.println("-->"+res);
            return res;
        }
        return "";
    }

    private static String LimpiarCadena(String t) {
        //transforma el texto a minusculas
        t = t.toLowerCase();
        //eliminamos todos los retornos de carro
        t = t.replaceAll("\n", "");
        //eliminamos caracteres prohibidos
        for (int i = 0; i < t.length(); i++) {
           
            int pos = tabla.indexOf(t.charAt(i));
            if (pos == -1) {
                t = t.replace(t.charAt(i), ' ');
            }
        }
        return t;
    }


    public static String encriptar(String t, int key) {
        String texto = LimpiarCadena(t);
        //aqui se almacena el resultado
        String res = "";
        for (int i = 0; i < texto.length(); i++) {
            //busca la posicion del caracter en la variable tabla
            int pos = tabla.indexOf(texto.charAt(i));
            //realiza el reemplazo
            if ((pos + key) < tabla.length()) {
                res = res + tabla.charAt(pos + key);
            } else {
                res = res + tabla.charAt((pos + key) - tabla.length());
            }
        }
        return res;
    }

    public static String desencriptar(String t, int key) {
        String texto = LimpiarCadena(t);
       
        String res = "";
        for (int i = 0; i < texto.length(); i++) {
            int pos = tabla.indexOf(texto.charAt(i));
            if ((pos - key) < 0) {
                res = res + tabla.charAt((pos - key) + tabla.length());
            } else {
                res = res + tabla.charAt(pos - key);
            }
        }
        return res;
    }


}
