package servicelocator;

import interfaces.SessionDomicilioFiscalRemote;

import interfaces.SessionEmpresaRemote;

import interfaces.SessionExpedidoenRemote;

import interfaces.SessionFoliosRemote;

import interfaces.SessionNotimbradosRemote;

import interfaces.SessionPermisosRemote;
import interfaces.SessionRecibosRemote;

import interfaces.SessionSerieRemote;

import interfaces.SessionSucursalRemote;

import interfaces.SessionTrabajadorRemote;

import interfaces.SessionUsuariosRemote;

import interfaces.SessionUsuariotransaccionesRemote;

import javax.naming.Context;
import javax.naming.InitialContext;


public class MyServiceLocator {
    private static String REF1 = "SessionDomicilioFiscal#interfaces/SessionDomicilioFiscalRemote";
    private static String REF2 = "SessionEmpresa#interfaces/SessionEmpresaRemote";
    private static String REF3 = "SessionExpedidoen#interfaces/SessionExpedidoenRemote";
    private static String REF4 = "SessionFolios#interfaces/SessionFoliosRemote";
    private static String REF5 = "SessionNotimbrados#interfaces/SessionNotimbradosRemote";
    private static String REF6 = "SessionRecibos#interfaces/SessionRecibosRemote";
    private static String REF7 = "SessionSerie#interfaces/SessionSerieRemote";
    private static String REF8 = "SessionSucursal#interfaces/SessionSucursalRemote";
    private static String REF9 = "SessionTrabajador#interfaces/SessionTrabajadorRemote";
    private static String REF10 = "SessionUsuarios#interfaces/SessionUsuariosRemote";
    private static String REF11 = "SessionPermisos#interfaces/SessionPermisosRemote";
    private static String REF12 = "SessionUsuariosTransaccion#interfaces/SessionUsuariotransaccionesRemote";


    public MyServiceLocator() {
        super();
    }


    public static SessionDomicilioFiscalRemote getDomicilioFiscalRemote() {
        Context ctx = getInitialContext();
        SessionDomicilioFiscalRemote ref = null;
        try {
            ref = (SessionDomicilioFiscalRemote)ctx.lookup(REF1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionEmpresaRemote getEmpresaRemote() {
        Context ctx = getInitialContext();
        SessionEmpresaRemote ref = null;
        try {
            ref = (SessionEmpresaRemote)ctx.lookup(REF2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionExpedidoenRemote getExpedidoenRemote() {
        Context ctx = getInitialContext();
        SessionExpedidoenRemote ref = null;
        try {
            ref = (SessionExpedidoenRemote)ctx.lookup(REF3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionFoliosRemote getFoliosRemote() {
        Context ctx = getInitialContext();
        SessionFoliosRemote ref = null;
        try {
            ref = (SessionFoliosRemote)ctx.lookup(REF4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionNotimbradosRemote getSessionNotimbradosRemote() {
        Context ctx = getInitialContext();
        SessionNotimbradosRemote ref = null;
        try {
            ref = (SessionNotimbradosRemote)ctx.lookup(REF5);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionRecibosRemote getSessionRecibosRemote() {
        Context ctx = getInitialContext();
        SessionRecibosRemote ref = null;
        try {
            ref = (SessionRecibosRemote)ctx.lookup(REF6);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionSerieRemote getSerieRemote() {
        Context ctx = getInitialContext();
        SessionSerieRemote ref = null;
        try {
            ref = (SessionSerieRemote)ctx.lookup(REF7);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionSucursalRemote getSucursalRemote() {
        Context ctx = getInitialContext();
        SessionSucursalRemote ref = null;
        try {
            ref = (SessionSucursalRemote)ctx.lookup(REF8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionTrabajadorRemote getTrabajadorRemote() {
        Context ctx = getInitialContext();
        SessionTrabajadorRemote ref = null;
        try {
            ref = (SessionTrabajadorRemote)ctx.lookup(REF9);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionUsuariosRemote getUsuariosRemote() {
        Context ctx = getInitialContext();
        SessionUsuariosRemote ref = null;
        try {
            ref = (SessionUsuariosRemote)ctx.lookup(REF10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }


    public static SessionPermisosRemote getPermisosRemote() {
        Context ctx = getInitialContext();
        SessionPermisosRemote ref = null;
        try {
            ref = (SessionPermisosRemote)ctx.lookup(REF11);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ref;
    }
    
    public static  SessionUsuariotransaccionesRemote getUsuarioTransaccionRemote()
    {
        Context ctx = getInitialContext();
        SessionUsuariotransaccionesRemote ref = null;
        try
        {
            ref = (SessionUsuariotransaccionesRemote)ctx.lookup(REF12);
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return ref;
    }
    


    private static Context getInitialContext() {
        Context ctx = null;
        try {
            ctx = new InitialContext();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ctx;
    }
}
