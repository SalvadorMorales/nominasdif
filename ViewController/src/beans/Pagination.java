package beans;

import classjava.TablaAdmin;

import entities.Trabajador;

import interfaces.SessionTrabajadorRemote;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;

import org.apache.myfaces.trinidad.component.UIXIterator;

import servicelocator.MyServiceLocator;

import util.CifradoCesar;

public class Pagination {
    private UIXIterator emplsTableIterator;
    private int firstRow = 0;
    private int rowsPerPage = 10;
    private int totalRows;
    private int totalPages;
    private int currentPage = 1;
    private SessionTrabajadorRemote ejbTrabajador;
    private List<TablaAdmin> listTabla = new ArrayList<TablaAdmin>();
    private List<Trabajador> listAdmins = new ArrayList<Trabajador>();
    private CifradoCesar cifrado;

    public Pagination() {
        ejbTrabajador = MyServiceLocator.getTrabajadorRemote();
        cifrado = new CifradoCesar();
        this.loadList();
        cargaUsuarios(firstRow, rowsPerPage);
    }

    public void loadList() {
        /**
         * Returns total amount of rows in table.
         * @return Total amount of rows in table.
         */

        int rows = ejbTrabajador.employeesTotalRows();
        this.setTotalRows(rows);

        double val1 = ((double)this.getTotalRows() / this.getRowsPerPage());
        int totalPagesCal = (int)Math.ceil(val1);
        this.setTotalPages((totalPagesCal != 0) ? totalPagesCal : 1);

    }

    public void firstActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(1);
        this.setFirstRow(0);
        cargaUsuarios(firstRow, rowsPerPage);
    }

    public void previousActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(this.getCurrentPage() - 1);
        this.setFirstRow(this.getFirstRow() - this.getRowsPerPage());
        cargaUsuarios(firstRow, rowsPerPage);
    }

    public void nextActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(this.getCurrentPage() + 1);
        this.setFirstRow(this.getFirstRow() + this.getRowsPerPage());
        cargaUsuarios(firstRow, rowsPerPage);
    }

    public void lastActionListener(ActionEvent actionEvent) {
        this.setCurrentPage(this.getTotalPages());
        this.setFirstRow(this.getTotalRows() -
                         ((this.getTotalRows() % this.getRowsPerPage() != 0) ? this.getTotalRows() %
                          this.getRowsPerPage() : this.getRowsPerPage()));
        cargaUsuarios(firstRow, rowsPerPage);
    }

    public boolean isBeforeDisabled() {
        return this.getFirstRow() == 0;
    }

    public boolean isAfterDisabled() {
        return this.getFirstRow() >= this.getTotalRows() - this.getRowsPerPage();
    }


    public void cargaUsuarios(int firstRow2, int rowsPerPage2) {
        TablaAdmin usuario = null;
        listTabla.clear();
        listAdmins.clear();
        listAdmins = ejbTrabajador.employeesByLimit(firstRow2, rowsPerPage2);
        try {
            for (Trabajador t : listAdmins) {
                usuario = new TablaAdmin();
                usuario.setIdAdmin(t.getIdtrabajador());
                usuario.setApellidos(cifrado.Desencriptar(t.getApellidos(), 10));
                usuario.setDepartamento(cifrado.Desencriptar(t.getDepartamento(), 10));
                usuario.setEmpresaRazon(cifrado.Desencriptar(t.getEmpresa1().getRazonSocial(), 10));
                usuario.setNombre(cifrado.Desencriptar(t.getNombre(), 10));
                usuario.setPuesto(cifrado.Desencriptar(t.getPuesto(), 10));
                usuario.setRfc(cifrado.Desencriptar(t.getRfc(), 10));
                usuario.setSucursalRazon(t.getSucursal1().getRazonSocial());
                usuario.setUsuario(cifrado.Desencriptar(t.getUsuario(), 10));
                usuario.setCorreo(t.getCorreo());
                usuario.setPass(cifrado.Desencriptar(t.getPassword(), 10));
                listTabla.add(usuario);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setEmplsTableIterator(UIXIterator emplsTableIterator) {
        this.emplsTableIterator = emplsTableIterator;
    }

    public UIXIterator getEmplsTableIterator() {
        return emplsTableIterator;
    }

    public void setFirstRow(int firstRow) {
        this.firstRow = firstRow;
    }

    public int getFirstRow() {
        return firstRow;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setListTabla(List<TablaAdmin> listTabla) {
        this.listTabla = listTabla;
    }

    public List<TablaAdmin> getListTabla() {
        return listTabla;
    }
}
