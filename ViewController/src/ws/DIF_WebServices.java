package ws;


import classjava.EnviarCorreo;

import entities.Empresa;
import entities.Sucursal;
import entities.Trabajador;

import interfaces.SessionEmpresaRemote;
import interfaces.SessionSucursalRemote;
import interfaces.SessionTrabajadorRemote;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import javax.mail.MessagingException;

import servicelocator.MyServiceLocator;

import util.CifradoCesar;


@WebService
public class DIF_WebServices {

    private Trabajador trabajador;
    private Empresa empresa;
    private Sucursal sucursal;
    private SessionTrabajadorRemote ejbTrabajador;
    private SessionEmpresaRemote ejbEmpresa;
    private SessionSucursalRemote ejbSucursal;
    private boolean existeTrabajador;
    private boolean guardarTrabajador;
    private boolean cambiarPassword;
    private boolean recuperarPassword;
    private static String key = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz��1234567890@.,;:-+*/$#�?!�=";

    public DIF_WebServices() {

        super();
        ejbTrabajador = MyServiceLocator.getTrabajadorRemote();
        ejbEmpresa = MyServiceLocator.getEmpresaRemote();
        ejbSucursal = MyServiceLocator.getSucursalRemote();
        trabajador = new Trabajador();
    }


    @WebMethod
    public boolean isExisteTrabajador(@WebParam(name = "arg0")
        String curp, @WebParam(name = "arg1")
        String password) {
        boolean existe = false;
        System.out.println("user : " + curp + " pass : " + password);
        trabajador = new Trabajador();
        if ((trabajador =
             ejbTrabajador.getFindUsuario(CifradoCesar.Encriptar(curp, 10), CifradoCesar.Encriptar(password, 10))) !=
            null) {
            if (trabajador.getTipo().compareTo("4") == 0)
                existe = true;
        }
        return existe;
    }


    @WebMethod
    public boolean setCambiarPassword(@WebParam(name = "arg0")
        String curp, @WebParam(name = "arg1")
        String mail, @WebParam(name = "arg2")
        String password, @WebParam(name = "arg3")
        String newPass) {
        boolean actualizar = false;
        if (isExisteTrabajador(curp, password)) {
            trabajador =
                    ejbTrabajador.getFindUsuario(CifradoCesar.Encriptar(curp, 10), CifradoCesar.Encriptar(password,
                                                                                                          10));
            trabajador.setPassword(CifradoCesar.Encriptar(newPass, 10));
            ejbTrabajador.mergeTrabajador(trabajador);
            enviarCorreo(mail, curp,
                         CifradoCesar.Desencriptar(trabajador.getNombre(), 10) + " " + CifradoCesar.Desencriptar(trabajador.getApellidos(),
                                                                                                                 10),
                         newPass);
            actualizar = true;
        }
        return actualizar;
    }

    @WebMethod(exclude = true)
    public void enviarCorreo(String correo, String CURP, String nombre, String contraseña) {
        EnviarCorreo enviaCorreo = new EnviarCorreo();
        try {
            String asunto = "�Hola " + nombre + ", Su registro se realiz� con �xito! ?";
            String mensaje =
                " Sus datos de autenticaci�n son:\n\n Usuario =  " + CURP + "\n Contrase�a = " + contraseña +
                "\n\n Utilice esta informaci�n para poder acceder al portal Consulta Empleado. \n\nSaludos";
            enviaCorreo.enviaEmailAltaTrabajador(correo, asunto, mensaje);
        } catch (MessagingException e) {
            System.out.println("log : " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @WebMethod
    public boolean setRecuperarPassword(@WebParam(name = "arg0")
        String curp, @WebParam(name = "arg1")
        String correo) {
        boolean recuperada = false;
        trabajador = ejbTrabajador.getFindCurp(CifradoCesar.Encriptar(curp, 10));
        if (trabajador != null) {
            String cve = getCve();
            trabajador.setClabe(CifradoCesar.Encriptar(cve, 10));
            trabajador.setPassword(CifradoCesar.Encriptar(cve, 10));
            ejbTrabajador.mergeTrabajador(trabajador);
            enviarCorreo(correo, curp,
                         CifradoCesar.Desencriptar(trabajador.getNombre(), 10) + " " + CifradoCesar.Desencriptar(trabajador.getApellidos(),
                                                                                                                 10),
                         cve);
            recuperada = true;
        }
        return recuperada;
    }

    @WebMethod(exclude = true)
    public String getCve() {
        String cve = "";
        int z = 0;
        for (int x = 0; x < 17; x++) {
            z = (int)(Math.random() * (key.length() - 1) + 1);
            cve += "" + key.charAt(z);
        }
        return cve;
    }

    @WebMethod(exclude = true)
    public void setGuardarTrabajador(boolean guardarTrabajador) {
        this.guardarTrabajador = guardarTrabajador;
    }

    @WebMethod
    public boolean isGuardarTrabajador(@WebParam(name = "arg0")
        String nombre, @WebParam(name = "arg1")
        String apellidoPaterno, @WebParam(name = "arg2")
        String apellidoMaterno, @WebParam(name = "arg3")
        String rfc, @WebParam(name = "arg4")
        String curp, @WebParam(name = "arg5")
        String nss, @WebParam(name = "arg6")
        String password, @WebParam(name = "arg7")
        String correo) {
        boolean alta = true;
        try {
            if (ejbTrabajador.getFindCurp(CifradoCesar.Encriptar(curp, 10)) == null) {

                trabajador = new Trabajador();
                trabajador.setNombre(CifradoCesar.Encriptar(nombre, 10));
                trabajador.setApellidos(CifradoCesar.Encriptar(apellidoPaterno, 10) + " " +
                                        CifradoCesar.Encriptar(apellidoMaterno, 10));
                trabajador.setRfc(CifradoCesar.Encriptar(rfc, 10));
                trabajador.setCurp(CifradoCesar.Encriptar(curp, 10));

                String user = "" + nombre.charAt(0);
                user += apellidoPaterno;
                user = user.toLowerCase();
                trabajador.setUsuario(CifradoCesar.Encriptar(user, 10));
                String pass = apellidoPaterno + "2015";
                pass = pass.toLowerCase();
                trabajador.setPassword(CifradoCesar.Encriptar(pass, 10));
                trabajador.setNumSeguridadSocial(CifradoCesar.Encriptar(nss, 10));

                trabajador.setCorreo(CifradoCesar.Encriptar(correo, 10));
                empresa = ejbEmpresa.getEmpresa(3);
                sucursal = ejbSucursal.getFindRfcSucursal("SND7701134L0");
                trabajador.setEmpresa1(empresa);
                trabajador.setSucursal1(sucursal);

                ejbTrabajador.mergeTrabajador(trabajador);
                enviarCorreo(CifradoCesar.Desencriptar(trabajador.getCorreo(), 10), curp,
                             CifradoCesar.Desencriptar(trabajador.getNombre(), 10) + " " +
                             CifradoCesar.Desencriptar(trabajador.getApellidos(), 10), password);
            } else {
                alta = false;
            }
        } catch (Exception e) {
            alta = false;
        }
        return alta;
    }


}
