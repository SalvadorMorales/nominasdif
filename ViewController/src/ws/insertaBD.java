package ws;

import entities.Trabajador;

import interfaces.SessionTrabajadorRemote;

import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import servicelocator.MyServiceLocator;

@WebService
public class insertaBD {
    private Trabajador trabajador;
    private SessionTrabajadorRemote ejbTrabajador;
    private String nombre;
    private String apellido;

    public insertaBD() {

        super();
        ejbTrabajador = MyServiceLocator.getTrabajadorRemote();
    }


    @WebMethod
    public boolean insertarTrabajador(@WebParam(name = "arg0")
        String nombre, @WebParam(name = "arg1")
        String apellido, @WebParam(name = "arg2")
        String rfc, @WebParam(name = "arg3")
        String curp, @WebParam(name = "arg4")
        String nss, @WebParam(name = "arg5")
        String pass, @WebParam(name = "arg6")
        String correo) {
        boolean band = true;
        try {
            trabajador = new Trabajador();

            trabajador.setNombre(nombre);
            trabajador.setApellidos(apellido);
            trabajador.setRfc(rfc);
            trabajador.setCurp(curp);
            trabajador.setNumSeguridadSocial(nss);
            trabajador.setPassword(pass);
            trabajador.setCorreo(correo);
            ejbTrabajador.mergeTrabajador(trabajador);
        } catch (Exception e) {
            band = false;
        }
        return band;
    }


    @WebMethod
    @Oneway
    public void setNombre(@WebParam(name = "arg0")
        String nombre) {
        this.nombre = nombre;
    }

    @WebMethod
    public String getNombre() {
        return nombre;
    }

    @WebMethod
    @Oneway
    public void setApellido(@WebParam(name = "arg0")
        String apellido) {
        this.apellido = apellido;
    }

    @WebMethod(exclude = true)
    public String getApellido() {
        return apellido;
    }
}
