package nomina;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class Incapacidad {
    private BigDecimal descuento;
    private BigDecimal diasIncapacidad;
    private byte tipoIncapacidad;


    public Incapacidad() {
        super();
    }

    public Incapacidad(BigDecimal descuento, BigDecimal diasIncapacidad, byte tipoIncapacidad) {
        super();
        this.descuento = descuento;
        this.diasIncapacidad = diasIncapacidad;
        this.tipoIncapacidad = tipoIncapacidad;
    }


    public void setDescuento(BigDecimal descuento) {
        this.descuento = descuento;
    }

    public BigDecimal getDescuento() {
        return descuento;
    }

    public void setDiasIncapacidad(BigDecimal diasIncapacidad) {
        this.diasIncapacidad = diasIncapacidad;
    }

    public BigDecimal getDiasIncapacidad() {
        return diasIncapacidad;
    }

    public void setTipoIncapacidad(byte tipoIncapacidad) {
        this.tipoIncapacidad = tipoIncapacidad;
    }

    public byte getTipoIncapacidad() {
        return tipoIncapacidad;
    }
}
