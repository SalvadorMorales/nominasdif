package nomina;

public class PercepcionesDeducciones {
    private String pcodigo;
    private String m_cities;
    private String m_importe;


    private String codigoD;
    private String descripD;
    private String importeD;

    public PercepcionesDeducciones(String pcodigo, String m_cities, String m_importe, String codigoD, String descripD,
                                   String m_importeD) {
        this.pcodigo = pcodigo;
        this.m_cities = m_cities;
        this.m_importe = m_importe;
        this.codigoD = codigoD;
        this.descripD = descripD;
        this.importeD = m_importeD;
    }

    public String getPcodigo() {
        return pcodigo;
    }

    public String getCities() {
        return m_cities;
    }

    public String getImporte() {
        return m_importe;
    }

    public String getCodigoD() {
        return codigoD;
    }

    public String getDescripD() {
        return descripD;
    }

    public String getImporteD() {
        return importeD;
    }
}
