package nomina;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class Percepciones {
    private String clave;
    private String concepto;
    private BigDecimal importeExento;
    private BigDecimal importeGravado;
    private String tipoPercepcion;


    public Percepciones() {
        super();
    }

    public Percepciones(String clave, String concepto, BigDecimal importeExento, BigDecimal importeGravado,
                        String tipoPercepcion) {
        super();
        this.clave = clave;
        this.concepto = concepto;
        this.importeExento = importeExento;
        this.importeGravado = importeGravado;
        this.tipoPercepcion = tipoPercepcion;
    }


    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getConcepto() {
        return concepto;
    }


    public void setImporteExento(BigDecimal importeExento) {
        this.importeExento = importeExento;
    }

    public BigDecimal getImporteExento() {
        return importeExento;
    }

    public void setImporteGravado(BigDecimal importeGravado) {
        this.importeGravado = importeGravado;
    }

    public BigDecimal getImporteGravado() {
        return importeGravado;
    }

    public void setTipoPercepcion(String tipoPercepcion) {
        this.tipoPercepcion = tipoPercepcion;
    }

    public String getTipoPercepcion() {
        return tipoPercepcion;
    }
}
