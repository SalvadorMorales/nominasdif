package nomina;

import javax.persistence.Transient;

public class Conceptos {
    private Long idConcepto;
    private String cantidad;
    private String unidadMedida;
    private String descripcion;
    private String importe;
    private String precioUnitario;
    private String ivaGeneral;

    private String valorunitario;
    private String noidentificacion;
    private String cuentapredial;
    private String descripciondetallada;
    private String clave;

    @Transient
    private double totalRetencionISR;
    @Transient
    private double totalRetencionIVA;
    @Transient
    private double totalTrasladoIEPS;
    @Transient
    private double totalTrasladoIVA;

    // ******** Constructores *********

    public Conceptos() {
        super();
    }

    public Conceptos(String cantidad, String unidadMedida, String descripcion, String importe) {
        this.setCantidad(cantidad);
        this.setUnidadMedida(unidadMedida);
        this.setDescripcion(descripcion);
        this.setImporte(importe);
    }

    public Conceptos(String cantidad, String unidadMedida, String descripcion, String importe, String precioUnitario) {
        this.setCantidad(cantidad);
        this.setUnidadMedida(unidadMedida);
        this.setDescripcion(descripcion);
        this.setImporte(importe);
        this.setPrecioUnitario(precioUnitario);

    }

    public Conceptos(String cantidad, String unidadMedida, String descripcion, String importe, String precioUnitario,
                     String clave) {
        this.setCantidad(cantidad);
        this.setUnidadMedida(unidadMedida);
        this.setDescripcion(descripcion);
        this.setImporte(importe);
        this.setPrecioUnitario(precioUnitario);
        this.setClave(clave);
    }


    public Conceptos(String cantidad, String unidadMedida, String descripcion, String importe, String precioUnitario,
                     String valorunitario, String noidentificacion, String cuentapredial,
                     String descripciondetallada) {
        this.setCantidad(cantidad);
        this.setUnidadMedida(unidadMedida);
        this.setDescripcion(descripcion);
        this.setImporte(importe);
        this.setPrecioUnitario(precioUnitario);
        this.setValorunitario(valorunitario);
        this.setNoidentificacion(noidentificacion);
        this.setCuentapredial(cuentapredial);
        this.setDescripciondetallada(descripciondetallada);

    }


    // **************************************

    // ********* SETTERS Y GETTERS **********

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getImporte() {
        return importe;
    }

    public void setPrecioUnitario(String precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public String getPrecioUnitario() {
        return precioUnitario;
    }

    public void setIdConcepto(Long idConcepto) {
        this.idConcepto = idConcepto;
    }

    public Long getIdConcepto() {
        return idConcepto;
    }

    public void setIvaGeneral(String ivaGeneral) {
        this.ivaGeneral = ivaGeneral;
    }

    public String getIvaGeneral() {
        return ivaGeneral;
    }

    public double getTotalRetencionISR() {
        return totalRetencionISR;
    }

    public void setTotalRetencionISR(double totalRetencionISR) {
        this.totalRetencionISR = totalRetencionISR;
    }

    public double getTotalRetencionIVA() {
        return totalRetencionIVA;
    }

    public void setTotalRetencionIVA(double totalRetencionIVA) {
        this.totalRetencionIVA = totalRetencionIVA;
    }

    public double getTotalTrasladoIEPS() {
        return totalTrasladoIEPS;
    }

    public void setTotalTrasladoIEPS(double totalTrasladoIEPS) {
        this.totalTrasladoIEPS = totalTrasladoIEPS;
    }

    public double getTotalTrasladoIVA() {
        return totalTrasladoIVA;
    }

    public void setTotalTrasladoIVA(double totalTrasladoIVA) {
        this.totalTrasladoIVA = totalTrasladoIVA;
    }

    public void setValorunitario(String valorunitario) {
        this.valorunitario = valorunitario;
    }

    public String getValorunitario() {
        return valorunitario;
    }

    public void setNoidentificacion(String noidentificacion) {
        this.noidentificacion = noidentificacion;
    }

    public String getNoidentificacion() {
        return noidentificacion;
    }

    public void setCuentapredial(String cuentapredial) {
        this.cuentapredial = cuentapredial;
    }

    public String getCuentapredial() {
        return cuentapredial;
    }

    public void setDescripciondetallada(String descripciondetallada) {
        this.descripciondetallada = descripciondetallada;
    }

    public String getDescripciondetallada() {
        return descripciondetallada;
    }


    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }
}
