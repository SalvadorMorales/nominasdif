package nomina;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class Horasextra {
    private byte dias;
    private BigDecimal horasExtra;
    private BigDecimal importePagado;
    private String tipoHoras;


    public Horasextra() {
        super();
    }

    public Horasextra(byte dias, BigDecimal horasExtra, BigDecimal importePagado, String tipoHoras) {
        super();
        this.dias = dias;
        this.horasExtra = horasExtra;
        this.importePagado = importePagado;
        this.tipoHoras = tipoHoras;
    }


    public void setDias(byte dias) {
        this.dias = dias;
    }

    public byte getDias() {
        return dias;
    }

    public void setHorasExtra(BigDecimal horasExtra) {
        this.horasExtra = horasExtra;
    }

    public BigDecimal getHorasExtra() {
        return horasExtra;
    }

    public void setImportePagado(BigDecimal importePagado) {
        this.importePagado = importePagado;
    }

    public BigDecimal getImportePagado() {
        return importePagado;
    }

    public void setTipoHoras(String tipoHoras) {
        this.tipoHoras = tipoHoras;
    }

    public String getTipoHoras() {
        return tipoHoras;
    }
}
