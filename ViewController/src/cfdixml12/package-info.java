//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.12.09 a las 11:05:53 AM CST 
//

@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.sat.gob.mx/cfd/3",
xmlns = {               
        @XmlNs(namespaceURI = "http://www.sat.gob.mx/cfd/3", prefix = "cfdi"),  
        @XmlNs(namespaceURI = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
        @XmlNs(namespaceURI = "http://www.sat.gob.mx/nomina12", prefix = "nomina"),
        @XmlNs(namespaceURI = "http://www.example.org/Schema-Ejemplo", prefix = "ns4")
    }
, elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)

package cfdixml12;

import javax.xml.bind.annotation.XmlNs;  
import javax.xml.bind.annotation.XmlSchema;  