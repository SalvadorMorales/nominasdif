package view;

import java.awt.image.BufferedImage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.servlet.*;
import javax.servlet.http.*;

//@WebServlet(name = "muestraFotoP", urlPatterns = { "/muestrafotop" })
public class muestraFotoE extends HttpServlet {
    private static final String CONTENT_TYPE = "image/gif; charset=utf-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            response.setContentType(CONTENT_TYPE);
            response.setContentType("image/jpg");
            String imageID = request.getParameter("rfc");
            OutputStream output = response.getOutputStream();
            String image = "E:\\Imagenes\\" + imageID + ".png";
            File imageFile = new File(image);
            BufferedImage input = ImageIO.read(imageFile);
            ImageIO.write(input, "JPG", output);
        } catch (IIOException e) {
        }
    }

}
