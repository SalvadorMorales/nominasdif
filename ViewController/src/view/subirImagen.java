package view;

import entities.Empresa;
import entities.Trabajador;

import interfaces.SessionEmpresaRemote;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;


import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.validator.ValidatorException;

import javax.imageio.ImageIO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import org.apache.myfaces.trinidad.model.UploadedFile;

import servicelocator.MyServiceLocator;


public class subirImagen implements Serializable {

    private Trabajador user;
    private Empresa admin;
    private BufferedImage imagen;
    private UploadedFile _file;


    @Override
    public String toString() {
        UploadedFile myfile;


        obtieneSesion();

        if (admin.getRfc() != null) {
            if (this.getFile() != null) {
                myfile = this.getFile();
                if (this.getFile().getLength() > 0) {
                    if (esImagen(this.getFile().getFilename())) {
                        String ruta = "E:\\Imagenes\\" + admin.getRfc() + ".png";
                        //Cambia a bytes
                        try {
                            File outFile = new File(ruta);
                            this.imagen = ImageIO.read(myfile.getInputStream());
                            FileOutputStream out = new FileOutputStream(outFile);
                            ImageIO.write(imagen, "png", out);
                            out.close();
                        } catch (IOException e) {
                        }
                    } else {
                        FacesMessage msg =
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Archivo seleccionado no es compatible.");
                        FacesContext.getCurrentInstance().addMessage(null, msg);
                    }
                } else {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Actualice la imagen seleccionada por favor.");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            } else {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Seleccione una imagen por favor.");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } else {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "No cuenta con los permisos suficientes para actualizar el logo de esta empresa, inicie sesi�n con la cuenta de administrador.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        return null;
    }

    public boolean esImagen(String Filename) {
        boolean isPicture = false;
        String leer = "", ext = "";
        for (int x = Filename.length() - 1; x > 0; x--)
            if (Filename.charAt(x) != '.')
                leer += Filename.charAt(x);
            else
                x = 0;

        for (int x = leer.length() - 1; x >= 0; x--)
            ext += leer.charAt(x);

        ext = ext.toLowerCase();

        if (ext.compareTo("png") == 0 || ext.compareTo("jpg") == 0 || ext.compareTo("gif") == 0 ||
            ext.compareTo("bmp") == 0 || ext.compareTo("jpeg") == 0 || ext.compareTo("tiff") == 0 ||
            ext.compareTo("raw") == 0)
            isPicture = true;

        return isPicture;
    }


    public void obtieneSesion() {
        user = new Trabajador();
        admin = new Empresa();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
        HttpSession session = req.getSession(true);
        if (session.getAttribute("usuario") != null) {
            user = (Trabajador)session.getAttribute("usuario");
        } else {
            admin = (Empresa)session.getAttribute("admin");
        }

    }

    public UploadedFile getFile() {
        return _file;
    }

    public void setFile(UploadedFile file) {
        _file = file;
    }

}
