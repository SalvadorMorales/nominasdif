package cargaxmlreporte;

import cfdixml12.AddNomina;
import cfdixml12.Comprobante;
import cfdixml12.Nomina;
import cfdixml12.Addenda;
import cfdixml12.TimbreFiscalDigital;

//import cfdixmlv11.Comprobante;

import classjava.CadenaOriginal;
import classjava.Sello;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;

import java.io.FileNotFoundException;

import java.io.IOException;
import java.io.InputStream;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;


public class LeerXml12 {
    private static Comprobante comprobante = null;
    private static Comprobante.Emisor emisor = null;
    private static Comprobante.Receptor receptor = null;
    private static Comprobante.Conceptos conceptos = null;
    private static Nomina nomina = null;
    private static Nomina.Emisor emisorNomina = null;
    private static Nomina.Receptor receptorNomina = null;
    private static Nomina.Deducciones deduccionesNomina = null;
    private static Nomina.Percepciones percepcionesNomina = null;
    private static Nomina.Incapacidades incapacidades = null;
    private static Nomina.OtrosPagos otrosPagosNomina = null;
    private static TimbreFiscalDigital timbreFiscalDigital;
    private static AddNomina addNomina = null;


    private static JAXBContext ctx = null;
    private static FileInputStream fis = null;

    private static InputStream cer;
    private static InputStream key;

    private static StringWriter strWriter;

    //Addenda
    private static String quincena;
    private static String clave;
    private static String horario;
    private static String zonadepago;
    private static String antiguedad;
    private static String cuenta;
    private static String banco;
    private static String plaza;
    private static String quinquenio;
    private static String mensaje;
    private static String cvePuesto;
    private static String totalLetra;
    
    
    private static String cadenaOriginal;
    
    private static String temp = "";


    private static byte[] bytes;


    public LeerXml12() {
        super();
        obtieneSesion();
    }


    public static void main(String[] args) {
        try {
            //FileInputStream fis = new FileInputStream("/home/dnova/Escritorio/prueba.xml");
            leeXml("E://prueba.xml");


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public static String leeXml(String fis) {
        String sello = "";
        try {
            ctx = JAXBContext.newInstance(Comprobante.class);
            Unmarshaller u = ctx.createUnmarshaller();
            comprobante = (Comprobante)u.unmarshal(new FileInputStream(fis));
            System.out.println("Version "+comprobante.getTotal());
            emisor = comprobante.getEmisor();
            receptor = comprobante.getReceptor();
            nomina = (Nomina)comprobante.getComplemento().getAny().get(0);
            emisorNomina = nomina.getEmisor();
            receptorNomina = nomina.getReceptor();
            deduccionesNomina = nomina.getDeducciones();
            percepcionesNomina = nomina.getPercepciones();
//          addNomina =  (AddNomina)comprobante.getAddenda().getAny().get(0);
//          System.out.println("--> "+addNomina.getAntiguedad());
            //objAddNomina(addNomina);
            timbreFiscalDigital = (TimbreFiscalDigital)comprobante.getComplemento().getAny().get(1);
            
             setCadenaOriginal("||" + comprobante.getVersion() + "|" + timbreFiscalDigital.getUUID() + "|" + timbreFiscalDigital.getFechaTimbrado() + "|" +
                              timbreFiscalDigital.getSelloCFD() + "|" + timbreFiscalDigital.getNoCertificadoSAT() + "||");

            
            Marshaller m = ctx.createMarshaller();
            strWriter = new StringWriter();
            m.marshal(comprobante, strWriter);


            System.out.println("-> " + strWriter);
            CadenaOriginal objetoCadenaOriginal = new CadenaOriginal();
            
            
            if(!temp.equals("")){
           /*  InputStream miXml = toUTF8InputStream(strWriter.toString());
            byte[] cadenaOriginal =
                objetoCadenaOriginal.generaCadenaDos(miXml, System.getProperty("user.dir") + "/Archivos/nomina.xslt");
            System.out.println(",,, " + cadenaOriginal);
            Sello miSello = new Sello("12345678a", bytes, cadenaOriginal);
            leeXmlDoc(fis); */
            //sello = miSello.generaSelloDigital();
            sello = comprobante.getSello();
            }else{
                sello= "";
                }
            System.out.println("SEllo " + sello);

        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
        }
        return sello;
    }


    public static void leeXmlDoc(String ruta) {
        SAXBuilder builder = new SAXBuilder();
        //Document doc = builder.build(ips);
        Document doc;
        System.out.println("===================================");
        try {
            doc = builder.build(new FileInputStream(ruta));
            Element raiz = doc.getRootElement();
            List<Element> hijosRaiz = raiz.getChildren();
            for (Element hijo : hijosRaiz) {
                String nombre = hijo.getName();
                if (nombre.equals("Addenda")) {
                    List<Element> ele = hijo.getChildren();
                    List<Element> listaSubNodos = new ArrayList<Element>();

                    listaSubNodos = ele.get(0).getChildren();
                    for (Element eh : listaSubNodos) {
                        if (eh.getName().equals("Antiguedad")) {
                            setAntiguedad(eh.getValue());
                        }
                        if (eh.getName().equals("Clave")) {
                            setClave(eh.getValue());
                        }
                        if (eh.getName().equals("Cuenta")) {
                            setCuenta(eh.getValue());
                        }
                        if (eh.getName().equals("Horario")) {
                            setHorario(eh.getValue());
                        }
                        if (eh.getName().equals("Quincena")) {
                            setQuincena(eh.getValue());
                        }
                        if (eh.getName().equals("Zonadepago")) {
                            setZonadepago(eh.getValue());
                        }
                        if (eh.getName().equals("Banco")) {
                            setBanco(eh.getValue());
                        }
                        if (eh.getName().equals("Plaza")) {
                            setPlaza(eh.getValue());
                        }
                        if (eh.getName().equals("Quinquenio")) {
                            setQuinquenio(eh.getValue());
                        }
                        if (eh.getName().equals("Mensaje")) {
                            setMensaje(eh.getValue());
                        }
                        if (eh.getName().equals("CvePuesto")) {
                            setCvePuesto(eh.getValue());
                        }
                        if (eh.getName().equals("TotalLetra")) {
                            setTotalLetra(eh.getValue());
                        }
                    }


                }
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static InputStream toUTF8InputStream(String str) {
        InputStream is = null;
        try {
            is = new ByteArrayInputStream(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            // UTF-8 should always be supported
            throw new AssertionError();
        }
        return is;
    }


    public void obtieneSesion() {
        
        try {
            FacesContext ctx = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
            HttpSession session = req.getSession(true);
            
            if(!session.getAttribute("sclave").equals("")){
                temp = session.getAttribute("sclave").toString();
            if (session.getAttribute("usuario") != null) {
                if(session.getAttribute("fileKey") != null){
                key = (InputStream)session.getAttribute("fileKey");
                if(key.read() > 0){
                        bytes = IOUtils.toByteArray(key);
                        System.out.println("mayor a cero");
                }else {
                    System.out.println("ENtra null");
                    bytes = null;
                    }
                
                
                }else{
                    System.out.println("nnnnnnnnnnnnnnnnnnnnnnnnnnn");
                    }
            }
            }
        } catch (Exception e) {
            System.out.println("Entraaaaaaaaaaaaaaaa");
            temp = "";
           //e.printStackTrace();
        }
    }

    public static void setComprobante(Comprobante comprobante) {
        LeerXml12.comprobante = comprobante;
    }

    public static Comprobante getComprobante() {
        return comprobante;
    }

    public static void setEmisor(Comprobante.Emisor emisor) {
        LeerXml12.emisor = emisor;
    }

    public static Comprobante.Emisor getEmisor() {
        return emisor;
    }

    public static void setReceptor(Comprobante.Receptor receptor) {
        LeerXml12.receptor = receptor;
    }

    public static Comprobante.Receptor getReceptor() {
        return receptor;
    }

    public static void setConceptos(Comprobante.Conceptos conceptos) {
        LeerXml12.conceptos = conceptos;
    }

    public static Comprobante.Conceptos getConceptos() {
        return conceptos;
    }

    public static void setNomina(Nomina nomina) {
        LeerXml12.nomina = nomina;
    }

    public static Nomina getNomina() {
        return nomina;
    }

    public static void setEmisorNomina(Nomina.Emisor emisorNomina) {
        LeerXml12.emisorNomina = emisorNomina;
    }

    public static Nomina.Emisor getEmisorNomina() {
        return emisorNomina;
    }

    public static void setReceptorNomina(Nomina.Receptor receptorNomina) {
        LeerXml12.receptorNomina = receptorNomina;
    }

    public static Nomina.Receptor getReceptorNomina() {
        return receptorNomina;
    }

    public static void setDeduccionesNomina(Nomina.Deducciones deduccionesNomina) {
        LeerXml12.deduccionesNomina = deduccionesNomina;
    }

    public static Nomina.Deducciones getDeduccionesNomina() {
        return deduccionesNomina;
    }

    public static void setPercepcionesNomina(Nomina.Percepciones percepcionesNomina) {
        LeerXml12.percepcionesNomina = percepcionesNomina;
    }

    public static Nomina.Percepciones getPercepcionesNomina() {
        return percepcionesNomina;
    }

    public static void setIncapacidades(Nomina.Incapacidades incapacidades) {
        LeerXml12.incapacidades = incapacidades;
    }

    public static Nomina.Incapacidades getIncapacidades() {
        return incapacidades;
    }

    public static void setOtrosPagosNomina(Nomina.OtrosPagos otrosPagosNomina) {
        LeerXml12.otrosPagosNomina = otrosPagosNomina;
    }

    public static Nomina.OtrosPagos getOtrosPagosNomina() {
        return otrosPagosNomina;
    }

    public static void setTimbreFiscalDigital(TimbreFiscalDigital timbreFiscalDigital) {
        LeerXml12.timbreFiscalDigital = timbreFiscalDigital;
    }

    public static TimbreFiscalDigital getTimbreFiscalDigital() {
        return timbreFiscalDigital;
    }

    public static void setAddNomina(AddNomina addNomina) {
        LeerXml12.addNomina = addNomina;
    }

    public static AddNomina getAddNomina() {
        return addNomina;
    }

    public static void setQuincena(String quincena) {
        LeerXml12.quincena = quincena;
    }

    public static String getQuincena() {
        return quincena;
    }

    public static void setClave(String clave) {
        LeerXml12.clave = clave;
    }

    public static String getClave() {
        return clave;
    }

    public static void setHorario(String horario) {
        LeerXml12.horario = horario;
    }

    public static String getHorario() {
        return horario;
    }

    public static void setZonadepago(String zonadepago) {
        LeerXml12.zonadepago = zonadepago;
    }

    public static String getZonadepago() {
        return zonadepago;
    }

    public static void setAntiguedad(String antiguedad) {
        LeerXml12.antiguedad = antiguedad;
    }

    public static String getAntiguedad() {
        return antiguedad;
    }

    public static void setCuenta(String cuenta) {
        LeerXml12.cuenta = cuenta;
    }

    public static String getCuenta() {
        return cuenta;
    }

    public static void setBanco(String banco) {
        LeerXml12.banco = banco;
    }

    public static String getBanco() {
        return banco;
    }

    public static void setPlaza(String plaza) {
        LeerXml12.plaza = plaza;
    }

    public static String getPlaza() {
        return plaza;
    }

    public static void setQuinquenio(String quinquenio) {
        LeerXml12.quinquenio = quinquenio;
    }

    public static String getQuinquenio() {
        return quinquenio;
    }

    public static void setMensaje(String mensaje) {
        LeerXml12.mensaje = mensaje;
    }

    public static String getMensaje() {
        return mensaje;
    }

    public static void setCvePuesto(String cvePuesto) {
        LeerXml12.cvePuesto = cvePuesto;
    }

    public static String getCvePuesto() {
        return cvePuesto;
    }

    public static void setTotalLetra(String totalLetra) {
        LeerXml12.totalLetra = totalLetra;
    }

    public static String getTotalLetra() {
        return totalLetra;
    }

    public static void setCadenaOriginal(String cadenaOriginal) {
        LeerXml12.cadenaOriginal = cadenaOriginal;
    }

    public static String getCadenaOriginal() {
        return cadenaOriginal;
    }
}
