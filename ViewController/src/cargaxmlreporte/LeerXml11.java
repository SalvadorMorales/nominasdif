package cargaxmlreporte;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import cfdixmlv11.Addenda;
import cfdixmlv11.Comprobante;

import cfdixmlv11.Comprobante.Emisor;
import cfdixmlv11.Comprobante.Receptor;

import cfdixmlv11.Nomina;

import cfdixmlv11.Nomina.Deducciones;
import cfdixmlv11.Nomina.Percepciones;

import cfdixmlv11.ObjectFactory;

import cfdixmlv11.TimbreFiscalDigital;

import classjava.CadenaOriginal;
import classjava.Sello;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.StringWriter;

import java.io.UnsupportedEncodingException;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import javax.xml.bind.Marshaller;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.io.IOUtils;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class LeerXml11 {
    private static Comprobante comprobante = null;
    private static Comprobante.Emisor emisor = null;
    private static Comprobante.Receptor receptor = null;
    private static Comprobante.Conceptos conceptos = null;
    private static Nomina nomina = null;
    private static Nomina.Deducciones deduccionesNomina = null;
    private static Nomina.Percepciones percepcionesNomina = null;
    private static Nomina.Incapacidades incapacidades = null;
    private static Addenda addNomina = null;


    private static InputStream cer;
    private static InputStream key;


    private static JAXBContext ctx = null;
    private static FileInputStream fis = null;
    private static StringWriter strWriter;


    //Timbre Fiscal
    private static String version;
    private static String uuid;
    private static String fechaTimbrado;
    private static String selloCFD;
    private static String noCertificadoSAT;
    private static String selloSAT;
    private static String cadenaOriginal;


    private static byte[] bytes;


    public LeerXml11() {
        obtieneSesion();

    }


    public static void main(String[] args) {
        try {
            FileInputStream fis = new FileInputStream("E://D24193A3-CFFC-4464-A636-FFA99E01.xml");
            //leeXml(fis);


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public static String leeXml(String fis) {
        String sello = "";

        try {
            ctx = JAXBContext.newInstance(Comprobante.class);
            Unmarshaller u = ctx.createUnmarshaller();
            comprobante = (Comprobante)u.unmarshal(new FileInputStream(fis));
            System.out.println("----- " + objComprobante(comprobante).getSerie());
            emisor = comprobante.getEmisor();
            objEmisor(emisor);
            receptor = comprobante.getReceptor();
            objReceptor(receptor);
            nomina = (Nomina)comprobante.getComplemento().getAny().get(0);
            System.out.println("...." + nomina.getCURP());
            objNomina(nomina);
            deduccionesNomina = nomina.getDeducciones();
            objDeduccionesNomina(deduccionesNomina);
            percepcionesNomina = nomina.getPercepciones();
            objPercepcionesNomina(percepcionesNomina);
//            addNomina = (Addenda)comprobante.getAddenda().getAny().get(0);
//            System.out.println("--> " + addNomina.getAntiguedad());
//            objAddNomina(addNomina);
            //            generaSelloFirma();
            Marshaller m = ctx.createMarshaller();
            strWriter = new StringWriter();
            m.marshal(comprobante, strWriter);


            System.out.println("-> " + strWriter);
            CadenaOriginal objetoCadenaOriginal = new CadenaOriginal();
            InputStream miXml = toUTF8InputStream(strWriter.toString());

            byte[] cadenaOriginal =
                objetoCadenaOriginal.generaCadenaDos(miXml, System.getProperty("user.dir") + "/Archivos/nomina.xslt");

            System.out.println(",,, " + cadenaOriginal);

            Sello miSello = new Sello("12345678a", bytes, cadenaOriginal);
            leeXmlDoc(fis);
            sello = miSello.generaSelloDigital();
            System.out.println("SEllo " + miSello.generaSelloDigital());
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
        }
        return sello;
    }


    public static InputStream toUTF8InputStream(String str) {
        InputStream is = null;
        try {
            is = new ByteArrayInputStream(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            // UTF-8 should always be supported
            throw new AssertionError();
        }
        return is;
    }


    public void obtieneSesion() {
        try {
            FacesContext ctx = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();
            HttpSession session = req.getSession(true);
            if (session.getAttribute("usuario") != null) {
                if(session.getAttribute("fileKey") != null){
                key = (InputStream)session.getAttribute("fileKey");
                    bytes = IOUtils.toByteArray(key);
                }
            } 
        } catch (Exception e) {
           //e.printStackTrace();
        }
    }


    public static void leeXmlDoc(String ruta) {
        SAXBuilder builder = new SAXBuilder();
        //Document doc = builder.build(ips);
        Document doc;
        System.out.println("===================================");
        try {
            doc = builder.build(new FileInputStream(ruta));
            Element raiz = doc.getRootElement();
            List<Element> hijosRaiz = raiz.getChildren();
            for (Element hijo : hijosRaiz) {
                String nombre = hijo.getName();
                if (nombre.equals("Complemento")) {
                    List<Element> timbre = hijo.getChildren();
                    for (Element a : timbre) {
                        String nomb = a.getName();
                        if (nomb.equals("TimbreFiscalDigital")) {
                            System.out.println("TimbreFiscalDigital     ____");
                            setUuid(a.getAttribute("UUID").getValue());
                            setNoCertificadoSAT(a.getAttribute("noCertificadoSAT").getValue());
                            setFechaTimbrado(a.getAttribute("FechaTimbrado").getValue());
                            setSelloCFD(a.getAttribute("selloCFD").getValue());
                            setSelloSAT(a.getAttribute("selloSAT").getValue());
                            setCadenaOriginal("||" + version + "|" + getUuid() + "|" + getFechaTimbrado() + "|" +
                                              getSelloCFD() + "|" + getNoCertificadoSAT() + "||");

                        }

                    }


                }
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static Nomina.Percepciones objPercepcionesNomina(Percepciones percepcionesNomina2) {
        return percepcionesNomina2;
    }


    public static Nomina.Deducciones objDeduccionesNomina(Deducciones deduccionesNomina2) {
        return deduccionesNomina2;
    }

    public static Nomina objNomina(Nomina nomina2) {
        return nomina2;
    }


    public static Receptor objReceptor(Receptor receptor2) {
        return receptor2;
    }


    public static Emisor objEmisor(Emisor emi) {
        return emi;
    }


    public static Comprobante objComprobante(Comprobante comp) {
        return comp;
    }

    private static Addenda objAddNomina(Addenda addenda) {
        return addenda;
    }

    public static void setComprobante(Comprobante comprobante) {
        LeerXml11.comprobante = comprobante;
    }

    public static Comprobante getComprobante() {
        return comprobante;
    }

    public static void setEmisor(Comprobante.Emisor emisor) {
        LeerXml11.emisor = emisor;
    }

    public static Comprobante.Emisor getEmisor() {
        return emisor;
    }

    public static void setReceptor(Comprobante.Receptor receptor) {
        LeerXml11.receptor = receptor;
    }

    public static Comprobante.Receptor getReceptor() {
        return receptor;
    }

    public static void setConceptos(Comprobante.Conceptos conceptos) {
        LeerXml11.conceptos = conceptos;
    }

    public static Comprobante.Conceptos getConceptos() {
        return conceptos;
    }

    public static void setNomina(Nomina nomina) {
        LeerXml11.nomina = nomina;
    }

    public static Nomina getNomina() {
        return nomina;
    }

    public static void setDeduccionesNomina(Nomina.Deducciones deduccionesNomina) {
        LeerXml11.deduccionesNomina = deduccionesNomina;
    }

    public static Nomina.Deducciones getDeduccionesNomina() {
        return deduccionesNomina;
    }

    public static void setPercepcionesNomina(Nomina.Percepciones percepcionesNomina) {
        LeerXml11.percepcionesNomina = percepcionesNomina;
    }

    public static Nomina.Percepciones getPercepcionesNomina() {
        return percepcionesNomina;
    }

    public static void setIncapacidades(Nomina.Incapacidades incapacidades) {
        LeerXml11.incapacidades = incapacidades;
    }

    public static Nomina.Incapacidades getIncapacidades() {
        return incapacidades;
    }

    public static void setAddNomina(Addenda addNomina) {
        LeerXml11.addNomina = addNomina;
    }

    public static Addenda getAddNomina() {
        return addNomina;
    }

    public static void setVersion(String version) {
        LeerXml11.version = version;
    }

    public static String getVersion() {
        return version;
    }

    public static void setUuid(String uuid) {
        LeerXml11.uuid = uuid;
    }

    public static String getUuid() {
        return uuid;
    }

    public static void setFechaTimbrado(String fechaTimbrado) {
        LeerXml11.fechaTimbrado = fechaTimbrado;
    }

    public static String getFechaTimbrado() {
        return fechaTimbrado;
    }

    public static void setSelloCFD(String selloCFD) {
        LeerXml11.selloCFD = selloCFD;
    }

    public static String getSelloCFD() {
        return selloCFD;
    }

    public static void setNoCertificadoSAT(String noCertificadoSAT) {
        LeerXml11.noCertificadoSAT = noCertificadoSAT;
    }

    public static String getNoCertificadoSAT() {
        return noCertificadoSAT;
    }

    public static void setSelloSAT(String selloSAT) {
        LeerXml11.selloSAT = selloSAT;
    }

    public static String getSelloSAT() {
        return selloSAT;
    }

    public static void setCadenaOriginal(String cadenaOriginal) {
        LeerXml11.cadenaOriginal = cadenaOriginal;
    }

    public static String getCadenaOriginal() {
        return cadenaOriginal;
    }
}
