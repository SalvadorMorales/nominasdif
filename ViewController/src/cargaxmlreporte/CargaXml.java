package cargaxmlreporte;

import classjava.Anexo20;

import nomina.Deducciones;
import nomina.Horasextra;
import nomina.Incapacidad;
import nomina.Percepciones;
import nomina.Conceptos;

import java.io.FileInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.math.BigDecimal;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import nomina.Conceptos;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class CargaXml {

    private List<Conceptos> miLista = new ArrayList<Conceptos>();

    private List<Percepciones> misPerrcepciones;
    private List<Deducciones> misDeducciones;
    private List<Horasextra> misHoraExtra;
    private List<Incapacidad> misIncapacidad;

    private String rutaXML;
    private String folioFiscal;
    private String noCertificadoSat;
    private String fechaCertificado;
    private String estadoEmisor;
    private String nombreEmisor;
    private String rfcEmisor;
    private String domicilioEmisor;
    private String codigoEmisor;
    private String calleEmisor;
    private String coloniaEmisor;
    private String municipioEmisor;
    private String domicilio1Emisor;
    private String paisEmisor;
    private String condicionesDePago;
    private String formaDePago;
    private String numcuentapago;
    private String regimenFiscal;
    private String numeroDeFolio;
    private String serie;
    private String nomIntEm;
    private String nomExtEM;
    private String codigoPostalEmisor;

    private String calleReceptor;
    private String coloniaRecepetor;
    private String municipioReceptor;
    private String estadoReceptor;
    private String nombreReceptor;
    private String rfcReceptor;
    private String domicilioReceptor;
    private String domicilio1Receptor;
    private String paisReceptor;
    private String codigoReceptor;
    private String razonSocialReceptor;
    private String codigoPostal;
    private String nomInt;
    private String noExterior;
    private String cantidad;
    private String unidadMedida;
    private String descripcion;
    private String importe;
    private String valorUnitario;
    private String Subtotal;
    private String Total;
    private String sellodigital;
    private String selloSAT;
    private String cadenaOriginal;
    private String fechaTimbrado;
    private String noSerieCertificadoCSD;

    private String calleExpedido;
    private String noExteriorExpedido;
    private String noInteriorExpedido;
    private String coloniaExpedido;
    private String localidadExpedido;
    private String referenciaExpedido;
    private String estadoExpedido;
    private String paisExpedido;
    private String codigoPostalExpedido;

    private Anexo20 reglas = new Anexo20();
    private String folioFiscalOriginal;

    private DecimalFormat formatoAnexo20 = new DecimalFormat("###,###,###,##0.00");

    private InputStream xml;
    private String version = "1.0";
    private String valorMoneda;
    private String descuento;
    private String tipocambio;
    private String tipoComprobante;

    //PFintegranteCoordinado
    private String versionPFintegrante;
    private String claveVehicular;
    private String placa;
    private String RFCPF;

    //Divisas
    private String versionDivisas;
    private String tipoOperacion;

    //Donatarias
    private String fechaAutorizacion;
    private String versionDonatarias;
    private String noAutorizacion;
    private String leyendaDonatarias;
    private String tasaRetencion;
    private String tasaTraslado;
    private String login;

    //Resumen nomina
    private String valNominaRegistroPatronal;
    private String valNominaNumeroEmpleado;
    private String valNominaCurp;
    private String valNominaTipoRegimen;
    private String valNominaNumeroSeguridadSocial;
    private String valNominaNumeroDiasPagados;
    private String valNominaDepartamento;
    private String valNominaClabe;
    private String valNominaBanco;
    private String valNominaFechaInicioRelacionLaboral;
    private String valNominaAntiguedad;
    private String valNominaPuesto;
    private String valNominaTipoContrato;
    private String valNominaTipoJornada;
    private String valNominaPeriodicidadPago;
    private String valNominaSalarioBaseCotApor;
    private String valNominaRiesgoPuesto;
    private String valNominaSalarioDiarioIntegrado;

    private String valNominaFechaFinal;
    private String valNominaFechaInicial;
    private String valNominaFechaPago;
    private String valNominaFechaInicioRelLaboral;

    private String valNominaTotalExentoPercepcion;
    private String valNominaTotalGravadoPercepcion;
    private String valNominaTotalExentoDeduccion;
    private String valNominaTotalGravadoDeduccion;

    private String ips = null;

    private String antiguedad;
    private String Clave;
    private String cuenta;
    private String horario;
    private String quincena;
    private String plaza;
    private String banco;
    private String quinquenio;
    private String mensaje;
    private String cvePuesto;
    private String totalLetra;


    private String zonaPago;


    public String getQuinquenio() {
        return quinquenio;
    }

    public void setQuinquenio(String quinquenio) {
        this.quinquenio = quinquenio;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


    public String getPlaza() {
        return plaza;
    }

    public void setPlaza(String plaza) {
        this.plaza = plaza;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }


    public String getZonaPago() {
        return zonaPago;
    }

    public void setZonaPago(String zonaPago) {
        this.zonaPago = zonaPago;
    }

    public String getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(String antiguedad) {
        this.antiguedad = antiguedad;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getQuincena() {
        return quincena;
    }

    public void setQuincena(String quincena) {
        this.quincena = quincena;
    }

    public CargaXml(String XML) {
        super();
        ips = XML;
        obtenDatosXml(XML);
    }

    public CargaXml() {
        super();
        this.rutaXML = rutaXML;
        //obtenDatosXml();
    }

    public void obtenDatosXml(String ruta) {
        try {
            File fileXml = new File(ruta);

            SAXBuilder builder = new SAXBuilder();
            //Document doc = builder.build(ips);
            Document doc = builder.build(new FileInputStream(ruta));
            Element raiz = doc.getRootElement();

            this.setFechaCertificado(raiz.getAttribute("fecha").getValue());
            this.setTotal(raiz.getAttribute("total").getValue());
            this.setSubtotal(raiz.getAttribute("subTotal").getValue());
            this.setNoSerieCertificadoCSD(raiz.getAttribute("noCertificado").getValue());
            this.setCondicionesDePago(raiz.getAttribute("formaDePago").getValue());
            this.setFormaDePago(raiz.getAttribute("metodoDePago").getValue());
            if (raiz.getAttribute("NumCtaPago") == null) //this.setNumcuentapago("No Identificado");
            {
                this.setNumcuentapago("-- -- -- --");
            } else {
                this.setNumcuentapago(raiz.getAttribute("NumCtaPago").getValue());
            }
            if (!(raiz.getAttribute("descuento") == null)) {
                this.setDescuento(raiz.getAttribute("descuento").getValue());
            }
            if (!(raiz.getAttribute("TipoCambio") == null)) {
                this.setTipocambio(raiz.getAttribute("TipoCambio").getValue());
            }
            if (!(raiz.getAttribute("tipoDeComprobante") == null)) {
                this.setTipoComprobante(raiz.getAttribute("tipoDeComprobante").getValue());
            }
            if (!(raiz.getAttribute("serie") == null)) {
                this.setSerie(raiz.getAttribute("serie").getValue());
            }
            if (!(raiz.getAttribute("folio") == null)) {
                this.setNumeroDeFolio(raiz.getAttribute("folio").getValue());
            }
            if (!(raiz.getAttribute("sello") == null)) {
                this.setSellodigital(raiz.getAttribute("sello").getValue());
            }
            if (!(raiz.getAttribute("Moneda") == null)) {
                this.setValorMoneda(raiz.getAttribute("Moneda").getValue());
            }
            if (raiz.getAttribute("FolioFiscalOrig") != null) {
                this.setFolioFiscalOriginal(raiz.getAttribute("FolioFiscalOrig").getValue());
            }

            List<Element> hijosRaiz = raiz.getChildren();

            for (Element hijo : hijosRaiz) {
                String nombre = hijo.getName();

                if (nombre.equals("Receptor")) {
                    this.setRfcReceptor(reglas.escapeSecuencia(hijo.getAttribute("rfc").getValue()));
                    if (hijo.getAttribute("nombre") != null) {
                        this.setRazonSocialReceptor(reglas.escapeSecuencia(hijo.getAttribute("nombre").getValue()));
                    }
                    List<Element> receptor = hijo.getChildren();
                    for (Element a : receptor) {
                        String nom = a.getName();
                        if (nom.equals("Domicilio")) {
                            if (a.getAttribute("municipio") != null) {
                                this.setMunicipioReceptor(a.getAttribute("municipio").getValue());
                            }
                            if (a.getAttribute("domicilio") != null) {
                                this.setDomicilioReceptor(a.getAttribute("domicilio").getValue());
                            }
                            if (a.getAttribute("colonia") != null) {
                                this.setColoniaRecepetor(a.getAttribute("colonia").getValue());
                            }
                            if (a.getAttribute("calle") != null) {
                                this.setCalleReceptor(a.getAttribute("calle").getValue());
                            }
                            if (a.getAttribute("codigoPostal") != null) {
                                this.setCodigoPostal(a.getAttribute("codigoPostal").getValue());
                            }
                            if (a.getAttribute("noInterior") != null) {
                                this.setNomInt(a.getAttribute("noInterior").getValue());
                            }
                            if (a.getAttribute("noExterior") != null) {
                                this.setNoExterior(a.getAttribute("noExterior").getValue());
                            }
                            if (a.getAttribute("estado") != null) {
                                this.setEstadoReceptor(a.getAttribute("estado").getValue());
                            }

                            this.setPaisReceptor(a.getAttribute("pais").getValue());
                        }
                    }
                } else if (nombre.equals("Emisor")) {
                    this.setRfcEmisor(reglas.escapeSecuencia(hijo.getAttribute("rfc").getValue()));
                    if (hijo.getAttribute("nombre") != null) {
                        this.setNombreEmisor(reglas.escapeSecuencia(hijo.getAttribute("nombre").getValue()));
                    }

                    List<Element> ghd = hijo.getChildren();
                    for (Element a : ghd) {
                        String domicilioCompleto = "";
                        String nomb = a.getName();
                        if (nomb.equals("RegimenFiscal")) {
                            this.setRegimenFiscal(a.getAttribute("Regimen").getValue());
                        }
                        if (nomb.equals("DomicilioFiscal")) {

                            domicilioCompleto += " " + a.getAttribute("calle").getValue();

                            if (a.getAttribute("noInterior") != null) {
                                this.setNomIntEm(a.getAttribute("noInterior").getValue());
                                domicilioCompleto += " Int.  " + getNomIntEm();
                            }
                            if (a.getAttribute("noExterior") != null) {
                                this.setNomExtEM(a.getAttribute("noExterior").getValue());
                                domicilioCompleto += " Ext. " + getNomExtEM();
                            }
                            if (a.getAttribute("colonia") != null) {
                                this.setColoniaEmisor(a.getAttribute("colonia").getValue());
                                domicilioCompleto += " Col. " + getColoniaEmisor();
                            }

                            this.setMunicipioEmisor(a.getAttribute("municipio").getValue());
                            domicilioCompleto += " " + getMunicipioEmisor();

                            this.setCalleEmisor(domicilioCompleto);

                            this.setCodigoPostalEmisor(a.getAttribute("codigoPostal").getValue());
                            this.setPaisEmisor(a.getAttribute("pais").getValue());
                            this.setEstadoEmisor(a.getAttribute("estado").getValue());
                            this.setCodigoPostalEmisor(a.getAttribute("codigoPostal").getValue());

                        }
                        if (nomb.equals("ExpedidoEn")) {
                            if (a.getAttribute("noInterior") != null) {
                                this.setNoInteriorExpedido(a.getAttribute("noInterior").getValue());
                            }
                            if (a.getAttribute("colonia") != null) {
                                this.setColoniaExpedido(a.getAttribute("colonia").getValue());
                            }
                            if (a.getAttribute("noExterior") != null) {
                                this.setNoExteriorExpedido(a.getAttribute("noExterior").getValue());
                            }
                            if (a.getAttribute("municipio") != null) {
                                this.setLocalidadExpedido(a.getAttribute("municipio").getValue());
                            }
                            if (a.getAttribute("calle") != null) {
                                this.setCalleExpedido(a.getAttribute("calle").getValue());
                            }
                            if (a.getAttribute("codigoPostal") != null) {
                                this.setCodigoPostalExpedido(a.getAttribute("codigoPostal").getValue());
                            }

                            this.setPaisExpedido(a.getAttribute("pais").getValue());
                            this.setEstadoExpedido(a.getAttribute("estado").getValue());
                            this.setCodigoPostalExpedido(a.getAttribute("codigoPostal").getValue());
                        }
                    }

                } else if (nombre.equals("Conceptos")) {
                    List<Element> concepto = hijo.getChildren();
                    String descripcionDetallada = null;
                    for (Element a : concepto) {
                        this.setImporte(a.getAttribute("importe").getValue());
                        this.setUnidadMedida(reglas.escapeSecuencia(a.getAttribute("unidad").getValue()));
                        this.setCantidad(a.getAttribute("cantidad").getValue());
                        this.setDescripcion(reglas.escapeSecuencia(a.getAttribute("descripcion").getValue()));
                        this.setValorUnitario(a.getAttribute("valorUnitario").getValue());

                        Conceptos contenedor =
                            new Conceptos("123", "43432", "23434", formatoAnexo20.format(123), formatoAnexo20.format(123));

                        miLista.add(contenedor);
                    }
                } else if (nombre.equals("Complemento")) {

                    List<Element> timbre = hijo.getChildren();
                    for (Element a : timbre) {
                        String nomb = a.getName();
                        if (nomb.equals("TimbreFiscalDigital")) {
                            this.setFolioFiscal(a.getAttribute("UUID").getValue());
                            this.setNoCertificadoSat(a.getAttribute("noCertificadoSAT").getValue());
                            this.setFechaTimbrado(a.getAttribute("FechaTimbrado").getValue());
                            this.setSellodigital(a.getAttribute("selloCFD").getValue());
                            this.setSelloSAT(a.getAttribute("selloSAT").getValue());
                            setCadenaOriginal("||" + version + "|" + getFolioFiscal() + "|" + getFechaTimbrado() +
                                              "|" + getSellodigital() + "|" + getNoCertificadoSat() + "||");

                        } else if (nomb.equals("PFintegranteCoordinado")) {
                            this.setClaveVehicular(a.getAttribute("ClaveVehicular").getValue());
                            this.setPlaca(a.getAttribute("Placa").getValue());
                            if (a.getAttribute("RFCPF") != null) {
                                this.setRFCPF(a.getAttribute("RFCPF").getValue());
                            }

                        } else if (nomb.equals("Divisas")) {
                            this.setVersionDivisas(a.getAttribute("version").getValue());
                            this.setTipoOperacion(a.getAttribute("tipoOperacion").getValue());
                        } else if (nomb.equals("Donatarias")) {
                            this.setVersionDonatarias(a.getAttribute("version").getValue());
                            this.setNoAutorizacion(a.getAttribute("noAutorizacion").getValue());
                            this.setFechaAutorizacion(a.getAttribute("fechaAutorizacion").getValue());
                            this.setLeyendaDonatarias(a.getAttribute("leyenda").getValue());
                        } else if (nomb.equals("Nomina")) {
                            if (a.getAttribute("RegistroPatronal") != null) {
                                this.setValNominaRegistroPatronal(a.getAttribute("RegistroPatronal").getValue());
                            }

                            if (a.getAttribute("NumEmpleado") != null) {
                                this.setValNominaNumeroEmpleado(a.getAttribute("NumEmpleado").getValue());
                            }

                            if (a.getAttribute("NumSeguridadSocial") != null) {
                                this.setValNominaNumeroSeguridadSocial(a.getAttribute("NumSeguridadSocial").getValue());
                            }

                            if (a.getAttribute("CURP") != null) {
                                this.setValNominaCurp(a.getAttribute("CURP").getValue());
                            }
                            if (a.getAttribute("TipoRegimen") != null) {
                                this.setValNominaTipoRegimen(a.getAttribute("TipoRegimen").getValue());
                            }
                            if (a.getAttribute("numeroSeguroSocial") != null) {
                                this.setValNominaNumeroSeguridadSocial(a.getAttribute("numeroSeguroSocial").getValue());
                            }
                            if (a.getAttribute("FechaPago") != null) {
                                this.setValNominaFechaPago(a.getAttribute("FechaPago").getValue());
                            }
                            if (a.getAttribute("FechaInicialPago") != null) {
                                this.setValNominaFechaInicial(a.getAttribute("FechaInicialPago").getValue());
                            }
                            if (a.getAttribute("FechaFinalPago") != null) {
                                this.setValNominaFechaFinal(a.getAttribute("FechaFinalPago").getValue());
                            }

                            if (a.getAttribute("NumDiasPagados") != null) {
                                this.setValNominaNumeroDiasPagados(a.getAttribute("NumDiasPagados").getValue());
                            }
                            if (a.getAttribute("Departamento") != null) {
                                this.setValNominaDepartamento(a.getAttribute("Departamento").getValue());
                            }
                            //this.setValNominaClabe(a.getAttribute("CLABE").getValue());
                            //Aqui hay un error con eso :/
                            if (a.getAttribute("CLABE") != null) {
                                this.setValNominaClabe(a.getAttribute("CLABE").getValue());
                            }
                            if (a.getAttribute("Banco") != null) {
                                this.setValNominaBanco(a.getAttribute("Banco").getValue());
                            }
                            if (a.getAttribute("FechaInicioRelLaboral") != null) {
                                this.setValNominaFechaInicioRelLaboral(a.getAttribute("FechaInicioRelLaboral").getValue());
                            }
                            if (a.getAttribute("Antiguedad") != null) {
                                this.setValNominaAntiguedad(a.getAttribute("Antiguedad").getValue());
                            }
                            if (a.getAttribute("Puesto") != null) {
                                this.setValNominaPuesto(a.getAttribute("Puesto").getValue());
                            }
                            if (a.getAttribute("TipoContrato") != null) {
                                this.setValNominaTipoContrato(a.getAttribute("TipoContrato").getValue());
                            }
                            if (a.getAttribute("TipoJornada") != null) {
                                this.setValNominaTipoJornada(a.getAttribute("TipoJornada").getValue());
                            }
                            if (a.getAttribute("PeriodicidadPago") != null) {
                                this.setValNominaPeriodicidadPago(a.getAttribute("PeriodicidadPago").getValue());
                            }
                            if (a.getAttribute("SalarioBaseCotApor") != null) {
                                this.setValNominaSalarioBaseCotApor(a.getAttribute("SalarioBaseCotApor").getValue());
                            }
                            if (a.getAttribute("RiesgoPuesto") != null) {
                                this.setValNominaRiesgoPuesto(a.getAttribute("RiesgoPuesto").getValue());
                            }
                            if (a.getAttribute("SalarioDiarioIntegrado") != null) {
                                this.setValNominaSalarioDiarioIntegrado(a.getAttribute("SalarioDiarioIntegrado").getValue());
                            }

                            List<Element> lista = new ArrayList<Element>();
                            lista = a.getChildren();

                            boolean tienePercepciones = false;
                            boolean tieneDeducciones = false;

                            List<Element> listaSubNodos = new ArrayList<Element>();

                            listaSubNodos = a.getChildren();

                            if (listaSubNodos.size() == 0) {
                                misDeducciones = new ArrayList<Deducciones>();
                                this.setValNominaTotalGravadoDeduccion("0.0");
                                this.setValNominaTotalExentoDeduccion("0.0");
                                Deducciones de = new Deducciones();
                                de.setClave("");
                                de.setConcepto("");
                                de.setImporteExento(new BigDecimal("0.0"));
                                de.setImporteGravado(new BigDecimal("0.0"));
                                de.setTipoDeduccion("");
                                misDeducciones = new ArrayList<Deducciones>();
                                misDeducciones.add(de);

                                Percepciones p = new Percepciones();
                                this.setValNominaTotalGravadoPercepcion("0.0");
                                this.setValNominaTotalExentoPercepcion("0.0");
                                p.setClave("");
                                p.setTipoPercepcion("");
                                p.setConcepto("");
                                p.setImporteExento(new BigDecimal("0.0"));
                                p.setImporteGravado(new BigDecimal("0.0"));
                                misPerrcepciones = new ArrayList<Percepciones>();
                                misPerrcepciones.add(p);

                            } else {
                                for (Element subNodosNomina : (List<Element>)a.getChildren()) {
                                    String nombreNodosNomina = subNodosNomina.getName();
                                    if (nombreNodosNomina.equals("Percepciones")) {

                                        tienePercepciones = true;
                                        List<Element> percepciones = subNodosNomina.getChildren();
                                        if (percepciones != null) {

                                            misPerrcepciones = new ArrayList<Percepciones>();

                                            this.setValNominaTotalGravadoPercepcion(subNodosNomina.getAttribute("TotalGravado").getValue());
                                            this.setValNominaTotalExentoPercepcion(subNodosNomina.getAttribute("TotalExento").getValue());
                                            for (Element b : percepciones) {
                                                Percepciones p = new Percepciones();

                                                if (b.getAttribute("Clave") != null) {
                                                    p.setClave(b.getAttribute("Clave").getValue());
                                                }
                                                if (b.getAttribute("TipoPercepcion") != null) {
                                                    p.setTipoPercepcion(b.getAttribute("TipoPercepcion").getValue().toString());
                                                }
                                                if (b.getAttribute("Concepto") != null) {
                                                    p.setConcepto(b.getAttribute("Concepto").getValue());
                                                }
                                                if (b.getAttribute("ImporteExento") != null) {
                                                    p.setImporteExento(new BigDecimal(b.getAttribute("ImporteExento").getValue()));
                                                }
                                                if (b.getAttribute("ImporteGravado") != null) {
                                                    p.setImporteGravado(new BigDecimal(b.getAttribute("ImporteGravado").getValue()));
                                                }
                                                misPerrcepciones.add(p);

                                            }
                                        }

                                    }

                                    if (nombreNodosNomina.equals("Deducciones")) {
                                        tieneDeducciones = true;
                                        List<Element> deducciones = subNodosNomina.getChildren();
                                        if (deducciones != null) {
                                            misDeducciones = new ArrayList<Deducciones>();
                                            this.setValNominaTotalGravadoDeduccion(subNodosNomina.getAttribute("TotalGravado").getValue());
                                            this.setValNominaTotalExentoDeduccion(subNodosNomina.getAttribute("TotalExento").getValue());
                                            for (Element c : deducciones) {
                                                Deducciones de = new Deducciones();
                                                if (c.getAttribute("Clave") != null) {
                                                    de.setClave(c.getAttribute("Clave").getValue());
                                                }
                                                if (c.getAttribute("Concepto") != null) {
                                                    de.setConcepto(c.getAttribute("Concepto").getValue());
                                                }
                                                if (c.getAttribute("ImporteExento") != null) {
                                                    de.setImporteExento(new BigDecimal(c.getAttribute("ImporteExento").getValue()));
                                                }
                                                if (c.getAttribute("ImporteGravado") != null) {
                                                    de.setImporteGravado(new BigDecimal(c.getAttribute("ImporteGravado").getValue()));
                                                }
                                                if (c.getAttribute("TipoDeduccion") != null) {
                                                    de.setTipoDeduccion(c.getAttribute("TipoDeduccion").getValue().toString());
                                                }
                                                misDeducciones.add(de);
                                            }
                                        }
                                    }

                                    if (nombreNodosNomina.equals("Incapacidad")) {
                                        List<Element> incapacidad = subNodosNomina.getChildren();
                                        if (incapacidad != null) {
                                            misIncapacidad = new ArrayList<Incapacidad>();
                                            for (Element d : incapacidad) {
                                                Incapacidad i = new Incapacidad();
                                                if (d.getAttribute("Descuento") != null) {
                                                    i.setDescuento(new BigDecimal(d.getAttribute("Descuento").getValue()));
                                                }
                                                if (d.getAttribute("TipoIncapacidad") != null) {
                                                    i.setTipoIncapacidad(Byte.valueOf(d.getAttribute("TipoIncapacidad").getValue()));
                                                }
                                                misIncapacidad.add(i);
                                            }
                                        }
                                    }

                                    if (nombreNodosNomina.equals("HorasExtra")) {
                                        List<Element> horaExtra = subNodosNomina.getChildren();
                                        if (horaExtra != null) {
                                            misHoraExtra = new ArrayList<Horasextra>();
                                            for (Element e : horaExtra) {
                                                Horasextra h = new Horasextra();
                                                if (e.getAttribute("Dias") != null) {
                                                    h.setDias(Byte.valueOf(e.getAttribute("Dias").getValue()));
                                                }
                                                if (e.getAttribute("HorasExtra") != null) {
                                                    h.setHorasExtra(new BigDecimal((e.getAttribute("HorasExtra").getValue())));
                                                }
                                                if (e.getAttribute("ImportePagado") != null) {
                                                    h.setImportePagado(new BigDecimal(e.getAttribute("ImportePagado").getValue()));
                                                }
                                                if (e.getAttribute("TipoHoras") != null) {
                                                    h.setTipoHoras(e.getAttribute("TipoHoras").getValue());
                                                }
                                                misHoraExtra.add(h);
                                            }
                                        }
                                    }
                                }


                            }
                        }

                    }
                } else if (nombre.equals("Addenda")) {
                    List<Element> ele = hijo.getChildren();
                    List<Element> listaSubNodos = new ArrayList<Element>();

                    listaSubNodos = ele.get(0).getChildren();
                    for (Element eh : listaSubNodos) {
                        if (eh.getName().equals("Antiguedad")) {
                            this.setAntiguedad(eh.getValue());
                        }
                        if (eh.getName().equals("Clave")) {
                            this.setClave(eh.getValue());
                        }
                        if (eh.getName().equals("Cuenta")) {
                            this.setCuenta(eh.getValue());
                        }
                        if (eh.getName().equals("Horario")) {
                            this.setHorario(eh.getValue());
                        }
                        if (eh.getName().equals("Quincena")) {
                            this.setQuincena(eh.getValue());
                        }
                        if (eh.getName().equals("Zonadepago")) {
                            this.setZonaPago(eh.getValue());
                        }
                        if (eh.getName().equals("Banco")) {
                            this.setBanco(eh.getValue());
                        }
                        if (eh.getName().equals("Plaza")) {
                            this.setPlaza(eh.getValue());
                        }
                        if (eh.getName().equals("Quinquenio")) {
                            this.setQuinquenio(eh.getValue());
                        }
                        if (eh.getName().equals("Mensaje")) {
                            this.setMensaje(eh.getValue());
                        }
                        if (eh.getName().equals("CvePuesto")) {
                            this.setCvePuesto(eh.getValue());
                        }
                        if (eh.getName().equals("TotalLetra")) {
                            this.setTotalLetra(eh.getValue());
                        }
                    }

                }
            }


        } catch (JDOMException e) {
            System.out.println("Entro a JDOMException");
        } catch (IOException e) {
            System.out.println("Entro a IOException");
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Usuario no registrado.");
        }

    }

    public void setRutaXML(String rutaXML) {
        this.rutaXML = rutaXML;
    }

    public String getRutaXML() {
        return rutaXML;
    }

    public void setFolioFiscal(String folioFiscal) {
        this.folioFiscal = folioFiscal;
    }

    public String getFolioFiscal() {
        return folioFiscal;
    }

    public void setNoCertificadoSat(String noCertificadoSat) {
        this.noCertificadoSat = noCertificadoSat;
    }

    public String getNoCertificadoSat() {
        return noCertificadoSat;
    }

    public void setFechaCertificado(String fechaCertificado) {
        this.fechaCertificado = fechaCertificado;
    }

    public String getFechaCertificado() {
        return fechaCertificado;
    }

    public void setEstadoEmisor(String estadoEmisor) {
        this.estadoEmisor = estadoEmisor;
    }

    public String getEstadoEmisor() {
        return estadoEmisor;
    }

    public void setNombreEmisor(String nombreEmisor) {
        this.nombreEmisor = nombreEmisor;
    }

    public String getNombreEmisor() {
        return nombreEmisor;
    }

    public void setRfcEmisor(String rfcEmisor) {
        this.rfcEmisor = rfcEmisor;
    }

    public String getRfcEmisor() {
        return rfcEmisor;
    }

    public void setDomicilioEmisor(String domicilioEmisor) {
        this.domicilioEmisor = domicilioEmisor;
    }

    public String getDomicilioEmisor() {
        return domicilioEmisor;
    }

    public void setCodigoEmisor(String codigoEmisor) {
        this.codigoEmisor = codigoEmisor;
    }

    public String getCodigoEmisor() {
        return codigoEmisor;
    }

    public void setCalleEmisor(String calleEmisor) {
        this.calleEmisor = calleEmisor;
    }

    public String getCalleEmisor() {
        return calleEmisor;
    }

    public void setColoniaEmisor(String coloniaEmisor) {
        this.coloniaEmisor = coloniaEmisor;
    }

    public String getColoniaEmisor() {
        return coloniaEmisor;
    }

    public void setMunicipioEmisor(String municipioEmisor) {
        this.municipioEmisor = municipioEmisor;
    }

    public String getMunicipioEmisor() {
        return municipioEmisor;
    }

    public void setDomicilio1Emisor(String domicilio1Emisor) {
        this.domicilio1Emisor = domicilio1Emisor;
    }

    public String getDomicilio1Emisor() {
        return domicilio1Emisor;
    }

    public void setPaisEmisor(String paisEmisor) {
        this.paisEmisor = paisEmisor;
    }

    public String getPaisEmisor() {
        return paisEmisor;
    }

    public void setCondicionesDePago(String condicionesDePago) {
        this.condicionesDePago = condicionesDePago;
    }

    public String getCondicionesDePago() {
        return condicionesDePago;
    }

    public void setFormaDePago(String formaDePago) {
        this.formaDePago = formaDePago;
    }

    public String getFormaDePago() {
        return formaDePago;
    }

    public void setNumcuentapago(String numcuentapago) {
        this.numcuentapago = numcuentapago;
    }

    public String getNumcuentapago() {
        return numcuentapago;
    }

    public void setRegimenFiscal(String regimenFiscal) {
        this.regimenFiscal = regimenFiscal;
    }

    public String getRegimenFiscal() {
        return regimenFiscal;
    }

    public void setNumeroDeFolio(String numeroDeFolio) {
        this.numeroDeFolio = numeroDeFolio;
    }

    public String getNumeroDeFolio() {
        return numeroDeFolio;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSerie() {
        return serie;
    }

    public void setNomIntEm(String nomIntEm) {
        this.nomIntEm = nomIntEm;
    }

    public String getNomIntEm() {
        return nomIntEm;
    }

    public void setNomExtEM(String nomExtEM) {
        this.nomExtEM = nomExtEM;
    }

    public String getNomExtEM() {
        return nomExtEM;
    }

    public void setCodigoPostalEmisor(String codigoPostalEmisor) {
        this.codigoPostalEmisor = codigoPostalEmisor;
    }

    public String getCodigoPostalEmisor() {
        return codigoPostalEmisor;
    }

    public void setCalleReceptor(String calleReceptor) {
        this.calleReceptor = calleReceptor;
    }

    public String getCalleReceptor() {
        return calleReceptor;
    }

    public void setColoniaRecepetor(String coloniaRecepetor) {
        this.coloniaRecepetor = coloniaRecepetor;
    }

    public String getColoniaRecepetor() {
        return coloniaRecepetor;
    }

    public void setMunicipioReceptor(String municipioReceptor) {
        this.municipioReceptor = municipioReceptor;
    }

    public String getMunicipioReceptor() {
        return municipioReceptor;
    }

    public void setEstadoReceptor(String estadoReceptor) {
        this.estadoReceptor = estadoReceptor;
    }

    public String getEstadoReceptor() {
        return estadoReceptor;
    }

    public void setNombreReceptor(String nombreReceptor) {
        this.nombreReceptor = nombreReceptor;
    }

    public String getNombreReceptor() {
        return nombreReceptor;
    }

    public void setRfcReceptor(String rfcReceptor) {
        this.rfcReceptor = rfcReceptor;
    }

    public String getRfcReceptor() {
        return rfcReceptor;
    }

    public void setDomicilioReceptor(String domicilioReceptor) {
        this.domicilioReceptor = domicilioReceptor;
    }

    public String getDomicilioReceptor() {
        return domicilioReceptor;
    }

    public void setDomicilio1Receptor(String domicilio1Receptor) {
        this.domicilio1Receptor = domicilio1Receptor;
    }

    public String getDomicilio1Receptor() {
        return domicilio1Receptor;
    }

    public void setPaisReceptor(String paisReceptor) {
        this.paisReceptor = paisReceptor;
    }

    public String getPaisReceptor() {
        return paisReceptor;
    }

    public void setCodigoReceptor(String codigoReceptor) {
        this.codigoReceptor = codigoReceptor;
    }

    public String getCodigoReceptor() {
        return codigoReceptor;
    }

    public void setRazonSocialReceptor(String razonSocialReceptor) {
        this.razonSocialReceptor = razonSocialReceptor;
    }

    public String getRazonSocialReceptor() {
        return razonSocialReceptor;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setNomInt(String nomInt) {
        this.nomInt = nomInt;
    }

    public String getNomInt() {
        return nomInt;
    }

    public void setNoExterior(String noExterior) {
        this.noExterior = noExterior;
    }

    public String getNoExterior() {
        return noExterior;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getImporte() {
        return importe;
    }

    public void setValorUnitario(String valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public String getValorUnitario() {
        return valorUnitario;
    }

    public void setSubtotal(String Subtotal) {
        this.Subtotal = Subtotal;
    }

    public String getSubtotal() {
        return Subtotal;
    }

    public void setTotal(String Total) {
        this.Total = Total;
    }

    public String getTotal() {
        return Total;
    }

    public void setSellodigital(String sellodigital) {
        this.sellodigital = sellodigital;
    }

    public String getSellodigital() {
        return sellodigital;
    }

    public void setSelloSAT(String selloSAT) {
        this.selloSAT = selloSAT;
    }

    public String getSelloSAT() {
        return selloSAT;
    }

    public void setCadenaOriginal(String cadenaOriginal) {
        this.cadenaOriginal = cadenaOriginal;
    }

    public String getCadenaOriginal() {
        return cadenaOriginal;
    }

    public void setFechaTimbrado(String fechaTimbrado) {
        this.fechaTimbrado = fechaTimbrado;
    }

    public String getFechaTimbrado() {
        return fechaTimbrado;
    }

    public void setNoSerieCertificadoCSD(String noSerieCertificadoCSD) {
        this.noSerieCertificadoCSD = noSerieCertificadoCSD;
    }

    public String getNoSerieCertificadoCSD() {
        return noSerieCertificadoCSD;
    }

    public void setCalleExpedido(String calleExpedido) {
        this.calleExpedido = calleExpedido;
    }

    public String getCalleExpedido() {
        return calleExpedido;
    }

    public void setNoExteriorExpedido(String noExteriorExpedido) {
        this.noExteriorExpedido = noExteriorExpedido;
    }

    public String getNoExteriorExpedido() {
        return noExteriorExpedido;
    }

    public void setNoInteriorExpedido(String noInteriorExpedido) {
        this.noInteriorExpedido = noInteriorExpedido;
    }

    public String getNoInteriorExpedido() {
        return noInteriorExpedido;
    }

    public void setColoniaExpedido(String coloniaExpedido) {
        this.coloniaExpedido = coloniaExpedido;
    }

    public String getColoniaExpedido() {
        return coloniaExpedido;
    }

    public void setLocalidadExpedido(String localidadExpedido) {
        this.localidadExpedido = localidadExpedido;
    }

    public String getLocalidadExpedido() {
        return localidadExpedido;
    }

    public void setReferenciaExpedido(String referenciaExpedido) {
        this.referenciaExpedido = referenciaExpedido;
    }

    public String getReferenciaExpedido() {
        return referenciaExpedido;
    }

    public void setEstadoExpedido(String estadoExpedido) {
        this.estadoExpedido = estadoExpedido;
    }

    public String getEstadoExpedido() {
        return estadoExpedido;
    }

    public void setPaisExpedido(String paisExpedido) {
        this.paisExpedido = paisExpedido;
    }

    public String getPaisExpedido() {
        return paisExpedido;
    }

    public void setCodigoPostalExpedido(String codigoPostalExpedido) {
        this.codigoPostalExpedido = codigoPostalExpedido;
    }

    public String getCodigoPostalExpedido() {
        return codigoPostalExpedido;
    }

    public void setFolioFiscalOriginal(String folioFiscalOriginal) {
        this.folioFiscalOriginal = folioFiscalOriginal;
    }

    public String getFolioFiscalOriginal() {
        return folioFiscalOriginal;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setValorMoneda(String valorMoneda) {
        this.valorMoneda = valorMoneda;
    }

    public String getValorMoneda() {
        return valorMoneda;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setTipocambio(String tipocambio) {
        this.tipocambio = tipocambio;
    }

    public String getTipocambio() {
        return tipocambio;
    }

    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public String getTipoComprobante() {
        return tipoComprobante;
    }

    public void setVersionPFintegrante(String versionPFintegrante) {
        this.versionPFintegrante = versionPFintegrante;
    }

    public String getVersionPFintegrante() {
        return versionPFintegrante;
    }

    public void setClaveVehicular(String claveVehicular) {
        this.claveVehicular = claveVehicular;
    }

    public String getClaveVehicular() {
        return claveVehicular;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getPlaca() {
        return placa;
    }

    public void setRFCPF(String RFCPF) {
        this.RFCPF = RFCPF;
    }

    public String getRFCPF() {
        return RFCPF;
    }

    public void setVersionDivisas(String versionDivisas) {
        this.versionDivisas = versionDivisas;
    }

    public String getVersionDivisas() {
        return versionDivisas;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setFechaAutorizacion(String fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    public String getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setVersionDonatarias(String versionDonatarias) {
        this.versionDonatarias = versionDonatarias;
    }

    public String getVersionDonatarias() {
        return versionDonatarias;
    }

    public void setNoAutorizacion(String noAutorizacion) {
        this.noAutorizacion = noAutorizacion;
    }

    public String getNoAutorizacion() {
        return noAutorizacion;
    }

    public void setLeyendaDonatarias(String leyendaDonatarias) {
        this.leyendaDonatarias = leyendaDonatarias;
    }

    public String getLeyendaDonatarias() {
        return leyendaDonatarias;
    }

    public void setTasaRetencion(String tasaRetencion) {
        this.tasaRetencion = tasaRetencion;
    }

    public String getTasaRetencion() {
        return tasaRetencion;
    }

    public void setTasaTraslado(String tasaTraslado) {
        this.tasaTraslado = tasaTraslado;
    }

    public String getTasaTraslado() {
        return tasaTraslado;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setValNominaRegistroPatronal(String valNominaRegistroPatronal) {
        this.valNominaRegistroPatronal = valNominaRegistroPatronal;
    }

    public String getValNominaRegistroPatronal() {
        return valNominaRegistroPatronal;
    }

    public void setValNominaNumeroEmpleado(String valNominaNumeroEmpleado) {
        this.valNominaNumeroEmpleado = valNominaNumeroEmpleado;
    }

    public String getValNominaNumeroEmpleado() {
        return valNominaNumeroEmpleado;
    }

    public void setValNominaCurp(String valNominaCurp) {
        this.valNominaCurp = valNominaCurp;
    }

    public String getValNominaCurp() {
        return valNominaCurp;
    }

    public void setValNominaTipoRegimen(String valNominaTipoRegimen) {
        this.valNominaTipoRegimen = valNominaTipoRegimen;
    }

    public String getValNominaTipoRegimen() {
        return valNominaTipoRegimen;
    }

    public void setValNominaNumeroSeguridadSocial(String valNominaNumeroSeguridadSocial) {
        this.valNominaNumeroSeguridadSocial = valNominaNumeroSeguridadSocial;
    }

    public String getValNominaNumeroSeguridadSocial() {
        return valNominaNumeroSeguridadSocial;
    }

    public void setValNominaNumeroDiasPagados(String valNominaNumeroDiasPagados) {
        this.valNominaNumeroDiasPagados = valNominaNumeroDiasPagados;
    }

    public String getValNominaNumeroDiasPagados() {
        return valNominaNumeroDiasPagados;
    }

    public void setValNominaDepartamento(String valNominaDepartamento) {
        this.valNominaDepartamento = valNominaDepartamento;
    }

    public String getValNominaDepartamento() {
        return valNominaDepartamento;
    }

    public void setValNominaClabe(String valNominaClabe) {
        this.valNominaClabe = valNominaClabe;
    }

    public String getValNominaClabe() {
        return valNominaClabe;
    }

    public void setValNominaBanco(String valNominaBanco) {
        this.valNominaBanco = valNominaBanco;
    }

    public String getValNominaBanco() {
        return valNominaBanco;
    }

    public void setValNominaFechaInicioRelacionLaboral(String valNominaFechaInicioRelacionLaboral) {
        this.valNominaFechaInicioRelacionLaboral = valNominaFechaInicioRelacionLaboral;
    }

    public String getValNominaFechaInicioRelacionLaboral() {
        return valNominaFechaInicioRelacionLaboral;
    }

    public void setValNominaAntiguedad(String valNominaAntiguedad) {
        this.valNominaAntiguedad = valNominaAntiguedad;
    }

    public String getValNominaAntiguedad() {
        return valNominaAntiguedad;
    }

    public void setValNominaPuesto(String valNominaPuesto) {
        this.valNominaPuesto = valNominaPuesto;
    }

    public String getValNominaPuesto() {
        return valNominaPuesto;
    }

    public void setValNominaTipoContrato(String valNominaTipoContrato) {
        this.valNominaTipoContrato = valNominaTipoContrato;
    }

    public String getValNominaTipoContrato() {
        return valNominaTipoContrato;
    }

    public void setValNominaTipoJornada(String valNominaTipoJornada) {
        this.valNominaTipoJornada = valNominaTipoJornada;
    }

    public String getValNominaTipoJornada() {
        return valNominaTipoJornada;
    }

    public void setValNominaPeriodicidadPago(String valNominaPeriodicidadPago) {
        this.valNominaPeriodicidadPago = valNominaPeriodicidadPago;
    }

    public String getValNominaPeriodicidadPago() {
        return valNominaPeriodicidadPago;
    }

    public void setValNominaSalarioBaseCotApor(String valNominaSalarioBaseCotApor) {
        this.valNominaSalarioBaseCotApor = valNominaSalarioBaseCotApor;
    }

    public String getValNominaSalarioBaseCotApor() {
        return valNominaSalarioBaseCotApor;
    }

    public void setValNominaRiesgoPuesto(String valNominaRiesgoPuesto) {
        this.valNominaRiesgoPuesto = valNominaRiesgoPuesto;
    }

    public String getValNominaRiesgoPuesto() {
        return valNominaRiesgoPuesto;
    }

    public void setValNominaSalarioDiarioIntegrado(String valNominaSalarioDiarioIntegrado) {
        this.valNominaSalarioDiarioIntegrado = valNominaSalarioDiarioIntegrado;
    }

    public String getValNominaSalarioDiarioIntegrado() {
        return valNominaSalarioDiarioIntegrado;
    }

    public void setValNominaFechaFinal(String valNominaFechaFinal) {
        this.valNominaFechaFinal = valNominaFechaFinal;
    }

    public String getValNominaFechaFinal() {
        return valNominaFechaFinal;
    }

    public void setValNominaFechaInicial(String valNominaFechaInicial) {
        this.valNominaFechaInicial = valNominaFechaInicial;
    }

    public String getValNominaFechaInicial() {
        return valNominaFechaInicial;
    }

    public void setValNominaFechaPago(String valNominaFechaPago) {
        this.valNominaFechaPago = valNominaFechaPago;
    }

    public String getValNominaFechaPago() {
        return valNominaFechaPago;
    }

    public void setValNominaFechaInicioRelLaboral(String valNominaFechaInicioRelLaboral) {
        this.valNominaFechaInicioRelLaboral = valNominaFechaInicioRelLaboral;
    }

    public String getValNominaFechaInicioRelLaboral() {
        return valNominaFechaInicioRelLaboral;
    }

    public void setMiLista(List<Conceptos> miLista) {
        this.miLista = miLista;
    }

    public List<Conceptos> getMiLista() {
        return miLista;
    }

    public void setMisPerrcepciones(List<Percepciones> misPerrcepciones) {
        this.misPerrcepciones = misPerrcepciones;
    }

    public List<Percepciones> getMisPerrcepciones() {
        //        System.out.println("-------------------------------------"+misPerrcepciones.size());
        return misPerrcepciones;
    }

    public void setMisDeducciones(List<Deducciones> misDeducciones) {
        this.misDeducciones = misDeducciones;
    }

    public List<Deducciones> getMisDeducciones() {
        return misDeducciones;
    }

    public void setMisHoraExtra(List<Horasextra> misHoraExtra) {
        this.misHoraExtra = misHoraExtra;
    }

    public List<Horasextra> getMisHoraExtra() {
        return misHoraExtra;
    }

    public void setMisIncapacidad(List<Incapacidad> misIncapacidad) {
        this.misIncapacidad = misIncapacidad;
    }

    public List<Incapacidad> getMisIncapacidad() {
        return misIncapacidad;
    }

    public JRDataSource getConexionDeducciones() {
        System.out.println(misDeducciones.get(0).getImporteExento());
        System.out.println(misDeducciones.get(0).getImporteGravado());
        return new JRBeanCollectionDataSource(misDeducciones);
    }

    public JRDataSource getConexionPercepcion() {
        return new JRBeanCollectionDataSource(misPerrcepciones);
    }

    public void setValNominaTotalGravadoPercepcion(String valNominaTotalGravadoPercepcion) {
        this.valNominaTotalGravadoPercepcion = valNominaTotalGravadoPercepcion;
    }

    public String getValNominaTotalGravadoPercepcion() {
        return valNominaTotalGravadoPercepcion;
    }

    public void setValNominaTotalExentoDeduccion(String valNominaTotalExentoDeduccion) {
        this.valNominaTotalExentoDeduccion = valNominaTotalExentoDeduccion;
    }

    public String getValNominaTotalExentoDeduccion() {
        return valNominaTotalExentoDeduccion;
    }

    public void setValNominaTotalGravadoDeduccion(String valNominaTotalGravadoDeduccion) {
        this.valNominaTotalGravadoDeduccion = valNominaTotalGravadoDeduccion;
    }

    public String getValNominaTotalGravadoDeduccion() {
        return valNominaTotalGravadoDeduccion;
    }

    public void setValNominaTotalExentoPercepcion(String valNominaTotalExentoPercepcion) {
        this.valNominaTotalExentoPercepcion = valNominaTotalExentoPercepcion;
    }

    public String getValNominaTotalExentoPercepcion() {
        return valNominaTotalExentoPercepcion;
    }

    public void setCvePuesto(String cvePuesto) {
        this.cvePuesto = cvePuesto;
    }

    public String getCvePuesto() {
        return cvePuesto;
    }

    public void setTotalLetra(String totalLetra) {
        this.totalLetra = totalLetra;
    }

    public String getTotalLetra() {
        return totalLetra;
    }
}
