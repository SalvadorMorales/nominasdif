package facturareporte;

import cargaxmlreporte.CargaXml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Reporte {
    private Map<String, Object> datos = new HashMap<String, Object>();
    private String urlReporte;
    private List<String> listFacuras = new ArrayList<String>();


    public void setDatos(Map<String, Object> datos) {
        this.datos = datos;
    }

    public Map<String, Object> getDatos() {
        return datos;
    }

    public void setUrlReporte(String urlReporte) {
        this.urlReporte = urlReporte;
    }

    public String getUrlReporte() {
        return urlReporte;
    }

    public void setListFacuras(List<String> listFacuras) {
        this.listFacuras = listFacuras;
    }

    public List<String> getListFacuras() {
        return listFacuras;
    }

    public void addFactura(String FC) {
        this.listFacuras.add(FC);
    }


}
