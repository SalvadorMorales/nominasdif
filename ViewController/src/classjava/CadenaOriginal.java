package classjava;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


public class CadenaOriginal {
    public CadenaOriginal() {
        super();
    }
    /**
     * Método que genera la cadena original del XML utilizando la hoja de transformación dada por el 
     * SAT
     *  
     * @param xml       Documento XML 
     * @param xslt      Hoja de transformación 
     * @return          Cadena original del documento XML
     */
    
    public byte[] generaCadenaDos(InputStream xml, String xslt){
        ByteArrayOutputStream output= new ByteArrayOutputStream();
        try{
            StreamSource xmlFile = new StreamSource(xml);
            StreamSource xsltFile = new StreamSource(new File(xslt));
            TransformerFactory xsltFactory = TransformerFactory.newInstance();
            Transformer transformer = xsltFactory.newTransformer(xsltFile);
            transformer.transform(xmlFile, new StreamResult(output));
            
            return output.toByteArray();
        }catch(Exception exp){
        
        }
        
        return output.toByteArray();
    }
    
    /**
     * Método que genera la cadena original del XML utilizando la hoja de transformación dada por el 
     * SAT
     *  
     * @param xml       Documento XML 
     * @param xslt      Hoja de transformación 
     * @return          Cadena original del documento XML
     */
    
    public String formarCadenaOriginal(InputStream xml, String xslt){
        try{
            StreamSource xmlFile = new StreamSource(xml);
            StreamSource xsltFile = new StreamSource(new File(xslt));
            TransformerFactory xsltFactory = TransformerFactory.newInstance();
            Transformer transformer = xsltFactory.newTransformer(xsltFile);
            ByteArrayOutputStream output= new ByteArrayOutputStream();            
            transformer.transform(xmlFile, new StreamResult(output));
            return output.toString();
        }catch(Exception exp){
           
        }
        return null;
    }

    /*  public String limpiaCadenaOriginal(String cadenaOriginal){
        String cadenaAux = "||";
        String cad = cadenaOriginal.replace("||", "");
        String [] split = cad.replace("|", "#").split("#");
        for(String a : split){
            cadenaAux += a.trim() + "|";   
        }    
        cadenaAux += "|";
        return cadenaAux;
    } */
}
