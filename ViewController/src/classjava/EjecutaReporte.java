/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classjava;

import cargaxmlreporte.CargaXml;

import cargaxmlreporte.LeerXml11;

import cargaxmlreporte.LeerXml12;
//
//import cfdixmlv11.Nomina;

import com.mx.quadrum.cfdi.CFDIv32;
import com.mx.quadrum.cfdi.complementos.nomina.v12.schema.Nomina;
import com.mx.quadrum.cfdi.complementos.timbrefiscaldigital.v01.schema.TimbreFiscalDigital;
import com.mx.quadrum.cfdi.utilerias.UtileriasCfdi;
import com.mx.quadrum.cfdi.v32.schema.Comprobante;
import com.mx.quadrum.cfdi.nomina.addenda.Addenda;
import com.mx.quadrum.cfdi.complementos.nomina.v12.schema.Nomina;
import com.mx.quadrum.cfdi.complementos.nomina.v12.schema.Nomina.Deducciones.Deduccion;
import com.mx.quadrum.cfdi.complementos.nomina.v12.schema.Nomina.Percepciones.Percepcion;
import com.mx.quadrum.cfdi.complementos.timbrefiscaldigital.v01.schema.TimbreFiscalDigital;

import entities.Recibos;

import generadorqr.QRZxing;

import interfaces.SessionRecibosRemote;

import java.awt.Desktop;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import java.math.BigDecimal;

import java.math.RoundingMode;

import java.nio.file.Files;
import java.nio.file.Path;

import java.nio.file.Paths;

import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import java.sql.Time;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletResponse;

import javax.xml.bind.JAXBException;

import nomina.Percepciones;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

import nomina.Deducciones;
import nomina.PercepcionesDeducciones;

import numeroConLetra.NumberToLetterConverter;

import net.sf.jasperreports.engine.JRException;

import org.joda.time.DateTime;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import servicelocator.MyServiceLocator;

import util.CifradoCesar;

public class EjecutaReporte {

    CargaXml documentoXML;
    LeerXml11 xml = new LeerXml11();
    LeerXml12 xml12 = new LeerXml12();
    DecimalFormat format = new DecimalFormat("#,###,###.00");
    //DecimalFormat df2 = new DecimalFormat("#,###,###.00");
    private String userDir = System.getProperty("user.dir");
    //private static String RUTA = "/data/dif/xml/DIFXML2/";
    private static String RUTA = "E:\\DIFXML2\\";
    SessionRecibosRemote ejbRecibos;
    Recibos recibo = null;
    DateTime dateTime = new DateTime();
    DateTimeFormatter atf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    CifradoCesar cifrado = new CifradoCesar();
    public static final SimpleDateFormat formatoFechaY = new SimpleDateFormat("yyyy-MM-dd");
            public static final SimpleDateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
            public static final DecimalFormat df2 = new DecimalFormat("#,###,###.00");
    public static void main(String[] args) {

        List<String> misUUIDS = new ArrayList<String>();
        misUUIDS.add("F240E8C6-B001-4D4A-9E94-4ADF57570227");
        // misUUIDS.add("78A05B20-4008-4633-9330-D5A4793D04EB");
        //misUUIDS.add("587D87F0-EA0F-4D09-8850-55497F0F1B6A");
        misUUIDS.add("97142A16-9929-4372-A6F0-A484600F4CA4");
        // misUUIDS.add("BC18D0F7-4E54-4618-A826-1F17086B598C");

        //generarReporte(misUUIDS);
    }

    public void generarReporte(List<String> misUUIDS, String tipo,String versionNomina) throws JAXBException {
        
        
        
        FacesContext fcontext = FacesContext.getCurrentInstance();
        ExternalContext econtext = fcontext.getExternalContext();
        List<JasperPrint> jasperPrint = new ArrayList<JasperPrint>();
        String nombre = "";
        if (misUUIDS.size() > 1) {
            nombre = "Facturas";
        } else {
            nombre = "" + misUUIDS.get(0);
        }
        try {
            for (String uuid : misUUIDS) {
                InputStream is = new ByteArrayInputStream(uuid.getBytes());
                Map<String, Object> params = new HashMap<String, Object>();
                //String ruta = "/data/dif/reporteee.jasper";
                String ruta = "C:\\Users\\Pedro Denova\\Documents\\NetBeansProjects\\DIF19102015\\copiadif\\validaciones\\jasper\\reporteee.jasper";
                JasperReport jasperReport = (JasperReport)JRLoader.loadObjectFromFile(ruta);
                if(versionNomina.equals("1.1")){
                    params = obtieneParametros11(uuid);
                    jasperPrint.add(JasperFillManager.fillReport(jasperReport, params, getDataSourceXML11(is, uuid)));
                }else{
                    //params = obtieneParametrosXML(uuid);
                    try {
                            Path dirXml = Paths.get(RUTA + uuid + ".xml");
                            byte[] fileXMLComprobante = Files.readAllBytes(dirXml);
                            Class[] classes = { Comprobante.class, Nomina.class, TimbreFiscalDigital.class,Addenda.class};
                            Comprobante comprobante = UtileriasCfdi.convertirAComprobante(fileXMLComprobante, classes);
                            Nomina nomina = (Nomina) UtileriasCfdi.getObjectoComplemento(comprobante.getComplemento().getAny(),
                                            Nomina.class);
                            TimbreFiscalDigital timbre = (TimbreFiscalDigital) UtileriasCfdi
                                            .getObjectoComplemento(comprobante.getComplemento().getAny(), TimbreFiscalDigital.class);
                            CFDIv32 cfdIv32 = new CFDIv32(comprobante);
                            
                            Addenda addenda=(Addenda)UtileriasCfdi.getObjectoComplemento(comprobante.getAddenda().getAny(), Addenda.class);

                            //GenerarPdf pdf = new GenerarPdf();

                            //pdf.ejecutar(comprobante, nomina, timbre, addenda, rutapdf);
                        
                        params = obtieneParametrosXMLRutaDif(comprobante, nomina, timbre, addenda);
                        jasperPrint.add(JasperFillManager.fillReport(jasperReport, params, getDataSource(nomina)));

                    } catch (Exception e) {
                            e.printStackTrace();
                    }
                    
                    }
                
            }
            JRExporter exporter = null;
            exporter = new JRPdfExporter();

            if (tipo.equals("web")) {
                System.out.println("web");
                HttpServletResponse response = (HttpServletResponse)econtext.getResponse();
                response.setContentType("application/pdf");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + nombre + ".pdf\";");
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Pragma", "no-cache");
                response.setDateHeader("Expires", 0);
                exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
                fcontext.responseComplete();

            } else if (tipo.equals("pdf")) {
                System.out.println("pdf" + nombre);
                File pdfEnvio = new File(userDir + "/PDF/" + nombre + ".pdf");
                System.out.println("pdfEnvio " + pdfEnvio);
                userDir = System.getProperty("user.dir");
                exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_FILE, pdfEnvio);
            }
            if (exporter != null) {

                exporter.exportReport();
                jasperPrint.clear();
                misUUIDS.clear();
            }

            //Desktop.getDesktop().open(path);
            //fileStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static Map<String, Object> obtieneParametrosXMLRutaDif(Comprobante comprobante, Nomina nomina,
                            TimbreFiscalDigital timbreFiscalDigital, Addenda addenda) throws JAXBException {
                    Map<String, Object> parametros = new HashMap<>();
                    InputStream log = null;
                     try {
                        //log = new FileInputStream("./archivos/imagen/logo.png");
                        log = new FileInputStream(new File("C:\\Users\\Pedro Denova\\Documents\\NetBeansProjects\\difRepositorio\\copiadif\\validaciones\\imagenes\\logo.png"));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    String pais = new String("M\\u00e9xico");
                    String inicio = formatoFechaY.format(nomina.getFechaInicialPago());
                            String fin = formatoFechaY.format(nomina.getFechaFinalPago());
                    String fecha = comprobante.getFecha().toString();
    //              String inicio = nomina.getFechaInicialPago().toString();
    //              String fin = nomina.getFechaFinalPago().toString();
                    parametros.put("logo", log);
                    //Llenar parametros desde XML
                    parametros.put("nombreEmpresa", comprobante.getEmisor().getNombre());
                    parametros.put("direccEmpresa", "Emiliano Zapata No. 340 Colonia Santa Cruz Atoyac, Delegación Benito Juárez, C.P. 03310, Ciudad de México.");
                    parametros.put("rfcEmpresa", comprobante.getEmisor().getRfc());
                    parametros.put("quincena", addenda.getQuincena());
                    parametros.put("regpEmpresa", comprobante.getEmisor().getRegimenFiscal());
                    parametros.put("lugarPago", pais.replace("?", "\u00e9"));
                    parametros.put("fechaPago",formatoFechaY.format(nomina.getFechaPago()));
                    parametros.put("zonaPago", addenda.getZonadepago());
                    //Datos Empleado
                    parametros.put("nombreTrabajador", comprobante.getReceptor().getNombre());
                    parametros.put("numTrabajador", nomina.getReceptor().getNumEmpleado());
                    parametros.put("horarioTrabajador", addenda.getHorario());
                    parametros.put("nombramientoTrab", addenda.getNombramiento());
                    parametros.put("puestoTrabajador", nomina.getReceptor().getPuesto());
                    parametros.put("jornadaTrabajador", addenda.getJornada());
                    parametros.put("rfcTrabajador", comprobante.getReceptor().getRfc());
                    parametros.put("depositoBanco", addenda.getBanco());
                    parametros.put("mensajes", addenda.getMensaje());
                    parametros.put("cuentaTrabajador", addenda.getCuenta());
                    //DIF
                    parametros.put("adscripcionTrabajador", nomina.getReceptor().getDepartamento());
                    parametros.put("antiguedadDIF", addenda.getAntiguedad());
                    parametros.put("claveDIF", addenda.getCvePuesto());
                    parametros.put("tipoRegimen", nomina.getReceptor().getTipoRegimen());
                    parametros.put("curpTrabajador", nomina.getReceptor().getCurp());
                    parametros.put("cuentaTrabajador", addenda.getCuenta());
                    parametros.put("quinquenios", addenda.getQuinquenio());
                    parametros.put("plaza", addenda.getPlaza());
                    parametros.put("claveNss", nomina.getReceptor().getNumSeguridadSocial());
                    parametros.put("nss", nomina.getReceptor().getNumSeguridadSocial());
                    parametros.put("periodoPago", inicio + " al " + fin);
                            double sumPercepciones = 0;
                            sumPercepciones = Double.valueOf(nomina.getTotalPercepciones().toString());

                            double sumDeducciones = 0;
                            sumDeducciones = Double.valueOf(nomina.getTotalDeducciones().toString());

                            parametros.put("percepcionesTotal", String.valueOf(df2.format(sumPercepciones)));
                            parametros.put("deduccionesTotal", String.valueOf(df2.format(sumDeducciones)));
                    parametros.put("totalNeto", String.valueOf(df2.format((comprobante.getTotal()))));

                    parametros.put("totalLetra", addenda.getTotalLetra());
                    parametros.put("clave", addenda.getClave());
                    //parametros.put("Descripcion", leerXml12.getConceptos().getConcepto().get(0).getValorUnitario());
                    String cadenaOriginal = "||" + comprobante.getVersion() + "|" + timbreFiscalDigital.getUUID() + "|"
                                            + timbreFiscalDigital.getFechaTimbrado() + "|" + timbreFiscalDigital.getSelloCFD() + "|"
                                            + timbreFiscalDigital.getNoCertificadoSAT() + "||";
                    parametros.put("Importe", String.valueOf(df2.format((comprobante.getTotal()))));
                    //SAT
                    parametros.put("folioFiscal",timbreFiscalDigital.getUUID());
                    parametros.put("fechaTimbrado", timbreFiscalDigital.getFechaTimbrado().toString());
                    parametros.put("nscSAT", timbreFiscalDigital.getNoCertificadoSAT());
                    parametros.put("recibiDe", comprobante.getEmisor().getRfc());
                    parametros.put("selloSAT", timbreFiscalDigital.getSelloSAT());
                    parametros.put("selloCFDI", timbreFiscalDigital.getSelloCFD());
                    parametros.put("coccdSAT", cadenaOriginal);
                    DecimalFormat formatoQr = new DecimalFormat("###########0000000000.000000");
                    QRZxing miQR = null;
                    miQR = new QRZxing(comprobante.getEmisor().getRfc(), comprobante.getReceptor().getRfc(),
                            formatoQr.format(Double.parseDouble(comprobante.getTotal().toString())),
                            "");
                    parametros.put("sello", miQR.getMiStream());
                    return parametros;
            }
    
    private static JRDataSource getDataSource(Nomina nomina) {
                    Collection<PercepcionesDeducciones> coll = new ArrayList<PercepcionesDeducciones>();
                    PercepcionesDeducciones bean;

                    int mayor = nomina.getPercepciones().getPercepcion().size() > nomina.getDeducciones().getDeduccion().size()
                                    ? nomina.getPercepciones().getPercepcion().size() : nomina.getDeducciones().getDeduccion().size();
                    String[][] claves = new String[mayor][2];
                    String[][] concepto = new String[mayor][2];
                    String[][] importe = new String[mayor][2];
                    int x = 0;
                    for (int f = 0; f < mayor; f++) {
                            claves[f][0] = claves[f][1] = concepto[f][0] = concepto[f][1] = importe[f][0] = importe[f][1] = "";
                    }
                    // Obtiene percepciones de XML
                    for (Percepcion obj : nomina.getPercepciones().getPercepcion()) {
                            claves[x][0] = obj.getClave();
                            concepto[x][0] = obj.getConcepto();
                            importe[x][0] = "" + df2.format(obj.getImporteGravado().add(obj.getImporteExento()));
                            x++;
                    }
                    // Deducciones con Deducciones
                    x = 0;
                    for (Deduccion obj : nomina.getDeducciones().getDeduccion()) {
                            claves[x][1] = obj.getClave();
                            concepto[x][1] = obj.getConcepto();
                            importe[x][1] = "" + df2.format(obj.getImporte());
                            x++;
                    }
                    for (int f = 0; f < mayor; f++) {
                            bean = new PercepcionesDeducciones(claves[f][0], concepto[f][0], importe[f][0], claves[f][1],
                                            concepto[f][1], importe[f][1]);
                            coll.add(bean);
                    }
                    return new JRBeanCollectionDataSource(coll);
            }

    public static void verPDF(JasperPrint im) {
        JasperViewer ver = new JasperViewer(im);
        ver.setTitle("Report");
        ver.setVisible(true);
    }

    private JRDataSource getDataSource(InputStream XML, String uuid) {
        Collection<PercepcionesDeducciones> coll = new ArrayList<PercepcionesDeducciones>();
        PercepcionesDeducciones bean;

        if (true) {
            //Deducciones con Percepciones
            int mayor =
                documentoXML.getMisPerrcepciones().size() > documentoXML.getMisDeducciones().size() ? documentoXML.getMisPerrcepciones().size() :
                documentoXML.getMisDeducciones().size();
            String[][] claves = new String[mayor][2];
            String[][] concepto = new String[mayor][2];
            String[][] importe = new String[mayor][2];

            int x = 0;
            for (int f = 0; f < mayor; f++) {
                claves[f][0] = claves[f][1] = concepto[f][0] = concepto[f][1] = importe[f][0] = importe[f][1] = "";
            }

            //Obtiene percepciones de XML
            for (Percepciones obj : documentoXML.getMisPerrcepciones()) {
                claves[x][0] = obj.getClave();
                concepto[x][0] = obj.getConcepto();
                importe[x][0] = "" + format.format(obj.getImporteGravado().add(obj.getImporteExento()));
                x++;
            }

            //Deducciones con Deducciones
            x = 0;
            for (Deducciones obj : documentoXML.getMisDeducciones()) {
                claves[x][1] = obj.getClave();
                concepto[x][1] = obj.getConcepto();
                importe[x][1] = "" + format.format(obj.getImporteExento().add(obj.getImporteGravado()));
                x++;
            }

            for (int f = 0; f < mayor; f++) {
                bean =
new PercepcionesDeducciones(claves[f][0], concepto[f][0], importe[f][0], claves[f][1], concepto[f][1], importe[f][1]);
                coll.add(bean);
            }
            return new JRBeanCollectionDataSource(coll);
        } else {

            //Agregar Percepciones y Deducciones
            bean = new PercepcionesDeducciones("001", "SUELDO BASE", "3,427.75", "291", "I.S.R", "1,881.03");
            coll.add(bean);

            bean = new PercepcionesDeducciones("006", "DESPENSA", "182.50", "295", "SEGURO COLECTIVO RETIRO", "7.27");
            coll.add(bean);

            bean = new PercepcionesDeducciones("008", "PRIMA QUINCENAL", "50.00", "305", "ALIMENTOS", "35.00");
            coll.add(bean);

            bean =
new PercepcionesDeducciones("011", "COMPENSACION GARANTIZADA", "7,648.90", "308", "AHORRO SOLIDARIO ISSTE", "69.56");
            coll.add(bean);

            bean =
new PercepcionesDeducciones("181", "SSI APORTACION DIF", "1,107.67", "309", "ISSSTE SEGURO DE RETIRO,CESANTIA Y VEJEZ",
                            "213.01");
            coll.add(bean);

            bean =
new PercepcionesDeducciones("184", "REINTEGRO ISR-SSI", "340.65", "310", "I.S.S.S.T.E SEGURO DE SALUD", "156.50");
            coll.add(bean);

            //Agregar Percepciones
            //Agregar Deducciones
            bean = new PercepcionesDeducciones("", "", "", "381", "SSI APORTACION DIF", "1,107.67");
            coll.add(bean);

            bean = new PercepcionesDeducciones("", "", "", "382", "PRIMA SSI", "1,107.67");
            coll.add(bean);

            bean = new PercepcionesDeducciones("", "", "", "384", "ISR-SSI", "340.65");
            coll.add(bean);

        }

        return new JRBeanCollectionDataSource(coll);
    }
    
    
    
    

    public String cambioFormato(String entrada) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String salida;
        Date date;
        Calendar calendar = Calendar.getInstance();
        int anio = 0, mes = 0, dia = 0;
        try {
            date = df.parse(entrada);
            calendar.setTime(date);
            anio = calendar.get(Calendar.YEAR);
            mes = calendar.get(Calendar.MONTH) + 1;
            dia = calendar.get(Calendar.DAY_OF_MONTH);
        } catch (ParseException ex) {
            Logger.getLogger(EjecutaReporte.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (mes < 10) {
            salida = dia + "/0" + mes + "/" + anio;
        } else {
            salida = dia + "/" + mes + "/" + anio;
        }
        return salida;
    }

    public Map<String, Object> obtieneParametrosXML(String uuid) throws JAXBException {

        Map<String, Object> parametros = new HashMap<String, Object>();
        documentoXML = new CargaXml(RUTA + uuid + ".xml");

        InputStream log = null;
        try {
            log =
new FileInputStream(new File("/data/dif/dif.jpg"));

            String pais = documentoXML.getPaisEmisor();
            String fecha = documentoXML.getValNominaFechaPago();
            String inicio = documentoXML.getValNominaFechaInicial();
            String fin = documentoXML.getValNominaFechaFinal();

            parametros.put("logo", log);
            //Llenar parametros desde XML
            parametros.put("nombreEmpresa", documentoXML.getNombreEmisor());
            parametros.put("direccEmpresa", documentoXML.getCalleEmisor());
            parametros.put("rfcEmpresa", documentoXML.getRfcEmisor());
            parametros.put("quincena", documentoXML.getQuincena());
            parametros.put("regpEmpresa", documentoXML.getRegimenFiscal());
            parametros.put("lugarPago", pais.replace("?", "\u00e9"));
            parametros.put("fechaPago", cambioFormato(fecha));
            parametros.put("zonaPago", documentoXML.getZonaPago());
            //Datos Empleado
            parametros.put("nombreTrabajador", documentoXML.getRazonSocialReceptor());
            parametros.put("numTrabajador", documentoXML.getValNominaNumeroEmpleado());
            parametros.put("horarioTrabajador", documentoXML.getHorario());
            parametros.put("nombramientoTrab", documentoXML.getValNominaTipoContrato());
            parametros.put("puestoTrabajador", documentoXML.getValNominaPuesto());
            parametros.put("jornadaTrabajador", documentoXML.getValNominaTipoJornada());
            parametros.put("rfcTrabajador", documentoXML.getRfcReceptor());
            parametros.put("depositoBanco", documentoXML.getBanco());
            parametros.put("mensajes", documentoXML.getMensaje());
            parametros.put("cuentaTrabajador", documentoXML.getCuenta());
            //DIF
            parametros.put("adscripcionTrabajador", documentoXML.getValNominaDepartamento());
            parametros.put("antiguedadDIF", documentoXML.getAntiguedad());
            parametros.put("claveDIF", documentoXML.getCvePuesto());
            parametros.put("tipoRegimen", documentoXML.getValNominaTipoRegimen());
            parametros.put("curpTrabajador", documentoXML.getValNominaCurp());
            parametros.put("cuentaTrabajador", documentoXML.getCuenta());
            parametros.put("quinquenios", documentoXML.getQuinquenio());
            parametros.put("plaza", documentoXML.getPlaza());
            parametros.put("claveNss", documentoXML.getClave());
            parametros.put("nss", documentoXML.getValNominaNumeroSeguridadSocial());
            parametros.put("periodoPago", cambioFormato(inicio) + " al " + cambioFormato(fin));
            double sumPercepciones = 0;
            sumPercepciones =
                    Double.valueOf(documentoXML.getValNominaTotalGravadoPercepcion()) + Double.valueOf(documentoXML.getValNominaTotalExentoPercepcion());

            double sumDeducciones = 0;
            sumDeducciones =
                    Double.valueOf(documentoXML.getValNominaTotalGravadoDeduccion()) + Double.valueOf(documentoXML.getValNominaTotalExentoDeduccion());

            parametros.put("percepcionesTotal", String.valueOf(df2.format(sumPercepciones)));
            parametros.put("deduccionesTotal", String.valueOf(df2.format(sumDeducciones)));
            parametros.put("totalNeto", String.valueOf(df2.format(new Double(documentoXML.getTotal()))));

            parametros.put("totalLetra", documentoXML.getTotalLetra());
            parametros.put("clave", documentoXML.getValNominaClabe());
            parametros.put("Descripcion", documentoXML.getValorUnitario());
            parametros.put("Importe", documentoXML.getTotal());
            //SAT
            parametros.put("folioFiscal", documentoXML.getFolioFiscal());
            parametros.put("fechaTimbrado", documentoXML.getFechaTimbrado());
            parametros.put("nscSAT", documentoXML.getNoCertificadoSat());
            parametros.put("recibiDe", documentoXML.getNombreEmisor());
            parametros.put("selloSAT", documentoXML.getSelloSAT());
            parametros.put("selloCFDI", documentoXML.getSellodigital());
            parametros.put("coccdSAT", documentoXML.getCadenaOriginal());
            DecimalFormat formatoQr = new DecimalFormat("###########0000000000.000000");
            QRZxing miQR = null;
            miQR =
new QRZxing(documentoXML.getRfcEmisor(), documentoXML.getRfcReceptor(), formatoQr.format(Double.parseDouble(documentoXML.getTotal())),
            "");
            parametros.put("QR", miQR.getMiStream());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return parametros;
    }
    
    
    
    
    public Map<String, Object> obtieneParametros11(String uuid) throws JAXBException {
        
        
        String dts = atf.print(dateTime);
        MyServiceLocator myService = new MyServiceLocator();
        //FileInputStream fis;
        String varFirma = "";
        Map<String, Object> parametros = new HashMap<String, Object>();

        try {
            //fis = new FileInputStream(RUTA + uuid + ".xml");
            
        ejbRecibos = myService.getSessionRecibosRemote();
    
        recibo = ejbRecibos.getReciboByUuid(cifrado.Encriptar(uuid,10));
            System.out.println("----->"+cifrado.Encriptar(uuid,10));
            /*  if(recibo.getFirmaDigital() == null){
                varFirma = xml.leeXml(RUTA + uuid + ".xml");
                recibo.setFirmaDigital(varFirma);
                recibo.setFechaConsulta(dts);
                ejbRecibos.mergeRecibos(recibo);
            } else {
                varFirma = recibo.getFirmaDigital();
                    xml.leeXml(RUTA + uuid + ".xml");
                } */
            String rfcE = xml.getEmisor().getRfc();
            InputStream log = null;
            log =
            new FileInputStream(new File("E:\\RFCs\\"+rfcE+"\\"+rfcE+"_Imagenes"+"\\h.jpg"));

            String pais = "M�xico";
            
            String fecha = xml.getNomina().getFechaPago().toString();
            String inicio = xml.getNomina().getFechaInicialPago().toString();
            System.out.println("******************** "+fecha);
            String fin = xml.getNomina().getFechaFinalPago().toString();

            parametros.put("logo", log);
            //Llenar parametros desde XML
            parametros.put("rfcEmpresa", xml.getEmisor().getRfc());
                        parametros.put("nombreEmpresa", xml.getEmisor().getNombre());    
                        parametros.put("regpEmpresa",  xml.getEmisor().getRegimenFiscal().get(0).getRegimen());
                        parametros.put("direccEmpresa",  xml.getComprobante().getLugarExpedicion());       
                        //parametros.put("cp","cp");
                        parametros.put("nombreTrabajador",  xml.getReceptor().getNombre());       
                        parametros.put("sueldoDiario",    xml.getNomina().getSalarioDiarioIntegrado().toString());
                        parametros.put("sueldoDiarioIntegrado",  xml.getNomina().getSalarioDiarioIntegrado().toString());
                        parametros.put("nss", xml.getNomina().getNumSeguridadSocial());
                        parametros.put("rfcTrabajador", xml.getReceptor().getRfc()); 
                        parametros.put("curpTrabajador",  xml.getNomina().getCURP());
                        parametros.put("periodoPago", xml.getNomina().getFechaInicialPago().toString()+ " al " +xml.getNomina().getFechaFinalPago().toString());
                        parametros.put("metodoPago",xml.getComprobante().getMetodoDePago());
                        parametros.put("dias",  xml.getNomina().getNumDiasPagados().toString());
                        parametros.put("numTrabajador", xml.getNomina().getNumEmpleado().toString()); 
                        parametros.put("percepcionesTotal",  xml.getNomina().getPercepciones().getTotalExento().toString());
                        parametros.put("deduccionesTotal",  xml.getNomina().getDeducciones().getTotalExento().toString());
                        parametros.put("tipoPago",  xml.getNomina().getPeriodicidadPago());
                        parametros.put("efirma",varFirma);
            //        else 
            //        {
            //            parametros.put("totalExentoDeduccion",  "0.0"); 
            //        }
            //            parametros.put("totalGravadoDeduccion",  documentoXML.getValNominaTotalGravadoDeduccion());
            //        else 
            //        {
            //            parametros.put("totalGravadoDeduccion", "0.0");
            //        }
            //        
                    
                        parametros.put("totalNeto",xml.getComprobante().getTotal().toString());
                     parametros.put("totalLetra",  NumberToLetterConverter.convertNumberToLetterMethod(xml.getComprobante().getTotal().toString(), xml.getComprobante().getMoneda()) + ", " + regresaCentavos(xml.getComprobante().getTotal().toString(), xml.getComprobante().getMoneda()));
                        parametros.put("folioFiscal",  xml.getUuid());      
                        parametros.put("nscSAT",  xml.getNoCertificadoSAT());
                        parametros.put("selloCFDI", xml.getSelloCFD());
                        parametros.put("selloSAT", xml.getSelloSAT());
                        parametros.put("coccdSAT",  xml.getCadenaOriginal());
                    //parametros.put("SelloSAT",  documentoXML.getSelloSAT());        
                        parametros.put("fechaTimbrado", xml.getComprobante().getFecha().toString());
            DecimalFormat formatoQr = new DecimalFormat("###########0000000000.000000");
            QRZxing miQR = null;
            miQR =
    new QRZxing(xml.getEmisor().getRfc(), xml.getReceptor().getRfc(), formatoQr.format(Double.parseDouble(xml.getComprobante().getTotal().toString())),
            "");
            parametros.put("QR", miQR.getMiStream());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return parametros;
    }
    
    
    
    public Map<String, Object> obtieneParametros12(String uuid) throws JAXBException {
        
        
        String dts = atf.print(dateTime);
        MyServiceLocator myService = new MyServiceLocator();
        //FileInputStream fis;
        String varFirma = "";
        Map<String, Object> parametros = new HashMap<String, Object>();

        try {
            //fis = new FileInputStream(RUTA + uuid + ".xml");
            ejbRecibos = myService.getSessionRecibosRemote();
            recibo = ejbRecibos.getReciboByUuid(cifrado.Encriptar(uuid,10));
            /* if(recibo.getFirmaDigital() == null){
            System.out.println("Entra null");
                varFirma = xml12.leeXml(RUTA + uuid + ".xml");
                
        
                recibo.setFirmaDigital(varFirma);
                recibo.setFechaConsulta(dts);
                ejbRecibos.mergeRecibos(recibo);
            }else{ */
                System.out.println("entra diferente a null");
                varFirma = xml12.leeXml(RUTA + uuid + ".xml");
            System.out.println("RUTA"+RUTA);
                //}
            String rfcE = xml12.getEmisor().getRfc();
        InputStream log = null;
            log =
            new FileInputStream(new File("/data/dif/dif.jpg"));

            String pais = "México";
            
            String fecha = xml12.getNomina().getFechaPago().toString();
            String inicio = xml12.getNomina().getFechaInicialPago().toString();
            System.out.println("******************** "+fecha);
            String fin = xml12.getNomina().getFechaFinalPago().toString();

            parametros.put("logo", log);
            //Llenar parametros desde XML
            parametros.put("rfcEmpresa", xml12.getEmisor().getRfc());
                        parametros.put("nombreEmpresa", xml12.getEmisor().getNombre());    
                        parametros.put("regpEmpresa",  xml12.getEmisorNomina().getRegistroPatronal());
                        parametros.put("direccEmpresa",  xml12.getComprobante().getLugarExpedicion());       
                        //parametros.put("cp","cp");
                        parametros.put("nombreTrabajador",  xml12.getReceptor().getNombre());
                        if(xml12.getReceptorNomina().getSalarioDiarioIntegrado() != null)
                        parametros.put("sueldoDiario",    xml12.getReceptorNomina().getSalarioDiarioIntegrado().toString());
                        if(xml12.getReceptorNomina().getSalarioDiarioIntegrado() != null)
                        parametros.put("sueldoDiarioIntegrado",  xml12.getReceptorNomina().getSalarioDiarioIntegrado().toString());
                        if(xml12.getReceptorNomina().getNumSeguridadSocial() != null)
                        parametros.put("nss", xml12.getReceptorNomina().getNumSeguridadSocial());
                        if(xml12.getReceptor().getRfc() != null)
                        parametros.put("rfcTrabajador", xml12.getReceptor().getRfc()); 
                        if(xml12.getReceptorNomina().getCurp() != null)
                        parametros.put("curpTrabajador",  xml12.getReceptorNomina().getCurp());
                        parametros.put("periodoPago", xml12.getNomina().getFechaInicialPago().toString()+ " al " +xml12.getNomina().getFechaFinalPago().toString());
                        if(xml12.getComprobante().getMetodoDePago() != null)
                        parametros.put("metodoPago",xml12.getComprobante().getMetodoDePago());
                        if(xml12.getNomina().getNumDiasPagados() != null)
                        parametros.put("dias",  xml12.getNomina().getNumDiasPagados().toString());
                        if(xml12.getReceptorNomina().getNumEmpleado() != null)
                        parametros.put("numTrabajador", xml12.getReceptorNomina().getNumEmpleado().toString()); 
                        parametros.put("percepcionesTotal",  xml12.getNomina().getTotalPercepciones().toString());
                        parametros.put("deduccionesTotal",  xml12.getNomina().getTotalDeducciones().toString());
                        if(xml12.getNomina().getReceptor().getPeriodicidadPago() != null)
                        parametros.put("tipoPago",  xml12.getNomina().getReceptor().getPeriodicidadPago());
                        parametros.put("totalNeto",xml12.getComprobante().getTotal().toString());
                        parametros.put("totalLetra",  NumberToLetterConverter.convertNumberToLetterMethod(xml12.getComprobante().getTotal().toString(), xml12.getComprobante().getMoneda()) + ", " + regresaCentavos(xml12.getComprobante().getTotal().toString(), xml12.getComprobante().getMoneda()));
                        parametros.put("folioFiscal",  xml12.getTimbreFiscalDigital().getUUID());      
                        parametros.put("nscSAT",  xml12.getTimbreFiscalDigital().getNoCertificadoSAT());
                        parametros.put("selloCFDI", xml12.getTimbreFiscalDigital().getSelloCFD());
                        parametros.put("selloSAT", xml12.getTimbreFiscalDigital().getSelloSAT());
                        parametros.put("coccdSAT",  xml12.getCadenaOriginal());
                        //DIF
                       
                        //*DIF
                        parametros.put("efirma", varFirma);
                    //parametros.put("SelloSAT",  documentoXML.getSelloSAT());  
                        parametros.put("fechaTimbrado", xml12.getComprobante().getFecha().toString());
                        DecimalFormat formatoQr = new DecimalFormat("###########0000000000.000000");
            QRZxing miQR = null;
            miQR =
    new QRZxing(xml12.getEmisor().getRfc(), xml12.getReceptor().getRfc(), formatoQr.format(Double.parseDouble(xml12.getComprobante().getTotal().toString())),
            "");
            parametros.put("QR", miQR.getMiStream());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return parametros;
    }
    
    
    /**
     * Método que obtiene los centavamos en caso de existir en el total de comprobante 
     */

    public String regresaCentavos(String total, String moneda) {
        BigDecimal num = new BigDecimal(total).setScale(2,RoundingMode.HALF_UP);
        System.out.println("_> "+num);
        String splitTotal[] = num.toString().replace(".", "#").split("#");
        if(moneda.compareTo("MXN") == 0)
            return splitTotal[1] + "/100 m.n.";
        return splitTotal[1] + " CENTAVOS.";
    }
    
    
    
    private JRDataSource getDataSourceXML(InputStream XML, String uuid) {
        Collection<PercepcionesDeducciones> coll = new ArrayList<PercepcionesDeducciones>();
        PercepcionesDeducciones bean;

        if (true) {
            //Deducciones con Percepciones
            int mayor =
                xml12.getNomina().getPercepciones().getPercepcion().size() > xml12.getNomina().getDeducciones().getDeduccion().size() ? xml12.getNomina().getPercepciones().getPercepcion().size() :
                xml12.getNomina().getDeducciones().getDeduccion().size();
            String[][] claves = new String[mayor][2];
            String[][] concepto = new String[mayor][2];
            String[][] importe = new String[mayor][2];

            int x = 0;
            for (int f = 0; f < mayor; f++) {
                claves[f][0] = claves[f][1] = concepto[f][0] = concepto[f][1] = importe[f][0] = importe[f][1] = "";
            }

            //Obtiene percepciones de XML
            for (cfdixml12.Nomina.Percepciones.Percepcion obj : xml12.getNomina().getPercepciones().getPercepcion()) {
                claves[x][0] = obj.getClave();
                concepto[x][0] = obj.getConcepto();
                importe[x][0] = "" + format.format(obj.getImporteGravado().add(obj.getImporteExento()));
                x++;
            }

            //Deducciones con Deducciones
            x = 0;
            for (cfdixml12.Nomina.Deducciones.Deduccion obj : xml12.getNomina().getDeducciones().getDeduccion()) {
                claves[x][1] = obj.getClave();
                concepto[x][1] = obj.getConcepto();
                importe[x][1] = "" + format.format(obj.getImporte().add(obj.getImporte()));
                x++;
            }

            for (int f = 0; f < mayor; f++) {
                bean =
    new PercepcionesDeducciones(claves[f][0], concepto[f][0], importe[f][0], claves[f][1], concepto[f][1], importe[f][1]);
                coll.add(bean);
            }
            return new JRBeanCollectionDataSource(coll);
        } else {

            //Agregar Percepciones y Deducciones
            bean = new PercepcionesDeducciones("001", "SUELDO BASE", "3,427.75", "291", "I.S.R", "1,881.03");
            coll.add(bean);

            bean = new PercepcionesDeducciones("006", "DESPENSA", "182.50", "295", "SEGURO COLECTIVO RETIRO", "7.27");
            coll.add(bean);

            bean = new PercepcionesDeducciones("008", "PRIMA QUINCENAL", "50.00", "305", "ALIMENTOS", "35.00");
            coll.add(bean);

            bean =
    new PercepcionesDeducciones("011", "COMPENSACION GARANTIZADA", "7,648.90", "308", "AHORRO SOLIDARIO ISSTE", "69.56");
            coll.add(bean);

            bean =
    new PercepcionesDeducciones("181", "SSI APORTACION DIF", "1,107.67", "309", "ISSSTE SEGURO DE RETIRO,CESANTIA Y VEJEZ",
                            "213.01");
            coll.add(bean);

            bean =
    new PercepcionesDeducciones("184", "REINTEGRO ISR-SSI", "340.65", "310", "I.S.S.S.T.E SEGURO DE SALUD", "156.50");
            coll.add(bean);

            //Agregar Percepciones
            //Agregar Deducciones
            bean = new PercepcionesDeducciones("", "", "", "381", "SSI APORTACION DIF", "1,107.67");
            coll.add(bean);

            bean = new PercepcionesDeducciones("", "", "", "382", "PRIMA SSI", "1,107.67");
            coll.add(bean);

            bean = new PercepcionesDeducciones("", "", "", "384", "ISR-SSI", "340.65");
            coll.add(bean);

        }

        return new JRBeanCollectionDataSource(coll);
    }
    
    
    private JRDataSource getDataSourceXML11(InputStream XML, String uuid) {
        Collection<PercepcionesDeducciones> coll = new ArrayList<PercepcionesDeducciones>();
        PercepcionesDeducciones bean;

        if (true) {
            //Deducciones con Percepciones
            int mayor =
                xml.getNomina().getPercepciones().getPercepcion().size() > xml.getNomina().getDeducciones().getDeduccion().size() ? xml.getNomina().getPercepciones().getPercepcion().size() :
                xml.getNomina().getDeducciones().getDeduccion().size();
            String[][] claves = new String[mayor][2];
            String[][] concepto = new String[mayor][2];
            String[][] importe = new String[mayor][2];

            int x = 0;
            for (int f = 0; f < mayor; f++) {
                claves[f][0] = claves[f][1] = concepto[f][0] = concepto[f][1] = importe[f][0] = importe[f][1] = "";
            }

            //Obtiene percepciones de XML
            for (cfdixmlv11.Nomina.Percepciones.Percepcion obj : xml.getNomina().getPercepciones().getPercepcion()) {
                claves[x][0] = obj.getClave();
                concepto[x][0] = obj.getConcepto();
                importe[x][0] = "" + format.format(obj.getImporteGravado().add(obj.getImporteExento()));
                x++;
            }

            //Deducciones con Deducciones
            x = 0;
            for (cfdixmlv11.Nomina.Deducciones.Deduccion obj : xml.getNomina().getDeducciones().getDeduccion()) {
                claves[x][1] = obj.getClave();
                concepto[x][1] = obj.getConcepto();
                importe[x][1] = "" + format.format(obj.getImporteExento().add(obj.getImporteGravado()));
                x++;
            }

            for (int f = 0; f < mayor; f++) {
                bean =
    new PercepcionesDeducciones(claves[f][0], concepto[f][0], importe[f][0], claves[f][1], concepto[f][1], importe[f][1]);
                coll.add(bean);
            }
            return new JRBeanCollectionDataSource(coll);
        } else {

            //Agregar Percepciones y Deducciones
            bean = new PercepcionesDeducciones("001", "SUELDO BASE", "3,427.75", "291", "I.S.R", "1,881.03");
            coll.add(bean);

            bean = new PercepcionesDeducciones("006", "DESPENSA", "182.50", "295", "SEGURO COLECTIVO RETIRO", "7.27");
            coll.add(bean);

            bean = new PercepcionesDeducciones("008", "PRIMA QUINCENAL", "50.00", "305", "ALIMENTOS", "35.00");
            coll.add(bean);

            bean =
    new PercepcionesDeducciones("011", "COMPENSACION GARANTIZADA", "7,648.90", "308", "AHORRO SOLIDARIO ISSTE", "69.56");
            coll.add(bean);

            bean =
    new PercepcionesDeducciones("181", "SSI APORTACION DIF", "1,107.67", "309", "ISSSTE SEGURO DE RETIRO,CESANTIA Y VEJEZ",
                            "213.01");
            coll.add(bean);

            bean =
    new PercepcionesDeducciones("184", "REINTEGRO ISR-SSI", "340.65", "310", "I.S.S.S.T.E SEGURO DE SALUD", "156.50");
            coll.add(bean);

            //Agregar Percepciones
            //Agregar Deducciones
            bean = new PercepcionesDeducciones("", "", "", "381", "SSI APORTACION DIF", "1,107.67");
            coll.add(bean);

            bean = new PercepcionesDeducciones("", "", "", "382", "PRIMA SSI", "1,107.67");
            coll.add(bean);

            bean = new PercepcionesDeducciones("", "", "", "384", "ISR-SSI", "340.65");
            coll.add(bean);

        }

        return new JRBeanCollectionDataSource(coll);
    }
    
}
