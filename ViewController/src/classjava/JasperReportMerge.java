package classjava;


import java.io.FileOutputStream;

import java.util.ArrayList;

import java.util.HashMap;

import net.sf.jasperreports.engine.JREmptyDataSource;

import net.sf.jasperreports.engine.JRExporter;

import net.sf.jasperreports.engine.JRParameter;

import net.sf.jasperreports.engine.JRPrintPage;

import net.sf.jasperreports.engine.JasperFillManager;

import net.sf.jasperreports.engine.JasperPrint;

import net.sf.jasperreports.engine.JasperReport;

import net.sf.jasperreports.engine.export.JRPdfExporter;

import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import net.sf.jasperreports.engine.fill.JRFileVirtualizer;

import net.sf.jasperreports.engine.util.JRLoader;

public class JasperReportMerge {

    ArrayList reportsList = new ArrayList();

    HashMap params = new HashMap();

    JasperPrint jrPrint = new JasperPrint();

    public void buildReport() throws Exception {

        String templateFile = "C:\\DANIEL\\NetBeans\\DIF\\validaciones\\jasper\\reporteee.jasper";

        JasperReport jrReport = (JasperReport)JRLoader.loadObjectFromFile(templateFile);

        JRFileVirtualizer virtualizer = null;

        JasperPrint jp = null;

        for (int i = 0; i < 5; i++) {

            virtualizer = new JRFileVirtualizer(2, "C://tmp");

            virtualizer.setReadOnly(true);

            params.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

            // Create dummy data for the each report

            params.put("name", "Hello " + i);

            params.put("ic", "IC " + i);

            params.put("address", i + " Address");

            jp = JasperFillManager.fillReport(jrReport, params, new JREmptyDataSource());

            reportsList.add(jp); // adding the report into list

            // Clean the virtualizer.

            // If you didnt clean it, you might encounter error such as

            // "The virtualizer is used by more contexts than its in-memory cache size XXX"

            virtualizer.cleanup();

        }

        if (reportsList != null && reportsList.size() > 0) {

            jrPrint = mergeReport(); // merge the report

        } else {

            jrPrint = JasperFillManager.fillReport(jrReport, params, new JREmptyDataSource());

        }

        exportReport();

    }

    protected JasperPrint mergeReport() throws Exception {

        JasperPrint jrPrint = new JasperPrint();

        JasperPrint jp = null;

        jp = (JasperPrint)reportsList.get(0);

        // set the report layout

        jrPrint.setOrientation(jp.getOrientationValue());

        jrPrint.setLocaleCode(jp.getLocaleCode());

        jrPrint.setPageHeight(jp.getPageHeight());

        jrPrint.setPageWidth(jp.getPageWidth());

        jrPrint.setTimeZoneId(jp.getTimeZoneId());

        jrPrint.setName(jp.getName());

        // get each report and merge it 1 by 1

        for (int i = 0; i < reportsList.size(); i++) {

            jp = (JasperPrint)reportsList.get(i);

            ArrayList list = (ArrayList)jp.getPages();

            // Merging the reports into 1 single report

            for (int j = 0; j < list.size(); i++) {

                jrPrint.addPage((JRPrintPage)list.get(j));

            }

        }

        return jrPrint;

    }

    protected void exportReport() throws Exception {

        FileOutputStream fileOut = new FileOutputStream("C://Testing//MergeReportTemplate.pdf");

        JRExporter exporter = new JRPdfExporter();

        exporter.setParameter(JRPdfExporterParameter.FORCE_LINEBREAK_POLICY, Boolean.TRUE);

        exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, fileOut);

        exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT, jrPrint);

        System.out.println("Exporting...");

        exporter.exportReport();

        fileOut.flush();

        fileOut.close();

    }

    public static void main(String[] args) throws Exception {

        JasperReportMerge jem = new JasperReportMerge();

        jem.buildReport();

        System.out.println("Done");

    }

}
