package classjava;


import java.io.IOException;

import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class EnviarCorreo {

    private static final String CORREO = "nomiquadrum@gmail.com";
    private static final String HOST = "smtp.gmail.com";
    private static final String PASSWORD = "Quadrum*14";
    private static final String PORT = "587";


    private static final String CORREOAUX = "nomifast@quadrum.mx";
    private static final String HOSTAUX = "quadrum.mx";
    private static final String PASSWORDAUX = "Quadrum201";
    private static final String PORTAUX = "587";


    private String login;
    private String monto;
    private String rfcRecepotor;
    private String fechaEmision;
    private String uuid;
    //privat String
    // ******* Constructor *******

    public EnviarCorreo() {
        super();
    }

    public void envia() {

    }

    public EnviarCorreo(String monto, String rfcReceptor, String fechaEmision, String uuid) {
        this.monto = monto;
        this.rfcRecepotor = rfcReceptor;
        this.fechaEmision = fechaEmision;
        this.uuid = uuid;

    }

    /**
     * Método que realiza el envio de correo electrónico
     *
     * @param correo        Dirección electronica destinataria
     * @param asunto        Asunto del correo
     * @param mensaje       Mensaje del correo
     * @throws MessagingException
     */

    public void enviaEmail(String correo, String asunto, String mensaje) throws MessagingException {

        String from = CORREO;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREO;
        String password = PASSWORD;
        Properties props = new Properties();
        props.setProperty("mail.host", HOST);
        props.setProperty("mail.smtp.port", PORT);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");

        Authenticator auth = new SMTPAuthenticator(login, password);
        Session session = Session.getInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);
        msg.setText(message);
        msg.setSubject(subject);
        msg.setFrom(new InternetAddress(from));
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        Transport.send(msg);
        Multipart mp = new MimeMultipart();
        int i = 0;

        String rutaPdf = System.getProperty("user.dir") + "\\PDF\\" + "4CBFB8D5-D069-4B8A-81D7-C13AE263F010" + ".pdf";


        MimeBodyPart attachment = new MimeBodyPart();
        try {
            attachment.attachFile(rutaPdf);
        } catch (IOException e) {
        }
        mp.addBodyPart(attachment);

    }

    /**
     * Método que envia correo de confirmacion en la creacion de cuenta
     * y olvide contraseña
     *
     * @param correos       Lista de direcciones de correo electrónico
     * @param asunto        Asunto del correo
     * @param mensaje       Mensaje del correo
     * @throws MessagingException
     */

    public void enviaEmails(String correos, String asunto, String mensaje) throws MessagingException {
        try {


            String from = CORREOAUX;
            String to = correos;
            String subject = asunto;
            String message = mensaje;
            String login = CORREOAUX;
            String password = PASSWORDAUX;
            String destinos[] = to.split(";");
            Properties props = new Properties();
            props.setProperty("mail.host", HOSTAUX);
            props.setProperty("mail.smtp.port", PORTAUX);
            props.setProperty("mail.smtp.auth", "true");
            //props.setProperty("mail.smtp.starttls.enable", "true");

            Authenticator auth = new SMTPAuthenticator(login, password);
            Session session = Session.getInstance(props, auth);
            MimeMessage msg = new MimeMessage(session);
            BodyPart texto = new MimeBodyPart();
            texto.setText(message);

            msg.setText(message);
            msg.setSubject(subject);
            msg.setFrom(new InternetAddress(from));


            msg.addRecipients(Message.RecipientType.TO, correos);

            Multipart mp = new MimeMultipart();
            mp.addBodyPart(texto);

            msg.setContent(mp);

            Transport.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void enviaEmail(String correo, String asunto, String mensaje, List<String> listaXML,
                           List<String> rutasPDFs) throws MessagingException, IOException {

        String from = CORREO;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREO;
        String password = PASSWORD;

        Properties props = new Properties();
        props.setProperty("mail.host", HOST);
        props.setProperty("mail.smtp.port", PORT);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");

        Authenticator auth = new SMTPAuthenticator(login, password);

        Session session = Session.getInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);

        msg.setText(message);
        msg.setSubject(subject);
        msg.setFrom(new InternetAddress(from));
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

        //****** adjuntar PDF ***********************************
        Multipart mp = new MimeMultipart();
        int i = 0;
        for (String file : rutasPDFs) {
            MimeBodyPart attachment = new MimeBodyPart();
            attachment.attachFile(file);
            mp.addBodyPart(attachment);
            //se agrega XML
            MimeBodyPart attachmentXML = new MimeBodyPart();
            attachmentXML.attachFile(listaXML.get(i));
            i++;
            mp.addBodyPart(attachmentXML);
        }
        //*******************************************************
        msg.setContent(mp);

        Transport.send(msg);
    }

    /**
     * Método que envia correo electrónico adjuntando un PDF, un XML y su acuse respectivo
     *
     * @param correo        Dirección de correo destinataria
     * @param asunto        Asunto del correo
     * @param mensaje       Mensaje del correo
     * @param rutaXML       Ubicacion del XML a enviar
     * @param rutaPDF       Ubicacion del PDF a enviar
     * @param rutaAcuse     Ubicacion del Acuse a enviar
     * @throws MessagingException
     * @throws IOException
     */

    public void enviaEmail(String correo, String asunto, String mensaje, String rutaXML, String rutaPDF,
                           String rutaAcuse) throws MessagingException, IOException {

        String from = CORREO;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREO;
        String password = PASSWORD;

        Properties props = new Properties();
        props.setProperty("mail.host", HOST);
        props.setProperty("mail.smtp.port", PORT);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");

        Authenticator auth = new SMTPAuthenticator(login, password);
        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        msg.setText(message);
        msg.setSubject(subject);
        msg.setFrom(new InternetAddress(from));
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

        Multipart mp = new MimeMultipart();
        //se agrega PDF
        MimeBodyPart attachment = new MimeBodyPart();
        attachment.attachFile(rutaPDF);
        mp.addBodyPart(attachment);
        //se agrega rutaXML
        MimeBodyPart attachmentXML = new MimeBodyPart();
        attachmentXML.attachFile(rutaXML);
        mp.addBodyPart(attachmentXML);
        //se agrega rutaAcuse
        MimeBodyPart acuse = new MimeBodyPart();
        acuse.attachFile(rutaAcuse);
        mp.addBodyPart(acuse);

        msg.setContent(mp);
        Transport.send(msg);
    }


    /***
     *Metodo que manda un correo al modificar el password desde el administrador, y manda una copia de
     * correo con la nueva contraseña para soporte@e-quadrum.com.mx
     *
     *
     * ***/
    public void enviaEmailCopia(String correo, String asunto, String mensaje) throws MessagingException, IOException {

        enviaEmail(correo, asunto, mensaje);

        String from = CORREOAUX;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREOAUX;
        String password = PASSWORDAUX;
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", HOSTAUX);
        props.setProperty("mail.smtp.port", PORTAUX);
        //props.setProperty("mail.smtp.auth", "false");
        props.setProperty("mail.smtp.auth", "true");
        //props.setProperty("mail.smtp.starttls.enable", "true");
        //props.setProperty("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.socketFactory.port", PORT);
        props.put("mail.smtp.socketFactory.fallback", "false");

        Authenticator auth = new SMTPAuthenticator(login, password);
        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        msg.setText(message);
        msg.setSubject(asunto);
        msg.setFrom(new InternetAddress(from));
        msg.addRecipient(Message.RecipientType.BCC, new InternetAddress("cmedina@e-quadrum.com.mx"));
        Transport.send(msg);
        //con copia para fjvelasco

    }


    public void enviaEmailMasivo(String correo, String asunto, String mensaje) throws MessagingException, IOException {

        String from = CORREO;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREO;
        String password = PASSWORD;
        String destinos[] = to.split(";");
        Properties props = new Properties();
        props.setProperty("mail.host", "quadrum.com.mx");
        props.setProperty("mail.smtp.port", PORT);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");

        Authenticator auth = new SMTPAuthenticator(login, password);

        Session session = Session.getInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);

        BodyPart texto = new MimeBodyPart();
        texto.setText(message);

        msg.setText(message);
        msg.setSubject(subject);
        msg.setFrom(new InternetAddress(from));
        InternetAddress[] receptores = new InternetAddress[destinos.length];
        int j = 0;
        try {
            while (j < destinos.length) {
                receptores[j] = new InternetAddress(destinos[j]);
                j++;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        msg.addRecipients(Message.RecipientType.BCC, receptores);


        //      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        //  msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

        //****** adjuntar PDF ***********************************
        Multipart mp = new MimeMultipart();
        mp.addBodyPart(texto);
        int i = 0;


        Transport.send(msg);
    }


    /**
     * Método que envia correo electrónico
     *
     * @param correo        Dirección de correo electrónico destinataria
     * @param asunto        Asunto del correo
     * @param mensaje       Mensaje del correo
     * @param uuids         Lista de UUIDs los cuales se enviaran su representacion impresa
     * @throws MessagingException
     * @throws IOException
     */

    public void enviaEmail(String correo, String asunto, String mensaje, List<String> uuids) throws MessagingException,
                                                                                                    IOException {

        String ruta = "E:/Nomina/histacusecancela/";
        String from = CORREO;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREO;
        String password = PASSWORD;
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", HOST);
        props.setProperty("mail.smtp.port", PORT);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");

        Authenticator auth = new SMTPAuthenticator(login, password);

        Session session = Session.getInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);

        msg.setText(message);
        msg.setSubject(subject);
        msg.setFrom(new InternetAddress(from));
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

        //****** adjuntar PDF ***********************************
        Multipart mp = new MimeMultipart();
        for (String uuid : uuids) {
            MimeBodyPart attachment = new MimeBodyPart();
            attachment.attachFile(ruta + uuid + ".xml");
            mp.addBodyPart(attachment);
        }
        //*******************************************************
        msg.setContent(mp);
        Transport.send(msg);
    }


    public void enviaEmailAux(String correo, String asunto, String mensaje,
                              List<String> uuids) throws MessagingException, IOException {
        String ruta = "E:/Nomina/histacusecancela/";
        String from = CORREOAUX;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREOAUX;
        String password = PASSWORDAUX;
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", HOSTAUX);
        props.setProperty("mail.smtp.port", PORTAUX);
        //props.setProperty("mail.smtp.auth", "false");
        props.setProperty("mail.smtp.auth", "true");
        //props.setProperty("mail.smtp.starttls.enable", "true");
        //props.setProperty("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.socketFactory.port", PORT);
        props.put("mail.smtp.socketFactory.fallback", "false");
        Authenticator auth = new SMTPAuthenticator(login, password);
        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        msg.setText(message);
        msg.setSubject(subject);
        msg.setFrom(new InternetAddress(from));
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        //****** adjuntar PDF ***********************************
        Multipart mp = new MimeMultipart();
        for (String uuid : uuids) {
            MimeBodyPart attachment = new MimeBodyPart();
            attachment.attachFile(ruta + uuid + ".xml");
            mp.addBodyPart(attachment);
        }
        //*******************************************************
        try {

            msg.setContent(mp);
            Transport.send(msg);
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    /**
     * Método que genera la contraseña con caracteres aleatorios
     *
     * @return  Contraseña con caracteres aleatorios
     */

    public String generaPassword() {
        String password = "";

        int numChar = aleatorio(8, 15); //genera la contraseña de 8 a 15 caracteres
        while (numChar > 0) {
            int eleccion = aleatorio(1, 4);
            if (eleccion == 1)
                password += (char)aleatorio(97, 122); //genera una letra minúscula
            if (eleccion == 2)
                password += (char)aleatorio(65, 90); //genera una letra mayúscula
            if (eleccion == 3)
                password += (char)aleatorio(48, 57); //genera un digito
            /* if (eleccion == 4)
                password += (char)aleatorio(33, 47); //genera un caracter especial */
            numChar--;
        }
        return password;
    }

    /**
     * Método que genera un número aleatorio entre los rangos indicados
     *
     * @param a     Rango inferior
     * @param b     Rango superior
     * @return      Número aleatorio entre @param a y @param b
     */


    public void enviaEmailSeparados(String correo, String asunto, String mensaje, List<String> listaXML,
                                    List<String> rutasPDFs) throws MessagingException, IOException {


        String from = CORREO;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREO;
        String password = PASSWORD;
        String destinos[] = to.split(";");
        Properties props = new Properties();
        props.setProperty("mail.host", HOST);
        props.setProperty("mail.smtp.port", PORT);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");

        Authenticator auth = new SMTPAuthenticator(login, password);

        Session session = Session.getInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);

        BodyPart texto = new MimeBodyPart();
        texto.setText(message);

        msg.setText(message);
        msg.setSubject(subject);
        msg.setFrom(new InternetAddress(from));
        InternetAddress[] receptores = new InternetAddress[destinos.length];
        int j = 0;
        try {
            while (j < destinos.length) {
                receptores[j] = new InternetAddress(destinos[j]);
                j++;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        msg.addRecipients(Message.RecipientType.TO, receptores);


        //      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        //  msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

        //****** adjuntar PDF ***********************************
        Multipart mp = new MimeMultipart();
        mp.addBodyPart(texto);
        int i = 0;
        for (String file : rutasPDFs) {
            MimeBodyPart attachment = new MimeBodyPart();
            attachment.attachFile(file);
            mp.addBodyPart(attachment);
            //se agrega XML
            MimeBodyPart attachmentXML = new MimeBodyPart();
            attachmentXML.attachFile(listaXML.get(i));
            i++;
            mp.addBodyPart(attachmentXML);
        }
        //*******************************************************
        msg.setContent(mp);

        Transport.send(msg);
    }


    public void enviaEmailSeparadosAux(String correo, String asunto, String mensaje, List<String> listaXML,
                                       List<String> rutasPDFs) throws MessagingException, IOException {


        String from = CORREOAUX;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREOAUX;
        String password = PASSWORDAUX;
        String destinos[] = to.split(";");
        Properties props = new Properties();
        props.setProperty("mail.host", HOSTAUX);
        props.setProperty("mail.smtp.port", PORTAUX);
        props.setProperty("mail.smtp.auth", "true");
        //props.setProperty("mail.smtp.starttls.enable", "true");

        Authenticator auth = new SMTPAuthenticator(login, password);

        Session session = Session.getInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);

        BodyPart texto = new MimeBodyPart();
        texto.setText(message);

        msg.setText(message);
        msg.setSubject(subject);
        msg.setFrom(new InternetAddress(from));
        InternetAddress[] receptores = new InternetAddress[destinos.length];
        int j = 0;
        try {
            while (j < destinos.length) {
                receptores[j] = new InternetAddress(destinos[j]);
                j++;
            }

            msg.addRecipients(Message.RecipientType.TO, receptores);


            //      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            //  msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            //****** adjuntar PDF ***********************************
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(texto);
            int i = 0;
            for (String file : rutasPDFs) {
                MimeBodyPart attachment = new MimeBodyPart();
                attachment.attachFile(file);
                mp.addBodyPart(attachment);

                //se agrega XML
                /* MimeBodyPart attachmentXML = new MimeBodyPart();
            attachmentXML.attachFile(listaXML.get(i));
            i++;
            mp.addBodyPart(attachmentXML); */
            }
            //*******************************************************
            msg.setContent(mp);

            Transport.send(msg);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    /*
     * Envia confirmacion de creacion de Trabajador
     *
     *
     */

    public void enviaEmailAltaTrabajador(String correo, String asunto, String mensaje) throws MessagingException,
                                                                                              IOException {


        String from = CORREOAUX;
        String to = correo;
        String subject = asunto;
        String message = mensaje;
        String login = CORREOAUX;
        String password = PASSWORDAUX;
        Properties props = new Properties();
        props.setProperty("mail.host", HOSTAUX);
        props.setProperty("mail.smtp.port", PORTAUX);
        props.setProperty("mail.smtp.auth", "true");
        //props.setProperty("mail.smtp.starttls.enable", "true");

        Authenticator auth = new SMTPAuthenticator(login, password);

        Session session = Session.getInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);

        BodyPart texto = new MimeBodyPart();
        texto.setText(message);

        msg.setText(message);
        msg.setSubject(subject);
        msg.setFrom(new InternetAddress(from));
        msg.addRecipients(Message.RecipientType.TO, correo);


        //      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        //  msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

        //****** adjuntar PDF ***********************************
        Multipart mp = new MimeMultipart();
        mp.addBodyPart(texto);

        msg.setContent(mp);

        Transport.send(msg);

    }


    public int aleatorio(int a, int b) {
        return (int)Math.floor(Math.random() * (b - a + 1) + a);
    }

    // ******** SETTERS Y GETTERS **********

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    private class SMTPAuthenticator extends Authenticator {

        private PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String password) {
            authentication = new PasswordAuthentication(login, password);
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }
    }


}
