package classjava;

import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.ssl.PKCS8Key;


public class Sello {
    public Sello() {
        super();
    }
        private byte[] clavePrivada;
        private String password;
        private byte[] textoCodificado;
        private String res;
        private String cadenaOriginal;
     


    /**
     * Se sobrecarga al constructor para generar el sello digital
     * 
     * @param pass
     * @param clavePrivada
     * @param cadenaOrigin
     */
    
    public Sello(String pass, byte[] clavePrivada, String cadenaOrigin){
            try{
                this.cadenaOriginal= cadenaOrigin;
                this.textoCodificado=cadenaOriginal.getBytes("UTF-8"); 
                this.password= pass;
                this.clavePrivada=clavePrivada; 
            }catch(Exception ea){
                ea.printStackTrace();
                
            }
        }

    
    public Sello(String pass, byte[] clavePrivada, byte[] cadenaOrigin){
            try{
                this.textoCodificado=cadenaOrigin; 
                this.password= pass;
                this.clavePrivada=clavePrivada; 
            }catch(Exception ea){
                ea.printStackTrace();
            }
        }
    
    /**
     * Método que genera el sello digital
     * implementando la firma SHA1withRSA
     * 
     * @return      Sello generado para el comprobante
     */
    
   /*  public String GeneraSelloDigital(){            
            try{
                
               
                PKCS8Key pkcs8= new PKCS8Key(this.clavePrivada,this.password.toCharArray());
                PrivateKey privateKey = pkcs8.getPrivateKey();
                Signature firma= Signature.getInstance("SHA1withRSA"); 
                firma.initSign(privateKey);  
                firma.update(this.textoCodificado);
                byte[] signed=firma.sign();
                Base64 base64=new Base64(-1); 
                return base64.encodeToString(signed);
                }catch(Exception ea){
            }           
            return this.res;
    } */
    
    
   public String generaSelloDigital(){            
           try{
             
               System.out.println("...  "+clavePrivada+" ... "+password);
               PKCS8Key pkcs8= new PKCS8Key(this.clavePrivada,this.password.toCharArray());
               PrivateKey privateKey = pkcs8.getPrivateKey(); 
               Signature sign=Signature.getInstance("SHA1withRSA"); 
               sign.initSign(privateKey,new SecureRandom());
               sign.update(this.textoCodificado);
               byte[] signed=sign.sign();
               return new String(Base64.encodeBase64(signed)); 
               }catch(Exception ea){
               ea.printStackTrace();
           }           
           return this.res;
   }
    
   
    
    
    
}
