package classjava;

import entities.Recibos;
import entities.Trabajador;

public class TablaConsultas {

    private Trabajador trabajador;
    private Recibos recibo;
    boolean descargar;

    public TablaConsultas() {
        super();
    }

    public TablaConsultas(Trabajador trabajador, Recibos recibo) {
        this.trabajador = trabajador;
        this.recibo = recibo;
    }


    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setRecibo(Recibos recibo) {
        this.recibo = recibo;
    }

    public Recibos getRecibo() {
        return recibo;
    }

    public void setDescargar(boolean descargar) {
        this.descargar = descargar;
    }

    public boolean isDescargar() {
        return descargar;
    }
}
