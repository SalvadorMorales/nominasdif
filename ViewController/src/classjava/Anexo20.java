package classjava;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import oracle.adf.view.rich.component.rich.RichPopup;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class Anexo20 {
    /**
     * Método que carga los estados en los formularios correspondientes
     * @return Lista de SelectItem con los Estados
     */


    public static List<SelectItem> listaEstados() {
        SelectItem item;
        List<SelectItem> datos = new ArrayList<SelectItem>();
        String[] estados =
        { "[ SELECCIONAR ESTADO ]", "AGUASCALIENTES", "BAJA CALIFORNIA SUR", "BAJA CALIFORNIA NORTE", "CAMPECHE",
          "CHIAPAS", "CHIHUAHUA", "COAHUILA", "COLIMA", "DISTRITO FEDERAL", "DURANGO", "ESTADO DE MEXICO",
          "GUANAJUATO", "GUERRERO", "HIDALGO", "JALISCO", "MICHOACAN", "MORELOS", "NAYARIT", "NUEVO LEON", "OAXACA",
          "PUEBLA", "QUERETARO", "QUINTANA ROO", "SAN LUIS POTOSI", "SINALOA", "SONORA", "TABASCO", "TAMAULIPAS",
          "TLAXCALA", "VERACRUZ", "YUCATAN", "ZACATECAS" };

        for (int i = 0; i < estados.length; i++) {
            item = new SelectItem();
            item.setLabel(estados[i]);
            item.setValue(estados[i]);
            datos.add(item);
        }
        return datos;
    }


    /**
     * Método que carga las personas morales en los formularios correspondientes
     * @return Lista de SelectItem con las personas morales
     * fecha de modificacion 12/06/2013
     */

    public static List<SelectItem> listaPersonaMoral() {
        SelectItem item;
        List<SelectItem> dato = new ArrayList<SelectItem>();
        dato.clear();
        String[] regimen =
        { "Regimen General de Personas Morales", "Regimen Simplificado de Personas Morales", "Regimen de Personas Morales con Fines no Lucrativos",
          "Asociaciones Religiosas", "Otro Régimen Fiscal" };

        for (int i = 0; i < regimen.length; i++) {
            item = new SelectItem();
            item.setLabel(regimen[i]);
            item.setValue(regimen[i]);
            dato.add(item);
        }
        return dato;
    }

    public static List<SelectItem> listaTrampa() {
        SelectItem item;
        List<SelectItem> dato = new ArrayList<SelectItem>();
        dato.clear();
        String[] regimen =
        { "Regimen General de Personas Morales", "Regimen Simplificado de Personas Morales", "Regimen de Personas Morales con Fines no Lucrativos",
          "Asociaciones Religiosas", "Otro Regimen Fiscal", "Actividades Empresariales y Profesionales",
          "Regimen de Pequeños Contribuyentes", "Por Salarios y Prestacion de servicios Personales Subordinados",
          "Arrendamiento", "Enajenacion de Bienes", "Adquisicion de Bienes", "Dividendos", "Intereses", "Premios",
          "De mas Ingresos", "Otro Regimen Fiscal" };

        for (int i = 0; i < regimen.length; i++) {
            item = new SelectItem();
            item.setLabel(regimen[i]);
            item.setValue(regimen[i]);
            dato.add(item);
        }
        return dato;
    }

    /**
     * Método que carga las personas fisicas en los formularios correspondientes
     * @return Lista de SelectItem con las personas fisicas
     * fecha de modificacion 12/06/2013
     */
    public static List<SelectItem> listaPersonaFisica() {
        SelectItem item;
        List<SelectItem> dato = new ArrayList<SelectItem>();

        dato.clear();
        String[] regimen =
        { "Actividades Empresariales y Profesionales", "Regimen de Pequeños Contribuyentes", "Por Salarios y Prestacion de servicios Personales Subordinados",
          "Arrendamiento", "Enajenacion de Bienes", "Adquisicion de Bienes", "Dividendos", "Intereses", "Premios",
          "De mas Ingresos", "Otro Regimen Fiscal" };

        for (int i = 0; i < regimen.length; i++) {
            item = new SelectItem();
            item.setLabel(regimen[i]);
            item.setValue(regimen[i]);
            dato.add(item);
        }
        return dato;
    }

    /**
     * Método que carga el regimen fiscal en los formularios correspondientes
     * @return Lista de SelectItem con el regimen fiscal
     * fecha de modificacion 12/06/2013
     */
    public static List<SelectItem> listaRegimenFiscal() {
        SelectItem item;
        List<SelectItem> dato = new ArrayList<SelectItem>();
        dato.clear();
        String[] regimenFiscal =
        { "REGIMEN FISCAL", "REGIMEN GENERAL DE PERSONAS MORALES", "PERSONAS MORALES CON FINES NO LUCRATIVOS",
          "SIMPLIFICADO", "OTRO REGIMEN" };

        for (int i = 0; i < regimenFiscal.length; i++) {
            item = new SelectItem();
            item.setLabel(regimenFiscal[i]);
            item.setValue(regimenFiscal[i]);
            dato.add(item);
        }
        return dato;
    }

    /**
     * Método que muestra componente PopUp
     *
     * @param event     Evento que se detona por un componente de la Vista
     * @param popup1    PopUp a mostrar
     */

    public static void showPopup(ActionEvent event, RichPopup popup1) {
        FacesContext context = FacesContext.getCurrentInstance();
        UIComponent source = (UIComponent)event.getSource();
        String alignId = source.getClientId(context);
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup1.show(hints);
    }

    /**
     * Método que oculta componente PopUp
     *
     * @param popup     PopUp a ocultar
     */

    public static void cerrarPopUp(RichPopup popup) {
        FacesContext fctx = FacesContext.getCurrentInstance();
        UIComponent component = fctx.getViewRoot().findComponent(popup.getClientId());
        ExtendedRenderKitService service = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);
        StringBuffer strbuffer = new StringBuffer();
        strbuffer.append("popup=AdfPage.PAGE.findComponent('");
        strbuffer.append(component.getClientId(fctx));
        strbuffer.append("');popup.hide();");
        service.addScript(fctx, strbuffer.toString());
    }

    /**
     * Método que reemplaza las secuencias de escape existentes para que no se vea alterardo el XML
     *
     * @param cadena    Texto digitado por el usuario
     * @return          Texto con reemplazo de secuencias de escape
     */

    public static String secuencias(String cadena) {
        String subcadena = "&amp;";
        int indice = cadena.indexOf(subcadena);
        if (!(indice != -1)) {
            cadena = cadena.replace("&", "&");
        }

        cadena = cadena.replace(String.valueOf('"'), "&quot;");
        cadena = cadena.replace("<", "&lt;");
        cadena = cadena.replace(">", "&gt;");
        cadena = cadena.replace("'", "&apos;");
        return cadena;
    }

    public static String secuenciasRFC(String cadena) {
        String subcadena = "&amp;";
        int indice = cadena.indexOf(subcadena);
        if (!(indice != -1)) {
            cadena = cadena.replace("&", "&");
        }

        cadena = cadena.replace(String.valueOf('"'), "&quot;");
        cadena = cadena.replace("<", "&lt;");
        cadena = cadena.replace(">", "&gt;");
        cadena = cadena.replace("'", "&apos;");
        return cadena;
    }

    /**
     * Método que reemplaza las secuencias de escape existentes para que no se vea alterardo el XML
     *
     * @param cadena    Texto digitado por el usuario
     * @return          Texto con reemplazo de secuencias de escape en formato UTF-8
     */

    public static String escapeSecuencia(String cadena) {
        byte[] cadena1 = null;
        String aux = null;
        try {
            cadena1 = cadena.getBytes("UTF8");
            aux = new String(cadena1, "UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        aux = aux.replace("&amp;", "&");
        aux = aux.replace("&quot;", String.valueOf('"'));
        aux = aux.replace("&lt;", "<");
        aux = aux.replace("&gt;", ">");
        aux = aux.replace("&apos;", "'");
        return aux;
    }


    /**
     * Método que permite el reemplazo de caractes acentuados nos permitidos en XML
     *
     * @param input     Texto digitado por usuario
     * @return          Valor sin caracteres permitidos
     */

    public static String formatCadena(String input) {
        //input = input.toUpperCase();
        if (input != null) {
            //input.toUpperCase();
            for (int i = 0; i < input.length(); i++) {
                if (input.charAt(i) == 193) {
                    input = input.replace(input.charAt(i), 'A');
                }
                if (input.charAt(i) == 201) {
                    input = input.replace(input.charAt(i), 'E');
                }
                if (input.charAt(i) == 205) {
                    input = input.replace(input.charAt(i), 'I');
                }
                if (input.charAt(i) == 211) {
                    input = input.replace(input.charAt(i), 'O');
                }
                if (input.charAt(i) == 218) {
                    input = input.replace(input.charAt(i), 'U');
                }
                //ahora a las minusculas
                if (input.charAt(i) == 225) {
                    input = input.replace(input.charAt(i), 'a');
                }
                if (input.charAt(i) == 233) {
                    input = input.replace(input.charAt(i), 'e');
                }
                if (input.charAt(i) == 237) {
                    input = input.replace(input.charAt(i), 'i');
                }
                if (input.charAt(i) == 243) {
                    input = input.replace(input.charAt(i), 'o');
                }
                if (input.charAt(i) == 250) {
                    input = input.replace(input.charAt(i), 'u');
                }
            }
            return input;
        }
        return input;
    }


    public static String retornaFecha(String fecha) {
        String mes = null;
        if (fecha.equals("ene"))
            mes = "01";
        if (fecha.equals("feb"))
            mes = "02";
        if (fecha.equals("mar"))
            mes = "03";
        if (fecha.equals("abr"))
            mes = "04";
        if (fecha.equals("may"))
            mes = "05";
        if (fecha.equals("jun"))
            mes = "06";
        if (fecha.equals("jul"))
            mes = "07";
        if (fecha.equals("ago"))
            mes = "08";
        if (fecha.equals("sep"))
            mes = "09";
        if (fecha.equals("oct"))
            mes = "10";
        if (fecha.equals("nov"))
            mes = "11";
        if (fecha.equals("dic"))
            mes = "12";

        return mes;
    }

    public static String convierteFecha(String fecha) {
        String dia = "";
        String mes = "";
        String anio = "";
        if (fecha.indexOf("-") != -1) {
            String[] cad = fecha.split("-");
            dia = cad[0];
            mes = retornaFecha(cad[1]);
            anio = cad[2];

        } else if (fecha.indexOf("/") != -1) {
            String[] cad = fecha.split("/");
            dia = cad[0];
            if (Integer.valueOf(cad[1]) > 0)
                mes = cad[1];
            else
                mes = retornaFecha(cad[1]);
            anio = cad[2];
        }

        return anio + "-" + mes + "-" + dia;
    }

    public static String riesgoPuesto(String riesgo) {
        if (riesgo.equalsIgnoreCase(""))
            return "fallo";
        HashMap<String, String> riesgoPuesto = new HashMap<String, String>();
        riesgoPuesto.put("I", "1");
        riesgoPuesto.put("II", "2");
        riesgoPuesto.put("III", "3");
        riesgoPuesto.put("IV", "4");
        riesgoPuesto.put("V", "5");
        return riesgoPuesto.get(riesgo);
    }


    public static String tipoIncapacidad(String tipo) {
        HashMap<String, String> incapacidad = new HashMap<String, String>();
        incapacidad.put("Riesgo de trabajo", "1");
        incapacidad.put("Enfermedad en general", "2");
        incapacidad.put("Maternidad", "3");

        return incapacidad.get(tipo);
    }

    public static String convierteRegimenContratacion(String regimen) {
        String tipoReg = "";
        regimen = regimen.toLowerCase().trim();
        if (regimen.equals("asimilados a salarios"))
            tipoReg = "1";
        if (regimen.equals("sueldos y salarios"))
            tipoReg = "2";
        if (regimen.equals("jubilados"))
            tipoReg = "3";
        if (regimen.equals("pensionados"))
            tipoReg = "4";
        if (regimen.length() == 1)
            tipoReg = regimen;


        return tipoReg;
    }

    public static String convierteRegimenContratacionAux(String regimen) {
        String tipoReg = "";
        regimen = regimen.toUpperCase().trim();
        if (regimen.equals("asimilados a salarios"))
            tipoReg = "1";
        if (regimen.equals("sueldos y salarios"))
            tipoReg = "2";
        if (regimen.equals("jubilados"))
            tipoReg = "3";
        if (regimen.equals("pensionados"))
            tipoReg = "4";
        if (regimen.length() == 1)
            tipoReg = regimen;


        return tipoReg;
    }


    public static String quitaDecimales(String cadena) {
        String numero = null;
        int iNumero = cadena.indexOf(".");
        if (iNumero != -1) {
            numero = cadena.substring(0, iNumero);
            return numero;
        }
        return cadena;
    }

    public static String regresaPercepcion(String var) {
        Hashtable<String, String> percepciones = new Hashtable<String, String>();
        percepciones.put("001", "Sueldos, Salarios Rayas y Jornales");
        percepciones.put("002", "Gratificacion Anual (Aguinaldo)");
        percepciones.put("003", "Participacion de los Trabajadores en las Utilidades PTU");
        percepciones.put("004", "Reembolso de Gastos Medicos Dentales y Hospitalarios");
        percepciones.put("005", "Fondo de Ahorro");
        percepciones.put("006", "Caja de ahorro");
        percepciones.put("009", "Contribuciones a Cargo del Trabajador Pagadas por el Patron");
        percepciones.put("010", "Premios por puntualidad");
        percepciones.put("011", "Prima de Seguro de vida");
        percepciones.put("012", "Seguro de Gastos Medicos Mayores");
        percepciones.put("013", "Cuotas Sindicales Pagadas por el Patron");
        percepciones.put("014", "Subsidios por incapacidad");
        percepciones.put("015", "Becas para trabajadores y/o hijos");
        percepciones.put("016", "Otros");
        percepciones.put("017", "Subsidio para el empleo");
        percepciones.put("019", "Horas extra");
        percepciones.put("020", "Prima dominical");
        percepciones.put("021", "Prima vacacional");
        percepciones.put("022", "Prima por antiguedad");
        percepciones.put("023", "Pagos por separacion");
        percepciones.put("024", "Seguro de retiro ");
        percepciones.put("025", "Indemnizaciones");
        percepciones.put("026", "Reembolso por funeral");
        percepciones.put("027", "Cuotas de seguridad social pagadas por el patron");
        percepciones.put("028", "Comisiones");
        percepciones.put("029", "Vales de despensa");
        percepciones.put("030", "Vales de restaurante");
        percepciones.put("031", "Vales de gasolina");
        percepciones.put("032", "Vales de ropa");
        percepciones.put("033", "Ayuda para renta");
        percepciones.put("034", "Ayuda para articulos escolares");
        percepciones.put("035", "Ayuda para anteojos");
        percepciones.put("036", "Ayuda para transporte");
        percepciones.put("037", "Ayuda para gastos de funeral");
        percepciones.put("038", "Otros ingresos por salarios");
        percepciones.put("039", "Jubilaciones, pensiones o haberes de retiro");
        return percepciones.get(var);
    }

    public static String regresaDeduccion(String var) {
        Hashtable<String, String> deducciones = new Hashtable<String, String>();
        deducciones.put("001", "Seguridad social");
        deducciones.put("002", "ISR");
        deducciones.put("003", "Aportaciones a retiro, cesantia en edad avanzada y vejez");
        deducciones.put("004", "Otros");
        deducciones.put("005", "Aportaciones a Fondo de vivienda");
        deducciones.put("006", "Descuento por incapacidad ");
        deducciones.put("007", "Pension alimenticia");
        deducciones.put("008", "Renta");
        deducciones.put("009", "Prestamos provenientes del Fondo Nacional de la Vivienda para los Trabajadores");
        deducciones.put("010", "Pago por credito de vivienda");
        deducciones.put("011", "Pago de abonos INFONACOT");
        deducciones.put("012", "Anticipo de salarios");
        deducciones.put("013", "Pagos hechos con exceso al trabajador");
        deducciones.put("014", "Errores");
        deducciones.put("015", "Perdidas");
        deducciones.put("016", "Averias");
        deducciones.put("017", "Adquisicion de articulos producidos por la empresa o establecimiento");
        deducciones.put("018",
                        "Cuotas para la constitucion y fomento de sociedades cooperativas y de cajas de ahorro");
        deducciones.put("019", "Cuotas sindicales");
        deducciones.put("020", "Ausencia (Ausentismo)");
        deducciones.put("021", "Cuotas obrero patronales");
        return deducciones.get(var);
    }


    public static String regresaClave(String clave) {
        if (clave.length() > 2)
            return clave;
        if (clave.length() == 2)
            return "0" + clave;
        return "00" + clave;
    }

    public List<SelectItem> listaBancos() {
        SelectItem lista;
        List<SelectItem> listaBanco = new ArrayList<SelectItem>();
        String[] bancos =
        { "BANAMEX", "BANCOMEXT", "BANOBRAS", "BBVA BANCOMER", "SANTANDER", "BANJERCITO", "HSBC", "HSBC", "BAJIO",
          "IXE", "INBURSA", "INTRACCIONES", "MIFEL", "SCOTIABANK", "BANREGIO", "INVEX", "BANSI", "AFIRME", "BANORTE",
          "THE ROYAL BANK", "AMERICAN EXPRESS", "BAMSA", "TOKYO", "JP MORGAN", "BMONEX", "VE POR MAS", "ING" };
        for (int i = 0; i < bancos.length; i++) {
            lista = new SelectItem();
            lista.setLabel(bancos[i]);
            lista.setValue(bancos[i]);
            listaBanco.add(lista);
        }
        return listaBanco;
    }

}

