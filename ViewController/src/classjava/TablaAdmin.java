package classjava;


public class TablaAdmin {
    private int idAdmin;
    private String usuario;
    private String departamento;
    private String puesto;
    private String rfc;
    private String nombre;
    private String apellidos;
    private String sucursalRazon;
    private String empresaRazon;
    private String pass;
    private String correo;


    public TablaAdmin() {
        super();
    }

    public TablaAdmin(String usuario, String departamento, String puesto, String rfc, String nombre, String apellidos,
                      String sucursalRazon, String empresaRazon, String pass, String correo, int id) {
        super();
        this.usuario = usuario;
        this.departamento = departamento;
        this.puesto = puesto;
        this.rfc = rfc;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.sucursalRazon = sucursalRazon;
        this.empresaRazon = empresaRazon;
        this.pass = pass;
        this.correo = correo;
        this.idAdmin = id;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getRfc() {
        return rfc;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setSucursalRazon(String sucursalRazon) {
        this.sucursalRazon = sucursalRazon;
    }

    public String getSucursalRazon() {
        return sucursalRazon;
    }

    public void setEmpresaRazon(String empresaRazon) {
        this.empresaRazon = empresaRazon;
    }

    public String getEmpresaRazon() {
        return empresaRazon;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPass() {
        return pass;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public int getIdAdmin() {
        return idAdmin;
    }
}
