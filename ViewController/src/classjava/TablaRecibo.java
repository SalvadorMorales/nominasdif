package classjava;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.util.Date;

public class TablaRecibo {


    private String fechaCertificacio;
    private String fechaEmision;
    private Date fechaFinalPago;
    private Date fechaInicialPago;
    private Date fechaPago;
    private int idrecibos;
    private long noDiasPagados;
    private String reciboXML;
    private BigDecimal totalExcentoDeduccion;
    private BigDecimal totalExcentoPercepcion;
    private BigDecimal totalGravadoDeduccion;
    private BigDecimal totalGravadoPercepcion;
    private String uuid;
    private String version;

    private boolean seleccionar;
    private String estatus;


    public TablaRecibo() {
        super();
    }


    public TablaRecibo(String fechaCertificacio, String fechaEmision, Date fechaFinalPago, Date fechaInicialPago,
                       Date fechaPago, int idrecibos, long noDiasPagados, String reciboXML,
                       BigDecimal totalExcentoDeduccion, BigDecimal totalExcentoPercepcion,
                       BigDecimal totalGravadoDeduccion, BigDecimal totalGravadoPercepcion, String uuid,
                       boolean seleccion, String estatus,String version) {
        super();
        this.fechaCertificacio = fechaCertificacio;
        this.fechaEmision = fechaEmision;
        this.fechaFinalPago = fechaFinalPago;
        this.fechaInicialPago = fechaInicialPago;
        this.fechaPago = fechaPago;
        this.idrecibos = idrecibos;
        this.noDiasPagados = noDiasPagados;
        this.reciboXML = reciboXML;
        this.totalExcentoDeduccion = totalExcentoDeduccion;
        this.totalExcentoPercepcion = totalExcentoPercepcion;
        this.totalGravadoDeduccion = totalGravadoDeduccion;
        this.totalGravadoPercepcion = totalGravadoPercepcion;
        this.uuid = uuid;
        this.seleccionar = seleccion;
        this.estatus = estatus;
        this.version = version;
    }

    public TablaRecibo(Date fechaFinalPago, Date fechaInicialPago, Date fechaPago, String uuid, boolean seleccionar,
                       String estatus, String version) {
        super();
        this.fechaFinalPago = fechaFinalPago;
        this.fechaInicialPago = fechaInicialPago;
        this.fechaPago = fechaPago;
        this.uuid = uuid;
        this.seleccionar = seleccionar;
        this.estatus = estatus;
        this.version = version;
    }


    public void setFechaCertificacio(String fechaCertificacio) {
        this.fechaCertificacio = fechaCertificacio;
    }

    public String getFechaCertificacio() {
        return fechaCertificacio;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaFinalPago(Date fechaFinalPago) {
        this.fechaFinalPago = fechaFinalPago;
    }

    public Date getFechaFinalPago() {
        return fechaFinalPago;
    }

    public void setFechaInicialPago(Date fechaInicialPago) {
        this.fechaInicialPago = fechaInicialPago;
    }

    public Date getFechaInicialPago() {
        return fechaInicialPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setIdrecibos(int idrecibos) {
        this.idrecibos = idrecibos;
    }

    public int getIdrecibos() {
        return idrecibos;
    }

    public void setNoDiasPagados(long noDiasPagados) {
        this.noDiasPagados = noDiasPagados;
    }

    public long getNoDiasPagados() {
        return noDiasPagados;
    }

    public void setReciboXML(String reciboXML) {
        this.reciboXML = reciboXML;
    }

    public String getReciboXML() {
        return reciboXML;
    }

    public void setTotalExcentoDeduccion(BigDecimal totalExcentoDeduccion) {
        this.totalExcentoDeduccion = totalExcentoDeduccion;
    }

    public BigDecimal getTotalExcentoDeduccion() {
        return totalExcentoDeduccion;
    }

    public void setTotalExcentoPercepcion(BigDecimal totalExcentoPercepcion) {
        this.totalExcentoPercepcion = totalExcentoPercepcion;
    }

    public BigDecimal getTotalExcentoPercepcion() {
        return totalExcentoPercepcion;
    }

    public void setTotalGravadoDeduccion(BigDecimal totalGravadoDeduccion) {
        this.totalGravadoDeduccion = totalGravadoDeduccion;
    }

    public BigDecimal getTotalGravadoDeduccion() {
        return totalGravadoDeduccion;
    }

    public void setTotalGravadoPercepcion(BigDecimal totalGravadoPercepcion) {
        this.totalGravadoPercepcion = totalGravadoPercepcion;
    }

    public BigDecimal getTotalGravadoPercepcion() {
        return totalGravadoPercepcion;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setSeleccionar(boolean seleccionar) {
        this.seleccionar = seleccionar;
    }

    public boolean isSeleccionar() {
        return seleccionar;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
