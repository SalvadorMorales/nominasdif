package classjava;

import entities.Usuariotransacciones;

import interfaces.SessionUsuariotransaccionesRemote;

import java.sql.Timestamp;

import java.util.GregorianCalendar;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.DecoderException;

import servicelocator.MyServiceLocator;


/**
 * Clase donde se realizan las pistas de auditoria, donde se almacena todo lo que es realizado por el
 * usuario en la base de datos
 */

public class PistasAuditoria {
    private String usuario;
    private String puerto;
    private String ip;


    private Usuariotransacciones auditoria = new Usuariotransacciones();
    private SessionUsuariotransaccionesRemote ejbPistas = MyServiceLocator.getUsuarioTransaccionRemote();
    //private AES AES;
    // ******** Constructor **********
    public PistasAuditoria() {
        //AES=new AES();
    }

    /**
     * Obtiene la direccion IP del cliente
     * para el registro de la pista de auditoria
     * 
     * @return      IP del cliente
     */
    
    public String obtenerDireccionIP() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();

        puerto = String.valueOf(request.getRemotePort());
        ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        } else {
            ip = "IP DESCONOCIDA";
        }
        return ip;
    }

    /**
     * Registra la acción realizada por el cliente
     * 
     * @param objeto        ENTIDAD BD
     * @param accion        DESCRIPCION DEL EVENTO
     * @param cliente       RFC
     * @param sql           SENTENCIA SQL
     * @throws DecoderException
     */
    
    public void eventosCliente(String objeto, String accion, String cliente, String sql) {
        try {
            
            auditoria.setObjeto(objeto);
            auditoria.setFechahora(new Timestamp(new GregorianCalendar().getTime().getTime()).toString());
            auditoria.setUserbd("administracion_nominas");
            auditoria.setClientaddress(obtenerDireccionIP());
            auditoria.setClientport(puerto);
            auditoria.setAccion(accion);
            auditoria.setCliente(cliente);
            auditoria.setSentenciasql(sql);
            ejbPistas.persistUsuariotransacciones(auditoria);
        } catch (Exception de) {    
            de.printStackTrace();
        }
    }
}