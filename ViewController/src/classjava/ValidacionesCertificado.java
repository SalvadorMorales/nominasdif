package classjava;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import javax.security.cert.CertificateException;
import javax.security.cert.CertificateExpiredException;
import javax.security.cert.X509Certificate;

import mx.bigdata.sat.security.KeyLoader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.ssl.PKCS8Key;


public class ValidacionesCertificado {
    private byte[] clavePrivada;
    private byte[] clavePublica;
    private String password;
    private String subject;
    private String PARAMETRO = "OU";
    private boolean correcto = true;
    private boolean error = false;
    private X509Certificate certificado;


    // ********* Constructores ************
    public ValidacionesCertificado(){
        
    }
    public ValidacionesCertificado(InputStream archivoClavePublica, InputStream archivoClavePrivada, String password, String rfc) {
        
        try {
            this.clavePrivada = IOUtils.toByteArray(archivoClavePrivada);
            this.clavePublica = IOUtils.toByteArray(archivoClavePublica);
            //this.clavePublica2=IOUtils.toString(archivoClavePublica);
            this.password = password;
            //this.cadenaOriginal= cadenaOrigin;
             //X509Certificate cert = X509Certificate.getInstance(this.clavePublica);
            //String statusFecha;
            //int statusFIEL=-1;
            //boolean statusCorresp;
            //boolean statusLCO;
            //statusFecha=validaFecha(cert);
            //System.out.println("\nel status fecha es: \n" + statusFecha);
            //statusFIEL=validaFIEL(cert);
            //System.out.println("el status FIEL es: " + statusFIEL);
            //statusCorresp=validaCorrespondencias();
            //System.out.println("\nel status de que el .key y el .cer correspondan es: \n" + statusCorresp);
            //statusLCO=validaLCO();
            
            //System.out.println("\nel status LCO es: \n" + statusLCO); 
            //Valida Si Certificado Corresponde a emisor
            //boolean correspondeRFC = false;
           
        } catch (Exception ea) {

            ea.printStackTrace();
        }
        
    }

    public ValidacionesCertificado(InputStream archivoCertificado) {
        try {
            this.clavePublica = IOUtils.toByteArray(archivoCertificado);
            certificado = X509Certificate.getInstance(this.clavePublica);
        } catch (IOException e) {

            e.printStackTrace();
        } catch (CertificateException e) {

            e.printStackTrace();
        }
    }
    // *************************************************************

    /**
     * Método que indicara si un certificado es válido
     *
     * @return      true si el certificado es valido, en otro caso false
     */

    public boolean validaCertificado() {
        return validaCertificado(certificado);
    }
    
    public Boolean valida(InputStream archivoClavePublica, InputStream archivoClavePrivada, String password, String rfc){
        boolean valido = true;
    try {
        this.clavePrivada = IOUtils.toByteArray(archivoClavePrivada);
        this.clavePublica = IOUtils.toByteArray(archivoClavePublica);
        //this.clavePublica2=IOUtils.toString(archivoClavePublica);
        this.password = password;
        //this.cadenaOriginal= cadenaOrigin;
         X509Certificate cert = X509Certificate.getInstance(this.clavePublica);
        String statusFecha;
        int statusFIEL=-1;
        boolean statusCorresp;
        boolean statusLCO;
        if(validaFecha(cert) != ""){
            valido = false;
            System.out.println("\n Caduco o fecha incorrecta : \n");    
        }
        if(!validaCorrespondencias()){
            valido = false;
            System.out.println("\n El archivo .cer y archivo .key no corresponden: \n");
        }
        if(!correspondenciaEmisor(cert,rfc)){
            valido = false;
            System.out.println("No Corresponde RFC!!"); 
        }
        if(!validaCertificado(cert)){
            valido = false;
            System.out.println("Certificado no valido!!");
        }
        //statusFIEL=validaFIEL(cert);
        //System.out.println("el status FIEL es: " + statusFIEL);
        //statusCorresp=validaCorrespondencias();
        
        //statusLCO=validaLCO();
        
        //System.out.println("\nel status LCO es: \n" + statusLCO); 
        //Valida Si Certificado Corresponde a emisor
        //boolean correspondeRFC = false;
        
    } catch (Exception ea) {

        ea.printStackTrace();
    }
    return valido;
}
    
    /**
     * Método que indica si es caduco, revocado o válido el certificado
     *
     * @return      caduco si es el certificado no es valido, en otro caso cadena vacia
     */

    public String validaFechaCertificado() {
        return validaFecha(certificado);
    }

    /**
     * Método que valida la correspondencia del certificado con el RFC del emisor
     *
     * @param rfc       RFC del emisor
     * @return          true si corresponde el certificado al emisor, en otro caso false
     */

    public boolean validaCertificado(String rfc) {
        return correspondenciaEmisor(certificado, rfc);
    }

    /***checa la validacion del certificado (fecha y hora)*****/

    /**
     * Método que revisa la valides del certificado (fecha y hora)
     *
     * @param cert      Certificado correspondiente al emisor
     * @return          caduco, fechaErronea si el certificado esta mal, en otro caso cadena vacia
     */

    public String validaFecha(X509Certificate cert) {
        String validacion = "";
        try {
            cert.checkValidity();
        } catch (CertificateExpiredException e) {
            validacion = "caduco";
            System.out.println("caduco");
            
        } catch (CertificateException e) {
            validacion = "fecha erronea";
            System.out.println("fecha erronea");
            
        }
        return validacion;
    }

    /**
     * Método que valida si el certificado corresponde al emisor
     *
     * @param cert      Certificado del emisor
     * @param rfc       RFC del emisor
     * @return          true si el certificado corresponde al emisor, en otro caso false
     */

    public boolean correspondenciaEmisor(X509Certificate cert, String rfc) {
        String Issuer = cert.getSubjectDN().getName();
        if (!Issuer.contains(rfc)) {
            return false;
        }
        return true;
    }


    /******** funcion que checa si es FIEL o no *************/

    /**
     * Método que valida si el certificado es valido ante el SAT
     *
     * @param cert      Certificado a validar
     * @return          true si el certificado es valido, en otro caso false
     */

    public boolean validaCertificado(X509Certificate cert) {
        boolean valido = false;
        int i = 0;
        int j = 0;
        String word, subword;
        StringTokenizer token, subToken;
        subject = cert.getSubjectDN().toString();
        token = new StringTokenizer(subject, ",");
        while (token.hasMoreTokens()) {
            word = token.nextToken();
            i = 1;
            subToken = new StringTokenizer(word, "=");
            while (subToken.hasMoreTokens()) {
                subword = subToken.nextToken();
                i++;
                if (subword.contains(PARAMETRO)) {
                    j++;
                }
            }
        }
        if (j > 0) {
        valido = true;
            
        } else {
            valido = false;
        }
        return valido;
    }

    /**
     * Método que valida el password y que la llave privada corresponda a la llave publica
     *
     * @return      true si el password y llave privada corresponden, en otro caso false
     */

    public boolean validaCorrespondencias() {

        try {

            System.out.println("la clave privada es " + clavePrivada);
            System.out.println("el password es " + password);
            PKCS8Key pkcs8 = new PKCS8Key(this.clavePrivada, this.password.toCharArray());
            //valida el pass
            PrivateKey pk = pkcs8.getPrivateKey();
            //valida que la llave privada corresponda  a la llave publica
            X509Certificate cert = X509Certificate.getInstance(this.clavePublica);
            Signature firma = Signature.getInstance("SHA1withRSA");
            firma.initSign(pk);
            byte[] firmado = firma.sign();
            firma.initVerify(cert.getPublicKey());
            if (firma.verify(firmado)) {
                return this.correcto;
            } else {
                return this.error;
            }
        } catch (GeneralSecurityException e) {
            return this.error;
        } catch (CertificateException e) {
            return this.error;
        }
    }

    /* Retornamos el nombre de archivos*/

    public static List<String> nombreArchivos() {
        List<String> nombre = new ArrayList<String>();
        String ruta = "E:\\certs\\raiz\\";
        File directorio = new File(ruta);
        File[] archivos = directorio.listFiles();
        for (File file : archivos) {
            nombre.add(file.getName());
        }
        return nombre;
    }


    /**valida si el certificado es apocrifo*/

    /**
     * Método que valida si el certificado es apocrifo, no valido ante el SAT
     *
     * @param cert      Certificado a validar
     * @return          true si el certificado es apocrifo, en otro caso false
     */
    public static boolean ValidateCertificate(java.security.cert.X509Certificate cert) {
        for (String nombre : nombreArchivos()) {
            try {
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                List mylist = new ArrayList();
                TrustAnchor anchor =
                    new TrustAnchor((java.security.cert.X509Certificate)importCertificate("E:\\certs\\raiz\\" +
                                                                                          nombre), null);
                mylist.add(cert);
                CertPath cp = cf.generateCertPath(mylist);
                PKIXParameters params = new PKIXParameters(Collections.singleton(anchor));
                params.setRevocationEnabled(false);
                CertPathValidator cpv = CertPathValidator.getInstance("PKIX");
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return false;
    }


    /**
     * Método donde se realiza la importacion de un certificado InputStream a un X509Certificate
     *
     * @param cer       Certificado del emisor
     * @return          Certiticado en el estandar X509Certificate
     * @throws CertificateException
     * @throws IOException
     */

    public static java.security.cert.X509Certificate importCertificate(InputStream cer) throws CertificateException,
                                                                                               IOException {
        try {
            return KeyLoader.loadX509Certificate(cer);
        } catch (CertificateException ex) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Método donde se realiza la importacion de un certificado InputStream a un X509Certificate
     *
     * @param path      Ruta del certificado emisor
     * @return          Certiticado en el estandar X509Certificate
     * @throws CertificateException
     * @throws IOException
     */


    public static java.security.cert.X509Certificate importCertificate(String path) throws CertificateException,
                                                                                           IOException {
        File file = new File(path);
        FileInputStream is = new FileInputStream(file);


        try {
            return KeyLoader.loadX509Certificate(is);
        } catch (CertificateException ex) {
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
