package classjava;

import entities.Sucursal;
import entities.Trabajador;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ReciboTabla {


    private Timestamp fechaCertificacio;
    private Timestamp fechaEmision;
    private Date fechaFinalPago;
    private Date fechaInicialPago;
    private Date fechaPago;
    private int idrecibos;
    private long noDiasPagados;
    private String reciboXML;
    private BigDecimal totalExcentoDeduccion;
    private BigDecimal totalExcentoPercepcion;
    private BigDecimal totalGravadoDeduccion;
    private BigDecimal totalGravadoPercepcion;
    private String uuid;

    public ReciboTabla() {
        super();
    }

    public ReciboTabla(Timestamp fechaCertificacio, Timestamp fechaEmision, Date fechaFinalPago, Date fechaInicialPago,
                       Date fechaPago, int idrecibos, long noDiasPagados, String reciboXML,
                       BigDecimal totalExcentoDeduccion, BigDecimal totalExcentoPercepcion,
                       BigDecimal totalGravadoDeduccion, BigDecimal totalGravadoPercepcion, String uuid) {
        super();
        this.fechaCertificacio = fechaCertificacio;
        this.fechaEmision = fechaEmision;
        this.fechaFinalPago = fechaFinalPago;
        this.fechaInicialPago = fechaInicialPago;
        this.fechaPago = fechaPago;
        this.idrecibos = idrecibos;
        this.noDiasPagados = noDiasPagados;
        this.reciboXML = reciboXML;
        this.totalExcentoDeduccion = totalExcentoDeduccion;
        this.totalExcentoPercepcion = totalExcentoPercepcion;
        this.totalGravadoDeduccion = totalGravadoDeduccion;
        this.totalGravadoPercepcion = totalGravadoPercepcion;
        this.uuid = uuid;
    }


    public void setFechaCertificacio(Timestamp fechaCertificacio) {
        this.fechaCertificacio = fechaCertificacio;
    }

    public Timestamp getFechaCertificacio() {
        return fechaCertificacio;
    }

    public void setFechaEmision(Timestamp fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Timestamp getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaFinalPago(Date fechaFinalPago) {
        this.fechaFinalPago = fechaFinalPago;
    }

    public Date getFechaFinalPago() {
        return fechaFinalPago;
    }

    public void setFechaInicialPago(Date fechaInicialPago) {
        this.fechaInicialPago = fechaInicialPago;
    }

    public Date getFechaInicialPago() {
        return fechaInicialPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setIdrecibos(int idrecibos) {
        this.idrecibos = idrecibos;
    }

    public int getIdrecibos() {
        return idrecibos;
    }

    public void setNoDiasPagados(long noDiasPagados) {
        this.noDiasPagados = noDiasPagados;
    }

    public long getNoDiasPagados() {
        return noDiasPagados;
    }

    public void setReciboXML(String reciboXML) {
        this.reciboXML = reciboXML;
    }

    public String getReciboXML() {
        return reciboXML;
    }

    public void setTotalExcentoDeduccion(BigDecimal totalExcentoDeduccion) {
        this.totalExcentoDeduccion = totalExcentoDeduccion;
    }

    public BigDecimal getTotalExcentoDeduccion() {
        return totalExcentoDeduccion;
    }

    public void setTotalExcentoPercepcion(BigDecimal totalExcentoPercepcion) {
        this.totalExcentoPercepcion = totalExcentoPercepcion;
    }

    public BigDecimal getTotalExcentoPercepcion() {
        return totalExcentoPercepcion;
    }

    public void setTotalGravadoDeduccion(BigDecimal totalGravadoDeduccion) {
        this.totalGravadoDeduccion = totalGravadoDeduccion;
    }

    public BigDecimal getTotalGravadoDeduccion() {
        return totalGravadoDeduccion;
    }

    public void setTotalGravadoPercepcion(BigDecimal totalGravadoPercepcion) {
        this.totalGravadoPercepcion = totalGravadoPercepcion;
    }

    public BigDecimal getTotalGravadoPercepcion() {
        return totalGravadoPercepcion;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
